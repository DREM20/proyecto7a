$(function () {

    $(document).on('click', ".btn_file_trigger", function () {
        $(this).closest('div.input-group').find('input[type="file"]').trigger('click');
    });

    $(document).on('change', 'input[type="file"]', function (e) {
        $(this).closest('div.form-group').find('img.preview_img').attr('src', '');

        let name = '';
        if(e.target.files && e.target.files[0]) name = e.target.files[0].name;

        $(this).closest('div.input-group').find('input[type="text"]').val(name);
        
    });

    /** FUNCIONES PARA CARGAR UN GET */
    const fetch = (url, data = {}) => $.ajax({ type: "GET", url: url, data: data });

    CargaDatosIniciales = function () {
        ActualizaTabla();
    }

    ActualizaTabla = function () {
        var modalRespuesta = swal.fire({
            title: 'Procesando...', 
            onBeforeOpen: () => {
                swal.showLoading(); 
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: `${ root_path }APIs/zonas.php/tbody`,
                    success: function (response) {
                        modalRespuesta.closeModal();
                        if(response.info) componenteTable(response);
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function (data) {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        });
    }

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        select: { style: 'single' },
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
    }

    var tabla = $("#table").DataTable(configtable);
    componenteTable = function (data) {
        tabla.destroy(); 

        let body = '';
        $.each(data.info, function (indexInArray, valueOfElement) {

            let estatus = { icon: '<i class="fas fa-lock"></i>', title: 'Activar' };
            if(typeof valueOfElement.estatus != undefined) { 
                if(valueOfElement.estatus === true) estatus = { icon: '<i class="fas fa-unlock"></i>', title: 'Descativar' };
            };

            let opciones = `<button class="btn btn-blue btn_estatus_change tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="${ estatus.title }">${ estatus.icon }</button>`;

            body += `<tr>
                        <td></td>
                        <td>${ opciones }</td>
                        <td>${ (typeof valueOfElement.estatus != undefined) ? (valueOfElement.estatus === true) ? 'ACTIVO' : 'INACTIVO' : '' }</td>
                        <td>${ (typeof valueOfElement.zona != undefined) ? valueOfElement.zona : '' }</td>
                        <td>${ (typeof valueOfElement.estado != undefined) ? valueOfElement.estado : '' }</td>
                        <td>${ (typeof valueOfElement.municipio != undefined) ? valueOfElement.municipio : '' }</td>
                        <td>${ (typeof valueOfElement.costo_membresia != undefined) ? Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(valueOfElement.costo_membresia) : '' }</td>
                    </tr>`;
        });

        $("#table tbody").html(body);
        tabla = $("#table").DataTable(configtable);
    }

    /** LIMPIAR EL FORMULARIO DE LAS ZONAS */
    limpiarFormulario = function () {
        $('#zona_form_validate, #config_zona_form')[0].reset();
        $('#zona_form_validate, #config_zona_form').trigger("reset");
        $("#config_zona_form input[type=checkbox], #zona_form_validate input[type=checkbox]").removeAttr('checked').iCheck('update');
        $("#config_zona_form input[type=checkbox], #zona_form_validate input[type=checkbox]").closest('li').find('label').find('span').html('NO').removeClass('text-success').addClass('text-danger');
    }

    /** FUNCION PARA CONFIGURAR ZONA */
    $(".btn_crear").on('click', function (e) {
        limpiarFormulario();
        $("#config_zona").modal('show');
        $("#config_zona_tipo").val('C');
    });
    
    let config = {
        mapa: null,
        marker: null,
        geocoder: null,
        infowindow: null,
    };

    configMarker = (position, mostrar = true) => {
        if(!mostrar) {
            config.marker.setVisible(false);
            config.infowindow.close();

            $("#config_zona_latitud, #config_zona_longitud").val('');
            
            return false; 
        }

        config.marker.setMap(config.mapa);
        config.marker.setVisible(true);
        config.marker.setPosition(position);
        config.mapa.panTo(position);

        $("#config_zona_latitud").val(position.lat.toFixed(5));
        $("#config_zona_longitud").val(position.lng.toFixed(5));

        // config.geocoder.geocode({ 'location': position }, function (results, status) {
        //     if (status === google.maps.GeocoderStatus.OK) {
        //         if(results.length > 0) {
        //             const info = results.filter(data => ~data.types.indexOf('locality'));

        //             if(typeof info[0].formatted_address !== undefined) {
        //                 const content = document.createElement("div");
        //                 const nameElement = document.createElement("p");

        //                 nameElement.textContent = info[0].formatted_address;
        //                 content.appendChild(nameElement);
        //                 config.infowindow.setContent(content);
        //                 config.infowindow.open(config.mapa, config.marker);
        //             }
        //         }
        //     }
        // })
    }

    createmap = (id, position, marker = null) => new google.maps.Map(document.getElementById(id), { center: position, zoom: 10, });

    configModal = function () {
        let position = { lat: 19.033333, lng: -98.183334 }; /** POSICION INICIAL DEL MAPA */
        config.mapa = createmap('map_location', position); /** CREAMOS EL MAPA */
        config.infowindow = new google.maps.InfoWindow();
        // config.geocoder = new google.maps.Geocoder();

        /** SI ESTA HABILITADA LA UBICACION, COLOCAMOS EL MAPA AHI */
        if(navigator.geolocation) navigator.geolocation.getCurrentPosition((position) => config.mapa.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude }));

        /** CREAMOS EL MARCADOR */
        config.marker = new google.maps.Marker();

        /** CREAMOS EL EVENTO QUE CREA EL MARCADOR */
        google.maps.event.addListener(config.mapa, 'click', function (event) {
            configMarker(event.latLng.toJSON());
        });

        $("#config_zona_estado, #config_zona_municipio").html('<option></option>');
        $.when(fetch(`${ root_path }APIs/catalogos.php/estados`)).then(function (r1) {
            if(r1.item) $.each(r1.item, (index, value) => $("#config_zona_estado").append(`<option value="${ value.id }">${ value.nombre }</option>`));

            $("#config_zona_estado, #config_zona_municipio").select2({ allowClear: true });
            $('#config_zona_estado').off('change').on('change', function () { cargarMunicipio($(this).val(), $("#config_zona_municipio")) });
            $("#config_zona_municipio").on('change', function () {
                $("#config_zona_nombre").val($(this).select2('data') ? $(this).select2('data').text : '');
                // if($(this).select2('data')) {
                //     $.ajax({
                //         type: "GET",
                //         url: `${ root_path }APIs/zonas.php/search_map_location`,
                //         data: {
                //             municipio: $(this).select2('data').text,
                //             estado: $("#config_zona_estado").select2('data').text
                //         },
                //         success: function (response) {
                //             if(response.lat && response.lng) configMarker(response);
                //         }
                //     });
                // }
                // else configMarker(null, false);
            })
        });

        const width = (typeof window.outerWidth != 'undefined') ? Math.max(window.outerWidth, $(window).width()) : $(window).width();
        if(width > 600) $("#map_location").css('height', $("#zona_form_validate").height());
    }

    $('#config_zona').on('shown.bs.modal', configModal);

    /** FUNCIONALIDADES DE ELEMENTOS DEL MODAL */
    $('#config_zona_latitud, #config_zona_longitud').on('input', function(e) {
        const lat = parseFloat($("#config_zona_latitud").val()),
        lng = parseFloat($("#config_zona_longitud").val());

        if ((!isNaN(lat) && lat <= 90 && lat >= -90) && (!isNaN(lng) && lng <= 180 && lng >= -180)) configMarker({ lat: lat, lng: lng });
        else configMarker(null, false);

    });

    /** FUNCIONALIDAD DE SOLO NUMEROS Y MAXIMO DOS DECIMALES */
    $(".number_decimal").keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) event.preventDefault();
        var text = $(this).val();
        if (((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2))) {
            event.preventDefault();
        }
    });

    /** FUNCIONALIDAD DE NO SOBREPASAR DE 100 */
    $(".number_max_percentage").on('keydown keyup change', function (e) {
        if($(this).val() > 100 && e.keyCode !== 46 && e.keyCode !== 8) { e.preventDefault(); $(this).val(100); }
    })

    $("#config_zona_form input[type=checkbox]").closest('li').find('label').find('span').html('NO').addClass('text-danger');
    $("#config_zona_form input[type=checkbox]").iCheck({
        checkboxClass: 'icheckbox_square-yellow',
        radioClass: 'iradio_square-yellow'
    });

    $("#config_zona_form input[type=checkbox]").on('ifChanged', function (e) {
        const valor = $(this).iCheck('update')[0].checked;
        if(valor) $(this).closest('li').find('label').find('span').html('SI').removeClass('text-danger').addClass('text-success');
        else $(this).closest('li').find('label').find('span').html('NO').removeClass('text-success').addClass('text-danger');
    });

    /** FUNCIONES PARA CARGAR ESTADOS */
    cargarMunicipio = (estado, elemento, valor) => {
        if(estado) {
            $.ajax({
                type: "GET",
                url: `${ root_path }APIs/catalogos.php/municipioByEstado`,
                data: {
                    estado
                },
                success: function (response) {
                    if(response.item) {
                        elemento.html('<option></option>');
                        $.each(response.item, function (index, value) { 
                            const selected = (value.id == valor) ? 'selected' : '';
                            elemento.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                        });
                        elemento.prop('disabled', false);
                        elemento.select2('val', valor);
                    }
                }
            });
        }
        else elemento.select2('val', '').html('').prop('disabled', true).trigger('change');

        return;
    }

    /** FUNCION QUE GUARDA LA INFORMACION DE LA ZONA */
    $("#modal_zona_guardar").on('click', function () {
        if(!$("#zona_form_validate").valid() || !$("#config_zona_form").valid()) {
            swal.fire({
                title: 'Información',
                type: 'info',
                text: 'Debe completar la información requerida'
            });

            return;
        }

        var modalRespuesta = swal.fire({
            title: 'Cargando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.when(
                    fetch(`${ root_path }APIs/zonas.php/existe_zona`, { municipio: $("#config_zona_municipio").val(), estado: $('#config_zona_estado').val() }),
                    fetch(`${ root_path }APIs/zonas.php/buscar_cobertura`, { municipio: $("#config_zona_municipio").select2('data').text })
                ).then(function (result1, result2) {
                    if(result1[0].code == 500) {
                        swal.fire({
                            title: 'Información',
                            type: 'info',
                            text: result1[0].notif
                        });

                        return;
                    }

                    if(result2[0].code == 500) {
                        swal.fire({
                            title: 'Información',
                            type: 'info',
                            text: result2[0].notif
                        });

                        return;
                    }

                    let data = {...getFormData($("#zona_form_validate")), ...getFormData($("#config_zona_form"))};

                    $.each($("#config_zona_form input[type=checkbox]"), function (index, value) {
                        let info = {};
                        info[$(value).attr('name')] = $(value).iCheck('update')[0].checked;
                        data = {...data, ...info};
                    });

                    data['id'] = $("#config_zona_id").val();
                    data['zona_name'] = $("#config_zona_municipio").select2('data').text;

                    swal.fire({
                        title: 'Procesar Información',
                        text: `¿Está seguro que desea crear/actualizar la zona ${ $("#config_zona_nombre").val() }?`,
                        confirmButtonText: 'Confirmar',
                        confirmButtonColor: '#00a651' ,
                        showCancelButton: true, 
                        cancelButtonText: 'Cancelar', 
                        cancelButtonColor: '#cc2424',
                        allowOutsideClick: false,
                        allowEnterKey: false,
                        type: 'question', 
                    }).then((result) => {
                        if(result.value) {
                            const ruta = ($("#config_zona_tipo").val() == 'C') ? 'crear_zona' : 'actualizar_zona';
                            var modalRespuesta = swal.fire({
                                title: 'Procesando...',
                                onBeforeOpen: () => {
                                    swal.showLoading()
                                },
                                onOpen: () => {
                                    $.ajax({
                                        type: "POST",
                                        url: `${ root_path }APIs/zonas.php/${ ruta }`,
                                        data: data,
                                        success: function (response) {
                                            if(response.code == 200) {
                                                swal.fire({
                                                    title: 'Éxito', 
                                                    type: 'success', 
                                                    text: response.notif,
                                                    onClose: () => {
                                                        limpiarFormulario();
                                                        $("#config_zona").modal('hide');
                                                    }
                                                })
                                            }
                                            else {
                                                if(typeof response.notif !== undefined) {
                                                    swal.fire({
                                                        title: 'Información', 
                                                        type: 'warning', 
                                                        text: response.notif
                                                    })
                                                }
                                                else {
                                                    swal.fire({
                                                        title: 'Información', 
                                                        type: 'warning', 
                                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                                    })
                                                }
                                            }
                                        },
                                        error: function () {
                                            swal.fire({
                                                title: 'Información', 
                                                type: 'warning', 
                                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                            })
                                        }
                                    });
                                }
                            })
                        }
                    });
                })
            }
        })
    });

    function getFormData($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
    
        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
    
        return indexed_array;
    }

    /** ACTUALIZAR EL DOCUMENTO KML */
    $(".btn_kml").on('click', function () {
        $("#config_kml_file").val('').trigger('change'); 
        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.when(fetch(`${ root_path }APIs/zonas.php/get_kml`)).then(function (r1) {
                    modalRespuesta.closeModal();

                    let file = {
                        url: (r1.code == 200) ? `<a href="${ root_path }${ r1.ruta }" class="text-info" download>Clic para descargar documento KML actual</a>`: '<span>No existe documento KML cargado actualmente</span>',
                        class: (r1.code == 200) ? 'alert-success' : 'alert-warning'
                    }

                    $("#config-kml-download").removeClass('alert-success alert-warning').addClass(file.class);
                    $("#config-kml-download").html(file.url);

                    $("#config_kml").modal('show', { backdrop: 'static', keyboard: false });
                })
            }
        })
    });

    $("#config_kml_guardar").on('click', function () {
        if(!$("#config_kml_form").valid()) return;

        swal.fire({
            title: 'Procesar Documento',
            text: 'El documento será procesado y sobrescribirá el anterior en caso de que exista ¿Está seguro?',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#00a651' ,
            showCancelButton: true, 
            cancelButtonText: 'Cancelar', 
            cancelButtonColor: '#cc2424',
            allowOutsideClick: false,
            allowEnterKey: false,
        }).then((result) => {
            if(result.value) {
                var modalRespuesta = swal.fire({
                    title: 'Procesando...',
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        const formData = new FormData();
                        formData.append('kml', $("#config_kml_file").prop('files')[0]);

                        $.ajax({
                            type: "POST",
                            url: `${ root_path }APIs/zonas.php/procesa_kml`,
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.code == 200) {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: response.notif,
                                        onClose: () => {
                                            $("#config_kml").modal('hide');
                                        }
                                    })
                                }
                                else {
                                    if(typeof response.notif !== undefined) {
                                        swal.fire({
                                            title: 'Información', 
                                            type: 'warning', 
                                            text: response.notif
                                        })
                                    }
                                    else {
                                        swal.fire({
                                            title: 'Información', 
                                            type: 'warning', 
                                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                        })
                                    }
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                })
            }
        });
    });

    
});