<?php 
    include('../../bar.php');
?>
    <h1>Zonas</h1>

    <?php if($permisos->getPermiso('ZONA_AGREGAR')) : ?>
        <button class="btn btn-primary btn_crear" style="margin-bottom: 5px;"><i class="fas fa-plus"></i> Configurar Zona</button>
    <?php endif; ?>
    
    <?php if($permisos->getPermiso('ZONA_ACTUALIZARKML')) : ?>
        <button class="btn btn-info btn_kml" style="margin-bottom: 5px;"><i class="fas fa-code"></i> Cargar Archivo KML</button>
    <?php endif; ?>

    <table class="table table-bordered datatable" id="table">
        <thead>
            <tr>
                <th></th>
                <th>Opciones</th>
                <th>Estatus</th>
                <th>Zona</th>
                <th>Estado</th>
                <th>Municipio</th>
                <th>Costo Membresía</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <div class="modal fade custom-width" id="config_zona">
		<div class="modal-dialog" style="width: 80%;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Configuración de Zona</h4>
				</div>
				
				<div class="modal-body">
                    <input type="hidden" name="config_zona_tipo" id="config_zona_tipo">
                    <input type="hidden" name="config_zona_id" id="config_zona_id">
                    <div class="row" style="font-size: 14px;">
                        <div class="col-md-6">
                            <div id="map_location" style="height: 400px"></div>	
                        </div>
                        <div class="col-md-6">
                            <form class="validate" id="zona_form_validate">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="config_zona_estado">Estado</label>
                                        <select name="config_zona_estado" id="config_zona_estado" class="form-control" data-validate="required" data-message-required="Dato requerido">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="config_zona_municipio">Municipio</label>
                                        <select name="config_zona_municipio" id="config_zona_municipio" class="form-control" disabled data-validate="required" data-message-required="Dato requerido"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="config_zona_nombre">Nombre Zona</label>
                                        <input class="form-control" id="config_zona_nombre" name="config_zona_nombre" data-validate="required" data-message-required="Dato requerido">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_costo" class="control-label">Costo Membresía</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control" id="config_zona_costo" name="config_zona_costo" data-validate="required" data-message-required="Dato requerido">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_iva" class="control-label">IVA (0% - 100%)</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control number_decimal number_max_percentage" id="config_zona_iva" name="config_zona_iva" data-validate="required" data-message-required="Dato requerido">
                                            <span class="input-group-addon"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_porcentaje" class="control-label">Porcentaje (0% - 100%)</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control number_decimal number_max_percentage" id="config_zona_porcentaje" name="config_zona_porcentaje" data-validate="required" data-message-required="Dato requerido">
                                            <span class="input-group-addon"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_gobierno" class="control-label">Gobierno (0% - 100%)</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control number_decimal number_max_percentage" id="config_zona_gobierno" name="config_zona_gobierno" data-validate="required" data-message-required="Dato requerido">
                                            <span class="input-group-addon"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_latitud" class="control-label">Latitud</label>
                                        <input type="text" class="form-control" id="config_zona_latitud" name="config_zona_latitud">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="config_zona_longitud" class="control-label">Longitud</label>
                                        <input type="text" class="form-control" id="config_zona_longitud" name="config_zona_longitud">
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-divider"><span>Configuración</span></p>
                            <form role="form" class="form-horizontal form-groups-bordered validate" id="config_zona_form">
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-4 control-label">Piloto automatico</label>
                                    <div class="col-sm-8">
                                        <ul style="list-style: none" class="text-left">
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_automatico" name="config_zona_automatico">
                                                <label for="minimal-checkbox-1-1" style="font-weight: bold; color: #000; font-size: 12px"><span></span></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Servicios</label>
                                    <div class="col-sm-8">
                                        <ul style="list-style: none" class="text-left">
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_taxi" name="config_zona_taxi">
                                                <label for="config_zona_taxi" style="font-weight: bold; color: #000; font-size: 12px">Taxi <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_ejecutivo" name="config_zona_ejecutivo">
                                                <label for="config_zona_ejecutivo" style="font-weight: bold; color: #000; font-size: 12px">Ejecutivo <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_van" name="config_zona_van">
                                                <label for="config_zona_van" style="font-weight: bold; color: #000; font-size: 12px">Van <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_sprinter" name="config_zona_sprinter">
                                                <label for="config_zona_sprinter" style="font-weight: bold; color: #000; font-size: 12px">Sprinter <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_paquete" name="config_zona_paquete">
                                                <label for="config_zona_paquete" style="font-weight: bold; color: #000; font-size: 12px">Paquete <span></span></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tarifa</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_kilometro"><i class="fas fa-road"></i></span>
                                            <input type="text" class="form-control" placeholder="Costo Kilometro" id="config_zona_kilometro" name="config_zona_kilometro">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_minuto"><i class="fas fa-stopwatch"></i></span>
                                            <input type="text" class="form-control" placeholder="Costo Minuto" id="config_zona_minuto" name="config_zona_minuto">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_banderazo"><i class="fas fa-flag"></i></span>
                                            <input type="text" class="form-control" placeholder="Costo Banderazo" id="config_zona_banderazo" name="config_zona_banderazo">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_costobase"><i class="fas fa-car-side"></i></span>
                                            <input type="text" class="form-control" placeholder="Costo Base" id="config_zona_costobase" name="config_zona_costobase">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_percesolicitud"><i class="fas fa-percentage"></i></span>
                                            <input type="text" class="form-control number_decimal number_max_percentage" placeholder="Porcentaje Solicitud" id="config_zona_percesolicitud" name="config_zona_percesolicitud">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_dinamica"><i class="fas fa-percentage"></i></span>
                                            <input type="text" class="form-control number_decimal number_max_percentage" placeholder="Dinamica" id="config_zona_dinamica" name="config_zona_dinamica">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_tarifamaxima"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control number_decimal" placeholder="Costo Tarifa Maxima" id="config_zona_tarifamaxima" name="config_zona_tarifamaxima">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_radioBusqueda"><i class="fas fa-search"></i></span>
                                            <input type="text" class="form-control" placeholder="Radio Busqueda" id="config_zona_radioBusqueda" name="config_zona_radioBusqueda">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_radioTarifaMinima"><i class="fas fa-search"></i></span>
                                            <input type="text" class="form-control" placeholder="Radio Tarifa Minima" id="config_zona_radioTarifaMinima" name="config_zona_radioTarifaMinima">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_radioTarifaMaxima"><i class="fas fa-search"></i></span>
                                            <input type="text" class="form-control" placeholder="Radio Tarifa Maxima" id="config_zona_radioTarifaMaxima" name="config_zona_radioTarifaMaxima">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_tiempoEsperaMin"><i class="fas fa-stopwatch"></i></span>
                                            <input type="text" class="form-control" placeholder="Tiempo de espera (Minutos)" id="config_zona_tiempoEsperaMin" name="config_zona_tiempoEsperaMin">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_economico"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control" placeholder="Economico" id="config_zona_economico" name="config_zona_economico">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_configiva"><i class="fas fa-percentage"></i></span>
                                            <input type="text" class="form-control number_decimal number_max_percentage" placeholder="IVA" id="config_zona_configiva" name="config_zona_configiva">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_perceVan"><i class="fas fa-percentage"></i></span>
                                            <input type="text" class="form-control number_decimal number_max_percentage" placeholder="Porcentaje VAN" id="config_zona_perceVan" name="config_zona_perceVan">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" for="config_zona_perceSprint"><i class="fas fa-percentage"></i></span>
                                            <input type="text" class="form-control number_decimal number_max_percentage" placeholder="Porcentaje Sprint" id="config_zona_perceSprint" name="config_zona_perceSprint">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Formas de Pago Usuario</label>
                                    <div class="col-sm-8">
                                        <ul style="list-style: none" class="text-left">
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_usuarioefectivo" name="config_zona_usuarioefectivo">
                                                <label for="config_zona_usuarioefectivo" style="font-weight: bold; color: #000; font-size: 12px">Efectivo <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_usuariotarjeta" name="config_zona_usuariotarjeta">
                                                <label for="config_zona_usuariotarjeta" style="font-weight: bold; color: #000; font-size: 12px">Tarjeta <span></span></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Formas de Pago Operador</label>
                                    <div class="col-sm-8">
                                        <ul style="list-style: none" class="text-left">
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_operadorefectivo" name="config_zona_operadorefectivo">
                                                <label for="config_zona_operadorefectivo" style="font-weight: bold; color: #000; font-size: 12px">Efectivo <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_operadortarjeta" name="config_zona_operadortarjeta">
                                                <label for="config_zona_operadortarjeta" style="font-weight: bold; color: #000; font-size: 12px">Tarjeta <span></span></label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_operadoroxxo" name="config_zona_operadoroxxo">
                                                <label for="config_zona_operadoroxxo" style="font-weight: bold; color: #000; font-size: 12px">Oxxo Pay <span></span></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Validación usuarios</label>
                                    <div class="col-sm-8">
                                        <ul style="list-style: none" class="text-left">
                                            <li>
                                                <input tabindex="5" type="checkbox" class="form-control" id="config_zona_codigomail" name="config_zona_codigomail">
                                                <label for="config_zona_codigomail" style="font-weight: bold; color: #000; font-size: 12px">Código email <span></span></label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="modal_zona_guardar">Guardar</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade custom-width" id="config_kml">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Configuración de KML</h4>
				</div>
				
				<div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="validate" id="config_kml_form">
                                <div class="form-group">
                                    <label for="config_kml_file" class="control-label">Seleccione un archivo</label>
                                    <div class="input-group">
                                        <input type="file" name="config_kml_file" id="config_kml_file" style="display: none;" accept="text/xml, .kml" data-validate="required" data-message-required="Requerido">
                                        <input type="text" name="config_kml_label" id="config_kml_label" class="form-control btn_file_trigger" readonly>
                                        <span class="input-group-btn">
                                            <button id="config_kml_trigger" class="btn btn-primary btn_file_trigger" type="button">
                                                Seleccionar
                                                <i class="fa fa-file-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert" id="config-kml-download"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="config_kml_guardar">Guardar</button>
				</div>
            </div>
        </div>
    </div>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php print(MAPS_KEY) ?>&libraries=visualization,places&v=weekly&channel=2"></script>

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>

<?php
    include('../../footer.php');
?>