$(function () {

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
        buttons: [{
                extend: 'excelHtml5',
                title: 'Reporte'
            }, {
                extend: 'pdfHtml5',
                title: 'Reporte'
            }
        ]
    }

    let map_operadores;
    let markers = [], actual_markers = [], infoOperadores = [], calor_viajes = [], calor = [];
    let heatmap;

    CargaDatosIniciales = function () {

        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {

            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/catalogos.php/zonas",
                    success: function (response) {
                        let html = '',
                        activo = (response.info.length == 1) ? 'selected' : '' ;
                        // html += '<option></option>';
                        $.each(response.info, function (index, value) { 
                            html += '<option ' + activo +' data-latitud="' + value.latitud + '" data-longitud="' + value.longitud + '">' + value.zona + '</option>';
                        });

                        $("#search_zona").html(html);
                        $("#search_zona").select2(optSelect);
                        $("#search_zona").addClass('visible')

                        map_operadores = new google.maps.Map(document.getElementById("map_operadores"), {
                            center: { lat: 19.0413, lng: -98.2062 },
                            zoom: 11,
                            styles: [{
                                "featureType": "administrative",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#d6e2e6"
                                }]
                            }, {
                                "featureType": "administrative",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": "#cfd4d5"
                                }]
                            }, {
                                "featureType": "administrative",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#7492a8"
                                }]
                            }, {
                                "featureType": "administrative.neighborhood",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "lightness": 25
                                }]
                            }, {
                                "featureType": "landscape.man_made",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#dde2e3"
                                }]
                            }, {
                                "featureType": "landscape.man_made",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": "#cfd4d5"
                                }]
                            }, {
                                "featureType": "landscape.natural",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#dde2e3"
                                }]
                            }, {
                                "featureType": "landscape.natural",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#7492a8"
                                }]
                            }, {
                                "featureType": "landscape.natural.terrain",
                                "elementType": "all",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            }, {
                                "featureType": "poi",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#dde2e3"
                                }]
                            }, {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#588ca4"
                                }]
                            }, {
                                "featureType": "poi",
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "saturation": -100
                                }]
                            }, {
                                "featureType": "poi.park",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#a9de83"
                                }]
                            }, {
                                "featureType": "poi.park",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": "#bae6a1"
                                }]
                            }, {
                                "featureType": "poi.sports_complex",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#c6e8b3"
                                }]
                            }, {
                                "featureType": "poi.sports_complex",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": "#bae6a1"
                                }]
                            }, {
                                "featureType": "road",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#41626b"
                                }]
                            }, {
                                "featureType": "road",
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "saturation": -45
                                }, {
                                    "lightness": 10
                                }, {
                                    "visibility": "on"
                                }]
                            }, {
                                "featureType": "road.highway",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#c1d1d6"
                                }]
                            }, {
                                "featureType": "road.highway",
                                "elementType": "geometry.stroke",
                                "stylers": [{
                                    "color": "#a6b5bb"
                                }]
                            }, {
                                "featureType": "road.highway",
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "visibility": "on"
                                }]
                            }, {
                                "featureType": "road.highway.controlled_access",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#9fb6bd"
                                }]
                            }, {
                                "featureType": "road.arterial",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#ffffff"
                                }]
                            }, {
                                "featureType": "road.local",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#ffffff"
                                }]
                            }, {
                                "featureType": "transit",
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "saturation": -70
                                }]
                            }, {
                                "featureType": "transit.line",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#b4cbd4"
                                }]
                            }, {
                                "featureType": "transit.line",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#588ca4"
                                }]
                            }, {
                                "featureType": "transit.station",
                                "elementType": "all",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            }, {
                                "featureType": "transit.station",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#008cb5"
                                }, {
                                    "visibility": "on"
                                }]
                            }, {
                                "featureType": "transit.station.airport",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "saturation": -100
                                }, {
                                    "lightness": -5
                                }]
                            }, {
                                "featureType": "water",
                                "elementType": "geometry.fill",
                                "stylers": [{
                                    "color": "#a6cbe3"
                                }]
                            }]
                        });

                        if($("#search_zona").select2().find(":selected").data("latitud") &&
                        $("#search_zona").select2().find(":selected").data("longitud")) {
                            map_operadores.panTo(new google.maps.LatLng($("#search_zona").select2().find(":selected").data("latitud"), $("#search_zona").select2().find(":selected").data("longitud")))
                        }

                        modalRespuesta.closeModal();
                    }
                });
            }
        });
    }

    /** SE CONFIGURAN SELECT */
    const optSelect = {
        allowClear: true,
        theme: "classic",
        placeholder: "Seleccione una opción"
    }

    $(".select2").select2(optSelect);
    $(".select2").addClass('visible');

    var conductores_activos = $("#conductores_activos").peity("line", {width: '100%'}),
    pasajeros_activos = $("#pasajeros_activos").peity("line", {width: '100%'}),
    solicitudes_viajes = $("#solicitudes_viajes").peity("line", {width: '100%'}),
    cancelacion_viajes = $("#cancelacion_viajes").peity("line", {width: '100%'}), 
    proceso_viajes = $("#proceso_viajes").peity("line", {width: '100%'}), 
    iniciado_viajes = $("#iniciado_viajes").peity("line", {width: '100%'}),
    finalizado_viajes = $("#finalizado_viajes").peity("line", {width: '100%'}),
    sinconductores_viajes = $("#sinconductores_viajes").peity("line", {width: '100%'});
    espera_viajes = $("#espera_viajes").peity("line", {width: '100%'});

    let dataSeries = [ [{
        x: Date.now(),
        y: 0
    }], [{
        x: Date.now(),
        y: 0
    }], [{
        x: Date.now(),
        y: 0
    }] ];
    
    var graph = new Rickshaw.Graph( {
        element: document.getElementById("contador_estadistica"),
        renderer: 'area',
        stroke: false,
        preserve: true,
        series: [{
                color: '#359ade',
                data: dataSeries[0],
                name: 'Viajes'
            }, {
                color: '#73c8ff',
                data: dataSeries[1],
                name: 'Pasajeros'
            }, {
                color: '#e0f2ff',
                data: dataSeries[2],
                name: 'Operadores'
            }
        ]
    } );
    
    graph.render();
    var hoverDetail = new Rickshaw.Graph.HoverDetail( {
        graph: graph,
        xFormatter: function(x) { return new Date(x).toString(); }
    });
    
    var legend = new Rickshaw.Graph.Legend({
        graph: graph,
        element: document.getElementById('contador_estadistica_label')
    });
    
    var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
        graph: graph,
        legend: legend
    });

    const infoWindow = new google.maps.InfoWindow();
    setInterval(function () {

        $.ajax({
            type: "GET",
            url: root_path + "APIs/monitoreo.php/conteo",
            data: {
                zona: $("#search_zona").val(),
                ambiente: $("#search_ambiente").val()
            },
            success: function (response) {
                contadores(response.data.pasajeros_activos, pasajeros_activos, 'pasajeros', 'pasajero');
                contadores(response.data.conductores_activos, conductores_activos, 'operadores', 'operador');
                contadores((response.data.viajes.find(viaje => viaje._id === 'peticion')) ? response.data.viajes.find(viaje => viaje._id === 'peticion').count : 0, solicitudes_viajes, 'peticion', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'cancelado')) ? response.data.viajes.find(viaje => viaje._id === 'cancelado').count : 0, cancelacion_viajes, 'cancelado', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'viaje aceptado')) ? response.data.viajes.find(viaje => viaje._id === 'viaje aceptado').count : 0, proceso_viajes, 'viaje aceptado', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'viaje iniciado')) ? response.data.viajes.find(viaje => viaje._id === 'viaje iniciado').count : 0, iniciado_viajes, 'viaje iniciado', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'viaje terminado')) ? response.data.viajes.find(viaje => viaje._id === 'viaje terminado').count : 0, finalizado_viajes, 'viaje terminado', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'sin conductores')) ? response.data.viajes.find(viaje => viaje._id === 'sin conductores').count : 0, sinconductores_viajes, 'sin conductores', 'viajes');
                contadores((response.data.viajes.find(viaje => viaje._id === 'operador en espera')) ? response.data.viajes.find(viaje => viaje._id === 'operador en espera').count : 0, espera_viajes, 'operador en espera', 'viajes');

                dataSeries[0].push({ x: Date.now(), y: (response.data.viajes.find(viaje => viaje._id === 'viaje iniciado')) ? response.data.viajes.find(viaje => viaje._id === 'viaje iniciado').count : 0 });
                dataSeries[1].push({ x: Date.now(), y: response.data.pasajeros_activos });
                dataSeries[2].push({ x: Date.now(), y: response.data.conductores_activos });

                if(dataSeries[0].length == 10) dataSeries[0].shift();
                if(dataSeries[1].length == 10) dataSeries[1].shift();
                if(dataSeries[2].length == 10) dataSeries[2].shift();
                
                graph.update();

                // deleteMarkers();
                actual_markers = [];
                $.each(response.data.conductores, function (index, value) {

                    actual_markers.push(value.idOperador);

                    addMarker({ lat: value.location.coordinates[1], lng: value.location.coordinates[0] }, 
                        imagenes[value.estatus], { 
                            id: value.idOperador,
                            nombre: value.infoOperador.nombre,
                            apellidos: value.infoOperador.apellidos,
                            telefono: value.infoOperador.telefono,
                            foto: value.infoOperador.fotoOperador,
                            version : value.version,
                            estatus : value.estatus,
                            viaje: value.ultimo_viaje
                        });
                });

                $.each(markers, function (indexInArray, valueOfElement) { 
                    if(!actual_markers.includes(valueOfElement.id)) markers[indexInArray].setMap(null);
                });

                let nuevo = false;
                zona = $("#search_zona").val();
                if(response.data.viajes_detalle !== undefined) {
                    $.each(response.data.viajes_detalle, function (indexInArray, valueOfElement) { 
                        if(!calor_viajes.includes(valueOfElement._id)) {
                            if(!valueOfElement.origen) return;
                            if(valueOfElement.origen.location == undefined) return;
                            if(valueOfElement.origen.location.coordinates == undefined) return;
                            if(valueOfElement.origen.location.coordinates[0] == undefined) return;
                            if(valueOfElement.origen.location.coordinates[1] == undefined) return;
    
                            if(!calor[zona]) calor[zona] = [];

                            calor[zona].push(new google.maps.LatLng(valueOfElement.origen.location.coordinates[1], valueOfElement.origen.location.coordinates[0]));
                            calor_viajes.push(valueOfElement._id);

                            nuevo = true;
                        }
                    });
                }

                if(nuevo) addHeatMap(zona);
            }
        });
    }, 2000);

    contadores = (valor, elemento, estatus, tipo) => {
        values = elemento.text().split(",");
        values.shift()
        values.push(valor);
        elemento.text(values.join(",")).change();
        elemento.closest('tr').find('td:nth-child(3) .label_text').text(valor);
        elemento.closest('tr').attr('data-estatus', estatus).attr('data-tipo', tipo);
    }

    var imagenes = [];
    imagenes['en-viaje'] = root_path + '../images/car_trip.png';
    imagenes['online'] = root_path + '../images/car_available.png';
    imagenes['ocupado'] = root_path + '../images/car_busy.png';
    imagenes['viaje-iniciado'] = root_path + '../images/car_trip-start.png';
    imagenes['avisar-llegada'] = root_path + '../images/car_trip-wait.png';
    imagenes['viaje-terminado'] = root_path + '../images/uber.png';

    addMarker = (location, imagen, info) => {

        infoOperadores[info.id] = {
            'id': info.id,
            'nombre': info.nombre,
            'apellidos': info.apellidos,
            'telefono': info.telefono,
            'version': info.version,
            'estatus': info.estatus,
            'foto': info.foto,
            'lat': location.lat,
            'lng': location.lng,
            'viaje': (new Array('en-viaje', 'viaje-iniciado', 'avisar-llegada').includes(info.estatus)) ? true : false,
            'travel': (new Array('en-viaje', 'viaje-iniciado', 'avisar-llegada').includes(info.estatus)) ? (info.viaje) ? (info.viaje[0]) ? (info.viaje[0]._id) ? info.viaje[0]._id : '' : '' : '' : '',
        };

        if(!markers.find(marker => marker.id === info.id)) {
            const marker = new google.maps.Marker({
                position: location,
                map: map_operadores,
                title: info.nombre,
                icon: {
                    url: imagen,
                    scaledSize: new google.maps.Size(20, 32),
                },
                id: info.id
            });

            marker.addListener('click', () => { infoOperador(info.id); })

            markers.push(marker);
        }
        else {
            let marker = markers.find(marker => marker.id === info.id);
            position = new google.maps.LatLng(location.lat, location.lng);

            marker.setPosition(position);
            marker.setIcon({
                url: imagen,
                scaledSize: new google.maps.Size(20, 32),
            });
            marker.setMap(map_operadores);
        }
    }

    infoOperador = (id) => {

        let html = 
        '<div class="row">' +
        '   <div class="col-md-6">' + 
        '       <div class="well well-sm">ID: ' + infoOperadores[id].id +  '</div>' + 
        '       <div class="well well-sm">Nombre: ' + infoOperadores[id].nombre + ' ' + infoOperadores[id].apellidos +  '</div>' + 
        '       <div class="well well-sm">Telefono: ' + infoOperadores[id].telefono +  '</div>' + 
        '       <div class="well well-sm">Versión: ' + infoOperadores[id].version +  '</div>' + 
        '       <div class="well well-sm">Estatus: ' + infoOperadores[id].estatus +  '</div>';            

        if(infoOperadores[id].viaje) {
            let parametros = {
                'o_zona': $("#search_zona").val(),
                'oid': id,
                'travel': infoOperadores[id].travel,
                'version_app': infoOperadores[id].version,
                'start_lat': infoOperadores[id].lat,
                'start_lng': infoOperadores[id].lng,
                'info_view': 'WEB'
            };
    
            link = '../../../compartir/trip.html?' + Object.keys(parametros).map(key => key + '=' + parametros[key]).join('&');;
            html += '<a class="btn btn-blue btn-block" href="' + link + '" target="_blank">Seguir Viaje</a>';
        }
        
        html +=
        '   </div>' +
        '   <div class="col-md-6">' + 
        '       <div class="well well-sm"><img src="' + infoOperadores[id].foto + '" style="width: 100%"></div>' +
        '   </div>' +
        '</div>'

        swal.fire({
            title: 'Operador',
            html: html,
            width: '50%',
        });
    }

    $("#search_zona").on('change', function () {
        if($(this).select2().find(":selected").data("latitud") && $(this).select2().find(":selected").data("longitud")) map_operadores.panTo(new google.maps.LatLng($(this).select2().find(":selected").data("latitud"), $(this).select2().find(":selected").data("longitud")))
    })

    $("#search_zona, #search_ambiente").on('change', function () {
        calor = [];
        calor_viajes = [];
        addHeatMap();
    });

    addHeatMap = (zona) => {
        if (heatmap) { heatmap.setMap(null); heatmap.setData([]); }
        heatmap = new google.maps.visualization.HeatmapLayer({
            data: calor[zona] ?? [],
            map: map_operadores,
        });

        heatmap.set("radius", heatmap.get("radius") ? null : 25);
    }

    $(document).on('click', '#table_monitoreo tbody tr', function (param) {
        let estatus = $(this).data('estatus'), 
        tipo = $(this).data('tipo');

        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/monitoreo.php/get_detalle",
                    data: {
                        estatus,
                        tipo,
                        zona: $("#search_zona").val(),
                        ambiente: $("#search_ambiente").val()
                    },
                    success: function (response) {
                        let html = $cabecera = $detalle = '';
                        switch(tipo) { 
                            case 'pasajero':
                                $cabecera += '<th></th>';
                                $cabecera += '<th>ID</th>';
                                $cabecera += '<th>Nombre</th>';
                                $cabecera += '<th>Teléfono</th>';
                                $cabecera += '<th>Correo</th>';
                                $cabecera += '<th>Estatus</th>';
                                $cabecera += '<th>Versión</th>';

                                $.each(response.data, function (index, value) { 
                                    $detalle += '<tr>';
                                    $detalle += '<td></td>';
                                    $detalle += '<td>' + value.idPasajero + '</td>';
                                    $detalle += '<td>' + value.infoPasajero.nombre + ' ' + value.infoPasajero.apellidos + '</td>';
                                    $detalle += '<td>' + value.infoPasajero.telefono + '</td>';
                                    $detalle += '<td>' + value.infoPasajero.mail + '</td>';
                                    $detalle += '<td>' + value.estatus + '</td>';
                                    $detalle += '<td>' + value.version + '</td>';
                                    $detalle += '</tr>';
                                });
                            break;
                            case 'operador':
                                $cabecera += '<th></th>';
                                $cabecera += '<th>ID</th>';
                                $cabecera += '<th>Nombre</th>';
                                $cabecera += '<th>Teléfono</th>';
                                $cabecera += '<th>Placas Vehículo</th>';
                                $cabecera += '<th>Marca Vehículo</th>';
                                $cabecera += '<th>Modelo Vehículo</th>';
                                $cabecera += '<th>Tipo Vehículo</th>';
                                $cabecera += '<th>Estatus</th>';
                                $cabecera += '<th>Zona</th>';
                                $cabecera += '<th>Versión</th>';

                                $.each(response.data, function (index, value) { 
                                    $detalle += '<tr>';
                                    $detalle += '<td></td>';
                                    $detalle += '<td>' + value.idOperador + '</td>';
                                    $detalle += '<td>' + value.infoOperador.nombre + ' ' + value.infoOperador.apellidos + '</td>';
                                    $detalle += '<td>' + value.infoOperador.telefono + '</td>';
                                    $detalle += '<td>' + value.infoVehiculo.placas + '</td>';
                                    $detalle += '<td>' + value.infoVehiculo.marca + '</td>';
                                    $detalle += '<td>' + value.infoVehiculo.modelo + '</td>';
                                    $detalle += '<td>' + value.tipoVehiculo + '</td>';
                                    $detalle += '<td>' + value.estatus + '</td>';
                                    $detalle += '<td>' + value.zona + '</td>';
                                    $detalle += '<td>' + value.version + '</td>';
                                    $detalle += '</tr>';
                                });
                                break;
                            case 'viajes':

                                $cabecera += '<th></th>';
                                $cabecera += '<th>ID Operador</th>';
                                $cabecera += '<th>Nombre Operador</th>';
                                $cabecera += '<th>Nombre Pasajero</th>';
                                $cabecera += '<th>Teléfono Pasajero</th>';
                                $cabecera += '<th>Origen</th>';
                                $cabecera += '<th>Destino</th>';
                                $cabecera += '<th>Estatus</th>';
                                if(estatus == 'cancelado') $cabecera += '<th>Usuario de Cancelación</th>';
                                $cabecera += '<th>Lista Negra</th>';
                                $cabecera += '<th>Forma de Pago</th>';
                                $cabecera += '<th>Distancia</th>';
                                $cabecera += '<th>Duración</th>';
                                $cabecera += '<th>Cupon</th>';
                                $cabecera += '<th>Descuento %</th>';
                                $cabecera += '<th>Descuento Monto</th>';
                                $cabecera += '<th>IVA</th>';
                                $cabecera += '<th>Porcentaje de Solicitud</th>';
                                $cabecera += '<th>SECTE</th>';
                                $cabecera += '<th>Tarifa Final</th>';

                                $.each(response.data, function (index, value) {

                                    let nombreOperador = '', 
                                    blacklist = '';

                                    if(value.infoOperador) { if(value.infoOperador.nombre) nombreOperador = value.infoOperador.nombre + ' ' + value.infoOperador.apellidos; }
                                    $.each(value.datos_operador, function (indexInArray, valueOfElement) { 
                                        if(valueOfElement.idOperador == value.idOperador) {
                                            if(valueOfElement.infoOperador) {
                                                if(valueOfElement.infoOperador.nombre) nombreOperador = valueOfElement.infoOperador.nombre + ' ' + valueOfElement.infoOperador.apellidos;
                                            }
                                        }
                                        else {
                                            if(valueOfElement.infoOperador) {
                                                if(valueOfElement.infoOperador.nombre) blacklist += '<li>' + valueOfElement.infoOperador.nombre + ' ' + valueOfElement.infoOperador.apellidos + '</li>';
                                            }
                                        }
                                    });

                                    $detalle += '<tr>';
                                    $detalle += '<td></td>';
                                    $detalle += (value.idOperador) ? '<td>' + value.idOperador + '</td>' : '<td></td>';
                                    $detalle += (nombreOperador) ? '<td>' + nombreOperador + '</td>' : '<td></td>';
                                    $detalle += (value.infoPasajero) ? '<td>' + value.infoPasajero.nombre + ' ' + value.infoPasajero.apellidos + '</td>' : '<td></td>';
                                    $detalle += (value.infoPasajero) ? '<td>' + value.infoPasajero.telefono + '</td>' : '<td></td>';
                                    $detalle += (value.origen) ? (value.origen.direccion) ? '<td>' + value.origen.direccion + '</td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.destino) ? (value.destino.direccion) ? '<td>' + value.destino.direccion + '</td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.estatus) ? '<td>' + value.estatus + '</td>' : '<td></td>';
                                    if(estatus == 'cancelado') $detalle += (value.canceled) ? '<td>' + value.canceled + '</td>' : '<td></td>';
                                    $detalle += (blacklist) ? '<td>' + blacklist + '</td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.formaPago) ? '<td>' + value.viaje.formaPago + '</td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.distancia) ? '<td>' + value.viaje.distancia + ' KM</td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.duracion) ? '<td>' + minutesToHour(value.viaje.duracion) + '</td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.codigo_promo) ? '<td>' + value.viaje.cupon.codigo_promo + '</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.descuento) ? '<td>' + (parseFloat(value.viaje.cupon.descuento) * 100) + '%</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.cupon) ? '<td>$' + new Intl.NumberFormat('en-US').format(parseFloat(value.viaje.cupon.cupon)) + '</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.iva) ? '<td>$' + new Intl.NumberFormat('en-US').format(parseFloat(value.viaje.cupon.iva)) + '</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.porce) ? '<td>$' + new Intl.NumberFormat('en-US').format(parseFloat(value.viaje.cupon.porce)) + '</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.cupon) ? (value.viaje.cupon.secte) ? '<td>$' + new Intl.NumberFormat('en-US').format(parseFloat(value.viaje.cupon.secte)) + '</td>' : '<td></td>' : '<td></td>' : '<td></td>';
                                    $detalle += (value.viaje) ? (value.viaje.tarifa) ? '<td>$' + new Intl.NumberFormat('en-US').format(parseFloat(value.viaje.tarifa)) + '</td>' : '<td></td>' : '<td></td>';
                                    $detalle += '</tr>';
                                });

                                break;
                        }

                        html += '<table class="table table-bordered" id="table_detalle">';
                        html += '    <thead>';
                        html += '        <tr>';
                        html +=          $cabecera;
                        html += '        </tr>';
                        html += '    </thead>';
                        html += '    <tbody>';
                        html +=          $detalle;
                        html += '    </tbody>';
                        html += '</table>';

                        swal.fire({
                            title: 'Información ' + tipo.toUpperCase(),
                            html: html,
                            width: '99%',
                            onOpen: () => {
                                $("#table_detalle").DataTable(configtable);
                            }
                        })
                    }
                });
            }
        })
    });

    function minutesToHour(minutos){
        var hours = Math.trunc(minutos / 60);
        var minutes = minutos % 60;

        let finish = '';
        if(hours > 0) { finish += hours + ' hora'; if(hours > 1) finish += 's'; };
        if(minutes > 0) { if(finish.length > 0) { finish += ', '; } finish += minutes + ' minuto'; if(minutes > 1) finish += 's'; }

        return finish;
      }
      
});
