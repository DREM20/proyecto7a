<?php 
    include('../../bar.php');
?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="search_zona">Zona</label>
                <select name="search_zona" id="search_zona" class="form-control select2">
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="search_ambiente">Ambiente</label>
                <select name="search_ambiente" id="search_ambiente" class="form-control select2">
                    <option value="PRO" selected>Productivo</option>
                    <option value="DEV">Calidad</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">	
                <div class="panel-heading">
                    <div class="panel-title">Monitoreo</div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    </div>
                </div>
                <table class="table table-bordered" id="table_monitoreo">
                    <thead>
                        <tr>
                            <th width="30%">Grafica</th>
                            <th>Usuarios</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span id="pasajeros_activos" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Pasajeros Conectados</td>
                            <td><span class="label label-success">Online</span><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="conductores_activos" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Conductores Conectados</td>
                            <td><span class="label label-success">Online</span><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="sinconductores_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>viajes No Atendidos.</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="solicitudes_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Solicitudes de Viaje</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="proceso_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Viajes Aceptados</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="espera_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Operador en Espera</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="iniciado_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Viajes Iniciados</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="finalizado_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Viajes Finalizados</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                        <tr>
                            <td><span id="cancelacion_viajes" class="grafico_line">0,0,0,0,0,0,0,0,0,0</span></td>
                            <td>Cancelación de Viajes</td>
                            <td><span class="label_text">0</span></td>
                        </tr>
                    </tbody>
                </table>
            
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>
                            Estadísticas en Tiempo Real
                        </h4>
                    </div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <div id="contador_estadistica">
                        <div id="contador_estadistica_label"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
	    <div  class="col-md-12">
		    <div id="map_operadores" style="height: 700px;"></div>	
	    </div>
    </div>
    <script src="main.js?i=<?php print(rand()); ?> "></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOmoWyhLvCIZ7H1k2ALHU9JaTWa56iXV8&libraries=visualization&v=weekly&channel=2"></script>
    

<?php
    include('../../footer.php');
?>
