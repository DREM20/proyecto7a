<?php 
    include('../../bar.php');
?>

    <h1>Perfiles</h1>

    <button class="btn btn-primary btn_crear" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Crear Perfil</button>
    
    <table class="table table-bordered datatable" id="profile-table">
        <thead>
            <tr>
                <th>Opciones</th>
                <th>Clave</th>
                <th>Descripción</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>

<?php
    include('../../footer.php');
?>