$(function () {
    CargaDatosIniciales = function () {
        ActualizaTabla(); 
    }

    ActualizaTabla = function () {
        var modalRespuesta = swal.fire({
            title: 'Procesando...', 
            onBeforeOpen: () => {
                swal.showLoading(); 
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/perfiles.php/tbdatos",
                    success: function (response) {
                        modalRespuesta.closeModal();
                        if(response.info) {
                            componenteTable(response);
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function (data) {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    }

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 50
    }

    var tabla = $("#profile-table").DataTable(configtable);
    componenteTable = function (data) {
        tabla.destroy(); 

        let body = '';
        $.each(data.info, function (index, value) { 
            body += `<tr>
                        <td>
                            <button type="button" class="btn btn-default tooltip-primary btn_configuracion" data-name="${ value.name }" data-desc="${ value.description }" data-id="${ value.group_id }" data-toggle="tooltip" data-placement="top" data-original-title="Configuración"><i class="fa fa-cog"></i></button>
                            <button type="button" class="btn btn-blue tooltip-primary btn_zonas" data-name="${ value.name }" data-desc="${ value.description }" data-id="${ value.group_id }" data-toggle="tooltip" data-placement="top" data-original-title="Configuración de Zonas"><i class="fas fa-map-marker-alt"></i></button>
                            <button type="button" class="btn btn-info tooltip-primary btn_editar" data-name="${ value.name }" data-desc="${ value.description }" data-id="${ value.group_id }" data-toggle="tooltip" data-placement="top" data-original-title="Modificar"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger tooltip-primary btn_eliminar" data-name="${ value.name }" data-desc="${ value.description }" data-id="${ value.group_id }" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Perfil"><i class="fa fa-times"></i></button>
                        </td>
                        <td>${ value.name }</td>
                        <td>${ value.description }</td>
                    </tr>`;
             
        });

        $("#profile-table tbody").html(body);
        tabla = $("#profile-table").DataTable(configtable);
    }

    /** CARGAMOS LA CONFIGURACION DEL PERFIL */
    $(document).on('click', '.btn_configuracion', function () {
        const id = $(this).data('id'), 
        name = $(this).data('name'), 
        description = $(this).data('desc');

        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: `${ root_path }APIs/perfiles.php/cargar_configuracion`,
                    data: {
                        id
                    },
                    success: function (response) {

                        if(response.code != 200) {
                            swal.fire({
                                title: 'Información', 
                                text: response.notif ?? 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico',
                                type: 'warning'
                            });

                            return;
                        }

                        if(response.info && response.permisos) {
                            let html = '';

                            html += `<div class="row">
                                        <div class="col-md-6">
                                            <div class="alert alert-info"><strong>Menú</strong></div>`;
                            
                            $.each(response.info, function (index, value) {
                                const activo = (value.activo == 1) ? 'checked' : '';

                                html += `<div class="row">
                                            <div class="col-md-12" style="text-align: left;">
                                                <ul style="margin-bottom: 5px; list-style: none">
                                                    <li>
                                                        <input tabindex="5" type="checkbox" data-key="${ value.clave }" class="icheck-parent" ${ activo }>
                                                        <label for="minimal-checkbox-1-1" style="font-weight: bold; color: #000; font-size: 12px">${ value.descripcion }</label>`;
                                
                                $.each(value.hijos, function (index_child, value_child) { 
                                    const activo = (value_child.activo == 1) ? 'checked' : '';
                                    
                                    html += `           <ul style="margin-bottom: 5px;list-style: none">
                                                            <li>
                                                                <input tabindex="5" type="checkbox" data-key="${ value_child.clave }" class="icheck-child" ${ activo }>
                                                                <label for="minimal-checkbox-1-1" style="font-weight: bold;">${ value_child.descripcion }</label>
                                                            </li>
                                                        </ul>`;
                                });
                                
                                html += `           </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr />`;
                            });

                            html += `   </div>
                                        <div class="col-md-6">
                                            <div class="alert alert-info"><strong>Permisos</strong></div>`;

                            $.each(response.permisos, function (index, value) {
                                html += `<p class="text-divider"><span>${ value.DESCRIPCION }</span></p>`;

                                $.each(value.INFO, function (index_permiso, value_permiso) { 
                                    const activo = (value_permiso.ACTIVO == 1) ? 'checked' : '';

                                    html += `<div class="row">
                                                <div class="col-md-12" style="text-align: left;">
                                                    <ul style="margin-bottom: 5px; list-style: none">
                                                        <li>
                                                            <input tabindex="5" type="checkbox" data-key="${ value_permiso.ID }" class="icheck-permiso" ${ activo }>
                                                        <label for="minimal-checkbox-1-1" style="font-weight: bold; color: #000; font-size: 12px">${ value_permiso.DESCRIPCION }</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>`;
                                });
                            });

                            html += `   </div>
                                    </div>`;

                            swal.fire({
                                type: 'info', 
                                title: '<i class="fa fa-edit"></i> Modificar Permisos', 
                                html: '<hr />' + html,
                                confirmButtonText: 'Aceptar',
                                confirmButtonColor: '#00a651',
                                position: 'top',
                                width: '80%',
                                onOpen: () => {
                                    $('input.icheck-parent').iCheck({
                                        checkboxClass: 'icheckbox_square-blue',
                                        radioClass: 'iradio_square-blue'
                                    });

                                    $('input.icheck-child').iCheck({
                                        checkboxClass: 'icheckbox_square-purple',
                                        radioClass: 'iradio_square-purple'
                                    });

                                    $("input.icheck-parent").on('ifChanged', function (e) {
                                        var estado = $(this).iCheck('update')[0].checked; 
                                        $(this).closest('li').find('ul').each(function (index, value) {
                                            (estado)
                                                ? $(value).find('input.icheck-child').iCheck('check')
                                                : $(value).find('input.icheck-child').iCheck('uncheck'); 
                                        })
                                    });

                                    $("input.icheck-child").on('ifChanged', function (e) {
                                        // Verificamos el estado de todos los hijos
                                        // Encontramos el nodo li padre
                                        var estado = false; 
                                        $(this).closest('ul').closest('li').find('ul').each(function (index, value) {
                                            if($(value).find('input.icheck-child').iCheck('update')[0].checked) {
                                                estado = true; 
                                            }
                                        });

                                        (estado)
                                            ? $(this).closest('ul').closest('li').find('input.icheck-parent').iCheck('check')
                                            : $(this).closest('ul').closest('li').find('input.icheck-parent').iCheck('uncheck')
                                    });

                                    $("input.icheck-parent, input.icheck-child").on('ifChanged', function (e) {
                                        let estado = ($(this).iCheck('update')[0].checked) ? '1' : '0', 
                                        clave = $(this).data('key');

                                        $.ajax({
                                            type: "POST",
                                            url: root_path + "APIs/perfiles.php/activar_menu",
                                            data: {
                                                clave,
                                                estado,
                                                id
                                            },
                                            success: function (response) {
                                                if(response.msg == '200') toastr.success("Permiso actualizado correctamente", '', { clearOnNew: true, maxOpened: 2 });
                                                else toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                            },
                                            error: function () {
                                                toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                            }
                                        });
                                    });

                                    $("input.icheck-permiso").iCheck({
                                        checkboxClass: 'icheckbox_square-yellow',
                                        radioClass: 'iradio_square-yellow'
                                    });

                                    $("input.icheck-permiso").on('ifChanged', function (e) {
                                        let estado = ($(this).iCheck('update')[0].checked) ? '1' : '0', 
                                        clave = $(this).data('key');

                                        $.ajax({
                                            type: "POST",
                                            url: root_path + "APIs/perfiles.php/activar_permiso",
                                            data: {
                                                clave,
                                                estado,
                                                id
                                            },
                                            success: function (response) {
                                                if(response.msg == '200') toastr.success("Permiso actualizado correctamente", '', { clearOnNew: true, maxOpened: 2 });
                                                else toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                            },
                                            error: function () {
                                                toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                            }
                                        });
                                    });
                                }
                            })
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    })

    $(document).on('click', '.btn_zonas', function () {
        const id = $(this).data('id'),
        name = $(this).data('name'),
        description = $(this).data('desc'); 

        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/perfiles.php/get_zonas",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            html = '';
                            html += '<div class="row">';
                            html += '   <div class="col-md-12">';

                            html += '       <div class="alert alert-info"><strong>Configuración de zonas</strong></div>';

                            $.each(response.info, function (index, value) { 
                                const activo = (value.activo == true) ? 'checked' : '';

                                html += '<div class="row">'; 
                                html += '  <div class="col-md-12" style="text-align: left;">';
                                html += '      <ul style="margin-bottom: 5px; list-style: none">';
                                html += '          <li>';
                                html += '              <input tabindex="5" type="checkbox" data-key="' + value.id + '" class="icheck-zona" ' + activo + '>';
                                html += '              <label for="minimal-checkbox-1-1" style="font-weight: bold; color: #000; font-size: 12px">' + value.zona + '</label>';
                                html += '          </li>';
                                html += '      </ul>';
                                html += '  </div>';
                                html += '</div>'; 
                            });

                            html += '   </div>';
                            html += '</div>';

                            swal.fire({
                                title: 'Zonas',
                                html: html,
                                confirmButtonText: 'Aceptar',
                                confirmButtonColor: '#00a651',
                                position: 'top',
                                onOpen: () => {
                                    $("input.icheck-zona").iCheck({
                                        checkboxClass: 'icheckbox_square-yellow',
                                        radioClass: 'iradio_square-yellow'
                                    });

                                    $("input.icheck-zona").on('ifChanged', function (e) {
                                        let estado = ($(this).iCheck('update')[0].checked) ? '1' : '0', 
                                        zona = $(this).data('key');

                                        $.ajax({
                                            type: "POST",
                                            url: root_path + "APIs/perfiles.php/zona_update",
                                            data: {
                                                zona,
                                                estado,
                                                id
                                            },
                                            success: function (response) {
                                                if(response.msg == '200') {
                                                    toastr.success("Permiso actualizado correctamente", '', { clearOnNew: true, maxOpened: 2 });
                                                }
                                                else {
                                                    toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                                }
                                            },
                                            error: function () {
                                                toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                            }
                                        });
                                    });
                                }
                            });
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    })

    /** FUNCION PARA CREAR UN NUEVO PERFIL */
    $(document).on('click', '.btn_crear', function () {
        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {

                var html = '';
                html += `<div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="input-profile-name">Nombre del perfil</label>
                                    <input class="form-control" name="input-profile-name" id="input-profile-name" data-validate="required" placeholder="Nombre del perfil">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="input-profile-desc">Descripción del perfil</label>
                                    <input class="form-control" name="input-profile-desc" id="input-profile-desc" data-validate="required" placeholder="Descripción del perfil">
                                </div>
                            </div>
                        </div>`;
                
                swal.fire({
                    type: 'info', 
                    title: '<i class="fa fa-edit"></i> Agregar Perfil', 
                    html: '<hr />' + html,
                    width: ($(window).width() > 600) ? '60%' : '90%',
                    confirmButtonText: 'Confirmar',
                    confirmButtonColor: '#00a651' ,
                    showCancelButton: true, 
                    cancelButtonText: 'Cancelar', 
                    cancelButtonColor: '#cc2424',
                    position: 'top',
                    allowOutsideClick: false,
                    allowEnterKey: false,
                    preConfirm: () => {
                        if(!$("#input-profile-name").val()) {
                            Swal.showValidationMessage('Debe indicar el nombre del perfil')

                            return false; 
                        }

                        if(!$("#input-profile-desc").val()) {
                            Swal.showValidationMessage('Debe indicar la descripción del perfil')

                            return false; 
                        }
                    }
                }).then((result) => {
                    if(result.value) {
                        var name = $("#input-profile-name").val(); 
                        var desc = $("#input-profile-desc").val(); 
                        var modalRespuesta = swal.fire({
                            title: 'Cargando...', 
                            onBeforeOpen: () => {
                                swal.showLoading()
                            },
                            onOpen: () => {
                                $.ajax({
                                    type: "POST",
                                    url: root_path + "APIs/perfiles.php/crear_prefil",
                                    data: {
                                        name,
                                        desc
                                    },
                                    success: function (response) {
                                        if(response.msg == '200') {
                                            swal.fire({
                                                title: 'Información', 
                                                type: 'success', 
                                                text: 'Se registró correctamente la información',
                                                onClose: () => {
                                                    ActualizaTabla(); 
                                                }
                                            })
                                        }
                                        else {
                                            swal.fire({
                                                title: 'Información', 
                                                type: 'warning', 
                                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                            })
                                        }
                                    },
                                    error: () => {
                                        swal.fire({
                                            title: 'Información', 
                                            type: 'warning', 
                                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                        })
                                    }
                                });
                            }
                        });
                    }
                })
            }
        })
    })

    /** FUNCION QUE EDITA LA INFORMACION DEL PERFIL */
    $(document).on('click', '.btn_editar', function () {
        const id = $(this).data('id'),
        name = $(this).data('name'),
        description = $(this).data('desc'); 

        var html = '';
        html += `<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="input-profile-name">Nombre del perfil</label>
                            <input class="form-control" name="input-profile-name" id="input-profile-name" data-validate="required" placeholder="Nombre del perfil" value="${ name }">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="input-profile-desc">Descripción del perfil</label>
                            <input class="form-control" name="input-profile-desc" id="input-profile-desc" data-validate="required" placeholder="Descripción del perfil" value="${ description }">
                        </div>
                    </div>
                </div>`;
        
        swal.fire({
            type: 'info', 
            title: '<i class="fa fa-edit"></i> Actualizar Perfil', 
            html: '<hr />' + html,
            width: ($(window).width() > 600) ? '60%' : '90%',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#00a651' ,
            showCancelButton: true, 
            cancelButtonText: 'Cancelar', 
            cancelButtonColor: '#cc2424',
            position: 'top',
            allowOutsideClick: false,
            allowEnterKey: false,
            preConfirm: () => {
                if(!$("#input-profile-name").val()) {
                    Swal.showValidationMessage('Debe indicar el nombre del perfil')

                    return false; 
                }

                if(!$("#input-profile-desc").val()) {
                    Swal.showValidationMessage('Debe indicar la descripción del perfil')

                    return false; 
                }
            }
        }).then((result) => {
            if(result.value) {
                var name = $("#input-profile-name").val(); 
                var desc = $("#input-profile-desc").val(); 
                var modalRespuesta = swal.fire({
                    title: 'Cargando...', 
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        $.ajax({
                            type: "PUT",
                            url: root_path + "APIs/perfiles.php/actualizar_perfil",
                            data: {
                                name,
                                desc,
                                id
                            },
                            success: function (response) {
                                if(response.msg == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se actualizó correctamente la información',
                                        onClose: () => {
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                    })
                                }
                            },
                            error: () => {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                });
            }
        })
    })

    /** FUNCION QUE ELIMINA EL PERFIL */
    $(document).on('click', '.btn_eliminar', function () {
        const id = $(this).data('id'),
        name = $(this).data('name'),
        description = $(this).data('desc'); 

        Swal.fire({
            title: '¿Eliminar Perfil?',
            type: 'warning',
            text: `El perfil (${ name }) será eliminado, esta acción es permanente, ¿está seguro?`,
            showCancelButton: true,
            showConfirmButton: true,
            cancelButtonColor: '#21a9e1',
            confirmButtonColor: '#cc2424',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar',
            allowOutsideClick: false,
            allowEnterKey: false,
        }).then((result) => {
            if(result.value) {
                var modalRespuesta = swal.fire({
                    title: 'Cargando...', 
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        $.ajax({
                            type: "DELETE",
                            url: root_path + "APIs/perfiles.php/eliminar_perfil",
                            data: {
                                id
                            },
                            success: function (response) {
                                if(response.msg == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se eliminó correctamente la información',
                                        onClose: () => {
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                    })
                                }
                            },
                            error: () => {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                });
            }
        }); 
    })
});