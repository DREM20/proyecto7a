<?php 
    include('../../bar.php');
?>
    <h1>Membresías</h1>

    <?php if($permisos->getPermiso('MEMBRESIA_AGREGAR')) : ?>
        <button class="btn btn-primary btn_crear" style="margin-bottom: 5px;"><i class="fas fa-user-plus"></i> Activar Membresía</button>
    <?php endif; ?> 

    <div id="panelBusuqedaSimple" class="panel panel-default panel-shadow" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-search"></i> Busqueda</div>
            <div class="panel-options">
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form onsubmit="return false;" id="form_search" method="post" class="form-horizontal form-groups-bordered validate">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_estatus">Mostrar membresías</label>
                                <select name="search_estatus" id="search_estatus" class="form-control">
                                    <option value="0">Todas</option>
                                    <option value="1">Activas</option>
                                    <option value="2">Inactivas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_nombre">Nombre Operador</label>
                                <input type="text" class="form-control" name="search_nombre" id="search_nombre">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_telefono">Teléfono Operador</label>
                                <input type="text" class="form-control" name="search_telefono" id="search_telefono">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_id">ID Operador</label>
                                <input type="text" class="form-control" name="search_id" id="search_id">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_fechapago">Fecha de Pago</label>
                                <input type="text" class="form-control" name="search_fechapago" id="search_fechapago">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_fechacorte">Fecha de Corte</label>
                                <input type="text" class="form-control" name="search_fechacorte" id="search_fechacorte">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_limite">Limite</label>
                                <input type="text" class="form-control" name="search_limite" id="search_limite" value="100">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" id="btn_buscar" class="btn btn-blue btn-icon icon-left btn">
                            Buscar
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered datatable" id="table">
        <thead>
            <tr>
                <th></th>
                <th>Nombre Operador</th>
                <th>Correo</th>
                <th>Fecha Nacimiento</th>
                <th>Genero</th>
                <th>Teléfono</th>
                <th>Fecha de Pago</th>
                <th>Fecha de Corte</th>
                <th>Descuento</th>
                <th>Subtotal</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>
<?php
    include('../../footer.php');
?>