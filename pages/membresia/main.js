$(function () {

    /** INICIALIZAMOS COMPONENTES */
    $("#search_estatus").select2({
        allowClear: false,
    });

    $("#search_fechapago, #search_fechacorte").daterangepicker();

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        select: { style: 'single' },
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
    }

    var tabla = $("#table").DataTable(configtable);

    ActualizaTabla = function () {
        let fechapagoInicio = '',
        fechapagoFin = '', 
        fechacorteInicio = '', 
        fechacorteFin = '';
        if($("#search_fechapago").val()) {
            fecha = $("#search_fechapago").val().split('-');
            fechapagoInicio = fecha[0] ? fecha[0].trim() :  '';
            fechapagoFin = fecha[1] ? fecha[1].trim() : '';
        }

        if($("#search_fechacorte").val()) {
            fecha = $("#search_fechacorte").val().split('-');
            fechacorteInicio = fecha[0] ? fecha[0].trim() :  '';
            fechacorteFin = fecha[1] ? fecha[1].trim() : '';
        }

        var modalRespuesta = swal.fire({
            title: 'Cargando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: `${ root_path }APIs/membresia.php/tbody`,
                    data: {
                        estatus: $("#search_estatus").val(),
                        operador: $("#search_nombre").val(),
                        telefono: $("#search_telefono").val(),
                        idOperador: $("#search_id").val(),
                        limite: $("#search_limite").val(),
                        fechapagoInicio,
                        fechapagoFin,
                        fechacorteInicio,
                        fechacorteFin
                    },
                    success: function (response) {
                        modalRespuesta.closeModal();
                        if(response.info) componenteTable(response);
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    }

    /** FUNCION QUE DIBUJA LA TABLA CON LA INFORMACION */
    componenteTable = function (data) {
        tabla.destroy(); 

        let body = '';
        $.each(data.info, function (indexInArray, valueOfElement) {

            body += '<tr>';

            body += '<td></td>';
            body += (valueOfElement.nombre)         ? '<td>' + valueOfElement.nombre + '</td>' : '<td></td>';
            body += (valueOfElement.email)          ? '<td>' + valueOfElement.email + '</td>' : '<td></td>';
            body += (valueOfElement.cumpleahos)     ? '<td>' + valueOfElement.cumpleahos + '</td>' : '<td></td>';
            body += (valueOfElement.genero)         ? '<td>' + valueOfElement.genero + '</td>' : '<td></td>';
            body += (valueOfElement.telefono)       ? '<td>' + valueOfElement.telefono + '</td>' : '<td></td>';
            body += (valueOfElement.fechaPago)      ? '<td>' + valueOfElement.fechaPago + '</td>' : '<td></td>';
            body += (valueOfElement.fechaCorte)     ? '<td>' + valueOfElement.fechaCorte + '</td>' : '<td></td>';
            body += (valueOfElement.subtotal)       ? '<td>' + Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(valueOfElement.subtotal) + '</td>' : '<td></td>';
            body += (valueOfElement.descuento)      ? '<td>' + Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(valueOfElement.descuento) + '</td>' : '<td></td>';
            body += (valueOfElement.total)          ? '<td>' + Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(valueOfElement.total) + '</td>' : '<td></td>';

            body += '</tr>';
        });

        $("#table tbody").html(body);
        tabla = $("#table").DataTable(configtable);
    }

    $("#btn_buscar").on('click', function () {
        ActualizaTabla();
    })

    /** FUNCION PARA ACTIVAR UNA MEMBRESIA */
    $(document).on('click', '.btn_crear', function () {
        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                var html = '';
                html += `<div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_operador" class="control-label">Operador</label>
                                        <select class="form-control" id="input_agregar_operador" name="input_agregar_operador"></select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_fechapago" class="control-label">Fecha Pago</label>
                                        <input type="text" class="form-control" id="input_agregar_fechapago" name="input_agregar_fechapago">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_fechacorte" class="control-label">Fecha Corte</label>
                                        <input type="text" class="form-control" id="input_agregar_fechacorte" name="input_agregar_fechacorte">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_subtotal" class="control-label">Subtotal</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control" id="input_agregar_subtotal" name="input_agregar_subtotal">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_descuento" class="control-label">Descuento</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control" id="input_agregar_descuento" name="input_agregar_descuento">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="input_agregar_total" class="control-label">Total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
                                            <input type="text" class="form-control" id="input_agregar_total" name="input_agregar_total">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="alert alert-info"><strong>Ultimas membresías pagadas</strong></div>
                                        <div id="input_agregar_operador_membresias"></div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                const fetch = (url) => {
                    return $.ajax({
                        type: "GET",
                        url: url,
                    });
                }

                $.when(fetch(`${ root_path }APIs/catalogos.php/operadores`)).then(function (r1) {
                    
                    $option = '<option></option>';
                    if(r1.item) {
                        $.each(r1.item, function (index, value) { 
                            $option += `<option value="${ value.id }">${ value.id } - ${ value.nombre }</option>`;
                        }); 
                    }

                    var modal_crear = swal.fire({
                        type: 'info', 
                        title: 'Activar Membresía', 
                        html: '<hr />' + html,
                        width: ($(window).width() > 600) ? '60%' : '90%',
                        confirmButtonText: 'Confirmar',
                        confirmButtonColor: '#00a651' ,
                        showCancelButton: true, 
                        cancelButtonText: 'Cancelar', 
                        cancelButtonColor: '#cc2424',
                        position: 'top',
                        allowOutsideClick: false,
                        allowEnterKey: false,
                        onBeforeOpen: () => {
                            $("#input_agregar_operador").html($option);
                            $("#input_agregar_operador").select2({
                                allowClear: true,
                            });

                            $("#input_agregar_fechapago, #input_agregar_fechacorte").datepicker({
                                autoclose: true
                            });

                            $("#input_agregar_subtotal, #input_agregar_descuento, #input_agregar_total").keypress(function (event) {
                                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) event.preventDefault();
                            });

                            $("#input_agregar_subtotal, #input_agregar_descuento").keyup(function (event) {
                                const subtotal = (!($("#input_agregar_subtotal").val())) ? 0 : parseFloat($("#input_agregar_subtotal").val());
                                const descuento = (!($("#input_agregar_descuento").val())) ? 0 : parseFloat($("#input_agregar_descuento").val());

                                const total = subtotal - descuento;

                                $("#input_agregar_total").val(total);
                            });

                            $('#input_agregar_operador').on('change', function () {
                                const id = $(this).val();

                                if(!id) {
                                    $("#input_agregar_operador_membresias").html('<div class="alert alert-warning"><strong>Sin Información</strong></div>');
                                    
                                    return;
                                }

                                $.ajax({
                                    type: "GET",
                                    url: `${ root_path }APIs/membresia.php/operador_membresias/${ id }`,
                                    data: {
                                        limit: 5
                                    },
                                    success: function (response) {
                                        table = '';
                                        if(response.info.length > 0) {
                                            table += `<table class="table table-condensed table-bordered responsive" id="table_agregar_operador_membresias">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Estatus</th>
                                                                <th>Fecha Pago</th>
                                                                <th>Fecha Corte</th>
                                                                <th>Subtotal</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>`;

                                            $membresia_activa = false;
                                            $.each(response.info, function (index, value) {
                                                if(value.activo == true) $membresia_activa = true;

                                                table += `  <tr>
                                                                <td></td>
                                                                <td class="text-center">${ (value.activo == true) 
                                                                        ? '<span style="font-size: 1.5em; color: #00a651;" data-toggle="tooltip" data-placement="top" data-original-title="Vigente"><i class="fas fa-check-circle"></i></span>' 
                                                                        : '<span style="font-size: 1.5em; color: #cc2424;" data-toggle="tooltip" data-placement="top" data-original-title="Vencida"><i class="fas fa-times-circle"></i></span>' }
                                                                </td>
                                                                <td>${ value.fechaPago }</td>
                                                                <td>${ value.fechaCorte }</td>
                                                                <td>${ Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(value.subtotal) }</td>
                                                                <td>${ Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(value.total) }</td>
                                                            </tr>`; 
                                            });

                                            table += `  </tbody>
                                                    </table>`; 

                                            if($membresia_activa) table += `<div class="alert alert-warning"><strong>El usuario ya cuenta con una membresía activa</strong></div>`;
                                        }
                                        else table += `<div class="alert alert-warning"><strong>Sin Información</strong></div>`;

                                        $("#input_agregar_operador_membresias").html(table);
                                        $("#table_agregar_operador_membresias").DataTable({
                                            ordering: false,
                                            bPaginate: false,
                                            bLengthChange: false,
                                            bFilter: true,
                                            bInfo: false,
                                            searching: false,
                                            responsive: {
                                                details: { type: 'column', target: 0 }
                                            },
                                            columnDefs: [{
                                                    className: 'control',
                                                    orderable: false,
                                                    targets: 0
                                                },
                                            ],
                                        });
                                    }
                                });
                            });
                        },
                        preConfirm: () => {
                            if(!$("#input_agregar_operador").val()) {
                                Swal.showValidationMessage('Debe indicar el operador')
            
                                return false; 
                            }

                            if(!$("#input_agregar_fechapago").val() || !$("#input_agregar_fechacorte").val()) {
                                Swal.showValidationMessage('Debe indicar la fecha de pago y fecha de corte')
            
                                return false; 
                            }
                        }
                    }).then((result) => {
                        if(result.value) {
                            let data = {
                                operador: $("#input_agregar_operador").val(),
                                fechapago: $("#input_agregar_fechapago").val(),
                                fechacorte: $("#input_agregar_fechacorte").val(), 
                                subtotal: $("#input_agregar_subtotal").val(),
                                descuento: $("#input_agregar_descuento").val(), 
                                total: $("#input_agregar_total").val()
                            }

                            var modalRespuesta = swal.fire({
                                title: 'Cargando...', 
                                onBeforeOpen: () => {
                                    swal.showLoading()
                                },
                                onOpen: () => {
                                    $.ajax({
                                        type: "POST",
                                        url: `${ root_path }APIs/membresia.php/activar_membresia`,
                                        data: data,
                                        success: function (response) {
                                            if(response.msg == 200) {
                                                swal.fire({
                                                    title: 'Información', 
                                                    type: 'success', 
                                                    text: 'Se actualizó correctamente la información',
                                                    onClose: () => {
                                                        ActualizaTabla(); 
                                                    }
                                                })
                                            }
                                            else {
                                                swal.fire({
                                                    title: 'Información', 
                                                    type: 'warning', 
                                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                                })
                                            }
                                        },
                                        error: function () {
                                            swal.fire({
                                                title: 'Información', 
                                                type: 'warning', 
                                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                            })
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
    })
})