<?php 
    include('../../bar.php');
?>

    <h1>Marcas</h1>  
    <button class="btn btn-primary btn-accion" id ="btnmarca"data-action="C"><i class="fa fa-plus" style="margin-bottom: 5px;"></i> Agregar marca</button>
    <br>
    <br>

    <table class="table table-bordered datatable" id="tabla">
        <thead>
            <tr>
                <th></th>
                <th>Acciones</th>
                <th>Nombre de la marca</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
   
    <!--Modal -->
    <div class="modal fade" id="modal-marca">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" area-hidden="true">&times;</button>
                    <h4 class="modal-title">Marca</h4>
                </div>
                <div class="modal-body">

                <div id="mensaje">

                </div>

                    <form role="form" class="form-horizontal form-groups-borded validate" id="marca-form">
                        <input type="hidden" name="marca_id" id="marca_id">
                        <input type="hidden"  name ="marca_action" id="marca_action" >
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre de la marca</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="marca_nombre" maxlength="100" required>
                            </div>
                        </div> 

                    </form>

                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="btn_marca_save">Guardar Cambios</button>
				</div>

            </div>

        </div>

    </div>

    <!--Modal ver -->
    <div class="modal fade" id="modal-marca_ver">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" area-hidden="true">&times;</button>
                    <h4 class="modal-title">Informacion de la marca</h4>
                </div>
                <div class="modal-body">

                <div id="mensaje">

                </div>

                    <form role="form" class="form-horizontal form-groups-borded validate" id="marca-form">
                        <input type="hidden" name="marca_id_ver" id="marca_id_ver">
                       
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre de la marca</label>
                            <div class="col-sm-5">
                                <label class="col-sm-12 control-label" id="marca_nombre_ver">  </label>
                                
                            </div>
                        </div> 

                    </form>

                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

				</div>

            </div>

        </div>

    </div>
    


  <!-- Direccion a los archivos -->    
    <link rel="stylesheet" href="stylecss?=<?php print(rand());?>">
    <script src="main.js?i=<?php print(rand());?>"></script>

<?php
    include('../../footer.php');
?>

