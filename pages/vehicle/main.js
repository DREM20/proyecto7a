$(function () {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    const optSelect = {
        allowClear: true,
        theme: "classic",
        placeholder: "Seleccione una opción"
    }

    $(".select2").select2(optSelect);
    $(".select2").addClass('visible');

    /** INPUT FILE PREVIEW */
    $(document).on('click', ".btn_file_trigger", function () {
        $(this).closest('div.input-group').find('input[type="file"]').trigger('click');
    });

    $(document).on('change', 'input[type="file"]', function (e) {
        $(this).closest('div.form-group').find('img.preview_img').attr('src', '');

        let name = '';
        readURL(this);
        if(e.target.files && e.target.files[0]) name = e.target.files[0].name;

        $(this).closest('div.input-group').find('input[type="text"][readonly]').val(name);
        
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) { 
                $(input).closest('div.form-group').find('img.preview_img').attr('src', e.target.result) 
            }
            reader.readAsDataURL(input.files[0]);
        }
        else $(input).closest('div.form-group').find('img.preview_img').attr('src', '');
    }

    CargaDatosIniciales = function () {
        ActualizaTabla(); 
    }

    ActualizaTabla = function () {
        var modalRespuesta = swal.fire({
            title: 'Procesando...', 
            onBeforeOpen: () => {
                swal.showLoading(); 
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/vehicle.php/tbody",
                    data: {

                    },
                    success: function (response) {
                        modalRespuesta.closeModal();
                        if(response.info) componenteTable(response);
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function (data) {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        });
    }

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        select: { style: 'single' },
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
    }

    var tabla = $("#table").DataTable(configtable);
    componenteTable = function (data) {
        tabla.destroy(); 

        let body = '';
        $.each(data.info, function (indexInArray, valueOfElement) { 

            let $opciones = '';
            $opciones += '<div class="btn-group">';
            $opciones += '  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
            $opciones += '      Acciones <span class="caret"></span>';
            $opciones += '  </button>';
            $opciones += '  <ul class="dropdown-menu">';
            $opciones += '      <li><a href="#" class="vehicle_view" data-id="' + valueOfElement.idVehiculo + '">Ver</a></li>';
            $opciones += '      <li><a href="#" class="vehicle_edit" data-id="' + valueOfElement.idVehiculo + '">Editar</a></li>';
            $opciones += '      <li><a href="#" class="vehicle_docs" data-id="' + valueOfElement.idVehiculo + '">Verificar Documentos</a></li>';
            $opciones += '  </ul>';
            $opciones += '</div>';

            body += '<tr>';
            body += '<td></td>';
            body += '<td>' + $opciones + '</td>';
            body += (valueOfElement.aho) ? '<td>' + valueOfElement.aho + '</td>' : '<td></td>';
            body += (valueOfElement.placas) ? '<td>' + valueOfElement.placas + '</td>' : '<td></td>';
            body += (valueOfElement.tipoVehiculo) ? '<td>' + valueOfElement.tipoVehiculo + '</td>' : '<td></td>';
            body += (valueOfElement.marca) ? '<td>' + valueOfElement.marca + '</td>' : '<td></td>';
            body += (valueOfElement.modelo) ? '<td>' + valueOfElement.modelo + '</td>' : '<td></td>';
            body += (valueOfElement.color) ? '<td>' + valueOfElement.color + '</td>' : '<td></td>';
            body += (valueOfElement.vigenciaTarjetaDeCirculacion) ? '<td>' + valueOfElement.vigenciaTarjetaDeCirculacion + '</td>' : '<td></td>';
            body += (valueOfElement.vigenciaPolizaSeguro) ? '<td>' + valueOfElement.vigenciaPolizaSeguro + '</td>' : '<td></td>';
            body += '</tr>';
        });

        $("#table tbody").html(body);
        tabla = $("#table").DataTable(configtable);
    }

    $(document).on('click', 'a.vehicle_edit', function () {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/vehicle.php/load_info",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            modalRespuesta.closeModal();

                            $("#modal-vehicle").modal('show', {backdrop: 'static'});

                            const data = response.info;

                            limpiarModal();
                            // loadCity($("#operator_city"), data.idEstado, data.idMunicipio);
                            // loadUsers($("#operator_user"), data.idUsuario);

                            $("#vehicle_id").val(id);
                            $("#vehicle_action").val('U');

                            $("#vehicle_serie").val(data.serie);
                            $("#vehicle_year").val(data.aho).trigger('change');
                            $("#vehicle_number").val(data.placas);
                            $("#vehicle_motor").val(data.numeroMotor);
                            $("#vehicle_card").val(data.tarjetaDeCirculacion);
                            $("#vehicle_datecard").val(data.vigenciaTarjetaDeCirculacion);
                            $("#vehicle_secure").val(data.polizaSeguro);
                            $("#vehicle_datesecure").val(data.vigenciaPolizaSeguro);
                            $("#vehicle_phonesecure").val(data.telefonoPolizaSeguro);
                            $("#vehicle_namesecure").val(data.nombrePolizaSeguro);
                            $("#vehicle_comments").val(data.comentarios);
                            
                            loadTypeVehicle($("#vehicle_type"), data.idTipoVehiculo);
                            loadMarca($("#vehicle_brand"), data.idMarca, data.idModelo);
                            loadAseguradora($("#vehicle_insurance"), data.idAseguradora);
                            loadColor($("#vehicle_color"), data.idColor);
                            loadUser($("#vehicle_user"), data.idUsuario);

                            if(data.foto) $("#modal-vehicle .preview_img").attr('src', '../../' + data.foto + '?' + Math.random());
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'No se pudo obtener la información del usuario'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    })

    $(document).on('click', 'a.vehicle_view', function (param) {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading();
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/vehicle.php/load_info",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            modalRespuesta.closeModal();

                            const data = response.info;

                            if(data.foto) $("#vehicle_view_image").attr('src', '../../' + data.foto + '?' + Math.random());
                            else $("#vehicle_view_image").attr('src', '../../img/yabu.png');

                            $("#vehicle_view_name").html(data.usuario);
                            $("#vehicle_view_number").html(data.placas);

                            $("#vehicle_view_serie").html(data.serie);
                            $("#vehicle_view_year").html(data.aho);
                            $("#vehicle_view_type").html(data.tipoVehiculo);
                            $("#vehicle_view_brand").html(data.marca);
                            $("#vehicle_view_model").html(data.modelo);
                            $("#vehicle_view_insurance").html(data.aseguradora);
                            $("#vehicle_view_color").html(data.color);
                            $("#vehicle_view_motor").html(data.numeroMotor);
                            $("#vehicle_view_card").html(data.tarjetaDeCirculacion);
                            $("#vehicle_view_datecard").html(data.vigenciaTarjetaDeCirculacion);
                            $("#vehicle_view_secure").html(data.polizaSeguro);
                            $("#vehicle_view_datesecure").html(data.vigenciaPolizaSeguro);
                            $("#vehicle_view_phonesecure").html(data.telefonoPolizaSeguro);
                            $("#vehicle_view_namesecure").html(data.nombrePolizaSeguro);
                            $("#vehicle_view_comments").html(data.comentarios);

                            $("#modal-vehicle-view").modal('show', { backdrop: 'static' });
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'No se pudo obtener la información del vehículo'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        });
    });

    /** MODAL ACTIONS */
    limpiarModal = function () {
        $('#vehicle_form')[0].reset();
        $(".select2").select2('val', '');
        $("#vehicle_model").prop('disabled', true);
        $("#vehicle_form .preview_img").attr('src', '');
    }

    loadTypeVehicle = function (element, valor) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/tipoVehiculo",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                }
            }
        });

        return;
    }

    loadMarca = function (element, valor, modelo) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/marca",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                    if(valor && modelo) loadModeloByMarca($("#vehicle_model"), valor, modelo);
                }
            }
        });

        return;
    }

    $("#vehicle_brand").on('change', function () {
        loadModeloByMarca($("#vehicle_model"), $(this).val());
    });

    loadModeloByMarca = function (element, marca, valor) {
        if(marca) {
            $.ajax({
                type: "GET",
                url: root_path + "APIs/catalogos.php/modeloByMarca",
                data: {
                    marca
                },
                success: function (response) {
                    if(response.item) {
                        element.html('<option></option>');
                        $.each(response.item, function (index, value) { 
                            const selected = (value.id == valor) ? 'selected' : '';
                            element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                        });
                        element.prop('disabled', false);
                        element.select2('val', valor);
                    }
                }
            });
        }
        else element.select2('val', '').html('').prop('disabled', true);

        return;
    }

    loadAseguradora = function (element, valor) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/aseguradora",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                }
            }
        });

        return;
    }

    loadColor = function (element, valor) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/color",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                }
            }
        });

        return;
    }

    loadUser = function (element, valor) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/usuarios",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                }
            }
        });

        return;
    }

    $(document).on('click', '.btn-accion', function () {
        let action = $(this).data('action'),
        id = $(this).data('id');

        switch(action) {
            case 'C':
                limpiarModal();
                $("#modal-vehicle").modal('show', {backdrop: 'static'});
                $("#vehicle_action").val(action);
                loadTypeVehicle($("#vehicle_type"));
                loadMarca($("#vehicle_brand"));
                loadAseguradora($("#vehicle_insurance"));
                loadColor($("#vehicle_color"));
                loadUser($("#vehicle_user"));
                break;
        }

    });

    /** SAVE OPERATOR INFO */
    $("#btn_vehicle_save").on('click', function () {
        if(!$("#vehicle_form").valid()) return;

        const action = $("#vehicle_action").val(), 
        id = $("#vehicle_id").val();

        /** SAVE ALL ELEMENTS */
        let data = {}
        $('#vehicle_form input.form-control, #vehicle_form select.form-control, #vehicle_form textarea.form-control').filter(':input').each(function(){
            data[$(this).attr('name')] = $(this).val();
        });

        data['id'] = $("#vehicle_id").val();

        const formData = new FormData();
        formData.append('vehicle_picture', $("#vehicle_picture").prop('files')[0]);
        formData.append('data', JSON.stringify(data));

        var modalRespuesta = swal.fire({
            title: 'Procesando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                switch(action) {
                    case 'C':  
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/vehicle.php/save",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.code == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                        onClose: () => {
                                            $("#modal-vehicle").modal('hide');
                                            limpiarModal();
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: data.msg
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                        break;
                    case 'U':
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/vehicle.php/update_vehicle",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.code == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                        onClose: () => {
                                            $("#modal-vehicle").modal('hide');
                                            limpiarModal();
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: data.msg
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                        break;
                }
            }
        })
    });

    /** EXPEDIENTE DE DOCUMENTOS */

    limpiarModalDocs = function () {
        $('#vehicle_form_docs')[0].reset();
        $("#vehicle_form_docs .preview_img").attr('src', '');
    }

    $(document).on('click', 'a.vehicle_docs', function () {
        let id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/vehicle.php/get_docs",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.doctos) {
                            let body = '';
                            $.each(response.doctos, function (index, value) { 

                                const file = (value.docto) ? '../../' + value.docto + '?' + Math.random() : '#';

                                body += '<div class="form-group">';
                                body += '    <label class="col-md-3 control-label">' + value.documento + '</label>';
                                body += '    <div class="col-md-5">';
                                body += '        <div class="input-group">';

                                const tipo_doc = 'application/pdf, image/png, image/jpg, image/jpeg';

                                body += '            <input type="file" name="vehicle_doc_' + value.idDocumento + '" id="vehicle_doc_' + value.idDocumento + '" class="noMostrar" accept="' + tipo_doc + '">';
                                body += '            <input type="text" name="vehicle_doc_' + value.idDocumento + '_label" id="vehicle_doc_' + value.idDocumento + '_label" class="form-control btn_file_trigger" readonly>';
                                body += '            <span class="input-group-btn">';
                                body += '                <button id="vehicle_doc_' + value.idDocumento + '_trigger" class="btn btn-primary btn_file_trigger" type="button">';
                                body += '                    Seleccionar';
                                body += '                    <i class="fa fa-file-o"></i>';
                                body += '                </button>';
                                body += '            </span>';

                                if(value.aplicaVigencia == 1) body += '           <input type="text" class="form-control datepicker" placeholder="Vigencia" value="' + value.vigencia + '">';

                                body += '            <span class="input-group-btn">';
                                body += '                <button class="btn btn-success btn_save_doc" data-type="' + value.main + '" data-name="' + value.codigo + '" type="button">Guardar</button>';
                                body += '            </span>';

                                body += '        </div>';
                                body += '    </div>';
                                body += '    <div class="col-md-4">';

                                if(value.docto) {
                                    body += '     <button type="button" class="btn btn-blue btn-sm view-doc" data-file="' + root_path + value.docto + '">Ver Documento</button>';
                                    body += '     <a class="btn btn-success btn-sm" href="' + root_path + value.docto + '" download>Descargar Documento</a>';
                                }
                                else body += '     <button type="button" class="btn btn-danger btn-sm">Sin Documento</button>';

                                body += '    </div>';
                                body += '</div>';
                            });

                            $("#vehicle_form_docs").html(body);
                            $("#vehicle_picture_id").val(id);

                            $("#modal-vehicle-docs").modal('show', { backdrop: 'static' });

                            $("#vehicle_form_docs .datepicker").datepicker();
                            
                            modalRespuesta.closeModal();
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    });

    $(document).on('click', '.btn_save_doc', function () {

        const docto = $(this).closest('div.form-group').find('input[type="file"]').attr('id'),
        file = $(this).closest('div.form-group').find('input[type="file"]').prop('files')[0],
        main = $(this).data('type'),
        codigo = $(this).data('name'),
        id = $("#vehicle_picture_id").val();

        if($(this).closest('div.form-group').find('input.datepicker').length > 0) {
            if(!$(this).closest('div.form-group').find('input.datepicker').val()) {
                swal.fire({
                    title: 'Información',
                    type: 'info',
                    text: 'Debe indicar la vigencia del documento'
                });
    
                return;
            }
        }

        if(!file) {
            swal.fire({
                title: 'Información',
                type: 'info',
                text: 'Ningun archivo a guardar'
            });

            return;
        }

        const vigencia = ($(this).closest('div.form-group').find('input.datepicker').length) ? $(this).closest('div.form-group').find('input.datepicker').val() : '';

        swal.fire({
            title: 'Guardar Documento',
            text: '¿Está seguro que desea guardar el documento?',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#00a651' ,
            showCancelButton: true, 
            cancelButtonText: 'Cancelar', 
            cancelButtonColor: '#cc2424',
            allowOutsideClick: false,
            allowEnterKey: false,
        }).then((result) => {
            if(result.value) {
                const formData = new FormData();
                formData.append('file', file);
                formData.append('id', id);
                formData.append('docto', docto);
                formData.append('main', main);
                formData.append('codigo', codigo);
                formData.append('vigencia', vigencia);

                var modalRespuesta = swal.fire({
                    title: 'Procesando...', 
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/vehicle.php/save_doc",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.image) {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                })
            }
        });
    })

    $(document).on('click', '.view-doc', function () {
        let file = $(this).attr('data-file'),
        extension = file.split('.').pop();

        if(file) {
            if(extension.toUpperCase() == 'PDF') {
                var html = ''; 
                html += '<div class="row">'; 
                html += '	<div class="col-md-12">'; 
                html += '		<div id="view-doc-div">'; 
                html += '		</div>'; 
                html += '	</div>'; 
                html += '</div>'; 
    
                swal.fire({
                    title: 'Documento Cargado', 
                    html: html,
                    width: '80%',
                    onOpen: () => {
                        PDFObject.embed(file + '?f=' + $.now(), '#view-doc-div' , {height: "800px"}); 
                    }
                })
            }
            else if (extension.match(/(jpg|jpeg|png|gif|tiff)$/i)) {
                let html = '<img src="' + file + '" class="" style="width: 100%;" >';

                swal.fire({
                    html: html, 
                    width: '50%'
                })
            }
        }
        else {
            swal.fire({
                title: 'Información', 
                type: 'info', 
                text: 'No existe documento cargado'
            }); 
        }
    });

    $(document).on('click', 'a.vehicle_asignar', function () {
        let id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "admin/APIs/vehicle.php/get_conductor",
                    data: {
                        id
                    },
                    success: function (response) {
                        
                    }
                });
            }
        })
    })
})