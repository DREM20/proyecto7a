<?php 
    include('../../bar.php');
?>

    <h1>Vehículos</h1>

    <button class="btn btn-primary btn-accion" data-action="C" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Crear Nuevo</button>

    <table class="table table-bordered datatable" id="table">
        <thead>
            <tr>
                <th></th>
                <th>Opciones</th>
                <th>Año</th>
                <th>Placas</th>
                <th>Tipo Vehículo</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Color</th>
                <th>Vigencia Tarjeta de Circulación</th>
                <th>Vigencia Poliza de Seguro</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- MODAL USER CONFIG -->
    <div class="modal fade" id="modal-vehicle">
		<div class="modal-dialog" style="width: 70%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Vehículo</h4>
				</div>
				<div class="modal-body">
                    <form role="form" class="form-horizontal form-groups-bordered validate" id="vehicle_form">
                        <input type="hidden" name="vehicle_id" id="vehicle_id">
                        <input type="hidden" name="vehicle_action" id="vehicle_action">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Serie</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_serie" name="vehicle_serie" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Año</label>
                            <div class="col-sm-5">
                                <select name="vehicle_year" id="vehicle_year" class="form-control select2" data-allow-clear="true" required>
                                    <?php 
                                        $first = 1901;
                                        $last = 2155;

                                        for($i = $first; $i <= $last; $i++) {
                                            print('<option>' . $i . '</option>');
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Placas</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_number" name="vehicle_number" maxlength="10">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número de Motor</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_motor" name="vehicle_motor" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tarjeta de Circulación</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_card" name="vehicle_card" maxlength="100" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Vigencia Tarjeta de Circulación</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" id="vehicle_datecard" name="vehicle_datecard" data-format="dd-mm-yyyy" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Poliza de Seguro</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_secure" name="vehicle_secure" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Vigencia Poliza de Seguro</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" id="vehicle_datesecure" name="vehicle_datesecure" data-format="dd-mm-yyyy" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Teléfono Poliza de Seguro</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_phonesecure" name="vehicle_phonesecure" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre Poliza de Seguro</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="vehicle_namesecure" name="vehicle_namesecure" maxlength="150">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comentarios</label>
                            <div class="col-sm-5">
                                <textarea rows="5" class="form-control" style="resize: vertical;" id="vehicle_comments" name="vehicle_comments"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo de Vehículo</label>
                            <div class="col-sm-5">
                                <select name="vehicle_type" id="vehicle_type" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Marca</label>
                            <div class="col-sm-5">
                                <select name="vehicle_brand" id="vehicle_brand" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Modelo</label>
                            <div class="col-sm-5">
                                <select name="vehicle_model" id="vehicle_model" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Aseguradora</label>
                            <div class="col-sm-5">
                                <select name="vehicle_insurance" id="vehicle_insurance" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Color</label>
                            <div class="col-sm-5">
                                <select name="vehicle_color" id="vehicle_color" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Usuario</label>
                            <div class="col-sm-5">
                                <select name="vehicle_user" id="vehicle_user" class="form-control select2" data-allow-clear="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Foto</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="file" name="vehicle_picture" id="vehicle_picture" class="noMostrar" accept="image/png, image/jpg, image/jpeg">
                                    <input type="text" name="vehicle_picture_label" id="vehicle_picture_label" class="form-control btn_file_trigger" readonly>
                                    <span class="input-group-btn">
                                        <button id="vehicle_picture_trigger" class="btn btn-primary btn_file_trigger" type="button">
                                            Seleccionar
                                            <i class="fa fa-file-o"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <img class="preview_img" src="#" alt="" width="140"/>
                            </div>
                        </div>
                    </form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="btn_vehicle_save">Guardar Cambios</button>
				</div>
			</div>
		</div>
	</div>

    <!-- MODAL VEHICLE VIEW -->
    <div class="modal fade" id="modal-vehicle-view">
		<div class="modal-dialog" style="width: 85%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Vehículo</h4>
				</div>
				<div class="modal-body">
                    <div class="profile-env">
                        <header class="row">
                            <div class="col-sm-2">
					            <a href="#" class="profile-picture">
						            <img src="" class="img-responsive img-circle" id="vehicle_view_image"/>
					            </a>
				            </div>
                            <div class="col-sm-10">
                                <ul class="profile-info-sections">
                                    <li>
                                        <div class="profile-name">
                                            <strong>
                                                <a href="#" id="vehicle_view_name"></a>						
                                            </strong>
                                            <span><a href="#">Usuario</a></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="profile-stat">
                                            <h3 id="vehicle_view_number"></h3>
                                            <span><a href="#">Placa</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </header>
                        <section class="profile-feed">
                            <div class="panel panel-gray" data-collapsed="0">
                                <!-- panel body -->
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal form-groups-bordered">
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Serie</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_serie"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Año</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_year"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Tipo de Vehículo</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_type"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Marca</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_brand"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Modelo</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_model"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Aseguradora</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_insurance"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Color</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_color"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Número de Motor</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_motor"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Tarjeta de Circulación</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_card"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Vigencia Tarjeta de Circulación</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_datecard"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Poliza de Seguro</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_secure"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Vigencia Poliza de Seguro</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_datesecure"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Nombre Poliza de Seguro</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_namesecure"><span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Comentarios</label>
                                            <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                <span id="vehicle_view_comments"><span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DOCS CONFIG -->
    <div class="modal fade" id="modal-vehicle-docs">
		<div class="modal-dialog" style="width: 90%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Operador</h4>
				</div>
				<div class="modal-body">
                    <input type="hidden" name="vehicle_picture_id" id="vehicle_picture_id">
                    <form role="form" class="form-horizontal form-groups-bordered validate" id="vehicle_form_docs">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>

<?php
    include('../../footer.php');
?>