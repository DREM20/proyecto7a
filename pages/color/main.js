/******* LLAMA LA FUNCION PARA MOSTRAR TABLA */
$(document).ready(function(){

    Swal.fire({
        title: "Cargando...",
        text: "Espere se estan cargando los datos.",
        timer: 5000,
        onOpen: function() {
            Swal.showLoading()
        }
    }).then(
        (function(result) {
            if (result.dismiss === "timer") {
                //al finalizar trae 
                //console.log('estoy en el 1')
                cru();
            }
            if(result.dismiss !="timer"){
                //console.log('estoy en el 2')
                swal.fire({
                    title: 'Información', 
                    text:  'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico',
                    type: 'warning'
                });
            }
        }
    ))
    
    
    
   
 });

 /***MUESTRA TABLA */
 function cru(){
    //tablita.destroy();
    var confi = {
        // datos  a mostarar  
        "bDestroy": true,/// destrulle la anteriror
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
            
     
        //estilo
        select: { 
            style: 'single' 
        },
        
        responsive: {
            details: { 
                type: 'column', 
                target: 0 
            }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        //trae opciones de  copiar 
        dom: 'lBfrtip',
    }

    var tablita=$("#tabla").DataTable(confi);
    tablita.destroy();
     $(document).ready(function(){
        
       
        //tablita.destroy();
         $.get(root_path + "APIs/catalogos.php/color",function(data){
                    
             let body='';
             
             data.item.forEach(element => {
                //console.log(element);
                 body += '<tr>'
                 body += '<td></td>'
                 body += '  <td>'
                 body += '      <div class="btn-group">'
                 body += '          <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                 body += '              Acciones <span class="caret"></span>'
                 body += '          </button>'
                 body += '          <ul class="dropdown-menu">'
                 body += '              <li><a href="#" class="color_view"   id="color_view"   data-id="' + element.id + '">Ver</a></li>'
                 body += '              <li><a href="#" class="color_edit"   id="color_edit"   data-id="' + element.id + '" data-nombre="' + element.nombre + '">Editar</a></li>'
                 body += '              <li><a href="#" class="color_delete" id="color_delete" data-id="' + element.id + '">Eliminar</a></li>'               
                 body += '          </ul>'
                 body += '      </div>'
                 body += '  </td>'

                 body += '  <td>' + element.nombre +'</td>'

                 body += '</tr>'

             });
             body+=''
             //var id=$(this).data("id");
             //console.log(id);
            
            $("#tabla tbody").html(body);
            tablita=$("#tabla").DataTable(confi);
            
 
         });
         //tablita=$("#tabla").DataTable(confi);
         //tabla.destroy();
        
     });

 }
 
 
 /**** LLAMA AL MODAL */
 $(document).ready(function(){
     $('#btncolor').click(function(){
         //llama al modal
         $("#modal-color").modal("show", {backdrop: 'static'});
     })

 
 });

 
 /*** AGREGA NUEVA COLOR */
 $(document).on('click','#btn_color_save',function(){

    var nombre = $('#color_nombre').val();
    //console.log(nombre);
    if(nombre==null ||nombre=="") {
        //console.log('entre');
        //Swal.fire('Debe indicar el nombre del color')
        
        Swal.fire({
        title:"Debe indicar el nombre del color", 
        type:"error"
    });
        

    }else{
        swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type:"POST",
                    url: root_path+"APIs/catalogos.php/color_save",
                    data:{
                        nombre,
                    
                    },
                    success: function (response) {
                        if(response.msg == '200') {
                            swal.fire({
                                title: 'Información', 
                                type: 'success', 
                                text: 'Se registró correctamente la información',
                                onClose: () => {
                                    cru(); 
                                }
                            })
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico '
                                
                            })
                        }
                    },
    
    
                })
            }
        });
     
    }
         

 });


/**** ELIMINA COLOR  */
 $(document).on('click','#color_delete',function(){

    const id = $(this).data('id');
    nombre=$(this).data('nombre');
    
    Swal.fire({
        title: '¿Eliminar el color?',
        type: 'warning',
        text: `El color (${ nombre }) será eliminado, esta acción es permanente, ¿está seguro?`,
        showCancelButton: true,
        showConfirmButton: true,
        cancelButtonColor: '#21a9e1',
        confirmButtonColor: '#cc2424',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Eliminar',
        allowOutsideClick: false,
        allowEnterKey: false,
        
    }).then((result) => {
        if(result.value) {
            swal.fire({
                title: 'Cargando...', 
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: () => {
                    $.ajax({
                        type: "DELETE",
                        url: root_path + "APIs/catalogos.php/color_delete",
                        data: {
                            id
                        },
                        success: function (response) {
                            if(response.msg == '200') {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'success', 
                                    text: 'Se eliminó correctamente la información',
                                    onClose: () => {
                                        cru(); 
                                    }
                                })
                            }
                            else {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        },
                        error: () => {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    });
                }
            });
        }
    }); 


 });


 /***** EDITA COLOR */
 $(document).on('click','#color_edit',function(){
    
    const color=$(this).data('nombre');
    const id = $(this).data('id');
    
   //console.log(color);
    var html ='';
    html += `
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label" for="color_nombre">Nombre del color</label>
                <input class="form-control" name="color_nombre" id="color_nombreM" data-validate="required" placeholder="Nombre del color" value="${ color }">
            </div>
        </div>
    </div>
    `;
    
    swal.fire({
        type: 'info', 
        title: '<i class="fa fa-edit"></i> Actualizar Color', 
        html: '<hr />' + html,
        width: ($(window).width() > 600) ? '60%' : '90%',
        confirmButtonText: 'Confirmar',
        confirmButtonColor: '#00a651' ,
        showCancelButton: true, 
        cancelButtonText: 'Cancelar', 
        cancelButtonColor: '#cc2424',
        position: 'top',
        allowOutsideClick: false,
        allowEnterKey: false,
        preConfirm: () => {
            //console.log(document.getElementById("color_nombreM").value);
            //console.log($("#color_nombreM").val())
           
            if(!$("#color_nombreM").val()) {
                Swal.showValidationMessage('Debe indicar el nombre del color')
               
                return false; 
            }
        }
    }).then((result) => {

        if(result.value) {
            var nombre = $("#color_nombreM").val(); 
            
            swal.fire({
                title: 'Cargando...', 
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: () => {
                    $.ajax({
                        type: "PUT",
                        url: root_path + "APIs/catalogos.php/color_edit",
                        data: {
                            nombre,
                            id
                        },
                        success: function (response) {
                            if(response.msg == '200') {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'success', 
                                    text: 'Se actualizó correctamente la información',
                                    onClose: () => {
                                        cru();  
                                    }
                                })
                            }
                            else {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        },
                        error: () => {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    });
                }
            });
        }
    })

 });


/**** VER COLOR */
 $(document).on('click','#color_view', function(){
   //muestra modal
    $("#modal-color_ver").modal("show", {backdrop: 'static'});

    // obtener el id
    const id = $(this).data('id');
    //console.log(id);

    $(document).ready(function(){
        $.get(root_path + "APIs/catalogos.php/color",function(data){
            let body='';


           
            data.item.forEach(element => {
          

               if(element.id==id){
                body += '<td>' + element.nombre +'<td>'
               }             
             
            });

            body+=''
            
            $("#color_nombre_ver").html(body);

        });
    })


});



 