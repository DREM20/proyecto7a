$(function () {

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
        buttons: [{
                extend: 'excelHtml5',
                title: 'Reporte'
            }, {
                extend: 'pdfHtml5',
                title: 'Reporte'
            }
        ]
    }

    /** SE CONFIGURAN SELECT */
    const optSelect = {
        allowClear: true,
        theme: "classic",
        placeholder: "Seleccione una opción"
    }

    $(".select2").select2(optSelect);
    $(".select2").addClass('visible');

    $("#search_fecha").daterangepicker();

    CargaDatosIniciales = function () {

        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {

            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/catalogos.php/zonas",
                    success: function (response) {
                        let html = '',
                        activo = (response.info.length == 1) ? 'selected' : '' ;
                        // html += '<option></option>';
                        $.each(response.info, function (index, value) { 
                            html += '<option ' + activo +' data-latitud="' + value.latitud + '" data-longitud="' + value.longitud + '">' + value.zona + '</option>';
                        });

                        $("#search_zona").html(html);
                        $("#search_zona").select2(optSelect);
                        $("#search_zona").addClass('visible')

                        modalRespuesta.closeModal();
                    }
                });
            }
        });
    }

    $("#search_reporte").on('change', function () {
        let filtros = $(this).select2().find(":selected").data("filtros"),
        valor = $(this).val();

        if(!valor) {
            $("#filtros").css('display', 'none');

            return;
        }

        $("#filtros").css('display', 'block');
        $("#filtros .form-control").prop('disabled', true);
        $("#filtros .form-control").closest('div.form-group').parent().hide();
        
        if(filtros) filtros = filtros.split(',');
        $.each(filtros, function (index, value) {
            $("#search_" + value).prop('disabled', false);
            $("#search_" + value).closest('div.form-group').parent().show('fast');
        });
    });

    $("#btn_buscar").on('click', function () {
        let reporte = $("#search_reporte").val();
        let fecha = $("#search_fecha").val();
        let fecha_inicio = '', fecha_fin = '';
        if(fecha) {
            fecha = fecha.split('-');
            if(fecha[0]) fecha_inicio = fecha[0].trim();
            if(fecha[1]) fecha_fin = fecha[1].trim();
        }

        var modalRespuesta = swal.fire({
            title: 'Generando Reporte...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/reportes.php/reporte_" + reporte,
                    data: {
                        fecha_inicio,
                        fecha_fin,
                        zona: $("#search_zona").val()
                    },
                    success: function (response) {

                        if(response.file) {
                            Swal.fire({
								type: 'success',
								title: 'Informacion',
								text: 'Se generará el reporte en Excel, espere un momento',
							});
                            location.href = "../../APIs/descargaxlsx.php?file=reportes/" + response.file;
                            modalRespuesta.closeModal();
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function (param) {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    });
})