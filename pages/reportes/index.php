<?php 
    include('../../bar.php');
?>

    <h1>Reportes</h1>

    <div id="panelBusuqedaSimple" class="panel panel-default panel-shadow" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-search"></i> Busqueda</div>

            <div class="panel-options">
                <!-- <a href="#" data-rel="optionsAdv" class="bg"><i class="entypo-cog"></i></a> -->
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <!-- <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> -->
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <form onsubmit="return false;" id="form_search" method="post" class="form-horizontal form-groups-bordered validate" autocomplete="off">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_reporte">Tipo de Reporte</label>
                                <select class="form-control select2" name="search_reporte" id="search_reporte">
                                    <option></option>
                                    <option value="viajes" data-filtros="fecha,zona">Historico Viajes</option>
                                    <option value="viajes_operador" data-filtros="fecha,zona">Viajes por Operador</option>
                                    <option value="viajes_pasajero" data-filtros="fecha,zona">Viajes por Pasajero</option>
                                    <option value="pasajeros" data-filtros="fecha">Usuarios Registrados</option>
                                    <option value="operadores" data-filtros="fecha">Operadores Registrados</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="filtros" style="display: none;">
                    <div class="hr-theme-slash-2">
                        <div class="hr-line"></div>
                        <div class="hr-icon"><i class="fa fa-search-plus"></i></div>
                        <div class="hr-line"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label" for="search_zona">Zona</label>
                                    <select name="search_zona" id="search_zona" class="form-control select2">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label" for="search_fecha">Fecha</label>
                                    <input type="text" class="form-control" name="search_fecha" id="search_fecha">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" id="btn_buscar" class="btn btn-blue btn-icon icon-left btn">
                            Generar Reporte
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="main.js?i=<?php print(rand()); ?> "></script>

<?php
    include('../../footer.php');
?>
