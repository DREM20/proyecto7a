<?php 
    include('../../bar.php');
?>
    <h1>Cupones</h1>

    <?php if($permisos->getPermiso('CUPON_AGREGAR')) : ?>
        <button class="btn btn-primary btn_crear" style="margin-bottom: 5px;"><i class="fas fa-plus"></i> Crear Cupón</button>
    <?php endif; ?>

    

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>

<?php
    include('../../footer.php');
?>