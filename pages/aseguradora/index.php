<?php 
    include('../../bar.php');
?>

    <h1>Aseguradoras</h1>  
    <button class="btn btn-primary btn-accion" id ="btnaseguradora"data-action="C"><i class="fa fa-plus" style="margin-bottom: 5px;"></i> Agregar aseguradora</button>
    <br>
    <br>

    <table class="table table-bordered datatable" id="tabla">
        <thead>
            <tr>
                <th></th>
                <th>Acciones</th>
                <th>Nombre de la aseguradora</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
   
    <!--Modal -->
    <div class="modal fade" id="modal-aceseguradora">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" area-hidden="true">&times;</button>
                    <h4 class="modal-title">Aseguradora</h4>
                </div>
                <div class="modal-body">

                <div id="mensaje">

                </div>

                    <form role="form" class="form-horizontal form-groups-borded validate" id="aceguradora-form">
                        <input type="hidden" name="aseguradora_id" id="aseguradora_id">
                        <input type="hidden"  name ="aseguradora_action" id="aseguradora_action" >
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre de la aseguradora</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="aseguradora_nombre" maxlength="100" required>
                            </div>
                        </div> 

                    </form>

                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="btn_aseguradora_save">Guardar Cambios</button>
				</div>

            </div>

        </div>

    </div>

    <!--Modal ver -->
    <div class="modal fade" id="modal-aceseguradora_ver">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" area-hidden="true">&times;</button>
                    <h4 class="modal-title">Informacion de aseguradora</h4>
                </div>
                <div class="modal-body">

                <div id="mensaje">

                </div>

                    <form role="form" class="form-horizontal form-groups-borded validate" id="aceguradora-form">
                        <input type="hidden" name="aseguradora_id_ver" id="aseguradora_id_ver">
                       
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre de la aseguradora</label>
                            <div class="col-sm-5">
                                <label class="col-sm-12 control-label" id="aseguradora_nombre_ver">  </label>
                                
                            </div>
                        </div> 

                    </form>

                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

				</div>

            </div>

        </div>

    </div>
    


  <!-- Direccion a los archivos -->    
    <link rel="stylesheet" href="stylecss?=<?php print(rand());?>">
    <script src="main.js?i=<?php print(rand());?>"></script>

<?php
    include('../../footer.php');
?>

