/******* LLAMA LA FUNCION PARA MOSTRAR TABLA */
$(document).ready(function(){

    Swal.fire({
        title: "Cargando...",
        text: "Espere se estan cargando los datos.",
        timer: 5000,
        onOpen: function() {
            Swal.showLoading()
        }
    }).then(
        (function(result) {
            if (result.dismiss === "timer") {
                //al finalizar trae 
                //console.log('estoy en el 1')
                cru();
            }
            if(result.dismiss !="timer"){
                //console.log('estoy en el 2')
                swal.fire({
                    title: 'Información', 
                    text:  'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico',
                    type: 'warning'
                });
            }
        }
    ))
    
    
    
   
 });

 /***MUESTRA TABLA */
 function cru(){
    //tablita.destroy();
    var confi = {
        // datos  a mostarar  
        "bDestroy": true,
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
            
     
        //estilo
        select: { 
            style: 'single' 
        },
        
        responsive: {
            details: { 
                type: 'column', 
                target: 0 
            }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        //trae opciones de  copiar 
        dom: 'lBfrtip',
    }

    var tablita=$("#tabla").DataTable(confi);
    tablita.destroy();
     $(document).ready(function(){
        
       
        //tablita.destroy();
         $.get(root_path + "APIs/catalogos.php/aseguradora",function(data){
                    
             let body='';
             
             data.item.forEach(element => {
                //console.log(element);
                 body += '<tr>'
                 body += '<td></td>'
                 body += '  <td>'
                 body += '      <div class="btn-group">'
                 body += '          <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                 body += '              Acciones <span class="caret"></span>'
                 body += '          </button>'
                 body += '          <ul class="dropdown-menu">'
                 body += '              <li><a href="#" class="aseguradora_view"   id="aseguradora_view"   data-id="' + element.id + '">Ver</a></li>'
                 body += '              <li><a href="#" class="aseguradora_edit"   id="aseguradora_edit"   data-id="' + element.id + '" data-nombre="' + element.nombre + '">Editar</a></li>'
                 body += '              <li><a href="#" class="aseguradora_delete" id="aseguradora_delete" data-id="' + element.id + '">Eliminar</a></li>'               
                 body += '          </ul>'
                 body += '      </div>'
                 body += '  </td>'

                 body += '  <td>' + element.nombre +'</td>'

                 body += '</tr>'

             });
             body+=''
             //var id=$(this).data("id");
             //console.log(id);
            
            $("#tabla tbody").html(body);
            tablita=$("#tabla").DataTable(confi);
            
 
         });
         //tablita=$("#tabla").DataTable(confi);
         //tabla.destroy();
        
     });

 }
 
 
 /**** LLAMA AL MODAL */
 $(document).ready(function(){
     $('#btnaseguradora').click(function(){
         //llama al modal
         $("#modal-aceseguradora").modal("show", {backdrop: 'static'});
     })

 
 });

 
 /*** AGREGA NUEVA ASEGURADORA */
 $(document).on('click','#btn_aseguradora_save',function(){

    var nombre = $('#aseguradora_nombre').val();
    //console.log(nombre);
    if(nombre==null ||nombre=="") {
        //console.log('entre');
        //Swal.fire('Debe indicar el nombre de la aseguradora')
        
        Swal.fire({
        title:"Debe indicar el nombre de la aseguradora!", 
        type:"error"
    });
        

    }else{
        swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type:"POST",
                    url: root_path+"APIs/catalogos.php/aseguradora_save",
                    data:{
                        nombre,
                    
                    },
                    success: function (response) {
                        if(response.msg == '200') {
                            swal.fire({
                                title: 'Información', 
                                type: 'success', 
                                text: 'Se registró correctamente la información',
                                onClose: () => {
                                    cru(); 
                                }
                            })
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico '
                                
                            })
                        }
                    },
    
    
                })
            }
        });
     
    }
         

 });


/**** ELIMINA ASEGURADORA  */
 $(document).on('click','#aseguradora_delete',function(){

    const id = $(this).data('id');
    nombre=$(this).data('nombre');
    
    Swal.fire({
        title: '¿Eliminar la aseguradora?',
        type: 'warning',
        text: `La aseguradora (${ nombre }) será eliminado, esta acción es permanente, ¿está seguro?`,
        showCancelButton: true,
        showConfirmButton: true,
        cancelButtonColor: '#21a9e1',
        confirmButtonColor: '#cc2424',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Eliminar',
        allowOutsideClick: false,
        allowEnterKey: false,
        
    }).then((result) => {
        if(result.value) {
            swal.fire({
                title: 'Cargando...', 
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: () => {
                    $.ajax({
                        type: "DELETE",
                        url: root_path + "APIs/catalogos.php/aseguradora_delete",
                        data: {
                            id
                        },
                        success: function (response) {
                            if(response.msg == '200') {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'success', 
                                    text: 'Se eliminó correctamente la información',
                                    onClose: () => {
                                        cru(); 
                                    }
                                })
                            }
                            else {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        },
                        error: () => {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    });
                }
            });
        }
    }); 


 });


 /***** EDITA ASEGURADORA */
 $(document).on('click','#aseguradora_edit',function(){
    
    const aseguradora=$(this).data('nombre');
    const id = $(this).data('id');
    
   //console.log(aseguradora);
    var html ='';
    html += `
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label" for="aseguradora_nombre">Nombre de la aseguradora</label>
                <input class="form-control" name="aseguradora_nombre" id="aseguradora_nombreM" data-validate="required" placeholder="Nombre de la aseguradora" value="${ aseguradora }">
            </div>
        </div>
    </div>
    `;
    
    swal.fire({
        type: 'info', 
        title: '<i class="fa fa-edit"></i> Actualizar Aseguradora', 
        html: '<hr />' + html,
        width: ($(window).width() > 600) ? '60%' : '90%',
        confirmButtonText: 'Confirmar',
        confirmButtonColor: '#00a651' ,
        showCancelButton: true, 
        cancelButtonText: 'Cancelar', 
        cancelButtonColor: '#cc2424',
        position: 'top',
        allowOutsideClick: false,
        allowEnterKey: false,
        preConfirm: () => {
            //console.log(document.getElementById("aseguradora_nombreM").value);
            //console.log($("#aseguradora_nombreM").val())
           
            if(!$("#aseguradora_nombreM").val()) {
                Swal.showValidationMessage('Debe indicar el nombre de la aseguradora')
               
                return false; 
            }
        }
    }).then((result) => {

        if(result.value) {
            var nombre = $("#aseguradora_nombreM").val(); 
            
            swal.fire({
                title: 'Cargando...', 
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: () => {
                    $.ajax({
                        type: "PUT",
                        url: root_path + "APIs/catalogos.php/aseguradora_edit",
                        data: {
                            nombre,
                            id
                        },
                        success: function (response) {
                            if(response.msg == '200') {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'success', 
                                    text: 'Se actualizó correctamente la información',
                                    onClose: () => {
                                        cru();  
                                    }
                                })
                            }
                            else {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        },
                        error: () => {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    });
                }
            });
        }
    })

 });


/**** VER ASEGURADORA */
 $(document).on('click','#aseguradora_view', function(){
   //muestra modal
    $("#modal-aceseguradora_ver").modal("show", {backdrop: 'static'});
    // obtener el id
    const id = $(this).data('id');
    //console.log(id);

    $(document).ready(function(){
        $.get(root_path + "APIs/catalogos.php/aseguradora",function(data){
            let body='';


           
            data.item.forEach(element => {
          

               if(element.id==id){
                body += '<td>' + element.nombre +'<td>'
               }             
             
            });

            body+=''
            
            $("#aseguradora_nombre_ver").html(body);

        });
    })


});



 