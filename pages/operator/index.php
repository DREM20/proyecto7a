<?php 
    include('../../bar.php');
?>

    <h1>Operadores</h1>
    
    <?php if($permisos->getPermiso('OPERADOR_CREAR')) : ?>
        <button class="btn btn-primary btn-accion" data-action="C" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Crear Nuevo</button>
    <?php endif; ?>

    <div id="panelBusuqedaSimple" class="panel panel-default panel-shadow" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-search"></i> Busqueda</div>
            <div class="panel-options">
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form onsubmit="return false;" id="form_search" method="post" class="form-horizontal form-groups-bordered validate">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_nombre">Nombre</label>
                                <input type="text" class="form-control" name="search_nombre" id="search_nombre">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_telefono">Teléfono</label>
                                <input type="text" class="form-control" name="search_telefono" id="search_telefono">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_id">ID</label>
                                <input type="text" class="form-control" name="search_id" id="search_id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_date">Fecha de Nacimiento</label>
                                <input type="text" class="form-control" name="search_date" id="search_date">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_genero">Genero</label>
                                <select name="search_genero" id="search_genero" class="form-control select2">
                                    <option></option>
                                    <option value="H">Masculino</option>
                                    <option value="M">Femenino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_estado">Estado</label>
                                <select name="search_estado" id="search_estado" class="form-control select2">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_municipio">Municipio</label>
                                <select name="search_municipio" id="search_municipio" class="form-control select2" disabled>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="search_limite">Limite</label>
                                <input type="text" class="form-control" name="search_limite" id="search_limite" value="100">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" id="btn_buscar" class="btn btn-blue btn-icon icon-left btn">
                            Buscar
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered datatable" id="table">
        <thead>
            <tr>
                <th></th>
                <th>Visible</th>
                <th>Opciones</th>
                <th>ID</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Fecha Nacimiento</th>
                <th>Genero</th>
                <th>Teléfono</th>
                <th>Operador Activo</th>
                <th>Foto</th>
                <th>INE</th>
                <th>Comisión</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- MODAL USER CONFIG -->
    <div class="modal fade" id="modal-operator">
		<div class="modal-dialog" style="width: 70%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Operador</h4>
				</div>
				<div class="modal-body">
                    <form role="form" class="form-horizontal form-groups-bordered validate" id="operator_form">
                        <input type="hidden" name="operator_id" id="operator_id">
                        <input type="hidden" name="operator_action" id="operator_action">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cumpleaños</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="operator_birth" name="operator_birth" data-format="dd-mm-yyyy" required>
                                    <div class="input-group-addon">
                                        <a href="#"><i class="far fa-calendar-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_name" name="operator_name" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Apellidos</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_lastname" name="operator_lastname" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">RFC</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_rfc" name="operator_rfc" maxlength="128">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_direction" name="operator_direction" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Genero</label>
                            <div class="col-sm-5">
                                <select name="operator_gender" id="operator_gender" class="form-control select2">
                                    <option value="H">Hombre</option>
                                    <option value="M">Mujer</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_email" name="operator_email" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Teléfono</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_phone" name="operator_phone" maxlength="15">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Teléfono Emergencia</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_emergencyphone" name="operator_emergencyphone" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contacto Emergencia</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_emergencycontact" name="operator_emergencycontact" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Extensión</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_extension" name="operator_extension" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cuenta</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_account" name="operator_account" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Licencia</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_license" name="operator_license" maxlength="50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Licencia Vigencia</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="operator_validlicense" name="operator_validlicense" data-format="dd-mm-yyyy" required>
                                    <div class="input-group-addon">
                                        <a href="#"><i class="far fa-calendar-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-sm-5">
                                <select name="operator_city" id="operator_city" class="form-control select2" data-allow-clear="true">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Municipio</label>
                            <div class="col-sm-5">
                                <select name="operator_town" id="operator_town" class="form-control select2" disabled>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Usuario</label>
                            <div class="col-sm-5">
                                <select name="operator_user" id="operator_user" class="form-control select2">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Operador Activo</label>
                            <div class="col-sm-5">
                                <select name="operator_active" id="operator_active" class="form-control select2">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comentarios</label>
                            <div class="col-sm-5">
                                <textarea rows="5" class="form-control" style="resize: vertical;" id="operator_comments" name="operator_comments"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Foto</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="file" name="operator_picture" id="operator_picture" class="noMostrar" accept="image/png, image/jpg, image/jpeg">
                                    <input type="text" name="operator_picture_label" id="operator_picture_label" class="form-control btn_file_trigger" readonly>
                                    <span class="input-group-btn">
                                        <button id="operator_picture_trigger" class="btn btn-primary btn_file_trigger" type="button">
                                            Seleccionar
                                            <i class="fa fa-file-o"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <img class="preview_img" src="#" alt="" width="140"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">No. INE</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_ine" name="operator_ine" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comisión</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="operator_commission" name="operator_commission" maxlength="4">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dueño</label>
                            <div class="col-sm-5">
                                <ul class="icheck-list">
								    <li>
                                        <input type="radio" name="operator_owner" id="operator_owner_1" class="iCheck" value="1">
								        <label for="operator_owner_1" class="hover">SI</label>
								    </li>
								    <li>
                                        <input type="radio" name="operator_owner" id="operator_owner_2" class="iCheck" value="0" checked>
								        <label for="operator_owner_1" class="">NO</label>
								    </li>
								</ul>
                            </div>
                        </div>
                    </form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" id="btn_operator_save">Guardar Cambios</button>
				</div>
			</div>
		</div>
	</div>

    <!-- MODAL USER VIEW -->
    <div class="modal fade" id="modal-operator-view">
		<div class="modal-dialog" style="width: 85%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Operador</h4>
				</div>
				<div class="modal-body">
                    <div class="profile-env">
                        <header class="row">
                            <div class="col-sm-2">
					            <a href="#" class="profile-picture">
						            <img src="" class="img-responsive img-circle" id="operator_view_image"/>
					            </a>
				            </div>
                            <div class="col-sm-10">
                                <ul class="profile-info-sections">
                                    <li>
                                        <div class="profile-name">
                                            <strong>
                                                <a href="#" id="operator_view_name"></a>						
                                            </strong>
                                            <span><a href="#">Operador</a></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="profile-stat">
                                            <h3 id="operator_view_travels"></h3>
                                            <span><a href="#">Viajes</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </header>
                        <section class="profile-info-tabs">
                            <div class="row">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <ul class="user-details">
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span id="operator_view_address"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="far fa-calendar-alt"></i>
                                                <span id="operator_view_birth"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-at"></i>
                                                <span id="operator_view_email"></span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- tabs for the profile links -->
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#profile-info">Perfil</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section class="profile-feed">
                            <div class="panel panel-gray" data-collapsed="0">
                                <!-- panel body -->
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="alert alert-info"><strong>Información</strong></div>
                                            </div>
                                            <form role="form" class="form-horizontal form-groups-bordered">
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">RFC</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_rfc"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Genero</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_gender"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Teléfono</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_phone"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Teléfono Emergencia</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_emercyphone"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Contacto Emergencia</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_emercycontact"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Extensión</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_extension"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Cuenta</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_account"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Licencia</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_license"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Licencia Vigencia</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_validlicense"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Estado</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_city"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Municipio</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_town"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Usuario</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_user"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Operador Activo</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_active"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Comentarios</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_comments"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">No. INE</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_ine"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Comisión</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_comition"><span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-3 control-label">Dueño</label>
                                                    <div class="col-sm-9 control-label" style="text-align: left !important;">
                                                        <span id="operator_view_owner"><span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="operator_view_docs">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="operador_vehiculos">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DOCS CONFIG -->
    <div class="modal fade" id="modal-operator-docs">
		<div class="modal-dialog" style="width: 80%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Operador</h4>
				</div>
				<div class="modal-body">
                    <input type="hidden" name="operator_picture_id" id="operator_picture_id">
                    <form role="form" class="form-horizontal form-groups-bordered validate" id="operator_form_docs">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="style.css?i=<?php print(rand()); ?>">
    <script src="main.js?i=<?php print(rand());  ?>"></script>

<?php
    include('../../footer.php');
?>