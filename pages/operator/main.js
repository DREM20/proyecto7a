$(function () {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    $("#search_date").datepicker();

    /** SE CONFIGURAN SELECT */
    const optSelect = {
        allowClear: true,
        theme: "classic",
        placeholder: "Seleccione una opción"
    }

    $(".select2").select2(optSelect);
    $(".select2").addClass('visible');

    /** INPUT FILE PREVIEW */
    $(document).on('click', ".btn_file_trigger", function () {
        $(this).closest('div.input-group').find('input[type="file"]').trigger('click');
    });

    $(document).on('change', 'input[type="file"]', function (e) {
        $(this).closest('div.form-group').find('img.preview_img').attr('src', '');

        let name = '';
        readURL(this);
        if(e.target.files && e.target.files[0]) name = e.target.files[0].name;

        $(this).closest('div.input-group').find('input[type="text"]').val(name);
        
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) { 
                $(input).closest('div.form-group').find('img.preview_img').attr('src', e.target.result) 
            }
            reader.readAsDataURL(input.files[0]);
        }
        else $(input).closest('div.form-group').find('img.preview_img').attr('src', '');
    }

    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-yellow',
        radioClass: 'iradio_square-yellow',
        increaseArea: '20%' // optional
    });

    CargaDatosIniciales = function () {
        loadCity($("#search_estado"));
    }

    $("#search_estado").on('change', function () {
        loadTownByCity($(this).val(), $("#search_municipio"));
    })

    $("#btn_buscar").on('click', function () {
        ActualizaTabla();
    });

    ActualizaTabla = function () {
        var modalRespuesta = swal.fire({
            title: 'Procesando...', 
            onBeforeOpen: () => {
                swal.showLoading(); 
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/tbody",
                    data: $("#form_search").serialize(),
                    success: function (response) {
                        modalRespuesta.closeModal();
                        if(response.info) componenteTable(response);
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function (data) {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        });
    }

    var configtable = {
        language: idiomaTablas,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        pageLength: 10,
        select: { style: 'single' },
        responsive: {
            details: { type: 'column', target: 0 }
        },
        columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        dom: 'lBfrtip',
    }

    var tabla = $("#table").DataTable(configtable);
    componenteTable = function (data) {
        tabla.destroy(); 

        let body = '';
        $.each(data.info, function (indexInArray, valueOfElement) {

            let $opciones = '';

            const $activado = (valueOfElement.operadorActivo == '1') ? 'DESACTIVAR' : 'ACTIVAR',
            $valoractivo = (valueOfElement.operadorActivo == '1') ? 0 : 1;

            $opciones += '<div class="btn-group">';
            $opciones += '  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
            $opciones += '      Acciones <span class="caret"></span>';
            $opciones += '  </button>';
            $opciones += '  <ul class="dropdown-menu">';
            if(data.options.ver)                $opciones += '      <li><a href="#" class="operator_view" data-id="' + valueOfElement.id + '">Ver</a></li>';
            if(data.options.editar)             $opciones += '      <li><a href="#" class="operator_edit" data-id="' + valueOfElement.id + '">Editar</a></li>';
            if(data.options.doctos)             $opciones += '      <li><a href="#" class="operator_docs" data-id="' + valueOfElement.id + '">Verificar Documentos</a></li>';
            if(data.options.recuperacion)       $opciones += '      <li><a href="#" class="operator_recuperacion" data-id="' + valueOfElement.id + '">Recuperar Contraseña</a></li>';
            if(data.options.vehiculos)          $opciones += '      <li><a href="#" class="operator_vehiculos" data-id="' + valueOfElement.id + '">Vehiculos Asignados</a></li>';
            if(data.options.activar)            $opciones += '      <li role="separator" class="divider"></li>';
            if(data.options.activar)            $opciones += '      <li><a href="#" class="operator_disable" data-id="' + valueOfElement.id + '" data-status="'+ $valoractivo +'">'+ $activado +'</a></li>';
            $opciones += '  </ul>';
            $opciones += '</div>';

            body += '<tr>';
            body += '<td></td>';

            if(data.options.asignar) {
                const checked = (valueOfElement.visible == 1) ? 'checked' : '';
                body += '<td><input class="check_visible" data-id="' + valueOfElement.id + '" type="checkbox" ' + checked + '></td>';
            }
            else body += '<td></td>';

            body += '<td>' + $opciones + '</td>';
            body += (valueOfElement.id)             ? '<td>' + valueOfElement.id + '</td>' : '<td></td>';
            body += (valueOfElement.nombre)         ? '<td>' + valueOfElement.nombre + '</td>' : '<td></td>';
            body += (valueOfElement.email)          ? '<td>' + valueOfElement.email + '</td>' : '<td></td>';
            body += (valueOfElement.cumpleahos)     ? '<td>' + valueOfElement.cumpleahos + '</td>' : '<td></td>';
            body += (valueOfElement.genero)         ? '<td>' + valueOfElement.genero + '</td>' : '<td></td>';
            body += (valueOfElement.telefono)       ? '<td>' + valueOfElement.telefono + '</td>' : '<td></td>';
            body += (valueOfElement.operadorActivo) ? '<td>' + valueOfElement.operadorActivo + '</td>' : '<td></td>';
            body += (valueOfElement.foto)           ? '<td>' + valueOfElement.foto + '</td>' : '<td></td>';
            body += (valueOfElement.noIFE)          ? '<td>' + valueOfElement.noIFE + '</td>' : '<td></td>';
            body += (valueOfElement.comision)       ? '<td>' + valueOfElement.comision + '</td>' : '<td></td>';

            body += '</tr>';
        });

        $("#table tbody").html(body);
        tabla = $("#table").DataTable(configtable);

        if(!data.options.asignar) $("#table td:nth-child(2), #table th:nth-child(2)").hide();
        else $("#table td:nth-child(2), #table th:nth-child(2)").show();

        if(!data.options.crear) $('button.btn-accion[data-action="C"]').remove();
    }


    /** ACTION BUTTON */
    loadCity = function (element, valor, municipio) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/estados",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);

                    if(valor) element.trigger('change');
                    if(valor && municipio) loadTownByCity(valor, $("#operator_town"), municipio);
                }
            }
        });

        return;
    }

    loadTownByCity = function (estado, element, valor) {
        if(estado) {
            $.ajax({
                type: "GET",
                url: root_path + "APIs/catalogos.php/municipioByEstado",
                data: {
                    estado
                },
                success: function (response) {
                    if(response.item) {
                        element.html('<option></option>');
                        $.each(response.item, function (index, value) { 
                            const selected = (value.id == valor) ? 'selected' : '';
                            element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                        });
                        element.prop('disabled', false);
                        element.select2('val', valor);
                    }
                }
            });
        }
        else element.select2('val', '').html('').prop('disabled', true);

        return;
    }

    loadUsers = function (element, valor) {
        $.ajax({
            type: "GET",
            url: root_path + "APIs/catalogos.php/usuarios",
            success: function (response) {
                if(response.item) {
                    element.html('<option></option>');
                    $.each(response.item, function (index, value) { 
                        const selected = (value.id == valor) ? 'selected' : '';
                        element.append('<option value="' + value.id + '" ' + selected + '>' + value.nombre + '</option>');
                    });
                    element.select2('val', valor);
                }
            }
        });
    }

    $(document).on('click', '.btn-accion', function () {
        let action = $(this).data('action'),
        id = $(this).data('id');

        switch(action) {
            case 'C':
                limpiarModal();
                $("#modal-operator").modal('show', {backdrop: 'static'});
                $("#operator_action").val(action);
                loadCity($("#operator_city"));
                loadUsers($("#operator_user"));
                break;
        }

    });

    $("#operator_city").on('change', function () {
        loadTownByCity($(this).val(), $("#operator_town"));
    })

    limpiarModal = function () {
        $('#operator_form')[0].reset();
        $(".select2").select2('val', '');
        $("#operator_town").prop('disabled', true);
        $("#operator_form .preview_img").attr('src', '');
    }

    /** SAVE OPERATOR INFO */
    $("#btn_operator_save").on('click', function () {
        if(!$("#operator_form").valid()) return;

        const action = $("#operator_action").val(), 
        id = $("#operator_id").val();

        /** SAVE ALL ELEMENTS */
        let data = {}
        $('#operator_form input.form-control, #operator_form select.form-control, #operator_form textarea.form-control').filter(':input').each(function(){
            data[$(this).attr('name')] = $(this).val();
        });

        const owner = $('input[name="operator_owner"]').iCheck('update')[0].checked; 
        data['operator_owner'] = (owner) ? '1': '0';

        data['id'] = $("#operator_id").val();

        const formData = new FormData();
        formData.append('operator_picture', $("#operator_picture").prop('files')[0]);
        formData.append('data', JSON.stringify(data));

        var modalRespuesta = swal.fire({
            title: 'Procesando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                switch(action) {
                    case 'C':  
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/operator.php/save",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.code == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                        onClose: () => {
                                            $("#modal-operator").modal('hide');
                                            limpiarModal();
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: response.msg
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                        break;
                    case 'U':
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/operator.php/update_operator",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.code == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                        onClose: () => {
                                            $("#modal-operator").modal('hide');
                                            limpiarModal();
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: data.msg
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                        break;
                }
            }
        })
    });

    $(document).on('click', 'a.operator_edit', function () {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/load_info",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            modalRespuesta.closeModal();

                            $("#modal-operator").modal('show', {backdrop: 'static'});

                            const data = response.info;

                            limpiarModal();
                            loadCity($("#operator_city"), data.idEstado, data.idMunicipio);
                            loadUsers($("#operator_user"), data.idUsuario);

                            $("#operator_id").val(id);
                            $("#operator_action").val('U');

                            $("#operator_birth").val(data.cumpleahos);
                            $("#operator_name").val(data.nombre);
                            $("#operator_lastname").val(data.apellidos);
                            $("#operator_rfc").val(data.rfc);
                            $("#operator_direction").val(data.direccion);
                            $("#operator_gender").val(data.genero).trigger('change');
                            $("#operator_email").val(data.email);
                            $("#operator_phone").val(data.telefono);
                            $("#operator_emergencyphone").val(data.telefonoEmergencia);
                            $("#operator_emergencycontact").val(data.contactoEmergencia);
                            $("#operator_extension").val(data.extension);
                            $("#operator_account").val(data.cuenta);
                            $("#operator_license").val(data.licencia);
                            $("#operator_validlicense").val(data.licenciaVigencia);
                            $("#operator_active").val(data.operadorActivo).trigger('change');
                            $("#operator_comments").val(data.comentarios);
                            $("#operator_ine").val(data.noIFE);
                            $("#operator_commission").val(data.comision);
                            
                            if(data.owner == '1') $("#operator_owner_1").iCheck('check')
                            else $("#operator_owner_2").iCheck('check')

                            if(data.foto) $("#modal-operator .preview_img").attr('src', '../../' + data.foto + '?' + Math.random());
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'No se pudo obtener la información del usuario'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    })

    $(document).on('click', 'a.operator_view', function () {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading();
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/load_info",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            modalRespuesta.closeModal();

                            const data = response.info;

                            if(data.foto) $("#operator_view_image").attr('src', '../../' + data.foto + '?' + Math.random());
                            else $("#operator_view_image").attr('src', '../../img/user.png');

                            $("#operator_view_name").html(data.nombre + ' ' + data.apellidos);
                            $("#operator_view_travels").html(data.viajes);
                            $("#operator_view_address").html(data.direccion);
                            $("#operator_view_birth").html(data.cumpleahos);
                            $("#operator_view_email").html(data.email);
                            $("#operator_view_rfc").html(data.rfc);
                            $("#operator_view_gender").html(data.genero);
                            $("#operator_view_phone").html(data.telefono);
                            $("#operator_view_emercyphone").html(data.telefonoEmergencia);
                            $("#operator_view_emercycontact").html(data.contactoEmergencia);
                            $("#operator_view_extension").html(data.extension);
                            $("#operator_view_account").html(data.cuenta);
                            $("#operator_view_license").html(data.licencia);
                            $("#operator_view_validlicense").html(data.licenciaVigencia);
                            $("#operator_view_city").html(data.estado);
                            $("#operator_view_town").html(data.municipio);
                            $("#operator_view_user").html(data.username);
                            $("#operator_view_active").html((data.operadorActivo) ? 'SI' : 'NO');
                            $("#operator_view_comments").html(data.comentarios);
                            $("#operator_view_ine").html(data.noIFE);
                            $("#operator_view_comition").html(data.comision);
                            $("#operator_view_owner").html((data.owner) ? 'SI' : 'NO');

                            $("#modal-operator-view").modal('show', { backdrop: 'static' });

                            $("#operator_view_docs").html('');
                            if(response.doctos) {
                                
                                let doctos = '';
                                doctos += ' <div class="row">';
                                
                                doctos += '     <div class="col-md-12">';
                                doctos += '         <div class="alert alert-info"><strong>Documentación</strong></div>';
                                doctos += '     </div>';
                                doctos += ' </div>';
                                doctos += ' <div class="row">';
                                doctos += '     <div class="col-md-12">';
                                doctos += '         <form role="form" class="form-horizontal form-groups-bordered">';

                                $.each(response.doctos, function (index, value) { 

                                    doctos += '<div class="form-group">';
                                    doctos += ' <label for="" class="col-sm-5 control-label">' + value.documento + '</label>';
                                    doctos += ' <div class="col-sm-7">';

                                    if(value.docto) {
                                        doctos += '     <button type="button" class="btn btn-blue btn-sm view-doc" data-file="' + root_path + value.docto + '">Ver Documento</button>';
                                        doctos += '     <a class="btn btn-success btn-sm view-doc" href="' + root_path + value.docto + '" download>Descargar Documento</a>';
                                    }
                                    else doctos += '     <button type="button" class="btn btn-danger btn-sm">Sin Documento</button>';

                                    doctos += ' </div>';
                                    doctos += '</div>';
                                });


                                doctos += '         </form>';
                                doctos += '     </div>';
                                doctos += ' </div>';

                                $("#operator_view_docs").html(doctos);
                            }

                            $("#operador_vehiculos").html('');
                            let vehiculos = '<br />';
                            if(response.vehiculos.length > 0) {

                                vehiculos += '<div class="row">';
                                vehiculos += '  <div class="col-md-12">';
                                vehiculos += '      <div class="alert alert-info"><strong>Vehículos Asignados</strong></div>';
                                
                                vehiculos += '          <table class="table table-bordered responsive">';
                                vehiculos += '              <thead>';
                                vehiculos += '                  <tr>';
                                vehiculos += '                      <th>Lo Conduce</th>';
                                vehiculos += '                      <th>Es Dueño</th>';
                                vehiculos += '                      <th>Activo</th>';
                                vehiculos += '                      <th>Placas</th>';
                                vehiculos += '                      <th>Marca</th>';
                                vehiculos += '                      <th>Modelo</th>';
                                vehiculos += '                      <th>Año</th>';
                                vehiculos += '                      <th>Serie</th>';
                                vehiculos += '                  <tr>';
                                vehiculos += '              </thead>';
                                vehiculos += '              <tbody>';

                                $.each(response.vehiculos, function (indexInArray, valueOfElement) { 
                                    vehiculos += '<tr>';
                                    vehiculos += (valueOfElement.conduce == '1') ? '<td>SI</td>' : '<td>NO</td>';
                                    vehiculos += (valueOfElement.duenio == '1') ? '<td>SI</td>' : '<td>NO</td>';
                                    vehiculos += (valueOfElement.activo == '1') ? '<td>SI</td>' : '<td>NO</td>';
                                    vehiculos += '<td>' + valueOfElement.placas + '</td>';
                                    vehiculos += '<td>' + valueOfElement.marca + '</td>';
                                    vehiculos += '<td>' + valueOfElement.modelo + '</td>';
                                    vehiculos += '<td>' + valueOfElement.aho + '</td>';
                                    vehiculos += '<td>' + valueOfElement.serie + '</td>';
                                    vehiculos += '</tr>';
                                });

                                vehiculos += '              </tbody>';
                                vehiculos += '          </table>';

                                vehiculos += '  </div>';
                                vehiculos += '</div>';
                            }

                            $("#operador_vehiculos").html(vehiculos);
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'No se pudo obtener la información del usuario'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })

    });

    $(document).on('click', '.operator_disable', function () {
        const id = $(this).data('id'), 
        status = $(this).data('status'),
        text = $(this).text();

        swal.fire({
            title: text + ' operador',
            text: 'Va a ' + text + ' al operador, ¿Está seguro?',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#00a651' ,
            showCancelButton: true, 
            cancelButtonText: 'Cancelar', 
            cancelButtonColor: '#cc2424',
            allowOutsideClick: false,
            allowEnterKey: false,
        }).then((result) => {
            if(result.value) {

                var modalRespuesta = swal.fire({
                    title: 'Procesando...',
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        $.ajax({
                            type: "DELETE",
                            url: root_path + "APIs/operator.php/status",
                            data: {
                                id,
                                status
                            },
                            success: function (response) {
                                if(response.code == '200') {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se actualizó correctamente la información',
                                        onClose: () => {
                                            ActualizaTabla(); 
                                        }
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                })
            }
        });
    })

    /** EXPEDIENTE */
    $(document).on('click', 'a.operator_docs', function () {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Cargando...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/get_docs",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.doctos) {
                            let body = '';
                            $.each(response.doctos, function (index, value) { 

                                const file = (value.docto) ? '../../' + value.docto + '?' + Math.random() : '#';

                                body += '<div class="form-group">';
                                body += '    <label class="col-sm-4 control-label">' + value.documento + '</label>';
                                body += '    <div class="col-sm-4">';
                                body += '        <div class="input-group">';

                                const tipo_doc = 'application/pdf, image/png, image/jpg, image/jpeg';

                                body += '            <input type="file" name="operator_doc_' + value.idDocumento + '" id="operator_doc_' + value.idDocumento + '" class="noMostrar" accept="' + tipo_doc + '">';
                                body += '            <input type="text" name="operator_doc_' + value.idDocumento + '_label" id="operator_doc_' + value.idDocumento + '_label" class="form-control btn_file_trigger" readonly>';
                                body += '            <span class="input-group-btn">';
                                body += '                <button id="operator_doc_' + value.idDocumento + '_trigger" class="btn btn-primary btn_file_trigger" type="button">';
                                body += '                    Seleccionar';
                                body += '                    <i class="fa fa-file-o"></i>';
                                body += '                </button>';
                                body += '            </span>';
                                body += '            <span class="input-group-btn">';
                                body += '                <button class="btn btn-success btn_save_doc" data-type="' + value.main + '" data-name="' + value.codigo + '" type="button">Guardar</button>';
                                body += '            </span>';
                                body += '        </div>';
                                body += '    </div>';
                                body += '    <div class="col-sm-4">';

                                if(value.docto) {
                                    body += '     <button type="button" class="btn btn-blue btn-sm view-doc" data-file="' + root_path + value.docto + '">Ver Documento</button>';
                                    body += '     <a class="btn btn-success btn-sm view-doc" href="' + root_path + value.docto + '" download>Descargar Documento</a>';
                                }
                                else body += '     <button type="button" class="btn btn-danger btn-sm">Sin Documento</button>';

                                // if(value.idDocumento == 15) body += '        <button type="button" class="btn btn-primary btn-sm view-doc" data-file="'+ file +'">VER DOCUMENTO</button>';
                                // else body += '        <img class="preview_img" id="vehicle_picture_car_preview" src="' + file + '" alt="" width="140"/>';

                                body += '    </div>';
                                body += '</div>';
                            });

                            $("#operator_form_docs").html(body);
                            $("#operator_picture_id").val(id);

                            $("#modal-operator-docs").modal('show', { backdrop: 'static' });
                            
                            modalRespuesta.closeModal();
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    });

    $(document).on('click', '.btn_save_doc', function () {

        const docto = $(this).closest('div.form-group').find('input[type="file"]').attr('id'),
        file = $(this).closest('div.form-group').find('input[type="file"]').prop('files')[0],
        main = $(this).data('type'),
        codigo = $(this).data('name'),
        id = $("#operator_picture_id").val();

        if(!file) {
            swal.fire({
                title: 'Información',
                type: 'info',
                text: 'Ningun archivo a guardar'
            });

            return;
        }

        swal.fire({
            title: 'Guardar Documento',
            text: '¿Está seguro que desea guardar el documento?',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#00a651' ,
            showCancelButton: true, 
            cancelButtonText: 'Cancelar', 
            cancelButtonColor: '#cc2424',
            allowOutsideClick: false,
            allowEnterKey: false,
        }).then((result) => {
            if(result.value) {
                const formData = new FormData();
                formData.append('file', file);
                formData.append('id', id);
                formData.append('docto', docto);
                formData.append('main', main);
                formData.append('codigo', codigo);

                var modalRespuesta = swal.fire({
                    title: 'Procesando...', 
                    onBeforeOpen: () => {
                        swal.showLoading()
                    },
                    onOpen: () => {
                        $.ajax({
                            type: "POST",
                            url: root_path + "APIs/operator.php/save_doc",
                            data: formData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if(response.image) {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'success', 
                                        text: 'Se registró correctamente la información',
                                    })
                                }
                                else {
                                    swal.fire({
                                        title: 'Información', 
                                        type: 'warning', 
                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                    })
                                }
                            },
                            error: function () {
                                swal.fire({
                                    title: 'Información', 
                                    type: 'warning', 
                                    text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                })
                            }
                        });
                    }
                })
            }
        });
    })

    $(document).on('change', 'input.check_visible', function () {
        let estado = $(this).prop('checked'),
        id = $(this).data('id');

        estado = (estado) ? '1' : '0';

        var modalRespuesta = swal.fire({
            title: 'Guardando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "PUT",
                    url: root_path + "APIs/operator.php/update_visible",
                    data: {
                        estado,
                        id
                    },
                    success: function (response) {
                        if(response.code == 200) {
                            swal.fire({
                                title: 'Información',
                                text: 'Se guardo la información correctamente',
                                type: 'success'
                            });
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    });

    /** RECUPERACION DE CONTRASEÑA */
    $(document).on('click', '.operator_recuperacion', function () {
        const id = $(this).data('id');

        var modalRespuesta = swal.fire({
            title: 'Procesando...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/get_telefono",
                    data: {
                        id
                    },
                    success: function (response) {
                        if(response.info) {
                            swal.fire({
                                title: 'Recuperación de Contraseña',
                                html: '<div class="alert alert-info"><strong>Operador ' + response.info.nombre + ' ' + response.info.apellidos + '</strong></div>' + 
                                        '<div class="col-md-12">' + 
                                        '   <div class="form-group">' +
                                        '       <label class="control-label" for="operador_telefono">Teléfono Operador</label>' +
                                        '       <input type="text" maxlength="10" class="form-control" name="operador_telefono" id="operador_telefono" value="' + response.info.telefono + '">' +
                                        '   </div>' +
                                        '</div>',
                                focusConfirm: false,
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'Cancelar',
                                confirmButtonText: 'Aceptar',
                                showLoaderOnConfirm: true, 
                                onOpen: () => {
                                    $("#operador_telefono").focus();

                                    $('#operador_telefono').keypress(function(event){
                                        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                                            event.preventDefault(); //stop character from entering input
                                        }
                                 
                                    });
                                }, 
                                preConfirm: () => {
                                    let telefono = Swal.getPopup().querySelector('#operador_telefono').value;
                                    if(!telefono) { Swal.showValidationMessage('Debe ingresar el teléfono del operador'); return false; };
                                    if(telefono.length != 10) { Swal.showValidationMessage('El teléfono debe constar de 10 digitos'); return false; };

                                    return { telefono }
                                }
                            }).then((result) => {
                                resultado = result.value;
                                console.log(result);
                                if (result.value) {
                                    var modalRespuesta = swal.fire({
                                        title: 'Procesando...',
                                        onBeforeOpen: () => {
                                            swal.showLoading()
                                        },
                                        onOpen: () => {
                                            $.ajax({
                                                type: "POST",
                                                url: root_path + "APIs/operator.php/recupera_contra",
                                                data: {
                                                    id,
                                                    valor: resultado.telefono
                                                },
                                                success: function (response) {
                                                    if(response.code == 200) {
                                                        swal.fire({
                                                            title: 'Información', 
                                                            type: 'success', 
                                                            text: 'Se actualizó correctamente la información'
                                                        })
                                                    }
                                                    else {
                                                        swal.fire({
                                                            title: 'Información', 
                                                            type: 'warning', 
                                                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                                        })
                                                    }
                                                },
                                                error: function () {
                                                    swal.fire({
                                                        title: 'Información', 
                                                        type: 'warning', 
                                                        text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                                                    })
                                                }
                                            });
                                        }
                                    })
                                }
                            });
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'No se encontro la información del operador'
                            })
                        }
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    })

    /** ASIGNACIÓN DE VEHICULOS */
    $(document).on('click', 'a.operator_vehiculos', function () {
        const id = $(this).data('id');

        obtieneVehiculos(id);
    })

    obtieneVehiculos = (id) => {
        var modalRespuesta = swal.fire({
            title: 'Cargando Información...',
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
                $.ajax({
                    type: "GET",
                    url: root_path + "APIs/operator.php/get_vehiculos",
                    data: {
                        id
                    },
                    success: function (response) {
                        
                        let html = '<table class="table table-bordered responsive" id="table_vehiculos">';
                        html += '   <thead>';
                        html += '       <tr>';
                        html += '           <th>Activo</th>';
                        html += '           <th>Placas</th>';
                        html += '           <th>Marca</th>';
                        html += '           <th>Modelo</th>';
                        html += '           <th>Año</th>';
                        html += '           <th>Serie</th>';
                        html += '       <tr>';
                        html += '   </thead>';
                        html += '   <tbody>';

                        $.each(response.info, function (index, value) { 
                            html += '<tr>';

                            $activo = (value.activo == '1') ? 'checked' : '';

                            html += '<td><input class="activa_vehiculo" type="checkbox" ' + $activo + ' data-vehiculo="' + value.idVehiculo + '" data-operador="' + id + '"></td>';
                            html += '<td>' + value.placas + '</td>';
                            html += '<td>' + value.marca + '</td>';
                            html += '<td>' + value.modelo + '</td>';
                            html += '<td>' + value.aho + '</td>';
                            html += '<td>' + value.serie + '</td>';
                            html += '</tr>';
                        });

                        html += '   </tbody>';
                        html += '</table>';

                        html += '<div class="alert alert-info"><strong>Asignar Nuevo Vehículo</strong></div>';

                        html += '<div class="row">';
                        html += '   <div class="col-md-12">';
                        html += '       <div class="input-group">';
                        html += '           <input type="text" class="form-control" placeholder="Placas" id="valor_buscar_placas">';
                        html += '           <span class="input-group-btn">';
                        html += '               <button class="btn btn-primary" id="btn_buscar_placas" type="button">Buscar</button>';
                        html += '           </span>';
                        html += '       </div>'
                        html += '   </div>';
                        html += '</div>';
                        html += '<div id="nuevo_buscar_placas">';
                        html += '</div>';

                        swal.fire({
                            title: 'Vehículos Asignados', 
                            html: html,
                            confirmButtonText: 'Cerrar',
                            width: '80%',
                            onOpen: () => {
                                Swal.getPopup().querySelector('#btn_buscar_placas').addEventListener('click', function(evt) {
                                    let placas = Swal.getPopup().querySelector('#valor_buscar_placas').value;
                                    if(!placas) { Swal.showValidationMessage('Debe ingresar el número de placa'); return false; };

                                    Swal.resetValidationMessage();
                                    $.ajax({
                                        type: "GET",
                                        url: root_path + "APIs/operator.php/get_vehiculo",
                                        data: {
                                            placas,
                                            id
                                        },
                                        success: function (response) {
                                            if(response.code == 401) { Swal.showValidationMessage('El vehículo ya fue asignado al operador'); Swal.getPopup().querySelector('#nuevo_buscar_placas').innerHTML = ''; return false; }
                                            else if(response.code == 200) {
                                                let html = '<input type="hidden" id="guardar_vehiculo_buscar_placas" value="' + response.info.idVehiculo + '">';
                                                html += '<table class="table table-bordered responsive">';
                                                html += '   <thead>';
                                                html += '       <tr>';
                                                html += '           <th>Activo</th>';
                                                html += '           <th>Placas</th>';
                                                html += '           <th>Marca</th>';
                                                html += '           <th>Modelo</th>';
                                                html += '           <th>Año</th>';
                                                html += '           <th>Serie</th>';
                                                html += '       <tr>';
                                                html += '   </thead>';
                                                html += '   <tbody>';
                                                html += '       </tr>';
                                                html += '           <td><button class="btn btn-success btn-sm" id="guardar_buscar_placas">Guardar</button></td>';
                                                html += '           <td>' + response.info.placas + '</td>';
                                                html += '           <td>' + response.info.marca + '</td>';
                                                html += '           <td>' + response.info.modelo + '</td>';
                                                html += '           <td>' + response.info.aho + '</td>';
                                                html += '           <td>' + response.info.serie + '</td>';
                                                html += '       </tr>';
                                                html += '   </tbody>';
                                                html += '</table>';

                                                Swal.getPopup().querySelector('#nuevo_buscar_placas').innerHTML = html;

                                                Swal.getPopup().querySelector('#guardar_buscar_placas').addEventListener('click', function(evt) {
                                                    let vehiculo = Swal.getPopup().querySelector("#guardar_vehiculo_buscar_placas").value;

                                                    Swal.showLoading();
                
                                                    $.ajax({
                                                        type: "POST",
                                                        url: root_path + "APIs/operator.php/save_nuevo_vehiculo",
                                                        data: {
                                                            vehiculo, 
                                                            id
                                                        },
                                                        success: function (response) {
                                                            if(response.code == '200') { toastr.success("Vehículo Registrado Correctamente", '', { clearOnNew: true, maxOpened: 2 }); obtieneVehiculos(id); }
                                                            else toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                                        },
                                                        error: function () {
                                                            toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                                        }
                                                    });
                                                })
                                            }
                                            else toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                        },
                                        error: function () {
                                            toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
                                        }
                                    });
                                });
                            }
                        });
                        
                    },
                    error: function () {
                        swal.fire({
                            title: 'Información', 
                            type: 'warning', 
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                        })
                    }
                });
            }
        })
    }

    $(document).on('click', '.activa_vehiculo', function () {
        estado = ($(this).prop('checked')) ? 1 : 0, 
        vehiculo = $(this).data('vehiculo'), 
        operador = $(this).data('operador');

        $.ajax({
            type: "POST",
            url: root_path + "APIs/operator.php/activa_vehiculo",
            data: {
                estado,
                vehiculo,
                operador
            },
            success: function (response) {
                if(response.code == '200') { toastr.success("Estado actualizado correctamente", '', { clearOnNew: true, maxOpened: 2 }); obtieneVehiculos(operador); }
                else toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
            },
            error: function () {
                toastr.error("Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico");
            }
        });
    })

    $(document).on('click', '.view-doc', function () {
        let file = $(this).attr('data-file'),
        extension = file.split('.').pop();

        if(file) {
            if(extension.toUpperCase() == 'PDF') {
                var html = ''; 
                html += '<div class="row">'; 
                html += '	<div class="col-md-12">'; 
                html += '		<div id="view-doc-div">'; 
                html += '		</div>'; 
                html += '	</div>'; 
                html += '</div>'; 
    
                swal.fire({
                    title: 'Documento Cargado', 
                    html: html,
                    width: '80%',
                    onOpen: () => {
                        PDFObject.embed(file + '?f=' + $.now(), '#view-doc-div' , {height: "800px"}); 
                    }
                })
            }
            else if (extension.match(/(jpg|jpeg|png|gif|tiff)$/i)) {
                let html = '<img src="' + file + '" class="" style="width: 100%;" >';

                swal.fire({
                    html: html, 
                    width: '50%'
                })
            }
        }
        else {
            swal.fire({
                title: 'Información', 
                type: 'info', 
                text: 'No existe documento cargado'
            }); 
        }
    });

    $(document).on('click', '.view_image', function () {

        let html = '<img src="' + $(this).attr('src') + '" class="" style="width: 100%;" >';

        swal.fire({
            html: html, 
            width: '50%'
        })
    });
})