<?php 
    include('bar.php');
?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="search_zona">Zona</label>
                <select name="search_zona" id="search_zona" class="form-control select2">
                </select>
            </div>
        </div>
    </div>

    <div class="show_menu" id="show_menu">
        
    </div>
    
    <script src="js/menu.js?i=<?php print(rand()); ?>"></script>
    <link rel="stylesheet" href="css/menu.css?i=<?php print(rand()); ?>">

<?php
    include('footer.php');
?>