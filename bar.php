<?php 
    include("headers.php");

    if(empty($_SESSION['LOGGED'])) { ?> <script>window.location.replace(<?php print('\'' . ROOT_PATH . 'index.php\''); ?>);</script> <?php }

    require_once("APIs/conexion.php");
    include("APIs/helpers/permisos.php");
    $permisos = new Permisos($conectar);
?>

<script>
    $('body').removeClass('login-page login-form-fall').addClass('skin-yellow page-left-in');
</script>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	<div class="sidebar-menu">
		<header class="logo-env">
			
			<!-- logo -->
			<div class="logo">
				<a href="<?php print(ROOT_PATH) ?>menu.php">
					<img src="<?php print(ROOT_PATH) ?>img/logo.png" width="120" alt="" />
				</a>
			</div>
			
            <!-- logo collapse icon -->			
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>					
			
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>	
		
				
		<ul id="main-menu" class="">
        <?php 

            // Obtenemos los menus que el usuario tendrá
            $consulta = "SELECT
                        M.id as menuid,
                        P.group_id, 
                        P.description as perfildescripcion,
                        M.clave, 
                        M.pagina, 
                        M.descripcion as menudescripcion, 
                        M.padre,
                        M.icono
                        FROM groupmenus PM, tb_groups P, menus M
                        WHERE PM.perfil = :groupid
                        AND P.group_id = PM.perfil 
                        AND M.clave = PM.menu
                        AND PM.activo = '1'
                        AND M.principal = '1'
                        ORDER BY M.orden ASC"; 

            $consulta = $conectar->prepare($consulta); 
            $consulta->bindParam(':groupid', $_SESSION['GROUP_ID']);
            $consulta->execute(); 
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($datos as $dato) {
                // Obtenemos si es una liga padre
                $consulta = "SELECT
                            P.group_id, 
                            P.description as perfildescripcion,
                            M.clave, 
                            M.pagina, 
                            M.descripcion as menudescripcion, 
                            M.padre,
                            M.icono
                            FROM groupmenus PM, tb_groups P, menus M
                            WHERE PM.perfil = :groupid
                            AND P.group_id = PM.perfil 
                            AND M.clave = PM.menu
                            AND PM.activo = '1'
                            AND M.padre = :clave
                            ORDER BY M.orden ASC";

                $consulta = $conectar->prepare($consulta); 
                $consulta->bindParam(':groupid', $_SESSION['GROUP_ID']);
                $consulta->bindParam(':clave', $dato['clave']);
                $consulta->execute(); 
                $hijos = $consulta->fetchAll(PDO::FETCH_ASSOC);

                if(count($hijos) == 0) {
                    ?>
                        <li id="menu-<?php print($dato['clave']) ?>">
                            <a class="item" href="<?php (!empty($dato['pagina'])) ? print(ROOT_PATH . $dato['pagina']) : print(ROOT_PATH . 'menu.php') ?>">
                                <i class="<?php print($dato['icono']) ?>"></i>
                                <span class="title"><?php print($dato['menudescripcion']) ?></span>
                            </a>
                        </li>
                    <?php
                }
                else {
                    ?>
                        <li id="menu-<?php print($dato['clave']) ?>">
                            <a class="item">
                                <i class="fa <?php print($dato['icono']) ?>"></i>
                                <span class="title"><?php print($dato['menudescripcion']) ?></span>
                            </a>
                            <ul>
                    <?php 
                                foreach($hijos as $hijo) {
                    ?>
                                <li class="">
                                    <a class="item" href="<?php print(ROOT_PATH) ?><?php print($hijo['pagina']) ?>">
                                        <i class="fa <?php print($hijo['icono']) ?>"></i>
                                        <span class="title"><?php print($hijo['menudescripcion']) ?></span>
                                    </a>
                                </li>
                    <?php
                                }
                    ?>
                            </ul>
                        </li>
                    <?php
                }
            }
            ?>
		</ul>
				
	</div>	
    <div class="main-content">
        <div class="row">				
            <!-- Profile Info and Notifications -->
            <div class="col-md-6 col-sm-8 clearfix">
                <ul class="user-info pull-left pull-none-xsm">

                    <!-- Profile Info -->
                    <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php print(ROOT_PATH) ?>img/149071.svg" alt="" class="img-circle" width="75" />
							<?php print($_SESSION['NAME']) ?><br><span class="lblPerfilUserTop"></span>
						</a>
                        <ul class="dropdown-menu">
                
                            <!-- Reverse Caret -->
                            <li class="caret"></li>

                            <!-- Profile sub-links -->
                            <!-- <li>
                                <a onclick="ActualizaPassword();" href="#">
                                    <i class="entypo-key"></i>
                                    Cambie su contraseña
                                </a>
                            </li> -->
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-4 clearfix">
				<ul class="list-inline links-list pull-right">
					<!-- <li>
						<a target="blank" href="<?php print(ROOT_PATH) ?>ManualProveedores.pdf">
							Ayuda <i class="entypo-help"></i>
						</a>
					</li> -->

					<li class="sep"></li>
					<li>
						<a href="<?php print(ROOT_PATH) ?>destruir.php">
							Cerrar Sesión <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
			</div>
        </div>
		<hr />