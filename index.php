<?php 
    include("headers.php");
?>

<script>
    $('body').addClass('login-page login-form-fall');
</script>

	<div class="login-container" style="background-color: #383838;">
		<div class="login-header login-caret" style="background-color: #fcc125;">
			<div class="login-content">	
				<a href="#" class="logo">
					<img src="<?php print(ROOT_PATH) ?>img/mip.png" width="180" alt="" />
				</a>
				<p class="description">Bienvenido</p>
				<!-- progress bar indicator -->
				<div class="login-progressbar-indicator">
					<h3>43%</h3>
					<span>logging in...</span>
				</div>
			</div>
		</div>

		<div class="login-progressbar">
			<div></div>
		</div>

		<div class="login-form">
			<div class="login-content">
				<form method="post" role="form" id="form_login">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-user"></i>
							</div>
							<input type="text" class="form-control" name="username" id="username" placeholder="Correo Electrónico" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>
							<input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<button id="btnLogin" type="submit" class="btn btn-primary btn-block btn-login">
							<i class="entypo-login"></i>
							Iniciar Sesión
						</button>
					</div>			
				</form>
				<div class="login-bottom-links">
					<br />
					<a href="extra-forgot-password.html" class="link">¿Olvidaste tu Contraseña?</a>
					<br />
				</div>
			</div>		
		</div>
	</div>


	<script src="<?php print(ROOT_PATH) ?>js/login.js?i=<?php print(rand()) ?>"></script>

<?php 
    include('footer.php');
?>