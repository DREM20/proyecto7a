<?php
    include_once('dirs.php');
	session_start();
?>

<!DOCTYPE html>
<html lang="es">
<head>

    <!-- Definimos todos los meta a utilizar -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Yabü México">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Yabü México</title>

    <link rel="icon" type="image/x-icon" href="<?php print_r(ROOT_PATH) ?>img/favicon.ico">

	<script src="<?php print(ROOT_PATH) ?>js/pdfobject.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>js/chat.js"></script>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

	<!-- Estilos de NEON -->
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<!-- <link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/font-icons/font-awesome/css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/custom.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/skins/yellow.css">

	<!-- SKINS -->
	<!-- <link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/css/skins/facebook.css"> -->

	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link href="<?php print(ROOT_PATH) ?>css/style.css" rel="stylesheet">
	<link href="<?php print(ROOT_PATH) ?>css/animate.css" rel="stylesheet">
	<script src="<?php print(ROOT_PATH) ?>js/jquery-3.2.1.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>js/jquery-1.12.4.js" ></script>
	<script src="<?php print(ROOT_PATH) ?>js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php print(ROOT_PATH) ?>js/validaciones.js"></script>
	<script src="<?php print(ROOT_PATH) ?>js/fontawesome.js"></script>

	<!-- sweetAlert -->
	<script src="<?php print(ROOT_PATH) ?>assets/sweetalert/src/SweetAlert.js"></script>
	<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>assets/sweetalert/dist/sweetalert2.min.css">


	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>assets/fancybox/dist/jquery.fancybox.min.css" />
	<script src="<?php print(ROOT_PATH) ?>assets/fancybox/dist/jquery.fancybox.min.js"></script>

	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>css/styles.css?i=<?php print(rand()) ?>">

    <script>
        $(document).ready(function () {
            $("#loading").fadeOut();
        }); 

		let root_path = <?php print('"' . ROOT_PATH . '"') ?>; 
    </script>

</head>
<body class="page-body" data-url="http://neon.dev">
    <div id="loading">
		<img id="loading-image" src="<?php print(ROOT_PATH) ?>img/logo.png" />
		<div class="spinnerWrapper">
			<div class="spinnerOuter"></div>
			<div class="spinnerMiddle"></div>
		</div>
	</div>