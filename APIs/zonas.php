<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("../assets/Slim/Slim.php");

    require_once('config.php');

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/tbody', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        /** OBTENEMOS LAS ZONAS DE MONGODB */
        $curl = curl_init();

        $info = json_encode(array('idEstado' => '', 'idMunicipio' => ''), JSON_UNESCAPED_SLASHES);

        curl_setopt_array($curl, array(
            CURLOPT_URL => API . "/admin/api/v3/ver_config_zona",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
            CURLOPT_POSTFIELDS => $info,
        ));

        $zonas = curl_exec($curl);
        if(!is_array($zonas)) $zonas = json_decode($zonas, true);

        curl_close($curl);

        $final_data = array();
        if(isset($zonas['data']['data'])) {
            foreach($zonas['data']['data'] as $clave => $zona) {
                $final_data[] = array(
                    'id' => $zona['idZona'],
                    'zona' => $zona['zona'],
                    'estado' => '',
                    'municipio' => '',
                    'costo_membresia' => '',
                    'idEstado' => $zona['idEstado'],
                    'idMunicipio' => $zona['idMunicipio'],
                    'estatus' => $zona['estatus'],
                    'mongo' => true,
                    'mysql' => false
                );
            }
        }

        $consulta = "SELECT
                        Z.id,
                        Z.zona,
                        E.estado,
                        M.municipio,
                        Z.costo_membresia,
                        Z.idEstado, 
                        Z.idMunicipio,
                        false AS estatus
                    FROM
                        zonas Z
                        LEFT JOIN municipio M ON M.idMunicipio = Z.idMunicipio 
                        AND M.idEstado = Z.idEstado
                        LEFT JOIN estado E ON E.idEstado = Z.idEstado;";

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $key => $dato) {
            $clave = array_search($dato['zona'], array_column($final_data, 'zona'));

            if(is_numeric($clave)) {
                $final_data[$clave]['estado'] = $dato['estado'];
                $final_data[$clave]['municipio'] = $dato['municipio'];
                $final_data[$clave]['costo_membresia'] = $dato['costo_membresia'];
                $final_data[$clave]['mysql'] = true;
            }
            else {
                $final_data[] = array(
                    'id' => $dato['id'],
                    'zona' => $dato['zona'],
                    'estado' => $dato['estado'],
                    'municipio' => $dato['municipio'],
                    'costo_membresia' => $dato['costo_membresia'],
                    'idEstado' => $dato['idEstado'],
                    'idMunicipio' => $dato['idMunicipio'],
                    'estatus' => filter_var($dato['estatus'], FILTER_VALIDATE_BOOLEAN),
                    'mongo' => false,
                    'mysql' => true
                );
            }
        }

        $response['info'] = $final_data;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/search_map_location', function() use ($app) {
        $response = array();

        $municipio = $app->request->get('municipio');
        $estado = $app->request->get('estado');

        $url = str_replace (' ', '%20', "https://maps.googleapis.com/maps/api/place/textsearch/json?query=$municipio,$estado&key=" . MAPS_KEY);

        // create curl resource
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $output = json_decode($output, true);

        if(isset($output['results'][0]['geometry']['location']['lat']) && isset($output['results'][0]['geometry']['location']['lng'])) 
            $response = array(
                'lat' => $output['results'][0]['geometry']['location']['lat'],
                'lng' => $output['results'][0]['geometry']['location']['lng']
            );

        curl_close($ch);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/existe_zona', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $municipio = $app->request->get('municipio');
        $estado = $app->request->get('estado');

        $consulta = "SELECT id FROM zonas WHERE idEstado = :estado AND idMunicipio = :municipio";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':estado', $estado);
        $consulta->bindValue(':municipio', $municipio);
        $consulta->execute();

        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);

        if(count($resultado) == 0) $response = array('code' => 200, 'notif' => "No exite la zona");
        else $response = array('code' => 500, 'notif' => "Ya existe una zona con ese municipio y estado");

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/buscar_cobertura', function() use ($app) {
        $response = array();

        $municipio = $app->request->get('municipio');

        $archivo_cobetura = '../assets/Cobertura.kml';
        if(!file_exists($archivo_cobetura)) $response = array('code' => 500, 'notif' => 'No se encontro el documento KML');
        else {
            $doc = new DOMDocument();
            $doc->load($archivo_cobetura);

            $response = array('code' => 500, 'notif' => "No se encontro cobertura en $municipio, actualice el documento KML");
            foreach($doc->getElementsByTagName('Placemark') as $place) {
                $nombre = $place->getElementsByTagName('name');
                if($nombre->length == 0) continue;

                $nombre = $nombre->item(0)->nodeValue;
                if(strtoupper($municipio) == strtoupper($nombre)) $response = array('code' => 200, 'notif' => "Existe Cobertura");
            }
        }

        $app->response->setBody(json_encode($response));
    });

    $app->post('/crear_zona', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $parametros = $app->request()->params();

        /** OBTENEMOS LA COBERTURA DEL MUNICIPIO */
        $coordenadas = array();
        $archivo_cobetura = '../assets/Cobertura.kml';
        if(file_exists($archivo_cobetura)) {
            $doc = new DOMDocument();
            $doc->load($archivo_cobetura);

            foreach($doc->getElementsByTagName('Placemark') as $place) {
                $nombre = $place->getElementsByTagName('name');
                if($nombre->length == 0) continue;

                $nombre = $nombre->item(0)->nodeValue;
                if(strtoupper($parametros['zona_name']) == strtoupper($nombre)) {
                    foreach($place->getElementsByTagName('coordinates') as $coord) {
                        $valores = explode(' ', $coord->nodeValue);
                        foreach($valores as $info) {
                            if(count(explode(',', $info)) < 2) continue;
                            list($lng, $lat) = explode(',', $info);
                            $coordenadas[] = array(floatval(trim($lng)), floatval(trim($lat)));
                        }
                    }
                }
            }
        }

        /** INSERTAMOS LA ZONA EN BD */
        $consulta = "INSERT INTO zonas (id, zona, idEstado, idMunicipio, costo_membresia, iva, porcentaje, gobierno, latitud, longitud)
                    VALUES (DEFAULT, :zona, :estado, :municipio, :costo, :iva, :porcentaje, :gobierno, :latitud, :longitud)";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':zona',           (isset($parametros['config_zona_nombre'])) ? $parametros['config_zona_nombre'] : '');
        $consulta->bindValue(':estado',         (isset($parametros['config_zona_estado'])) ? $parametros['config_zona_estado'] : '');
        $consulta->bindValue(':municipio',      (isset($parametros['config_zona_municipio'])) ? $parametros['config_zona_municipio'] : '');
        $consulta->bindValue(':costo',          (isset($parametros['config_zona_costo'])) ? $parametros['config_zona_costo'] : '');
        $consulta->bindValue(':iva',            (isset($parametros['config_zona_iva'])) ? floatval($parametros['config_zona_iva']) / 100 : '');
        $consulta->bindValue(':porcentaje',     (isset($parametros['config_zona_porcentaje'])) ? floatval($parametros['config_zona_porcentaje']) / 100 : '');
        $consulta->bindValue(':gobierno',       (isset($parametros['config_zona_gobierno'])) ? floatval($parametros['config_zona_gobierno']) / 100 : '');
        $consulta->bindValue(':latitud',        (isset($parametros['config_zona_latitud'])) ? floatval($parametros['config_zona_latitud']) : '');
        $consulta->bindValue(':longitud',       (isset($parametros['config_zona_longitud'])) ? floatval($parametros['config_zona_longitud']) : '');

        if($consulta->execute()) {
            $id = $conectar->lastInsertId();
            $datos = crear_obj_zona($id, $parametros, $coordenadas);
            
            /** SE ENVIA LA INFORMACION AL SERVER */
            if(!empty($datos)) {
                $curl = curl_init();

                $info = json_encode(array('objconfig' => $datos), JSON_UNESCAPED_SLASHES);

                curl_setopt_array($curl, array(
                    CURLOPT_URL => API . "/admin/api/v3/config_zona",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
                    CURLOPT_POSTFIELDS => $info,
                ));

                $output = curl_exec($curl);

                curl_close($curl);

                if(!is_array($output)) $output = json_decode($output, true);
                if(!isset($output['data'])) $response = array('code' => 500, 'notif' => "No fue posible guardar la información, comuníquese con el área de soporte");
                else {
                    if($output['data'] == true) $response = array('code' => 200, 'notif' => "Se guardo correctamente la información");
                    else $response = array('code' => 500, 'notif' => "No fue posible guardar la información, comuníquese con el área de soporte");
                }

                if(!is_dir('../assets/coberturas/')) mkdir('../assets/coberturas/');
                file_put_contents('../assets/coberturas/' . $parametros['zona_name'] . '.json', $info);
            }
            else $response = array('code' => 500, 'notif' => "No fue posible guardar la información, comuníquese con el área de soporte");
        }
        else $response = array('code' => 500, 'notif' => "No fue posible guardar la información, comuníquese con el área de soporte");

        $app->response->setBody(json_encode($response));
    });

    $app->post('/actualizar_zona', function() use ($app) {
        $response = array();

        $parametros = $app->request()->params();

        $curl = curl_init();

        $info = array(
            'objconfig' => array(
                'estatus' => filter_var($parametros['estatus'], FILTER_VALIDATE_BOOLEAN)
            ),
            'id' => $parametros['id']
        );

        $info = json_encode($info, JSON_UNESCAPED_SLASHES);

        curl_setopt_array($curl, array(
            CURLOPT_URL => API . "/admin/api/v3/config_zona",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
            CURLOPT_POSTFIELDS => $info,
        ));

        $output = curl_exec($curl);

        print_r($output);

        curl_close($curl);

        $app->response->setBody(json_encode($response));
    });

    function crear_obj_zona($id = null, $parametros = array(), $coordenadas = array()) {
        $datos = array(
            "zona"              => (isset($parametros['config_zona_nombre'])) ? $parametros['config_zona_nombre'] : '',
            "idZona"            => intval($id), 
            "estatus"           => true, 
            "idEstado"          => (isset($parametros['config_zona_estado'])) ? intval($parametros['config_zona_estado']) : '',
            "idMunicipio"       => (isset($parametros['config_zona_municipio'])) ? intval($parametros['config_zona_municipio']) : '',
            "configuracion"     => array(
                "token" => "",
                "pilotoAutomatico" => (isset($parametros['config_zona_automatico'])) ? filter_var($parametros['config_zona_automatico'], FILTER_VALIDATE_BOOLEAN) : false,
                "servicios" => array(
                    "taxi"      => (isset($parametros['config_zona_taxi'])) ? filter_var($parametros['config_zona_taxi'], FILTER_VALIDATE_BOOLEAN) : false,
                    "ejecutivo" => (isset($parametros['config_zona_ejecutivo'])) ? filter_var($parametros['config_zona_ejecutivo'], FILTER_VALIDATE_BOOLEAN) : false,
                    "van"       => (isset($parametros['config_zona_van'])) ? filter_var($parametros['config_zona_van'], FILTER_VALIDATE_BOOLEAN) : false,
                    "sprinter"  => (isset($parametros['config_zona_sprinter'])) ? filter_var($parametros['config_zona_sprinter'], FILTER_VALIDATE_BOOLEAN) : false,
                    "paquete"   => (isset($parametros['config_zona_paquete'])) ? filter_var($parametros['config_zona_paquete'], FILTER_VALIDATE_BOOLEAN) : false,
                ),
                "tarifa" => array(
                    "km"                => (isset($parametros['config_zona_kilometro'])) ? floatval($parametros['config_zona_kilometro']) : '',
                    "min"               => (isset($parametros['config_zona_minuto'])) ? floatval($parametros['config_zona_minuto']) : '',
                    "costoBanderazo"    => (isset($parametros['config_zona_banderazo'])) ? floatval($parametros['config_zona_banderazo']) : '',
                    "costoBase"         => (isset($parametros['config_zona_costobase'])) ? floatval($parametros['config_zona_costobase']) : '',
                    "perceSolicitud"    => (isset($parametros['config_zona_percesolicitud'])) ? floatval($parametros['config_zona_percesolicitud']) / 100 : '',
                    "dinamica"          => (isset($parametros['config_zona_dinamica'])) ? floatval($parametros['config_zona_dinamica']) / 100 : '',
                    "costoTarifaMaxima" => (isset($parametros['config_zona_tarifamaxima'])) ? intval($parametros['config_zona_tarifamaxima']) : '',
                    "radioBusqueda"     => (isset($parametros['config_zona_radioBusqueda'])) ? intval($parametros['config_zona_radioBusqueda']) : '',
                    "radioTarifaMinima" => (isset($parametros['config_zona_radioTarifaMinima'])) ? intval($parametros['config_zona_radioTarifaMinima']) : '',
                    "radioTarifaMaxima" => (isset($parametros['config_zona_radioTarifaMaxima'])) ? intval($parametros['config_zona_radioTarifaMaxima']) : '',
                    "tiempoEsperaMin"   => (isset($parametros['config_zona_tiempoEsperaMin'])) ? intval($parametros['config_zona_tiempoEsperaMin']) : '',
                    "impuestoGobierno"  => (isset($parametros['config_zona_gobierno'])) ? floatval($parametros['config_zona_gobierno']) / 100 : 0,
                    "economico"         => (isset($parametros['config_zona_economico'])) ? floatval($parametros['config_zona_economico']) : '',
                    "iva"               => (isset($parametros['config_zona_configiva'])) ? floatval($parametros['config_zona_configiva']) / 100 : 0,
                    "perceVan"          => (isset($parametros['config_zona_perceVan'])) ? intval($parametros['config_zona_perceVan']) : 0,
                    "perceSprint"       => (isset($parametros['config_zona_perceSprint'])) ? intval($parametros['config_zona_perceSprint']) : '',
                ),
                "formasPagoUsuario" => array(
                    "efectivo"          => (isset($parametros['config_zona_usuarioefectivo'])) ? filter_var($parametros['config_zona_usuarioefectivo'] , FILTER_VALIDATE_BOOLEAN): false,
                    "tarjeta"           => (isset($parametros['config_zona_usuariotarjeta'])) ? filter_var($parametros['config_zona_usuariotarjeta'], FILTER_VALIDATE_BOOLEAN) : false
                ),
                "formasPagoOperador" => array(
                    "efectivo"          => (isset($parametros['config_zona_operadorefectivo'])) ? filter_var($parametros['config_zona_operadorefectivo'], FILTER_VALIDATE_BOOLEAN): false,
                    "tarjeta"           => (isset($parametros['config_zona_operadortarjeta'])) ? filter_var($parametros['config_zona_operadortarjeta'], FILTER_VALIDATE_BOOLEAN): false,
                    "oxxoPay"           => (isset($parametros['config_zona_operadoroxxo'])) ? filter_var($parametros['config_zona_operadoroxxo'], FILTER_VALIDATE_BOOLEAN) : false
                ),
                "validacionUsuarios" => array(
                    "codigoMail"        => (isset($parametros['config_zona_codigomail'])) ? filter_var($parametros['config_zona_codigomail'], FILTER_VALIDATE_BOOLEAN) : false
                ),
                "styleMapa" => array(
                    "nocturno" => false,
                    "dia" => false
                ),
                "styleVehiculos" => array(
                    "Septiembre" => false,
                    "Octubre" => false,
                    "Noviembre" => false,
                    "Diciembre" => false,
                    "Enero" => false
                ),
                "enRutamiento" => array(
                    "Switch" => false,
                    "APIs" => "http://127.0.0.1:8080/APIs/",
                    "Socket" => "http://127.0.0:5030"
                )
            ),
            "geometry" => array(
                "coordinates" => array($coordenadas),
                "type" => "Polygon"
            )
        );

        return $datos;
    }

    $app->get('/get_kml', function() use ($app) {
        $response = array();

        $archivo_cobetura = '../assets/Cobertura.kml';
        if(file_exists($archivo_cobetura)) $response = array('code' => 200, 'notif' => 'Documento Existe', 'ruta' => 'assets/Cobertura.kml');
        else $response = array('code' => 500, 'notif' => 'Documento No Existe');

        $app->response->setBody(json_encode($response));
    });

    $app->post('/procesa_kml', function() use ($app) {
        $response = array();

        $documento = $_FILES['kml'] ?? '';

        if(empty($documento)) $response = array('code' => 500, 'notif' => 'No se pudo cargar el documento, intentelo más tarde, si el error persiste comuniquese con soporte técnico');
        else {
            if(move_uploaded_file($documento['tmp_name'], '../assets/Cobertura.kml')) $response = array('code' => 200, 'notif' => 'El archivo fue cargado exitosamente');
            else $response = array('code' => 500, 'notif' => 'No se pudo cargar el documento, intentelo más tarde, si el error persiste comuniquese con soporte técnico');
        }

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>