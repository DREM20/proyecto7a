<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("libs/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/cambiarpwd',function() use ($app) {
        require "conexion.php";
        $response = array();
        $correo = $app->request->get('correo');
        $contra = $app->request->get('contra');
    
        $rs = mysqli_query($conectar,"UPDATE usuario SET password = MD5('".$contra."') WHERE usuario = '".$correo."'");
    
        ($rs) ? $response['estatus']='OK' : $response['estatus']='Error al guardar';

        if(isset($conectar)) {
            mysqli_close($conectar);
        }
    
        $app->response->setBody(json_encode($response));
    });

    $app->post('/cambiocontra', function() use ($app) {
        session_start();
        require "conexion.php";
        $response = array();
        
        $usuario = $app->request->post('usuario');
        $codigo = $app->request->post('c');
        $contra = $app->request->post('contra');
        $contraR = $app->request->post('contraR');
        
        try {
            if($contra == $contraR) {
                $row = mysqli_fetch_row(mysqli_query($conectar, "SELECT password FROM usuario WHERE usuario = '".$usuario."' and usuario_id = '".$codigo."'"));
                if(md5($contra) != $row[0]) {
                    if(mysqli_query($conectar, "UPDATE usuario SET password = MD5('$contra') WHERE usuario = '".$usuario."' AND usuario_id = '".$codigo."'")) {
                        $response['msg'] = 'ok';
                        $response['notif'] = 'Lo cambios fueron guardados correctamente';
                        $_SESSION['restablecer'] = false;
                    }
                }
                else {
                    $response['msg'] = 'error';
                    $response['notif'] = 'No puede registrar su contraseña actual';
                }
            }
            else {
                $response['msg'] = 'error';
                $response['notif'] = 'La contraseñas no coinciden';
            }
        }
        catch(Exception $e)
        {
            $response['msg'] = 'error';
            $response['notif'] = $e->getMessage();
        }

        if(isset($conectar)) {
            mysqli_close($conectar);
        }
        
        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>