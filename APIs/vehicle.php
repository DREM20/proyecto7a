<?php 

    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');

    $app->get('/tbody', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT 
                    V.idVehiculo, 
                    V.aho, 
                    V.placas, 
                    T.tipoVehiculo, 
                    M.marca, 
                    MO.modelo, 
                    C.color, 
                    V.vigenciaTarjetaDeCirculacion, 
                    V.vigenciaPolizaSeguro
                    FROM vehiculo V 
                    LEFT JOIN tipovehiculo T ON V.idTipoVehiculo = T.idTipoVehiculo
                    LEFT JOIN marca M ON M.idMarca = V.idMarca
                    LEFT JOIN modelo MO ON MO.idMarca = V.idMarca AND MO.idModelo = V.idModelo
                    LEFT JOIN color C ON C.idColor = V.idColor";

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['info'] = $datos;

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));

    });

    $app->get('/load_info', function() use ($app) {
        require_once("conexion.php");   
        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT 
                    V.idVehiculo, 
                    V.serie, 
                    V.aho, 
                    V.placas, 
                    T.tipoVehiculo, 
                    M.marca, 
                    MO.modelo, 
                    A.aseguradora, 
                    C.color, 
                    V.numeroMotor, 
                    V.tarjetaDeCirculacion, 
                    V.vigenciaTarjetaDeCirculacion, 
                    V.polizaSeguro, 
                    V.vigenciaPolizaSeguro, 
                    V.telefonoPolizaSeguro, 
                    V.nombrePolizaSeguro, 
                    V.comentarios, 
                    CONCAT(U.first_name, ' ', U.last_name) AS usuario, 
                    V.foto,
                    V.idTipoVehiculo,
                    V.idMarca, 
                    V.idModelo,
                    V.idColor,
                    V.idAseguradora,
                    V.idUsuario
                    FROM vehiculo V 
                    LEFT JOIN tipovehiculo T ON V.idTipoVehiculo = T.idTipoVehiculo
                    LEFT JOIN marca M ON M.idMarca = V.idMarca
                    LEFT JOIN modelo MO ON MO.idMarca = V.idMarca AND MO.idModelo = V.idModelo
                    LEFT JOIN color C ON C.idColor = V.idColor
                    LEFT JOIN aseguradora A ON A.idAseguradora = V.idAseguradora
                    LEFT JOIN tb_users U ON U.id = V.idUsuario
                    WHERE V.idVehiculo = :id";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);
        $consulta->execute(); 
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) {
            $response['info'] = $dato;

            if(!file_exists('../' . $response['info']['foto'])) $response['info']['foto'] = '';
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save', function() use ($app) {
        require_once("conexion.php");
        $response = array();
        
        $photo = (isset($_FILES['vehicle_picture'])) ? $_FILES['vehicle_picture'] : '' ;
        $data = json_decode($app->request->post('data'));

        $consulta = "INSERT INTO vehiculo (idVehiculo, serie, aho, placas, numeroMotor, tarjetaDeCirculacion, vigenciaTarjetaDeCirculacion, polizaSeguro, 
        vigenciaPolizaSeguro, telefonoPolizaSeguro, nombrePolizaSeguro, comentarios, idTipoVehiculo, idMarca, idModelo, idAseguradora, idColor, idUsuario)
        VALUES (DEFAULT, :serie, :aho, :placas, :numeroMotor, :tarjetaDeCirculacion, :vigenciaTarjetaDeCirculacion, :polizaSeguro, 
        :vigenciaPolizaSeguro, :telefonoPolizaSeguro, :nombrePolizaSeguro, :comentarios, :idTipoVehiculo, :idMarca, :idModelo, 
        :idAseguradora, :idColor, :idUsuario)";

        $consulta = $conectar->prepare($consulta); 

        $consulta->bindValue(':serie'                           ,$data->vehicle_serie);
        $consulta->bindValue(':aho'                             ,$data->vehicle_year);
        $consulta->bindValue(':placas'                          ,$data->vehicle_number);
        $consulta->bindValue(':numeroMotor'                     ,$data->vehicle_motor);
        $consulta->bindValue(':tarjetaDeCirculacion'            ,$data->vehicle_card);
        $consulta->bindValue(':vigenciaTarjetaDeCirculacion'    ,date('Y-m-d', strtotime($data->vehicle_datecard)));
        $consulta->bindValue(':polizaSeguro'                    ,$data->vehicle_secure);
        $consulta->bindValue(':vigenciaPolizaSeguro'            ,date('Y-m-d', strtotime($data->vehicle_datesecure)));
        $consulta->bindValue(':telefonoPolizaSeguro'            ,$data->vehicle_phonesecure);
        $consulta->bindValue(':nombrePolizaSeguro'              ,$data->vehicle_namesecure);
        $consulta->bindValue(':comentarios'                     ,$data->vehicle_comments);
        $consulta->bindValue(':idTipoVehiculo'                  ,$data->vehicle_type);
        $consulta->bindValue(':idMarca'                         ,$data->vehicle_brand);
        $consulta->bindValue(':idModelo'                        ,$data->vehicle_model);
        $consulta->bindValue(':idAseguradora'                   ,$data->vehicle_insurance);
        $consulta->bindValue(':idColor'                         ,$data->vehicle_color);
        $consulta->bindValue(':idUsuario'                       ,$data->vehicle_user);

        if($consulta->execute()) {
            $id = $conectar->lastInsertId();

            /** PROCESAMOS IMAGEN EN CASO QUE EXISTA */
            if(!empty($photo)) {
                $carpeta = '../img';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculos';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculo' . $id;
                if(!is_dir($carpeta)) mkdir($carpeta);

                $extension = strtolower(pathinfo($photo['name'], PATHINFO_EXTENSION));
                $nombre = 'vehiculo' . $id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(move_uploaded_file($photo['tmp_name'], $ubicacion)) {
                    $ubicacion = 'img/Vehiculos/Vehiculo' . $id . '/vehiculo' . $id . '.' . $extension;
                    $consulta = "UPDATE vehiculo SET foto = :foto WHERE idVehiculo = :id";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindValue(':foto', $ubicacion);
                    $consulta->bindValue(':id', $id);

                    if($consulta->execute()) $response['image'] = true;
                    else $response['image'] = false; 
                }
                else $response['image'] = false;
            }

            $response['code'] = 200;
        }
        else {
            $response['code'] = 500;
            $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));

    });

    $app->post('/update_vehicle', function() use($app) {
        require_once("conexion.php");
        $response = array();
        
        $photo = (isset($_FILES['vehicle_picture'])) ? $_FILES['vehicle_picture'] : '' ;
        $data = json_decode($app->request->post('data'));

        $consulta = "UPDATE vehiculo SET serie = :serie, aho = :aho, placas = :placas, numeroMotor = :numeroMotor, tarjetaDeCirculacion = :tarjetaDeCirculacion,
        vigenciaTarjetaDeCirculacion = :vigenciaTarjetaDeCirculacion, polizaSeguro = :polizaSeguro, vigenciaPolizaSeguro = :vigenciaPolizaSeguro, 
        telefonoPolizaSeguro = :telefonoPolizaSeguro, nombrePolizaSeguro = :nombrePolizaSeguro, comentarios = :comentarios, idTipoVehiculo = :idTipoVehiculo,
        idMarca = :idMarca, idModelo = :idModelo, idAseguradora = :idAseguradora, idColor = :idColor, idUsuario = :idUsuario 
        WHERE idVehiculo = :idVehiculo";

        $consulta = $conectar->prepare($consulta); 

        $consulta->bindValue(':serie'                           ,$data->vehicle_serie);
        $consulta->bindValue(':aho'                             ,$data->vehicle_year);
        $consulta->bindValue(':placas'                          ,$data->vehicle_number);
        $consulta->bindValue(':numeroMotor'                     ,$data->vehicle_motor);
        $consulta->bindValue(':tarjetaDeCirculacion'            ,$data->vehicle_card);
        $consulta->bindValue(':vigenciaTarjetaDeCirculacion'    ,date('Y-m-d', strtotime($data->vehicle_datecard)));
        $consulta->bindValue(':polizaSeguro'                    ,$data->vehicle_secure);
        $consulta->bindValue(':vigenciaPolizaSeguro'            ,date('Y-m-d', strtotime($data->vehicle_datesecure)));
        $consulta->bindValue(':telefonoPolizaSeguro'            ,$data->vehicle_phonesecure);
        $consulta->bindValue(':nombrePolizaSeguro'              ,$data->vehicle_namesecure);
        $consulta->bindValue(':comentarios'                     ,$data->vehicle_comments);
        $consulta->bindValue(':idTipoVehiculo'                  ,$data->vehicle_type);
        $consulta->bindValue(':idMarca'                         ,$data->vehicle_brand);
        $consulta->bindValue(':idModelo'                        ,$data->vehicle_model);
        $consulta->bindValue(':idAseguradora'                   ,$data->vehicle_insurance);
        $consulta->bindValue(':idColor'                         ,$data->vehicle_color);
        $consulta->bindValue(':idUsuario'                       ,$data->vehicle_user);
        $consulta->bindValue(':idVehiculo'                      ,$data->id);

        if($consulta->execute()) {

            /** PROCESAMOS IMAGEN EN CASO QUE EXISTA */
            if(!empty($photo)) {
                $carpeta = '../img';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculos';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculo' . $data->id;
                if(!is_dir($carpeta)) mkdir($carpeta);

                $extension = strtolower(pathinfo($photo['name'], PATHINFO_EXTENSION));
                $nombre = 'vehiculo' . $data->id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(move_uploaded_file($photo['tmp_name'], $ubicacion)) {
                    $ubicacion = 'img/Vehiculos/Vehiculo' . $data->id . '/vehiculo' . $data->id . '.' . $extension;
                    $consulta = "UPDATE vehiculo SET foto = :foto WHERE idVehiculo = :id";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindValue(':foto', $ubicacion);
                    $consulta->bindValue(':id', $data->id);

                    if($consulta->execute()) $response['image'] = true;
                    else $response['image'] = false; 
                }
                else $response['image'] = false;
            }

            $response['code'] = 200;
        }
        else {
            $response['code'] = 500;
            $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));

    });

    $app->get('/get_docs', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT idDocumento, documento, UPPER(codigo) AS codigo, '' AS docto, 'M' as main,
                    0 as valido, 0 as rechazo, '' as motivorechazo, '' as vigencia, aplicaVigencia
                    FROM documento WHERE tipo = 'V' AND obligatorio = 1 ORDER BY documento";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $consulta = "SELECT idDocumento, extension, valido, rechazo, motivorechazo, vigencia FROM vehiculodocumento WHERE idVehiculo = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':id', $id);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) {
            foreach($response['doctos'] as $key => $docto) {
                if($docto['idDocumento'] == $dato['idDocumento']) {
                    $response['doctos'][$key]['docto'] = $dato['extension'];
                    $response['doctos'][$key]['valido'] = $dato['valido'];
                    $response['doctos'][$key]['rechazo'] = $dato['rechazo'];
                    $response['doctos'][$key]['motivorechazo'] = $dato['motivorechazo'];
                    $response['doctos'][$key]['vigencia'] = (!empty($dato['vigencia'])) ? date('d/m/Y', strtotime($dato['vigencia'])) : '';
                    $response['doctos'][$key]['informacion'] = array();
                }
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save_doc', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->post('id');
        $docto = $app->request->post('docto');
        $main = $app->request->post('main');
        $codigo = $app->request->post('codigo');
        $vigencia = $app->request->post('vigencia');
        $file = (isset($_FILES['file'])) ? $_FILES['file'] : '' ;

        if(!empty($vigencia)) $vigencia = date('Y-m-d', strtotime(str_replace('/', '-', $vigencia))); 
        else $vigencia = null;

        if(!empty($file)) {
            $carpeta = '../img';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/Vehiculos';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/Vehiculo' . $id;
            if(!is_dir($carpeta)) mkdir($carpeta);

            $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

            $file_type = explode('_', $docto)[2];
            
            switch($main) {
                case 'G':
                    $nombre = 'vehiculo' . $id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;

                    if(move_uploaded_file($file['tmp_name'], $ubicacion)) {
                        $ubicacion = 'img/Vehiculos/Vehiculo' . $id . '/vehiculo' . $id . '.' . $extension;
                        $consulta = "UPDATE vehiculo SET foto = :foto WHERE idVehiculo = :id";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(':foto', $ubicacion);
                        $consulta->bindValue(':id', $id);
        
                        if($consulta->execute()) $response['image'] = true;
                        else $response['image'] = false; 
                    }
                    break;
                case 'M':
                    $nombre = $codigo . $id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;

                    if(move_uploaded_file($file['tmp_name'], $ubicacion)) {
                        $consulta = "SELECT idDocumento FROM vehiculodocumento WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(":vehiculo", $id);
                        $consulta->bindValue(":documento", $file_type);
                        $consulta->execute();

                        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                        $ubicacion = 'img/Vehiculos/Vehiculo' . $id . '/' . $codigo . $id . '.' . $extension;
                        if(count($datos) > 0) {
                            $consulta = "UPDATE vehiculodocumento SET extension = :file, vigencia = :vigencia WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':file', $ubicacion);
                            $consulta->bindValue(':vehiculo', $id);
                            $consulta->bindValue(':documento', $file_type);
                            $consulta->bindValue(':vigencia', $vigencia);
            
                            if($consulta->execute()) $response['image'] = true;
                            else $response['image'] = false; 
                        }
                        else {
                            $consulta = "INSERT INTO vehiculodocumento (idVehiculo, idDocumento, extension, vigencia)
                                        VALUES (:id, :idfile, :file, :vigencia)";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':id', $id);
                            $consulta->bindValue(':idfile', $file_type);
                            $consulta->bindValue(':file', $ubicacion);
                            $consulta->bindValue(':vigencia', $vigencia);
            
                            if($consulta->execute()) $response['image'] = true;
                            else $response['image'] = false; 
                        }
                    }
                    break;
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_conductor', function() use ($app) {
        require_once("conexion.php");
        $response = array();
        
        $id = $app->request->get('id');

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>