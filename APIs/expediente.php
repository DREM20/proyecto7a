<?php
    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/get_data', function() use ($app) {
        require_once("conexion.php");
        session_start();
        $response = array();

        $usuario = (!empty($_SESSION['operador']['correo'])) ? $_SESSION['operador']['correo'] : '';
        $telefono = (!empty($_SESSION['operador']['telefono'])) ? $_SESSION['operador']['telefono'] : '';
        $id = (!empty($_SESSION['operador']['id'])) ? $_SESSION['operador']['id'] : '';

        $consulta = "SELECT O.nombre, O.apellidos, O.email, O.telefono, O.idEstado, O.idMunicipio,
                    O.direccion, DATE_FORMAT(O.cumpleahos, '%d/%m/%Y') AS cumpleahos, O.genero, O.rfc,
                    O.telefonoEmergencia, O.contactoEmergencia, O.noIFE, O.licencia, DATE_FORMAT(O.licenciaVigencia, '%d/%m/%Y') AS licenciaVigencia, 
                    O.cuenta, O.folioPenales, O.cursoEquidad, O.foto, O.foliotoxico, O.grupo
                    FROM operador O 
                    WHERE O.id = :id";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);

        $consulta->execute(); 

        $row = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if(count($row) == 0) {
            $response['code'] = 500;
            $response['msg'] = 'Verifique la información de usuario';
        }
        else {
            $response['code'] = 200;
            $response['info'] = $row[0];

            /**
             * OBTENEMOS LOS DOCUMENTOS
             */
            $consulta = "SELECT idDocumento, documento, UPPER(codigo) AS codigo, '' AS docto, 'M' as main,
                        0 as valido, 0 as rechazo, '' as motivorechazo
                        FROM documento WHERE tipo = 'O' AND obligatorio = 1 ORDER BY documento";
            $consulta = $conectar->prepare($consulta);
            $consulta->execute();
            $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $consulta = "SELECT idDocumento, extension, valido, rechazo, motivorechazo FROM operadordocumento WHERE idOperador = :id";
            $consulta = $conectar->prepare($consulta);
            $consulta->bindValue(':id', $id);
            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($datos as $dato) {
                foreach($response['doctos'] as $key => $docto) {
                    if($docto['idDocumento'] == $dato['idDocumento']) {
                        $response['doctos'][$key]['docto'] = $dato['extension'];
                        $response['doctos'][$key]['valido'] = $dato['valido'];
                        $response['doctos'][$key]['rechazo'] = $dato['rechazo'];
                        $response['doctos'][$key]['motivorechazo'] = $dato['motivorechazo'];
                    }
                }
            }

            /** OBTENEMOS LOS VEHICULOS DADOS DE ALTA */
            $consulta = "SELECT V.serie, V.aho, V.placas, V.numeroMotor, 
                        DATE_FORMAT(V.vigenciaTarjetaDeCirculacion, '%d/%m/%Y') AS vigenciaTarjetaDeCirculacion, 
                        V.foto, V.polizaSeguro, V.nombrePolizaSeguro, 
                        V.telefonoPolizaSeguro, 
                        DATE_FORMAT(V.vigenciaPolizaSeguro, '%d/%m/%Y') AS vigenciaPolizaSeguro, 
                        C.color, M.marca, MO.modelo, 
                        T.tipoVehiculo, A.aseguradora,
                        V.idVehiculo as clave
                        FROM vehiculo V 
                        LEFT JOIN operadorvehiculo O ON O.idVehiculo = V.idVehiculo 
                        LEFT JOIN color C ON C.idColor = V.idColor 
                        LEFT JOIN marca M ON M.idMarca = V.idMarca 
                        LEFT JOIN modelo MO ON MO.idMarca = V.idMarca AND MO.idModelo = V.idModelo 
                        LEFT JOIN tipovehiculo T ON T.idTipoVehiculo = V.idTipoVehiculo 
                        LEFT JOIN aseguradora A ON A.idAseguradora = V.idAseguradora
                        WHERE O.idOwner = :idOperador";
            
            $consulta = $conectar->prepare($consulta);
            $consulta->bindValue(':idOperador', $id);
            $consulta->execute();
            $response['vehiculos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save_generales', function() use ($app) {
        require_once("conexion.php");
        session_start();
        $response = array();

        $info = json_decode($app->request->post('info'));
        $foto           = (!empty($_FILES['generales_foto']))   ? $_FILES['generales_foto'] : '';
        $antecedentes   = (!empty($_FILES['antecedentes']))     ? $_FILES['antecedentes'] : ''; 
        $curso          = (!empty($_FILES['curso']))            ? $_FILES['curso'] : ''; 
        $toxico         = (!empty($_FILES['toxico']))           ? $_FILES['toxico'] : ''; 
        $cedula         = (!empty($_FILES['cedula']))           ? $_FILES['cedula'] : ''; 
        $domicilio      = (!empty($_FILES['domicilio']))        ? $_FILES['domicilio'] : ''; 
        $ifeA           = (!empty($_FILES['ifeA']))             ? $_FILES['ifeA'] : ''; 
        $ifeB           = (!empty($_FILES['ifeB']))             ? $_FILES['ifeB'] : ''; 
        $licenciaA      = (!empty($_FILES['licenciaA']))        ? $_FILES['licenciaA'] : ''; 
        $licenciaB      = (!empty($_FILES['licenciaB']))        ? $_FILES['licenciaB'] : ''; 

        $id = (!empty($_SESSION['operador']['id'])) ? $_SESSION['operador']['id'] : '';
        if(empty($id)) {
            $response['code'] = 401;
            $response['msg'] = 'La sesión ha caducado, inicie sesión e inténtelo más tarde';
        }
        else {
            $consulta = "UPDATE operador 
                        SET cumpleahos = :cumpleahos, 
                        nombre = :nombre, 
                        apellidos = :apellidos, 
                        direccion = :direccion, 
                        genero = :genero, 
                        telefonoEmergencia = :telefonoEmergencia, 
                        contactoEmergencia = :contactoEmergencia, 
                        cuenta = :cuenta, 
                        licencia = :licencia, 
                        licenciaVigencia = :licenciaVigencia, 
                        idEstado = :idEstado,
                        idMunicipio = :idMunicipio, 
                        noIFE = :noIFE, 
                        rfc = :rfc, 
                        folioPenales = :folioPenales,
                        cursoEquidad = :cursoEquidad,
                        foliotoxico = :foliotoxico,
                        grupo = :grupo
                        WHERE id = :id";

            $consulta = $conectar->prepare($consulta); 

            $consulta->bindValue(':cumpleahos'          , (!empty($info->generales_nacimiento)) ? date('Y-m-d', strtotime(str_replace('/', '-', $info->generales_nacimiento))) : '');
            $consulta->bindValue(':nombre'              , $info->generales_nombre);
            $consulta->bindValue(':apellidos'           , $info->generales_apellido);
            $consulta->bindValue(':direccion'           , $info->generales_direccion);
            $consulta->bindValue(':genero'              , $info->generales_genero);
            $consulta->bindValue(':telefonoEmergencia'  , $info->generales_telefono_emergencia);
            $consulta->bindValue(':contactoEmergencia'  , $info->generales_contacto_emergencia);
            $consulta->bindValue(':cuenta'              , $info->generales_cuenta_bancaria);
            $consulta->bindValue(':licencia'            , $info->generales_numero_licencia);
            $consulta->bindValue(':licenciaVigencia'    , (!empty($info->generales_vigencia_licencia)) ? date('Y-m-d', strtotime(str_replace('/', '-', $info->generales_vigencia_licencia))) : '');
            $consulta->bindValue(':idEstado'            , $info->generales_estado);
            $consulta->bindValue(':idMunicipio'         , $info->generales_municipio);
            $consulta->bindValue(':noIFE'               , $info->generales_numero_ine);
            $consulta->bindValue(':rfc'                 , $info->generales_rfc);
            $consulta->bindValue(':folioPenales'        , $info->generales_antecedentes_folio);
            $consulta->bindValue(':cursoEquidad'        , $info->generales_equidad_folio);
            $consulta->bindValue(':foliotoxico'         , $info->generales_toxicologico_folio);
            $consulta->bindValue(':grupo'               , $info->generales_grupo);
            $consulta->bindValue(':id'                  , $id);

            if($consulta->execute()) $response['code'] = 200;
            else {
                $response['code'] = 500;
                $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
            }

            $carpeta = '../img';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/Operadores';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/operador' . $id;
            if(!is_dir($carpeta)) mkdir($carpeta);

            if(!empty($foto)) {
                $extension = strtolower(pathinfo($foto['name'], PATHINFO_EXTENSION));
                $nombre = 'Operador' . $id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(move_uploaded_file($foto['tmp_name'], $ubicacion)) {
                    $ubicacion = 'img/Operadores/operador' . $id . '/Operador' . $id . '.' . $extension;
                    $consulta = "UPDATE operador SET foto = :foto WHERE id = :id";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindParam(':foto', $ubicacion);
                    $consulta->bindParam(':id', $id);

                    if($consulta->execute()) $response['foto'] = array('proceso' => true, 'ubicacion' => $ubicacion);
                    else $response['foto'] = array('proceso' => false);
                }
                else $response['foto'] = array('proceso' => false);
            }

            /** PROCESAMOS LOS DOCUMENTOS */
            $documentos = array(
                'antecedentes' => (!empty($antecedentes)) ? $antecedentes : array(),
                'curso' => (!empty($curso)) ? $curso : array(),
                'toxico' => (!empty($toxico)) ? $toxico : array(),
                'cedula' => (!empty($cedula)) ? $cedula : array(),
                'domicilio' => (!empty($domicilio)) ? $domicilio : array(),
                'ifeA' => (!empty($ifeA)) ? $ifeA : array(),
                'ifeB' => (!empty($ifeB)) ? $ifeB : array(),
                'licenciaA' => (!empty($licenciaA)) ? $licenciaA : array(),
                'licenciaB' => (!empty($licenciaB)) ? $licenciaB : array(),
            );

            $consulta = "SELECT idDocumento, documento, codigo FROM documento WHERE tipo = 'O' ORDER BY documento";
            $consulta = $conectar->prepare($consulta);
            $consulta->execute();
            $info_docs = $consulta->fetchAll(PDO::FETCH_ASSOC);
            foreach($info_docs as $info) { if(isset($documentos[$info['codigo']])) $documentos[$info['codigo']]['id'] = $info['idDocumento']; }

            $response['doctos'] = array();
            foreach($documentos as $key => $documento) {
                if(empty($documento['name'])) continue;
                $extension = strtolower(pathinfo($documento['name'], PATHINFO_EXTENSION));
                $nombre = $key . $id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(empty($documento['id'])) continue;

                if(move_uploaded_file($documento['tmp_name'], $ubicacion)) {
                    $consulta = "SELECT idDocumento FROM operadordocumento WHERE idOperador = :operador AND idDocumento = :documento";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindValue(":operador", $id);
                    $consulta->bindValue(":documento", $documento['id']);
                    $consulta->execute();

                    $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                    $ubicacion = 'img/Operadores/operador' . $id . '/' . $key . $id . '.' . $extension;
                    if(count($datos) > 0) {
                        $consulta = "UPDATE operadordocumento SET extension = :file, rechazo = 0 WHERE idOperador = :operador AND idDocumento = :documento";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(':file', $ubicacion);
                        $consulta->bindValue(':operador', $id);
                        $consulta->bindValue(':documento', $documento['id']);
        
                        if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                        else $response['doctos'][$key] = array('proceso' => false); 
                    }
                    else {
                        $consulta = "INSERT INTO operadordocumento (idOperador, idDocumento, extension, rechazo)
                                    VALUES (:id, :idfile, :file, 0)";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(':id', $id);
                        $consulta->bindValue(':idfile', $documento['id']);
                        $consulta->bindValue(':file', $ubicacion);
        
                        if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                        else $response['doctos'][$key] = array('proceso' => false); 
                    }
                }
                else $response['doctos'][$key] = array('proceso' => false); 
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->post('/crear_vehiculo', function() use($app) {
        require_once("conexion.php");
        session_start();
        $response = array();

        $info = json_decode($app->request->post('info'));
        $foto = (!empty($_FILES['vehiculo_tarjeta_auto'])) ? $_FILES['vehiculo_tarjeta_auto'] : '';
        $circulacionA = (!empty($_FILES['circulacionA'])) ? $_FILES['circulacionA'] : '';
        $circulacionB = (!empty($_FILES['circulacionB'])) ? $_FILES['circulacionB'] : '';
        $polizaA = (!empty($_FILES['polizaA'])) ? $_FILES['polizaA'] : '';

        $usuario = (!empty($_SESSION['operador']['correo'])) ? $_SESSION['operador']['correo'] : '';
        $telefono = (!empty($_SESSION['operador']['telefono'])) ? $_SESSION['operador']['telefono'] : '';
        $id = (!empty($_SESSION['operador']['id'])) ? $_SESSION['operador']['id'] : '';

        if(empty($id)) {
            $response['code'] = 401;
            $response['msg'] = 'La sesión ha caducado, inicie sesión e inténtelo más tarde';
        }
        else {
            $consulta = "SELECT idVehiculo FROM vehiculo WHERE serie = :serie";
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':serie', $info->vehiculo_tarjeta_serie);
            $consulta->execute(); 

            $row = $consulta->fetchAll(PDO::FETCH_ASSOC);
            if(count($row) > 0) {
                $response['code'] = 400;
                $response['msg'] = 'El vehículo con número de serie ' . $info->vehiculo_tarjeta_serie . ' ya se encuentra registrado.';
            }
            else {
                $time = date('Y-m-d H:i:s');
                $consulta = "INSERT INTO vehiculo (idVehiculo, serie, aho, placas, numeroMotor, vigenciaTarjetaDeCirculacion, polizaSeguro, 
                            vigenciaPolizaSeguro, telefonoPolizaSeguro, nombrePolizaSeguro, idTipoVehiculo, idMarca, idModelo, idAseguradora, 
                            idColor, fechahora, idUsuario) VALUES (DEFAULT, :serie, :aho, :placas, :numeroMotor, :vigenciaTarjetaDeCirculacion, :polizaSeguro, 
                            :vigenciaPolizaSeguro, :telefonoPolizaSeguro, :nombrePolizaSeguro, :idTipoVehiculo, :idMarca, :idModelo, :idAseguradora, 
                            :idColor, :fechahora, 0)";
                $consulta = $conectar->prepare($consulta);

                $consulta->bindValue(':serie',                          $info->vehiculo_tarjeta_serie);
                $consulta->bindValue(':aho',                            $info->vehiculo_tarjeta_anio);
                $consulta->bindValue(':placas',                         $info->vehiculo_tarjeta_placa);
                $consulta->bindValue(':numeroMotor',                    $info->vehiculo_tarjeta_motor);
                $consulta->bindValue(':vigenciaTarjetaDeCirculacion',   date('Y-m-d', strtotime(str_replace('/', '-', $info->vehiculo_tarjeta_vigencia))));
                $consulta->bindValue(':polizaSeguro',                   $info->vehiculo_poliza_numero);
                $consulta->bindValue(':vigenciaPolizaSeguro',           date('Y-m-d', strtotime(str_replace('/', '-', $info->vehiculo_poliza_vigencia))));
                $consulta->bindValue(':telefonoPolizaSeguro',           $info->vehiculo_poliza_telefono);
                $consulta->bindValue(':nombrePolizaSeguro',             $info->vehiculo_poliza_nombre);
                $consulta->bindValue(':idTipoVehiculo',                 $info->vehiculo_tarjeta_tipo);
                $consulta->bindValue(':idMarca',                        $info->vehiculo_tarjeta_marca);
                $consulta->bindValue(':idModelo',                       $info->vehiculo_tarjeta_modelo);
                $consulta->bindValue(':idAseguradora',                  $info->vehiculo_poliza_aseguradora);
                $consulta->bindValue(':idColor',                        $info->vehiculo_tarjeta_color);
                $consulta->bindValue(':fechahora',                      $time);

                if($consulta->execute()) {
                    $response['vehiculo'] = array('proceso' => true);
                    $response['code'] = 200;

                    $id_vehiculo = $conectar->lastInsertId();

                    $consulta = "INSERT operadorvehiculo (idOperador, idVehiculo, activo, idOwner) VALUES (:idOperador, :idVehiculo, 1, :idOwner)";
                    $consulta = $conectar->prepare($consulta);

                    $conducir = ($info->vehiculo_conducir) ? $id : 0;

                    $consulta->bindParam(':idOperador', $conducir);
                    $consulta->bindParam(':idVehiculo', $id_vehiculo);
                    $consulta->bindParam(':idOwner', $id);

                    if($consulta->execute()) $response['operador_vehiculo'] = array('proceso' => true);
                    else $response['operador_vehiculo'] = array('proceso' => false);

                    $carpeta = '../img';
                    if(!is_dir($carpeta)) mkdir($carpeta);
                    $carpeta = $carpeta . '/Vehiculos';
                    if(!is_dir($carpeta)) mkdir($carpeta);
                    $carpeta = $carpeta . '/Vehiculo' . $id_vehiculo;
                    if(!is_dir($carpeta)) mkdir($carpeta);

                    if(!empty($foto)) {
                        $extension = strtolower(pathinfo($foto['name'], PATHINFO_EXTENSION));
                        $nombre = 'vehiculo' . $id_vehiculo . '.' . $extension;
                        $ubicacion = $carpeta . '/' . $nombre;
        
                        if(move_uploaded_file($foto['tmp_name'], $ubicacion)) {
                            $ubicacion = 'img/Vehiculos/Vehiculo' . $id_vehiculo . '/vehiculo' . $id_vehiculo . '.' . $extension;
                            $consulta = "UPDATE vehiculo SET foto = :foto WHERE idVehiculo = :id";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindParam(':foto', $ubicacion);
                            $consulta->bindParam(':id', $id_vehiculo);
        
                            if($consulta->execute()) $response['foto'] = array('proceso' => true, 'ubicacion' => $ubicacion);
                            else $response['foto'] = array('proceso' => false);
                        }
                        else $response['foto'] = array('proceso' => false);
                    }
                    else $response['foto'] = array('proceso' => false);

                    /** PROCESAMOS LOS DOCUMENTOS */
                    $documentos = array(
                        'circulacionA' => (!empty($circulacionA)) ? $circulacionA : array(),
                        'circulacionB' => (!empty($circulacionB)) ? $circulacionB : array(),
                        'polizaA' => (!empty($polizaA)) ? $polizaA : array(),
                    );

                    $consulta = "SELECT idDocumento, documento, codigo FROM documento WHERE tipo = 'V' ORDER BY documento";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->execute();
                    $info_docs = $consulta->fetchAll(PDO::FETCH_ASSOC);
                    foreach($info_docs as $info) { if(isset($documentos[$info['codigo']])) $documentos[$info['codigo']]['id'] = $info['idDocumento']; }

                    $response['doctos'] = array();
                    foreach($documentos as $key => $documento) {
                        if(empty($documento['name'])) continue;
                        $extension = strtolower(pathinfo($documento['name'], PATHINFO_EXTENSION));
                        $nombre = $key . $id_vehiculo . '.' . $extension;
                        $ubicacion = $carpeta . '/' . $nombre;

                        if(empty($documento['id'])) continue;

                        if(move_uploaded_file($documento['tmp_name'], $ubicacion)) {
                            $consulta = "SELECT idDocumento FROM vehiculodocumento WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(":vehiculo", $id_vehiculo);
                            $consulta->bindValue(":documento", $documento['id']);
                            $consulta->execute();

                            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                            $ubicacion = 'img/Vehiculos/Vehiculo' . $id_vehiculo . '/' . $key . $id_vehiculo . '.' . $extension;
                            if(count($datos) > 0) {
                                $consulta = "UPDATE vehiculodocumento SET extension = :file, rechazo = 0 WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                                $consulta = $conectar->prepare($consulta);
                                $consulta->bindValue(':file', $ubicacion);
                                $consulta->bindValue(':vehiculo', $id_vehiculo);
                                $consulta->bindValue(':documento', $documento['id']);
                
                                if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                                else $response['doctos'][$key] = array('proceso' => false); 
                            }
                            else {
                                $consulta = "INSERT INTO vehiculodocumento (idVehiculo, idDocumento, extension, rechazo)
                                            VALUES (:id, :idfile, :file, 0)";
                                $consulta = $conectar->prepare($consulta);
                                $consulta->bindValue(':id', $id_vehiculo);
                                $consulta->bindValue(':idfile', $documento['id']);
                                $consulta->bindValue(':file', $ubicacion);
                
                                if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                                else $response['doctos'][$key] = array('proceso' => false); 
                            }
                        }
                        else $response['doctos'][$key] = array('proceso' => false); 
                    }
                }
                else {
                    $response['code'] = 500;
                    $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
                    $response['vehiculo'] = array('proceso' => false);
                    $response['query'] = $consulta->errorInfo();
                }
            }
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_vehiculo', function() use ($app) {
        require_once("conexion.php");
        session_start();
        $response = array();

        $clave = $app->request->get('clave');

        $id = (!empty($_SESSION['operador']['id'])) ? $_SESSION['operador']['id'] : '';


        if(empty($id)) {
            $response['code'] = 401;
            $response['msg'] = 'La sesión ha caducado, inicie sesión e inténtelo más tarde';
        }
        else {
            $consulta = "SELECT 
                        V.serie AS vehiculo_tarjeta_serie, 
                        V.aho AS vehiculo_tarjeta_anio, 
                        V.placas AS vehiculo_tarjeta_placa, 
                        V.numeroMotor AS vehiculo_tarjeta_motor, 
                        DATE_FORMAT(V.vigenciaTarjetaDeCirculacion, '%d/%m/%Y') AS vehiculo_tarjeta_vigencia, 
                        V.idColor as vehiculo_tarjeta_color, 
                        V.idMarca as marca, V.idModelo as modelo, 
                        V.idTipoVehiculo as vehiculo_tarjeta_tipo, 
                        V.foto, 
                        V.idAseguradora as vehiculo_poliza_aseguradora, 
                        V.polizaSeguro AS vehiculo_poliza_numero, 
                        V.nombrePolizaSeguro AS vehiculo_poliza_nombre, 
                        V.telefonoPolizaSeguro AS vehiculo_poliza_telefono, 
                        DATE_FORMAT(V.vigenciaPolizaSeguro, '%d/%m/%Y') AS vehiculo_poliza_vigencia, 
                        OV.idOperador AS conductor
                        FROM vehiculo V 
                        LEFT JOIN operadorvehiculo OV ON OV.idVehiculo = V.idVehiculo 
                        WHERE V.idVehiculo = :idVehiculo";
            
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':idVehiculo', $clave);
            $consulta->execute();

            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
            $response['info'] = array();
            if(count($datos) > 0) {
                $response['info'] = $datos[0];
                if($response['info']['conductor'] == $id) $response['info']['conductor'] = true;
                else $response['info']['conductor'] = false;
            }

            $consulta = "SELECT V.extension, 
                        V.rechazo, V.valido, V.motivorechazo, D.documento, UPPER(D.codigo) AS codigo
                        FROM vehiculodocumento V, documento D WHERE D.idDocumento = V.idDocumento AND V.idVehiculo = :id";
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $clave);
            $consulta->execute();

            $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($response['doctos'] as $key => $docto) {
                $response['doctos'][$key]['type'] = strtoupper(pathinfo($docto['extension'], PATHINFO_EXTENSION));
            }

            $response['code'] = 200;
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/modificar_vehiculo', function() use($app) {
        require_once("conexion.php");
        session_start();
        $response = array();

        $info = json_decode($app->request->post('info'));
        $foto = (!empty($_FILES['vehiculo_tarjeta_auto'])) ? $_FILES['vehiculo_tarjeta_auto'] : '';
        $circulacionA = (!empty($_FILES['circulacionA'])) ? $_FILES['circulacionA'] : '';
        $circulacionB = (!empty($_FILES['circulacionB'])) ? $_FILES['circulacionB'] : '';
        $polizaA = (!empty($_FILES['polizaA'])) ? $_FILES['polizaA'] : '';

        $usuario = (!empty($_SESSION['operador']['correo'])) ? $_SESSION['operador']['correo'] : '';
        $telefono = (!empty($_SESSION['operador']['telefono'])) ? $_SESSION['operador']['telefono'] : '';
        $id = (!empty($_SESSION['operador']['id'])) ? $_SESSION['operador']['id'] : '';

        if(empty($id)) {
            $response['code'] = 401;
            $response['msg'] = 'La sesión ha caducado, inicie sesión e inténtelo más tarde';
        }
        else {

            $consulta = "UPDATE vehiculo SET serie = :serie, aho = :aho, placas = :placas, numeroMotor = :numeroMotor, 
                        vigenciaTarjetaDeCirculacion = :vigenciaTarjetaDeCirculacion, polizaSeguro = :polizaSeguro, 
                        vigenciaPolizaSeguro = :vigenciaPolizaSeguro, telefonoPolizaSeguro = :telefonoPolizaSeguro, 
                        nombrePolizaSeguro = :nombrePolizaSeguro, idTipoVehiculo = :idTipoVehiculo, idMarca = :idMarca, 
                        idModelo = :idModelo, idAseguradora = :idAseguradora, idColor = :idColor WHERE idVehiculo = :idVehiculo";
            
            $consulta = $conectar->prepare($consulta);

            $consulta->bindValue(':serie',                          $info->vehiculo_tarjeta_serie);
            $consulta->bindValue(':aho',                            $info->vehiculo_tarjeta_anio);
            $consulta->bindValue(':placas',                         $info->vehiculo_tarjeta_placa);
            $consulta->bindValue(':numeroMotor',                    $info->vehiculo_tarjeta_motor);
            $consulta->bindValue(':vigenciaTarjetaDeCirculacion',   (!empty($info->vehiculo_tarjeta_vigencia)) ? date('Y-m-d', strtotime(str_replace('/', '-', $info->vehiculo_tarjeta_vigencia))) : '');
            $consulta->bindValue(':polizaSeguro',                   $info->vehiculo_poliza_numero);
            $consulta->bindValue(':vigenciaPolizaSeguro',           (!empty($info->vehiculo_poliza_vigencia)) ? date('Y-m-d', strtotime(str_replace('/', '-', $info->vehiculo_poliza_vigencia))) : '');
            $consulta->bindValue(':telefonoPolizaSeguro',           $info->vehiculo_poliza_telefono);
            $consulta->bindValue(':nombrePolizaSeguro',             $info->vehiculo_poliza_nombre);
            $consulta->bindValue(':idTipoVehiculo',                 $info->vehiculo_tarjeta_tipo);
            $consulta->bindValue(':idMarca',                        $info->vehiculo_tarjeta_marca);
            $consulta->bindValue(':idModelo',                       $info->vehiculo_tarjeta_modelo);
            $consulta->bindValue(':idAseguradora',                  $info->vehiculo_poliza_aseguradora);
            $consulta->bindValue(':idColor',                        $info->vehiculo_tarjeta_color);
            $consulta->bindValue(':idVehiculo',                     $info->id);

            if($consulta->execute()) {
                $response['vehiculo'] = array('proceso' => true);
                $response['code'] = 200;

                $consulta = "UPDATE operadorvehiculo SET idOperador = :idOperador WHERE idVehiculo = :idVehiculo AND idOwner = :idOwner";
                $consulta = $conectar->prepare($consulta);

                $conducir = ($info->vehiculo_conducir) ? $id : 0;

                $consulta->bindParam(':idOperador', $conducir);
                $consulta->bindParam(':idVehiculo', $info->id);
                $consulta->bindParam(':idOwner', $id);

                if($consulta->execute()) $response['operador_vehiculo'] = array('proceso' => true);
                else $response['operador_vehiculo'] = array('proceso' => false, 'query' => $consulta->errorInfo());


                $carpeta = '../img';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculos';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Vehiculo' . $info->id;
                if(!is_dir($carpeta)) mkdir($carpeta);

                if(!empty($foto)) {
                    $extension = strtolower(pathinfo($foto['name'], PATHINFO_EXTENSION));
                    $nombre = 'vehiculo' . $info->id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;
    
                    if(move_uploaded_file($foto['tmp_name'], $ubicacion)) {
                        $ubicacion = 'img/Vehiculos/Vehiculo' . $info->id . '/vehiculo' . $info->id . '.' . $extension;
                        $consulta = "UPDATE vehiculo SET foto = :foto WHERE idVehiculo = :id";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindParam(':foto', $ubicacion);
                        $consulta->bindParam(':id', $info->id);
    
                        if($consulta->execute()) $response['foto'] = array('proceso' => true, 'ubicacion' => $ubicacion);
                        else $response['foto'] = array('proceso' => false);
                    }
                    else $response['foto'] = array('proceso' => false);
                }
                else $response['foto'] = array('proceso' => false);

                /** PROCESAMOS LOS DOCUMENTOS */
                $documentos = array(
                    'circulacionA' => (!empty($circulacionA)) ? $circulacionA : array(),
                    'circulacionB' => (!empty($circulacionB)) ? $circulacionB : array(),
                    'polizaA' => (!empty($polizaA)) ? $polizaA : array(),
                );

                $consulta = "SELECT idDocumento, documento, codigo FROM documento WHERE tipo = 'V' ORDER BY documento";
                $consulta = $conectar->prepare($consulta);
                $consulta->execute();
                $info_docs = $consulta->fetchAll(PDO::FETCH_ASSOC);
                foreach($info_docs as $info) { if(isset($documentos[$info['codigo']])) $documentos[$info['codigo']]['id'] = $info['idDocumento']; }

                $response['doctos'] = array();
                foreach($documentos as $key => $documento) {
                    if(empty($documento['name'])) continue;
                    $extension = strtolower(pathinfo($documento['name'], PATHINFO_EXTENSION));
                    $nombre = $key . $info->id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;

                    if(empty($documento['id'])) continue;

                    if(move_uploaded_file($documento['tmp_name'], $ubicacion)) {
                        $consulta = "SELECT idDocumento FROM vehiculodocumento WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(":vehiculo", $info->id);
                        $consulta->bindValue(":documento", $documento['id']);
                        $consulta->execute();

                        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                        $ubicacion = 'img/Vehiculos/Vehiculo' . $info->id . '/' . $key . $info->id . '.' . $extension;
                        if(count($datos) > 0) {
                            $consulta = "UPDATE vehiculodocumento SET extension = :file, rechazo = 0 WHERE idVehiculo = :vehiculo AND idDocumento = :documento";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':file', $ubicacion);
                            $consulta->bindValue(':vehiculo', $info->id);
                            $consulta->bindValue(':documento', $documento['id']);
            
                            if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                            else $response['doctos'][$key] = array('proceso' => false); 
                        }
                        else {
                            $consulta = "INSERT INTO vehiculodocumento (idVehiculo, idDocumento, extension, rechazo)
                                        VALUES (:id, :idfile, :file, 0)";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':id', $info->id);
                            $consulta->bindValue(':idfile', $documento['id']);
                            $consulta->bindValue(':file', $ubicacion);
            
                            if($consulta->execute()) $response['doctos'][$key] = array('proceso' => true, 'ubicacion' => $ubicacion);
                            else $response['doctos'][$key] = array('proceso' => false); 
                        }
                    }
                    else $response['doctos'][$key] = array('proceso' => false); 
                }
            }
            else {
                $response['code'] = 500;
                $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
                $response['vehiculo'] = array('proceso' => false);
                // $response['query'] = $consulta->errorInfo();
            }
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>