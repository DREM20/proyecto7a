<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("../assets/Slim/Slim.php");
    require('helpers/generarexcel.php');

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/dashboard', function() use ($app) {
        require("conexion.php");
        $response = array();

        $zona = $app->request->get('zona');
        $estado = $app->request->get('estado');
        $municipio = $app->request->get('municipio');

        $condicion = '';
        if(!empty($zona)) $condicion = " AND zona = '".$zona."'";

        /** OBTENEMOS LOS VIAJES DEL MES */
        $consulta = "SELECT count(*) as conteo FROM viajes_historico WHERE MONTH(fecha) = MONTH(CURRENT_DATE()) AND YEAR(fecha) = YEAR(CURRENT_DATE()) AND estatus in ('cancelado', 'viaje terminado')" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['VIAJES_ACUMULADO'] = array(
            'TITULO' => 'VIAJES ACUMULADOS',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-primary',
            'SIZE' => 3,
            'DATA' => intval($consulta->fetchAll(PDO::FETCH_ASSOC)[0]['conteo'])
        );

        /** OBTENEMOS LOS VIAJES FINALIZADOS DEL MES */
        $consulta = "SELECT count(*) as conteo FROM viajes_historico WHERE MONTH(fecha) = MONTH(CURRENT_DATE()) AND YEAR(fecha) = YEAR(CURRENT_DATE()) AND estatus = 'viaje terminado'" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['VIAJES_TERMINADOS'] = array(
            'TITULO' => 'VIAJES TERMINADOS',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-blue',
            'SIZE' => 3,
            'DATA' => intval($consulta->fetchAll(PDO::FETCH_ASSOC)[0]['conteo'])
        );

        /** OBTENEMOS LOS VIAJES CANCELADOS DEL MES */
        $consulta = "SELECT count(*) as conteo FROM viajes_historico WHERE MONTH(fecha) = MONTH(CURRENT_DATE()) AND YEAR(fecha) = YEAR(CURRENT_DATE()) AND estatus = 'cancelado'" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['VIAJES_CANCELADOS'] = array(
            'TITULO' => 'VIAJES CANCELADOS',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-red',
            'SIZE' => 3,
            'DATA' => intval($consulta->fetchAll(PDO::FETCH_ASSOC)[0]['conteo'])
        );

        $conteo = 0;
        if(($data = @file_get_contents(ADMIN_APIS . 'contador_viajes_activos?zona=' . urlencode($zona))) !== false) {
            if($data = json_decode($data, true)) {
                if(isset($data['data']['conteo'])) $conteo = $data['data']['conteo'];
            }
        }

        /** OBTENEMOS LOS VIAJES EN PROCESO */
        $response['VIAJES_PROCESO'] = array(
            'TITULO' => 'VIAJES EN PROCESO',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-cyan',
            'SIZE' => 3,
            'DATA' => $conteo
        );

        $response['VIAJES_ACUMULADO']['DATA'] += $conteo;

        /** OBTENEMOS LOS NUEVOS USUARIOS DEL MES */
        $consulta = "SELECT COUNT(*) AS conteo FROM pasajero WHERE MONTH(Fecha) = MONTH(CURRENT_DATE()) AND YEAR(Fecha) = YEAR(CURRENT_DATE())" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['NUEVOS_USUARIOS'] = array(
            'TITULO' => 'NUEVOS USUARIOS',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-plum',
            'SIZE' => 6,
            'DATA' => $consulta->fetchAll(PDO::FETCH_ASSOC)[0]['conteo']
        );

        /** OBTENEMOS LOS NUEVOS OPERADORES DEL MES */

        $condicion = '';
        if(!empty($estado)) $condicion = " AND O.idEstado = '".$estado."'";
        if(!empty($municipio)) $condicion = " AND O.idMunicipio = '".$municipio."'";

        $consulta = "SELECT COUNT(*) AS conteo FROM operador O WHERE MONTH(created_at) = MONTH(CURRENT_DATE()) AND YEAR(created_at) = YEAR(CURRENT_DATE())" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['NUEVOS_OPERADORES'] = array(
            'TITULO' => 'NUEVOS OPERADORES',
            'TIPO' => 'CONTADOR', 
            'COLOR' => 'tile-purple',
            'SIZE' => 6,
            'DATA' => $consulta->fetchAll(PDO::FETCH_ASSOC)[0]['conteo']
        );

        /** OBTENEMOS LOS DOCUMENTOS PROXIMOS A VENCER */
        $consulta = "SELECT
                        O.id AS 'ID OPERADOR',
                        V.idVehiculo AS 'ID VEHÍCULO',
                        V.placas AS 'PLACAS',
                        CONCAT(O.nombre, ' ', O.apellidos) AS 'NOMBRE',
                        O.telefono AS 'TELÉFONO',
                        O.email AS 'CORREO',
                        IF(O.licenciaVigencia <= DATE_ADD(NOW(), INTERVAL 6 MONTH), O.licenciaVigencia, '') AS 'VIGENCIA LICENCIA',
                        IF(V.vigenciaTarjetaDeCirculacion <= DATE_ADD(NOW(), INTERVAL 6 MONTH), V.vigenciaTarjetaDeCirculacion, '') AS 'VIGENCIA TARJETA DE CIRCULACIÓN',
                        IF(V.vigenciaPolizaSeguro <= DATE_ADD(NOW(), INTERVAL 6 MONTH), V.vigenciaPolizaSeguro, '') AS 'VIGENCIA PÓLIZA DE SEGURO'
                    FROM
                        operador O
                        LEFT JOIN operadorvehiculo OV ON O.id = OV.idOperador
                        LEFT JOIN vehiculo V ON V.idVehiculo = OV.idVehiculo 
                    WHERE
                        O.operadorActivo = 1 
                        AND OV.activo = 1 
                        AND (
                            O.licenciaVigencia <= DATE_ADD(NOW(), INTERVAL 6 MONTH ) 
                            OR V.vigenciaTarjetaDeCirculacion <= DATE_ADD(NOW(), INTERVAL 6 MONTH ) 
                            OR V.vigenciaPolizaSeguro <= DATE_ADD(NOW(), INTERVAL 6 MONTH )) $condicion
                    ORDER BY
                        O.fechahora";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $response['DOCUMENTOS_VIGENCIA'] = array(
            'TITULO' => 'DOCUMENTOS PROXIMOS A VENCER',
            'TIPO' => 'TABLA', 
            'COLOR' => 'panel-warning',
            'SIZE' => 12,
            'CABECERA' => array(),
            'DATA' => array(),
        );

        $response['DOCUMENTOS_VIGENCIA']['DATA'] = $consulta->fetchAll(PDO::FETCH_ASSOC);
        
        foreach ($response as $key => $reporte) {
            if($reporte['TIPO'] != 'TABLA') continue;
            foreach ($reporte['DATA'] as $dato) {
                foreach($dato as $clave => $info) { $response[$key]['CABECERA'][] = $clave; }

                break;
            }
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/reporte', function() use ($app) {
        require("conexion.php");
        $response = array();
        
        $reporte = $app->request->get('reporte');
        $zona = $app->request->get('zona');
        $estado = $app->request->get('estado');
        $municipio = $app->request->get('municipio');

        $response['DATA'] = array(
            'CABECERAS' => array(),
            'INFO' => array()
        );

        $condicion = '';
        if(!empty($zona)) $condicion = " AND zona = '".$zona."'";

        switch($reporte) {
            case 'VIAJES_ACUMULADO':
                $consulta = "SELECT 
                            V.idOperador AS 'ID OPERADOR',
                            IF(V.nombreOperador = '' OR V.nombreOperador IS NULL, CONCAT(O.nombre, ' ', O.apellidos), V.nombreOperador) AS 'NOMBRE OPERADOR',
                            V.nombrePasajero AS 'NOMBRE PASAJERO',
                            V.origen_direccion AS origen,
                            V.destino_direccion AS destino,
                            CONCAT(V.distancia, ' KM') AS distancia,
                            CONCAT(FLOOR(duracion/60),'h ',MOD(duracion,60),'m') AS duracion,
                            CONCAT('$', FORMAT(V.costo_viaje, 2)) AS 'COSTO VIAJE',
                            CONCAT('$', FORMAT(V.tarifa_final, 2)) AS 'TARIFA FINAL',
                            CONCAT('$', FORMAT(V.iva, 2)) AS 'IVA',
                            V.porcentaje,
                            V.impuesto,
                            V.forma_pago,
                            V.tipo,
                            V.descuento,
                            V.peticion_hora AS 'PETICION POR USUARIO',
                            V.aceptar_viaje AS 'VIAJE ACEPTADO POR OPERADOR',
                            V.aviso_llegada AS 'LLEGADA DE OPERADOR',
                            V.inicio_viaje AS 'VIAJE INICIADO',
                            V.fin_viaje AS 'VIAJE FINALIZADO',
                            V.fecha,
                            V.cancelado AS 'REALIZO CANCELACION',
                            V.zona,
                            V.estatus,
                            V.placas,
                            V.modelo,
                            V.marca
                            FROM viajes_historico V
                            LEFT JOIN operador O ON O.id = V.idOperador WHERE MONTH(V.fecha) = MONTH(CURRENT_DATE()) AND YEAR(V.fecha) = YEAR(CURRENT_DATE()) AND V.estatus IN ('viaje terminado', 'cancelado')" . $condicion;

                $consulta = $conectar->prepare($consulta);
                $consulta->execute();

                $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                
                if(($data = @file_get_contents(ADMIN_APIS . 'detalle_viajes_activos?fecha=1&zona=' . urlencode($zona))) !== false) {
                    if($data = json_decode($data, true)) {
                        if(is_array($data['data'])) {
                            foreach ($data['data'] as $key => $value) {
                                $nombreOperador = '';
                                if(isset($value['datos_operador'][0]['infoOperador']['nombre'])) $nombreOperador = $value['datos_operador'][0]['infoOperador']['nombre'];
                                if(isset($value['datos_operador'][0]['infoOperador']['apellidos'])) $nombreOperador .= ' ' . $value['datos_operador'][0]['infoOperador']['apellidos'];

                                $nombrePasajero = '';
                                if(isset($value['infoPasajero']['nombre'])) $nombrePasajero = $value['infoPasajero']['nombre'];
                                if(isset($value['infoPasajero']['apellidos'])) $nombrePasajero .= ' ' . $value['infoPasajero']['apellidos'];

                                $datos[] = array(
                                    'ID OPERADOR' => (isset($value['datos_operador'][0]['idOperador'])) ? $value['datos_operador'][0]['idOperador'] : '',
                                    'NOMBRE OPERADOR' => $nombreOperador,
                                    'NOMBRE PASAJERO' => $nombrePasajero,
                                    'ORIGEN' => (isset($value['origen']['direccion'])) ? $value['origen']['direccion'] : '',
                                    'DESTINO' => (isset($value['destino']['direccion'])) ? $value['destino']['direccion'] : '',
                                    'DISTANCIA' => (isset($value['viaje']['distancia'])) ? $value['viaje']['distancia'] . ' KM': '',
                                    'DURACION' => (isset($value['viaje']['duracion'])) ? convertToHoursMins($value['viaje']['duracion']) : '',
                                    'COSTO VIAJE' => '',
                                    'TARIFA FINAL' => (isset($value['viaje']['tarifa'])) ? '$' . number_format($value['viaje']['tarifa'], 2) : 0,
                                    'IVA' => (isset($value['viaje']['cupon']['iva'])) ? '$' . number_format($value['viaje']['cupon']['iva'], 2) : 0,
                                    'PORCENTAJE' => (isset($value['viaje']['cupon']['porce'])) ? '$' . number_format($value['viaje']['cupon']['porce'], 2) : 0,
                                    'IMPUESTO' => (isset($value['viaje']['cupon']['secte'])) ? '$' . number_format($value['viaje']['cupon']['secte'], 2) : 0,
                                    'FORMA_PAGO' => (isset($value['viaje']['formaPago'])) ? $value['viaje']['formaPago'] : '',
                                    'TIPO' => (isset($value['viaje']['tipo'])) ? $value['viaje']['tipo'] : '',
                                    'DESCUENTO' => (isset($value['viaje']['cupon']['descuento'])) ? (floatval($value['viaje']['cupon']['descuento']) * 100) . '%' : '',
                                    'PETICION POR USUARIO' => (isset($value['request'])) ? $value['request'] : '',
                                    'VIAJE ACEPTADO POR OPERADOR' => (isset($value['accept_trip'])) ? $value['accept_trip'] : '',
                                    'LLEGADA DE OPERADOR' => (isset($value['driver_waiting'])) ? $value['driver_waiting'] : '',
                                    'VIAJE INICIADO' => (isset($value['driver_start'])) ? $value['driver_start'] : '',
                                    'VIAJE FINALIZADO' => (isset($value['end_trip'])) ? $value['end_trip'] : '',
                                    'FECHA' => (isset($value['date_request'])) ? date('Y-m-d', strtotime($value['date_request'])) : '',
                                    'REALIZO CANCELACION' => (isset($value['canceled'])) ? $value['canceled'] : '',
                                    'ZONA' => (isset($value['zona'])) ? $value['zona'] : '',
                                    'ESTATUS' => (isset($value['estatus'])) ? $value['estatus'] : '',
                                    'PLACAS' => (isset($value['infoVehiculo']['placas'])) ? $value['infoVehiculo']['placas'] : '',
                                    'MODELO' => (isset($value['infoVehiculo']['modelo'])) ? $value['infoVehiculo']['modelo'] : '',
                                    'MARCA' => (isset($value['infoVehiculo']['marca'])) ? $value['infoVehiculo']['marca'] : ''
                                );
                            }
                        }
                    }
                }

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('VIAJES_ACUMULADO', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
            case 'VIAJES_TERMINADOS':
                $consulta = "SELECT 
                            V.idOperador AS 'ID OPERADOR',
                            IF(V.nombreOperador = '' OR V.nombreOperador IS NULL, CONCAT(O.nombre, ' ', O.apellidos), V.nombreOperador) AS 'NOMBRE OPERADOR',
                            V.nombrePasajero AS 'NOMBRE PASAJERO',
                            V.origen_direccion AS origen,
                            V.destino_direccion AS destino,
                            CONCAT(V.distancia, ' KM') AS distancia,
                            CONCAT(FLOOR(duracion/60),'h ',MOD(duracion,60),'m') AS duracion,
                            CONCAT('$', FORMAT(V.costo_viaje, 2)) AS 'COSTO VIAJE',
                            CONCAT('$', FORMAT(V.tarifa_final, 2)) AS 'TARIFA FINAL',
                            CONCAT('$', FORMAT(V.iva, 2)) AS 'IVA',
                            V.porcentaje,
                            V.impuesto,
                            V.forma_pago,
                            V.tipo,
                            V.descuento,
                            V.peticion_hora AS 'PETICION POR USUARIO',
                            V.aceptar_viaje AS 'VIAJE ACEPTADO POR OPERADOR',
                            V.aviso_llegada AS 'LLEGADA DE OPERADOR',
                            V.inicio_viaje AS 'VIAJE INICIADO',
                            V.fin_viaje AS 'VIAJE FINALIZADO',
                            V.fecha,
                            V.cancelado AS 'REALIZO CANCELACION',
                            V.zona,
                            V.estatus,
                            V.placas,
                            V.modelo,
                            V.marca
                            FROM viajes_historico V
                            LEFT JOIN operador O ON O.id = V.idOperador WHERE MONTH(V.fecha) = MONTH(CURRENT_DATE()) AND YEAR(V.fecha) = YEAR(CURRENT_DATE()) AND V.estatus = 'viaje terminado'" . $condicion;

                $consulta = $conectar->prepare($consulta);
                $consulta->execute();

                $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('VIAJES_TERMINADOS', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
            case 'VIAJES_CANCELADOS':
                $consulta = "SELECT 
                            V.idOperador AS 'ID OPERADOR',
                            IF(V.nombreOperador = '' OR V.nombreOperador IS NULL, CONCAT(O.nombre, ' ', O.apellidos), V.nombreOperador) AS 'NOMBRE OPERADOR',
                            V.nombrePasajero AS 'NOMBRE PASAJERO',
                            V.origen_direccion AS origen,
                            V.destino_direccion AS destino,
                            CONCAT(V.distancia, ' KM') AS distancia,
                            CONCAT(FLOOR(duracion/60),'h ',MOD(duracion,60),'m') AS duracion,
                            CONCAT('$', FORMAT(V.costo_viaje, 2)) AS 'COSTO VIAJE',
                            CONCAT('$', FORMAT(V.tarifa_final, 2)) AS 'TARIFA FINAL',
                            CONCAT('$', FORMAT(V.iva, 2)) AS 'IVA',
                            V.porcentaje,
                            V.impuesto,
                            V.forma_pago,
                            V.tipo,
                            V.descuento,
                            V.peticion_hora AS 'PETICION POR USUARIO',
                            V.aceptar_viaje AS 'VIAJE ACEPTADO POR OPERADOR',
                            V.aviso_llegada AS 'LLEGADA DE OPERADOR',
                            V.inicio_viaje AS 'VIAJE INICIADO',
                            V.fin_viaje AS 'VIAJE FINALIZADO',
                            V.fecha,
                            V.cancelado AS 'REALIZO CANCELACION',
                            V.zona,
                            V.estatus,
                            V.placas,
                            V.modelo,
                            V.marca
                            FROM viajes_historico V
                            LEFT JOIN operador O ON O.id = V.idOperador WHERE MONTH(V.fecha) = MONTH(CURRENT_DATE()) AND YEAR(V.fecha) = YEAR(CURRENT_DATE()) AND V.estatus = 'cancelado'" . $condicion;

                $consulta = $conectar->prepare($consulta);
                $consulta->execute();

                $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('VIAJES_CANCELADOS', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
            case 'VIAJES_PROCESO':
                $datos = [];
                if(($data = @file_get_contents(ADMIN_APIS . 'detalle_viajes_activos?fecha=1&zona=' . urlencode($zona))) !== false) {
                    if($data = json_decode($data, true)) {
                        if(is_array($data['data'])) {
                            foreach ($data['data'] as $key => $value) {
                                $nombreOperador = '';
                                if(isset($value['datos_operador'][0]['infoOperador']['nombre'])) $nombreOperador = $value['datos_operador'][0]['infoOperador']['nombre'];
                                if(isset($value['datos_operador'][0]['infoOperador']['apellidos'])) $nombreOperador .= ' ' . $value['datos_operador'][0]['infoOperador']['apellidos'];

                                $nombrePasajero = '';
                                if(isset($value['infoPasajero']['nombre'])) $nombrePasajero = $value['infoPasajero']['nombre'];
                                if(isset($value['infoPasajero']['apellidos'])) $nombrePasajero .= ' ' . $value['infoPasajero']['apellidos'];

                                $datos[] = array(
                                    'ID OPERADOR' => (isset($value['datos_operador'][0]['idOperador'])) ? $value['datos_operador'][0]['idOperador'] : '',
                                    'NOMBRE OPERADOR' => $nombreOperador,
                                    'NOMBRE PASAJERO' => $nombrePasajero,
                                    'ORIGEN' => (isset($value['origen']['direccion'])) ? $value['origen']['direccion'] : '',
                                    'DESTINO' => (isset($value['destino']['direccion'])) ? $value['destino']['direccion'] : '',
                                    'DISTANCIA' => (isset($value['viaje']['distancia'])) ? $value['viaje']['distancia'] . ' KM': '',
                                    'DURACION' => (isset($value['viaje']['duracion'])) ? convertToHoursMins($value['viaje']['duracion']) : '',
                                    'COSTO VIAJE' => '',
                                    'TARIFA FINAL' => (isset($value['viaje']['tarifa'])) ? '$' . number_format($value['viaje']['tarifa'], 2) : 0,
                                    'IVA' => (isset($value['viaje']['cupon']['iva'])) ? '$' . number_format($value['viaje']['cupon']['iva'], 2) : 0,
                                    'PORCENTAJE' => (isset($value['viaje']['cupon']['porce'])) ? '$' . number_format($value['viaje']['cupon']['porce'], 2) : 0,
                                    'IMPUESTO' => (isset($value['viaje']['cupon']['secte'])) ? '$' . number_format($value['viaje']['cupon']['secte'], 2) : 0,
                                    'FORMA_PAGO' => (isset($value['viaje']['formaPago'])) ? $value['viaje']['formaPago'] : '',
                                    'TIPO' => (isset($value['viaje']['tipo'])) ? $value['viaje']['tipo'] : '',
                                    'DESCUENTO' => (isset($value['viaje']['cupon']['descuento'])) ? (floatval($value['viaje']['cupon']['descuento']) * 100) . '%' : '',
                                    'PETICION POR USUARIO' => (isset($value['request'])) ? $value['request'] : '',
                                    'VIAJE ACEPTADO POR OPERADOR' => (isset($value['accept_trip'])) ? $value['accept_trip'] : '',
                                    'LLEGADA DE OPERADOR' => (isset($value['driver_waiting'])) ? $value['driver_waiting'] : '',
                                    'VIAJE INICIADO' => (isset($value['driver_start'])) ? $value['driver_start'] : '',
                                    'VIAJE FINALIZADO' => (isset($value['end_trip'])) ? $value['end_trip'] : '',
                                    'FECHA' => (isset($value['date_request'])) ? date('Y-m-d', strtotime($value['date_request'])) : '',
                                    'REALIZO CANCELACION' => (isset($value['canceled'])) ? $value['canceled'] : '',
                                    'ZONA' => (isset($value['zona'])) ? $value['zona'] : '',
                                    'ESTATUS' => (isset($value['estatus'])) ? $value['estatus'] : '',
                                    'PLACAS' => (isset($value['infoVehiculo']['placas'])) ? $value['infoVehiculo']['placas'] : '',
                                    'MODELO' => (isset($value['infoVehiculo']['modelo'])) ? $value['infoVehiculo']['modelo'] : '',
                                    'MARCA' => (isset($value['infoVehiculo']['marca'])) ? $value['infoVehiculo']['marca'] : ''
                                );
                            }
                        }
                    }
                }

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('VIAJES_PROCESO', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
            case 'NUEVOS_USUARIOS':
                $consulta = "SELECT P.Nombre, P.Apellido, P.Email, P.Telefono, P.Fecha FROM pasajero P WHERE MONTH(Fecha) = MONTH(CURRENT_DATE()) AND YEAR(Fecha) = YEAR(CURRENT_DATE())" . $condicion;
                $consulta = $conectar->prepare($consulta);
                $consulta->execute();

                $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('NUEVOS_USUARIOS', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
            case 'NUEVOS_OPERADORES':
                $condicion = '';
                if(!empty($estado)) $condicion = " AND O.idEstado = '".$estado."'";
                if(!empty($municipio)) $condicion = " AND O.idMunicipio = '".$municipio."'";

                $consulta = "SELECT O.id, O.cumpleahos as 'Fecha Nacimiento', CONCAT(O.nombre, ' ', O.apellidos) AS 'Nombre', O.direccion, O.email, O.telefono FROM operador O WHERE MONTH(created_at) = MONTH(CURRENT_DATE()) AND YEAR(created_at) = YEAR(CURRENT_DATE())" . $condicion;
                $consulta = $conectar->prepare($consulta);
                $consulta->execute();

                $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

                $cabeceras = [];
                foreach($datos as $dato) {
                    foreach($dato as $key => $info) { $cabeceras[] = $key; }

                    break;
                }

                $excel = new CrearExcel('NUEVOS_USUARIOS', $cabeceras, $datos);
                $response['file'] = $excel->CreateDoc();
            break;
        }
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

    function convertToHoursMins($time, $format = '%02d horas %02d minutos') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

?>