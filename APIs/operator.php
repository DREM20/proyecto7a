<?php 

    session_start();
    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");
    include('helpers/permisos.php');

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');

    $app->get('/tbody', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $permisos = new Permisos($conectar);

        $condicion = '';

        $ver_info = false;
        if($permisos->getPermiso('OPERADOR_VER_ESPECIFICOS')) { $condicion = ' WHERE visible = 1'; $ver_info = true; }
        if($permisos->getPermiso('OPERADOR_VER_TODOS')) { $condicion = ''; $ver_info = true; };

        $nombre = $app->request->get('search_nombre');
        $telefono = $app->request->get('search_telefono');
        $id = $app->request->get('search_id');
        $nacimiento = $app->request->get('search_date');
        $genero = $app->request->get('search_genero');
        $estado = $app->request->get('search_estado');
        $municipio = $app->request->get('search_municipio');
        $limite = $app->request->get('search_limite');

        if(!empty($nombre)) { if(empty($condicion)) $condicion = " WHERE nombre like '%" . $nombre . "%'"; else $condicion .= " AND nombre like '%" . $nombre . "%'"; }
        if(!empty($telefono)) { if(empty($condicion)) $condicion = " WHERE telefono = '" . trim($telefono) . "'"; else $condicion .= " AND telefono = '" . trim($telefono) . "'"; }
        if(!empty($id)) { if(empty($condicion)) $condicion = " WHERE id = '" . trim($id) . "'"; else $condicion .= " AND id = '" . trim($id) . "'"; }
        if(!empty($nacimiento)) { if(empty($condicion)) $condicion = " WHERE cumpleahos = '" . date('Y-m-d', strtotime(str_replace('/', '-', $nacimiento))) . "'"; else $condicion .= " AND cumpleahos = '" . date('Y-m-d', strtotime(str_replace('/', '-', $nacimiento))) . "'"; }
        if(!empty($genero)) { if(empty($condicion)) $condicion = " WHERE genero = '" . $genero . "'"; else $condicion .= " AND genero = '" . $genero . "'"; }
        if(!empty($estado)) { if(empty($condicion)) $condicion = " WHERE idEstado = '" . $estado . "'"; else $condicion .= " AND idEstado = '" . $estado . "'"; }
        if(!empty($municipio)) { if(empty($condicion)) $condicion = " WHERE idMunicipio = '" . $municipio . "'"; else $condicion .= " AND idMunicipio = '" . $municipio . "'"; }
        if(!empty($limite)) { $condicion .= ' LIMIT ' . $limite; }

        $response['info'] = array();
        if($ver_info) {
            $consulta = "SELECT 
                        id,
                        CONCAT(nombre, ' ', apellidos) AS nombre, 
                        email,
                        cumpleahos,
                        genero,
                        telefono,
                        operadorActivo,
                        foto,
                        noIFE,
                        comision,
                        visible
                        FROM operador" . $condicion;

            $consulta = $conectar->prepare($consulta);
            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $response['info'] = $datos;
        }

        $response['options'] = array(
            'asignar' => $permisos->getPermiso('OPERADOR_ASIGNAR'),
            'crear' => $permisos->getPermiso('OPERADOR_CREAR'),
            'ver' => $permisos->getPermiso('OPERADOR_VER'),
            'editar' => $permisos->getPermiso('OPERADOR_EDITAR'),
            'doctos' => $permisos->getPermiso('OPERADOR_DOCUMENTOS'),
            'activar' => $permisos->getPermiso('OPERADOR_DESACTIVAR'),
            'recuperacion' => $permisos->getPermiso('OPERADOR_CLAVE'),
            'vehiculos' => $permisos->getPermiso('OPERADOR_VEHICULOS'),
        );

        if(isset($conectar)) unset($conectar);        

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save', function() use ($app) {
        require_once("conexion.php");
        $response = array();
        
        $photo = (isset($_FILES['operator_picture'])) ? $_FILES['operator_picture'] : '' ;
        $data = json_decode($app->request->post('data'));

        /** GUARDAMOS LA INFORMACIÓN EN BASE DE DATOS */
        $consulta = "INSERT INTO operador (id, cumpleahos, nombre, apellidos, direccion, genero, email, contrasena, telefono,
                    telefonoEmergencia, contactoEmergencia, extension, cuenta, licencia, licenciaVigencia, idEstado, idMunicipio, 
                    idUsuario, fechahora, operadorActivo, comentarios, noIFE, comision, owner, rfc, stmEstatus)
                    VALUES (DEFAULT, :cumpleahos, :nombre, :apellidos, :direccion, :genero, :email, :contrasena, :telefono, 
                    :telefonoEmergencia, :contactoEmergencia, :extension, :cuenta, :licencia, :licenciaVigencia, :idEstado, :idMunicipio,
                    :idUsuario, :fechahora, :operadorActivo, :comentarios, :noIFE, :comision, :owner, :rfc, 0)";

        $consulta = $conectar->prepare($consulta); 

        $consulta->bindValue(':cumpleahos'           ,date('Y-m-d', strtotime($data->operator_birth)));
        $consulta->bindValue(':nombre'               ,$data->operator_name);
        $consulta->bindValue(':apellidos'            ,$data->operator_lastname);
        $consulta->bindValue(':direccion'            ,$data->operator_direction);
        $consulta->bindValue(':genero'               ,$data->operator_gender);
        $consulta->bindValue(':email'                ,$data->operator_email);
        $consulta->bindValue(':contrasena'           ,MD5($data->operator_password));
        $consulta->bindValue(':telefono'             ,$data->operator_phone);
        $consulta->bindValue(':telefonoEmergencia'   ,$data->operator_emergencyphone);
        $consulta->bindValue(':contactoEmergencia'   ,$data->operator_emergencycontact);
        $consulta->bindValue(':extension'            ,$data->operator_extension);
        $consulta->bindValue(':cuenta'               ,$data->operator_account);
        $consulta->bindValue(':licencia'             ,$data->operator_license);
        $consulta->bindValue(':licenciaVigencia'     ,date('Y-m-d', strtotime($data->operator_validlicense)));
        $consulta->bindValue(':idEstado'             ,$data->operator_city);
        $consulta->bindValue(':idMunicipio'          ,$data->operator_town);
        $consulta->bindValue(':idUsuario'            ,$data->operator_user);
        $consulta->bindValue(':fechahora'            ,date('Y-m-d H:i:s'));
        $consulta->bindValue(':operadorActivo'       ,$data->operator_active);
        $consulta->bindValue(':comentarios'          ,$data->operator_comments);
        $consulta->bindValue(':noIFE'                ,$data->operator_ine);
        $consulta->bindValue(':comision'             ,$data->operator_commission);
        $consulta->bindValue(':owner'                ,$data->operator_owner);
        $consulta->bindValue(':rfc'                  ,$data->operator_rfc);

        if($consulta->execute()) {
            $id = $conectar->lastInsertId();

            /** PROCESAMOS IMAGEN EN CASO QUE EXISTA */
            if(!empty($photo)) {
                $carpeta = '../img';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Operadores';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/operador' . $id;
                if(!is_dir($carpeta)) mkdir($carpeta);

                $extension = strtolower(pathinfo($photo['name'], PATHINFO_EXTENSION));
                $nombre = 'Operador' . $id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(move_uploaded_file($photo['tmp_name'], $ubicacion)) {
                    $ubicacion = 'img/Operadores/operador' . $id . '/Operador' . $id . '.' . $extension;
                    $consulta = "UPDATE operador SET foto = :foto WHERE id = :id";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindParam(':foto', $ubicacion);
                    $consulta->bindParam(':id', $id);

                    if($consulta->execute()) $response['image'] = true;
                    else $response['image'] = false;
                }
                else $response['image'] = false;
            }

            $response['code'] = 200;
        }
        else {
            $response['code'] = 500;
            $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->get('/load_info', function() use ($app) {
        require_once("conexion.php");   
        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT 
                    O.cumpleahos, O.nombre, O.apellidos, O.direccion, O.genero, O.email, O.telefono,
                    O.telefonoEmergencia, O.contactoEmergencia, O.extension, O.cuenta, O.licencia, O.licenciaVigencia, 
                    O.fechahora, O.operadorActivo, O.comentarios, O.noIFE, O.comision, O.owner, O.rfc, O.foto, E.estado, 
                    M.municipio, U.username, (SELECT COUNT(*) FROM viajes V WHERE V.idoperador = O.id) AS viajes
                    FROM operador O 
                    LEFT JOIN estado E ON O.idEstado = E.idEstado 
                    LEFT JOIN municipio M ON M.idMunicipio = O.idMunicipio AND M.idEstado = O.idEstado
                    LEFT JOIN tb_users U ON U.id = O.idUsuario
                    WHERE O.id = :id";
        
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);
        $consulta->execute(); 
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) {
            $response['info'] = $dato;

            if(!file_exists('../' . $response['info']['foto'])) $response['info']['foto'] = '';

            $consulta = "SELECT idDocumento, documento, UPPER(codigo) AS codigo, '' AS docto, 'M' as main,
                        0 as valido, 0 as rechazo, '' as motivorechazo
                        FROM documento WHERE tipo = 'O' AND obligatorio = 1 ORDER BY documento";
            $consulta = $conectar->prepare($consulta);
            $consulta->execute();
            $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $consulta = "SELECT idDocumento, extension, valido, rechazo, motivorechazo FROM operadordocumento WHERE idOperador = :id";
            $consulta = $conectar->prepare($consulta);
            $consulta->bindValue(':id', $id);
            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($datos as $dato) {
                foreach($response['doctos'] as $key => $docto) {
                    if($docto['idDocumento'] == $dato['idDocumento']) {
                        $response['doctos'][$key]['docto'] = $dato['extension'];
                        $response['doctos'][$key]['valido'] = $dato['valido'];
                        $response['doctos'][$key]['rechazo'] = $dato['rechazo'];
                        $response['doctos'][$key]['motivorechazo'] = $dato['motivorechazo'];
                    }
                }
            }

            /** OBTENEMOS LOS VEHICULOS ASIGNADOS */
            $consulta = "SELECT V.idVehiculo, V.serie, V.aho, V.placas, M.marca, MO.modelo, OV.activo, OV.idOperador, OV.idOwner
                        FROM operadorvehiculo OV 
                        LEFT JOIN vehiculo V ON OV.idVehiculo = V.idVehiculo 
                        LEFT JOIN marca M ON M.idMarca = V.idMarca 
                        LEFT JOIN modelo MO ON MO.idModelo = V.idModelo AND MO.idMarca = V.idMarca 
                        WHERE OV.idOperador = :idOperador OR OV.idOwner = :idOperador";
            
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':idOperador', $id);

            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $response['vehiculos'] = array();
            foreach($datos as $key => $dato) {

                $dato['conduce'] = ($dato['idOperador'] == $id) ? '1' : '0';
                $dato['duenio'] = ($dato['idOwner'] == $id) ? '1' : '0';

                $response['vehiculos'][] = $dato;
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->post('/update_operator', function() use($app) {
        require_once("conexion.php");
        $response = array();
        
        $photo = (isset($_FILES['operator_picture'])) ? $_FILES['operator_picture'] : '' ;
        $data = json_decode($app->request->post('data'));

        /** GUARDAMOS LA INFORMACIÓN EN BASE DE DATOS */
        $consulta = "UPDATE operador SET cumpleahos = :cumpleahos, nombre = :nombre, apellidos = :apellidos, direccion = :direccion, genero = :genero,
                    email = :email, contrasena = :contrasena, telefono = :telefono, telefonoEmergencia = :telefonoEmergencia, contactoEmergencia = :contactoEmergencia,
                    extension = :extension, cuenta = :cuenta, licencia = :licencia, licenciaVigencia = :licenciaVigencia, idEstado = :idEstado, idMunicipio = :idMunicipio,
                    idUsuario = :idUsuario, fechahora = :fechahora, operadorActivo = :operadorActivo, comentarios = :comentarios, noIFE = :noIFE, comision = :comision,
                    owner = :owner, rfc = :rfc WHERE id = :id";

        $consulta = $conectar->prepare($consulta); 

        $consulta->bindValue(':cumpleahos'           ,date('Y-m-d', strtotime($data->operator_birth)));
        $consulta->bindValue(':nombre'               ,($data->operator_name));
        $consulta->bindValue(':apellidos'            ,($data->operator_lastname));
        $consulta->bindValue(':direccion'            ,($data->operator_direction));
        $consulta->bindValue(':genero'               ,($data->operator_gender));
        $consulta->bindValue(':email'                ,($data->operator_email));
        $consulta->bindValue(':contrasena'           ,MD5(($data->operator_password)));
        $consulta->bindValue(':telefono'             ,($data->operator_phone));
        $consulta->bindValue(':telefonoEmergencia'   ,($data->operator_emergencyphone));
        $consulta->bindValue(':contactoEmergencia'   ,($data->operator_emergencycontact));
        $consulta->bindValue(':extension'            ,($data->operator_extension));
        $consulta->bindValue(':cuenta'               ,($data->operator_account));
        $consulta->bindValue(':licencia'             ,($data->operator_license));
        $consulta->bindValue(':licenciaVigencia'     ,date('Y-m-d', strtotime($data->operator_validlicense)));
        $consulta->bindValue(':idEstado'             ,($data->operator_city));
        $consulta->bindValue(':idMunicipio'          ,($data->operator_town));
        $consulta->bindValue(':idUsuario'            ,($data->operator_user));
        $consulta->bindValue(':fechahora'            ,date('Y-m-d H:i:s'));
        $consulta->bindValue(':operadorActivo'       ,($data->operator_active));
        $consulta->bindValue(':comentarios'          ,($data->operator_comments));
        $consulta->bindValue(':noIFE'                ,($data->operator_ine));
        $consulta->bindValue(':comision'             ,($data->operator_commission));
        $consulta->bindValue(':owner'                ,($data->operator_owner));
        $consulta->bindValue(':rfc'                  ,($data->operator_rfc));
        $consulta->bindValue(':id'                   ,($data->id));

        if($consulta->execute()) {
            /** PROCESAMOS IMAGEN EN CASO QUE EXISTA */
            if(!empty($photo)) {
                $carpeta = '../img';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/Operadores';
                if(!is_dir($carpeta)) mkdir($carpeta);
                $carpeta = $carpeta . '/operador' . $data->id;
                if(!is_dir($carpeta)) mkdir($carpeta);

                $extension = strtolower(pathinfo($photo['name'], PATHINFO_EXTENSION));
                $nombre = 'Operador' . $data->id . '.' . $extension;
                $ubicacion = $carpeta . '/' . $nombre;

                if(move_uploaded_file($photo['tmp_name'], $ubicacion)) {
                    $ubicacion = 'img/Operadores/operador' . $data->id . '/Operador' . $data->id . '.' . $extension;
                    $consulta = "UPDATE operador SET foto = :foto WHERE id = :id";
                    $consulta = $conectar->prepare($consulta);
                    $consulta->bindParam(':foto', $ubicacion);
                    $consulta->bindParam(':id', $data->id);

                    if($consulta->execute()) $response['image'] = true;
                    else $response['image'] = false;
                }
                else $response['image'] = false;
            }

            $response['code'] = 200;
        }
        else {
            $response['code'] = 500;
            $response['msg'] = 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico';
            $response['query'] = $consulta->errorInfo();
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->delete('/status', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->delete('id');
        $status = $app->request->delete('status');

        $consulta = "UPDATE operador SET operadorActivo = :status WHERE id = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':status', $status);
        $consulta->bindParam(':id', $id);

        if($consulta->execute()) $response['code'] = 200;
        else $response['code'] = 500;

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_docs', function() use($app) {
        require_once("conexion.php");

        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT idDocumento, documento, UPPER(codigo) AS codigo, '' AS docto, 'M' as main,
                    0 as valido, 0 as rechazo, '' as motivorechazo
                    FROM documento WHERE tipo = 'O' AND obligatorio = 1 ORDER BY documento";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $consulta = "SELECT idDocumento, extension, valido, rechazo, motivorechazo FROM operadordocumento WHERE idOperador = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':id', $id);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) {
            foreach($response['doctos'] as $key => $docto) {
                if($docto['idDocumento'] == $dato['idDocumento']) {
                    $response['doctos'][$key]['docto'] = $dato['extension'];
                    $response['doctos'][$key]['valido'] = $dato['valido'];
                    $response['doctos'][$key]['rechazo'] = $dato['rechazo'];
                    $response['doctos'][$key]['motivorechazo'] = $dato['motivorechazo'];
                    $response['doctos'][$key]['informacion'] = array();
                }
            }
        }


        // $consulta = "SELECT idDocumento, documento, codigo, '' AS docto, 'M' as main FROM documento WHERE tipo = 'O' AND obligatorio = 1 ORDER BY documento";
        // $consulta = $conectar->prepare($consulta);
        // $consulta->execute();
        // $response['doctos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

        // $consulta = "SELECT idDocumento, extension FROM operadordocumento WHERE idOperador = :id";
        // $consulta = $conectar->prepare($consulta);
        // $consulta->bindValue(':id', $id);
        // $consulta->execute();
        // $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        // foreach($datos as $dato) {
        //     foreach($response['doctos'] as $key => $docto) {
        //         if($docto['idDocumento'] == $dato['idDocumento']) $response['doctos'][$key]['docto'] = $dato['extension'];
        //     }
        // }

        // /**OBTENEMOS LA FOTO */
        // $consulta = "SELECT foto FROM operador WHERE id = :id";
        // $consulta = $conectar->prepare($consulta);
        // $consulta->bindValue(':id', $id);
        // $consulta->execute();
        // $foto = $consulta->fetch(PDO::FETCH_ASSOC);

        // $response['doctos'][] = array(
        //     'idDocumento' => null,
        //     'documento' => 'FOTO DE OPERADOR',
        //     'codigo' => 'foto',
        //     'docto' => $foto['foto'],
        //     'main' => 'G'
        // );

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    }); 

    $app->post('/save_doc', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->post('id');
        $docto = $app->request->post('docto');
        $main = $app->request->post('main');
        $codigo = $app->request->post('codigo');
        $file = (isset($_FILES['file'])) ? $_FILES['file'] : '' ;

        if(!empty($file)) {
            $carpeta = '../img';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/Operadores';
            if(!is_dir($carpeta)) mkdir($carpeta);
            $carpeta = $carpeta . '/operador' . $id;
            if(!is_dir($carpeta)) mkdir($carpeta);

            $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

            $file_type = explode('_', $docto)[2];
            
            switch($main) {
                case 'G':
                    $nombre = 'Operador' . $id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;

                    if(move_uploaded_file($file['tmp_name'], $ubicacion)) {
                        $ubicacion = 'img/Operadores/operador' . $id . '/Operador' . $id . '.' . $extension;
                        $consulta = "UPDATE operador SET foto = :foto WHERE id = :id";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(':foto', $ubicacion);
                        $consulta->bindValue(':id', $id);
        
                        if($consulta->execute()) $response['image'] = true;
                        else $response['image'] = false; 
                    }
                    break;
                case 'M':
                    $nombre = $codigo . $id . '.' . $extension;
                    $ubicacion = $carpeta . '/' . $nombre;

                    if(move_uploaded_file($file['tmp_name'], $ubicacion)) {
                        $consulta = "SELECT idDocumento FROM operadordocumento WHERE idOperador = :operador AND idDocumento = :documento";
                        $consulta = $conectar->prepare($consulta);
                        $consulta->bindValue(":operador", $id);
                        $consulta->bindValue(":documento", $file_type);
                        $consulta->execute();

                        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
                        $ubicacion = 'img/Operadores/operador' . $id . '/' . $codigo . $id . '.' . $extension;
                        if(count($datos) > 0) {
                            $consulta = "UPDATE operadordocumento SET extension = :file WHERE idOperador = :operador AND idDocumento = :documento";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':file', $ubicacion);
                            $consulta->bindValue(':operador', $id);
                            $consulta->bindValue(':documento', $file_type);
            
                            if($consulta->execute()) $response['image'] = true;
                            else $response['image'] = false; 
                        }
                        else {
                            $consulta = "INSERT INTO operadordocumento (idOperador, idDocumento, extension)
                                        VALUES (:id, :idfile, :file)";
                            $consulta = $conectar->prepare($consulta);
                            $consulta->bindValue(':id', $id);
                            $consulta->bindValue(':idfile', $file_type);
                            $consulta->bindValue(':file', $ubicacion);
            
                            if($consulta->execute()) $response['image'] = true;
                            else $response['image'] = false; 
                        }
                    }
                    break;
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->put('/update_visible', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $estado = $app->request->put('estado');
        $id = $app->request->put('id');

        $consulta = "UPDATE operador SET visible = :visible WHERE id = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':visible', $estado);
        $consulta->bindParam(':id', $id);

        if($consulta->execute()) $response['code'] = 200;
        else $response['code'] = 500;

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_telefono', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT telefono, nombre, apellidos FROM operador WHERE id = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
        $response['info'] = array();
        if(count($datos) > 0) {
            $response['info'] = $datos[0];
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/recupera_contra', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->post('id');
        $valor = $app->request->post('valor');

        $consulta = "UPDATE operador SET telefono = :telefono, contrasena = :contra WHERE id = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':telefono',   $valor);
        $consulta->bindValue(':contra',     md5($valor));
        $consulta->bindValue(':id',         $id);

        if($consulta->execute()) $response['code'] = 200;
        else $response['code'] = 500;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_vehiculos', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $id = $app->request->get('id');

        $consulta = "SELECT V.idVehiculo, V.serie, V.aho, V.placas, M.marca, MO.modelo, OV.activo
                    FROM operadorvehiculo OV 
                    LEFT JOIN vehiculo V ON OV.idVehiculo = V.idVehiculo 
                    LEFT JOIN marca M ON M.idMarca = V.idMarca 
                    LEFT JOIN modelo MO ON MO.idModelo = V.idModelo AND MO.idMarca = V.idMarca 
                    WHERE OV.idOperador = :idOperador";
        
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':idOperador', $id);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['info'] = $datos;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/activa_vehiculo', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $estado = $app->request->post('estado');
        $vehiculo = $app->request->post('vehiculo');
        $operador = $app->request->post('operador');

        $consulta = "UPDATE operadorvehiculo SET activo = 0 WHERE idOperador = :operador AND idVehiculo != :vehiculo";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':operador', $operador);
        $consulta->bindParam(':vehiculo', $vehiculo);

        $consulta->execute();

        $consulta = "UPDATE operadorvehiculo SET activo = 0 WHERE idOperador != :operador AND idVehiculo = :vehiculo";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':operador', $operador);
        $consulta->bindParam(':vehiculo', $vehiculo);

        $consulta->execute();

        $consulta = "UPDATE operadorvehiculo SET activo = :estado WHERE idOperador = :operador AND idVehiculo = :vehiculo";
        $consulta = $conectar->prepare($consulta);

        $consulta->bindParam(':estado', $estado);
        $consulta->bindParam(':operador', $operador);
        $consulta->bindParam(':vehiculo', $vehiculo);

        if($consulta->execute()) $response['code'] = 200;
        else $response['code'] = 500;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/get_vehiculo', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $placas = $app->request->get('placas');
        $id = $app->request->get('id');

        $consulta = "SELECT V.idVehiculo, V.serie, V.aho, V.placas, M.marca, MO.modelo, OV.idOperador
                    FROM vehiculo V
                    LEFT JOIN operadorvehiculo OV  ON OV.idVehiculo = V.idVehiculo 
                    LEFT JOIN marca M ON M.idMarca = V.idMarca 
                    LEFT JOIN modelo MO ON MO.idModelo = V.idModelo AND MO.idMarca = V.idMarca 
                    WHERE V.placas = :placas";
        
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':placas', $placas);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if(count($datos) > 0) {
            $existe = false;
            foreach($datos as $dato) { if($dato['idOperador'] == $id) $existe = true; $response['code'] = '401'; }

            if(!$existe) {
                $response['code'] = '200';
                $response['info'] = $datos[0];
            }
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save_nuevo_vehiculo', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $vehiculo = $app->request->post('vehiculo');
        $id = $app->request->post('id');

        $consulta = "SELECT idOwner FROM operadorvehiculo WHERE idOwner = :idOwner AND idVehiculo = :idVehiculo";
        $consulta = $conectar->prepare($consulta);

        $consulta->bindParam(':idOwner', $id);
        $consulta->bindParam(':idVehiculo', $vehiculo);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        if(count($datos) == 0) $consulta = "INSERT INTO operadorvehiculo (idOperador, idVehiculo, activo) VALUES (:idOperador, :idVehiculo, 0)";
        else $consulta = "UPDATE operadorvehiculo SET idOperador = :idOperador WHERE idOwner = :idOperador AND idVehiculo = :idVehiculo";

        $consulta = $conectar->prepare($consulta);

        $consulta->bindParam(':idOperador', $id);
        $consulta->bindParam(':idVehiculo', $vehiculo);

        if($consulta->execute()) $response['code'] = 200;
        else $response['code'] = 500;
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>