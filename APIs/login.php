<?php
    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');


    $app->get('/', function() use ($app) {

        $app->response->setBody(json_encode("API Ok"));
    });

    $app->post('/login', function() use ($app) {
        require_once "conexion.php";
        $response = array();

        $email = $app->request->post('email');
        $pass = md5($app->request->post('pass'));

        /** VERIFICAMOS SI EL USUARIO EXISTE */
        $consulta = "SELECT U.username, U.email, U.avatar, U.active, G.group_id, G.name
                    FROM tb_usuarios U
                    LEFT JOIN tb_groups G ON G.group_id = U.group_id
                    WHERE email = :email AND password = :password";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':email', $email);
        $consulta->bindParam(':password', $pass);
        $consulta->execute(); 

        $row = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if(count($row) == 0) {
            $response['code'] = 500;
            $response['msg'] = 'Verifique Usuario/Contraseña';
        }
        else {
            session_start();
            $_SESSION = array(
                'LOGGED' => true,
                'NAME' => $row[0]['username'],
                'EMAIL' => $row[0]['email'],
                'AVATAR' => $row[0]['avatar'],
                'ACTIVE' => $row[0]['active'],
                'GROUP_ID' => $row[0]['group_id'],
                'GROUP_NAME' => $row[0]['name'],
            );

            $response['code'] = 200;
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/login_operador', function() use ($app) {
        require_once "conexion.php";
        $response = array();

        $email = $app->request->post('email');
        $pass = md5($app->request->post('pass'));

        /** VERIFICAMOS SI EL USUARIO EXISTE */
        $consulta = "SELECT O.nombre, O.apellidos, O.email, O.telefono, O.id,
                    O.correo_valido
                    FROM operador O
                    WHERE email = :email AND contrasena = :contrasena";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':email', $email);
        $consulta->bindParam(':contrasena', $pass);
        $consulta->execute(); 

        $row = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if(count($row) == 0) {
            $response['code'] = 500;
            $response['msg'] = 'Verifique Usuario/Contraseña';
        }
        else {
            if($row[0]['correo_valido'] == 0) {
                $response['code'] = 500;
                $response['msg'] = 'Correo Electrónico sin Verificar';
            }
            else {
                session_start();

                $_SESSION = array(
                    'logged_operador' => true,
                    'operador' => array(
                        'correo' => $row[0]['email'],
                        'telefono' => $row[0]['telefono'],
                        'id' => $row[0]['id']
                    )
                );

                $response['code'] = 200;
            }
        }

        if(isset($conectar)) {
            unset($conectar);
        }

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>