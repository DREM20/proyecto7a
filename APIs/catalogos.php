<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("../assets/Slim/Slim.php");
    include('helpers/permisos.php');

    session_start();

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/operadores', function() use($app) {
        require_once('conexion.php'); 
        $response = array(); 

        $consulta = "SELECT id, CONCAT(nombre, ' ', apellidos) as nombre FROM operador";

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['item'] = array();
        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['id'], 'nombre' => $dato['nombre']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/estados', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT idEstado, estado FROM estado WHERE activo = 1";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['item'] = array();
        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idEstado'], 'nombre' => $dato['estado']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });
    
    $app->get('/municipioByEstado', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $estado = $app->request->get('estado');

        $consulta = "SELECT idMunicipio, municipio FROM municipio WHERE idEstado = :estado AND activo = 1";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':estado', $estado);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['item'] = array();
        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idMunicipio'], 'nombre' => $dato['municipio']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/usuarios', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT id, first_name, last_name FROM tb_users";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['item'] = array();
        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['id'], 'nombre' => $dato['first_name'] . ' ' . $dato['last_name']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/tipoVehiculo', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT idTipoVehiculo, tipoVehiculo FROM tipovehiculo WHERE activo = '1'";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idTipoVehiculo'], 'nombre' => $dato['tipoVehiculo']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });
    
    $app->get('/marca', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT idMarca, marca FROM marca";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idMarca'], 'nombre' => $dato['marca']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE GUARDA MARCA */
    $app->post('/marca_save', function() use ($app) {

        require_once("conexion.php");
        $response = array();
        
        $nombre=  $app->request->post('nombre');

        $consulta="INSERT INTO marca(idMarca,marca) VALUES (DEFAULT,'".$nombre."')";
        $consulta=$conectar->prepare($consulta);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ELIMINA MARCA */
    $app->delete('/marca_delete', function() use ($app) {

        require_once("conexion.php");
        $response = array();
    
        $id = $app->request->delete('id');
    
        $consulta ="DELETE FROM marca WHERE idMarca = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id',$id);
    
        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';
    
        if(isset($conectar)) unset($conectar);
        $app->response->setBody(json_encode($response));
    });
    
    /** FUNCION QUE EDITA MARCA */
    $app->put('/marca_edit', function() use ($app) {
        require_once("conexion.php");
        $response = array();
    
        $nombre = $app-> request->put('nombre');
        $id = $app->request->post('id');
    
        $consulta ="UPDATE marca SET marca = :marca WHERE idMarca = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':marca', $nombre);
        $consulta->bindParam(':id',$id);
    
        if($consulta->execute()) $response['msg'] = '200'; 
        else $response['msg'] = '500'; 
    
        if(isset($conectar)) unset($conectar);
    
        $app->response->setBody(json_encode($response));
    
    });

    $app->get('/modeloByMarca', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $marca = $app->request->get('marca');

        $consulta = "SELECT idModelo, modelo FROM modelo WHERE idMarca = :marca";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':marca', $marca);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['item'] = array();
        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idModelo'], 'nombre' => $dato['modelo']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/aseguradora', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT idAseguradora, aseguradora FROM aseguradora";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idAseguradora'], 'nombre' => $dato['aseguradora']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE GUARDA ASEGURADORA */
    $app->post('/aseguradora_save', function() use ($app) {

        require_once("conexion.php");
        $response = array();
        
        $nombre=  $app->request->post('nombre');

        $consulta="INSERT INTO aseguradora(idAseguradora,aseguradora) VALUES (DEFAULT,'".$nombre."')";
        $consulta=$conectar->prepare($consulta);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ELIMINA ASEGURADORA */
    $app->delete('/aseguradora_delete', function() use ($app) {

        require_once("conexion.php");
        $response = array();

        $id = $app->request->delete('id');

        $consulta ="DELETE FROM aseguradora WHERE idAseguradora = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id',$id);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';

        if(isset($conectar)) unset($conectar);
        $app->response->setBody(json_encode($response));
    });

    $app->put('/aseguradora_edit', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $nombre = $app-> request->put('nombre');
        $id = $app->request->post('id');

        $consulta ="UPDATE aseguradora SET aseguradora = :aseguradora WHERE idAseguradora = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':aseguradora', $nombre);
        $consulta->bindParam(':id',$id);

        if($consulta->execute()) $response['msg'] = '200'; 
        else $response['msg'] = '500'; 

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));

    });

    /** FUNCION QUE MUESTRA COLOR */
    $app->get('/color', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT idColor, color FROM color";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['idColor'], 'nombre' => $dato['color']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE GUARDA COLOR */
    $app->post('/color_save', function() use ($app) {

        require_once("conexion.php");
        $response = array();
        
        $nombre=  $app->request->post('nombre');

        $consulta="INSERT INTO color(idColor,color) VALUES (DEFAULT,'".$nombre."')";
        $consulta=$conectar->prepare($consulta);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ELIMINA COLOR */
    $app->delete('/color_delete', function() use ($app) {

        require_once("conexion.php");
        $response = array();
    
        $id = $app->request->delete('id');
    
        $consulta ="DELETE FROM color WHERE idColor = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id',$id);
    
        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg']='500';
    
        if(isset($conectar)) unset($conectar);
        $app->response->setBody(json_encode($response));
    });
    
    /** FUNCION QUE EDITA COLOR */
    $app->put('/color_edit', function() use ($app) {
        require_once("conexion.php");
        $response = array();
    
        $nombre = $app-> request->put('nombre');
        $id = $app->request->post('id');
    
        $consulta ="UPDATE color SET color = :color WHERE idColor = :id";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':color', $nombre);
        $consulta->bindParam(':id',$id);
    
        if($consulta->execute()) $response['msg'] = '200'; 
        else $response['msg'] = '500'; 
    
        if(isset($conectar)) unset($conectar);
    
        $app->response->setBody(json_encode($response));
    
    });
    

    $app->get('/grupos', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $consulta = "SELECT id, grupo FROM grupos";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($datos as $dato) { $response['item'][] = array('id' => $dato['id'], 'nombre' => $dato['grupo']); }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/zonas', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $permisos = new Permisos($conectar);
        $response['info'] = $permisos->getZonas();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });
    
    $app->run();

?>