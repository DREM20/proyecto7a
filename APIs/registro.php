<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/verifica_datos', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $correo = $app->request->get('correo');
        $telefono = $app->request->get('telefono');

        $consulta = "SELECT email FROM operador WHERE email = :correo";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':correo', $correo);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC); 
        $response['correo'] = (count($datos) > 0) ? true : false;

        $consulta = "SELECT telefono FROM operador WHERE telefono = :telefono";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':telefono', $telefono);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC); 
        $response['telefono'] = (count($datos) > 0) ? true : false;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/save_data', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $estado = $app->request->post('select_estado');
        $municipio = $app->request->post('select_municipio');
        $nombre = $app->request->post('registro_nombre');
        $apellidos = $app->request->post('registro_apellidos');
        $telefono = $app->request->post('registro_telefono');
        $correo = $app->request->post('registro_correo');

        $consulta = "INSERT INTO operador (nombre, apellidos, email, contrasena, telefono, idEstado, idMunicipio, stmEstatus)
                    VALUES (:nombre, :apellidos, :email, :contrasena, :telefono, :estado, :municipio, 0)";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':nombre', $nombre);
        $consulta->bindParam(':apellidos', $apellidos);
        $consulta->bindParam(':email', $correo);
        $consulta->bindValue(':contrasena', md5($telefono));
        $consulta->bindParam(':telefono', $telefono);
        $consulta->bindParam(':estado', $estado);
        $consulta->bindParam(':municipio', $municipio);

        if($consulta->execute()) {
            $response = array('data' => true, 'notifica' => false);
            $notifica = json_decode(file_get_contents(APIS . 'codigoPreregistro?correo=' . $correo));

            $response['notifica'] = $notifica->data->respuesta;
        }
        else $response = array('data' => false, 'notifica' => false, 'query' => $consulta->errorInfo());

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/reenviar', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $correo = $app->request->post('correo');

        $notifica = json_decode(file_get_contents(APIS . 'codigoPreregistro?correo=' . urlencode($correo)));

        $response = $notifica->data;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->post('/verifica_token', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $correo = $app->request->post('correo');
        $token = $app->request->post('token');

        $notifica = json_decode(file_get_contents(APIS . 'validarcodigo?correo=' . urlencode($correo) . '&codigo=' . urlencode($token)));

        if(isset($notifica->data->verificacion)) $response = $notifica->data->verificacion;
        else $response = array('respuesta' => false);

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>