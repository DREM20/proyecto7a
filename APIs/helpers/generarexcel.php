<?php 
    setlocale(LC_MONETARY, 'en_US.UTF-8');

    if (!defined('PHPEXCEL_FILE')) define('PHPEXCEL_FILE', dirname(__FILE__) . '/../../assets/');
    if (!defined('REPORTES_DIR')) define('REPORTES_DIR', dirname(__FILE__) . '/../reportes/');
    require_once(PHPEXCEL_FILE . 'PHPExcel/Classes/PHPExcel.php');

    class CrearExcel {
        private $objExcel;
        private $cabecera;
        private $datos;
        private $ubication;
        private $reporte;
        private $estilos = array(
            'TABLA_GENERAL' => array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'FFFFFF'))),
            'ESTILO_TITULO' => array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,), 'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '002c3a')),'font'  => array('color' => array('rgb' => 'FFFFFF'),))
        );

        public function __construct($reporte = 'REPORTE', $cabecera = [], $datos = []) {
            $this->cabecera = $cabecera;
            $this->datos = $datos;
            $this->reporte = $reporte;

            $this->objExcel = new PHPExcel();
            PHPExcel_Cell::setValueBinder(new PHPExcel_Cell_AdvancedValueBinder());
            $this->objExcel->getDefaultStyle()->applyFromArray($this->estilos['TABLA_GENERAL']);
        }

        private function createCabecera() {
            $col = 0;
            foreach($this->cabecera as $cabecera) {
                $this->objExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, strtoupper($cabecera));
                
                $column = PHPExcel_Cell::stringFromColumnIndex($col);
                
                $this->objExcel->getActiveSheet()->getStyle($column . '10')->applyFromArray($this->estilos['ESTILO_TITULO'])->getFont()->setBold(true)->setName('Calibri')->setSize(11);
			    $this->objExcel->getActiveSheet()->getStyle($column . '10')->getAlignment()->setIndent(1);

                $col++;
            }
        }

        private function createData() {
            $row = 11;
            foreach($this->datos as $dato) {
                $col = 0;
                foreach($dato as $key => $info) {
                    $this->objExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, strtoupper($info));
                    $col++;
                }

                $row++;
            }
        }

        private function addImage() {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName($this->reporte);
            $objDrawing->setDescription($this->reporte);
            $objDrawing->setPath(dirname(__FILE__) . '/../../../images/logo.png');
            $objDrawing->setOffsetX(0);
            $objDrawing->setOffsetY(0);
            $objDrawing->setCoordinates('B2');
            $objDrawing->setWidth(180);
            $objDrawing->setHeight(120);
            $objDrawing->setWorksheet($this->objExcel->getActiveSheet());
        }

        private function setSize() {
            for($i = 0; $i < count($this->cabecera); $i++) { 
                $column = PHPExcel_Cell::stringFromColumnIndex($i);
                $this->objExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            }
        }

        public function CreateDoc() {

            $this->objExcel->getActiveSheet()->setTitle($this->reporte);
            $this->addImage();
            $this->createCabecera();
            $this->createData();

            $this->objExcel->getActiveSheet()->freezePane('A11');

            $this->setSize();

            if(!is_dir(REPORTES_DIR)) mkdir(REPORTES_DIR);

            $fecha = new DateTime();
            $file_name = $this->reporte . '_' . date('Ymd') . '_' . $fecha->getTimestamp() . '.xlsx';
            
            $objWriter = PHPExcel_IOFactory::createWriter($this->objExcel, 'Excel2007');
            $objWriter->save(REPORTES_DIR . $file_name);

            $this->ubication = $file_name;

            return $this->ubication;
        }
    }

?>