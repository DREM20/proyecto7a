<?php 

    class Permisos {

        private $conectar;
        private $perfil;

        public function __construct($conectar) {
            $this->conectar = $conectar;
            $this->perfil = (isset($_SESSION['GROUP_ID'])) ? $_SESSION['GROUP_ID'] : '';
        }

        public function getPermiso($permiso) {
            $consulta = "SELECT P.clave, P.descripcion, G.activo 
                        FROM permisos P 
                        LEFT JOIN permisos_grupo G ON P.id = G.permiso
                        WHERE G.grupo = :grupo
                        AND P.clave = :clave";
            
            $consulta = $this->conectar->prepare($consulta);
            $consulta->bindValue(':grupo', $this->perfil);
            $consulta->bindValue(':clave', $permiso);
            $consulta->execute();
            $datos = $consulta->fetch(PDO::FETCH_ASSOC);

            if(empty($datos)) return false;
            else if($datos['activo'] == 1) return true;
            else return false;
        }

        public function getZonas() {
            $consulta = "SELECT ZG.idZona, Z.zona, Z.idEstado, Z.idMunicipio, Z.latitud, Z.longitud
                        FROM zonas_grupos ZG 
                        LEFT JOIN zonas Z ON ZG.idZona = Z.id 
                        WHERE ZG.idGrupo = :idGrupo AND ZG.activo = 1;";

            $consulta = $this->conectar->prepare($consulta);
            $consulta->bindValue(':idGrupo', $this->perfil);
            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            return $datos;
        }
    }

?>