<?php 

    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');

    /** MUESTRA LAS MEMBRESIAS */
    $app->get('/tbody', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $estatus            = $app->request->get('estatus') ?? 0;
        $operador           = $app->request->get('operador');
        $telefono           = $app->request->get('telefono');
        $idOperador         = $app->request->get('idOperador');
        $limite             = $app->request->get('limite');
        $fechapagoInicio    = $app->request->get('fechapagoInicio');
        $fechapagoFin       = $app->request->get('fechapagoFin');
        $fechacorteInicio   = $app->request->get('fechacorteInicio');
        $fechacorteFin      = $app->request->get('fechacorteFin');

        $condicion = '';
        if(!empty($operador)) { if(empty($condicion)) $condicion = " WHERE CONCAT( O.nombre, ' ', O.apellidos ) LIKE '%$operador%'"; else $condicion .= " AND CONCAT( O.nombre, ' ', O.apellidos ) LIKE '%$operador%'"; }
        if(!empty($telefono)) { if(empty($condicion)) $condicion = " WHERE O.telefono = '". $telefono ."'"; else $condicion .= " AND O.telefono = '". $telefono ."'"; }
        if(!empty($idOperador)) { if(empty($condicion)) $condicion = " WHERE M.idOperador = '" . $idOperador . "'"; else $condicion .= " AND M.idOperador = '" . $idOperador . "'"; }
        if(!empty($fechapagoInicio) && !empty($fechapagoFin)) {
            $fechapagoInicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechapagoInicio)));
            $fechapagoFin = date('Y-m-d', strtotime(str_replace('/', '-', $fechapagoFin)));

            if(empty($condicion)) $condicion = " WHERE M.fechaPago BETWEEN '".$fechapagoInicio."' AND '".$fechapagoFin."'";
            else $condicion .= " AND M.fechaPago BETWEEN '".$fechapagoInicio."' AND '".$fechapagoFin."'";
        }

        if(!empty($fechacorteInicio) && !empty($fechacorteFin)) {
            $fechacorteInicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechacorteInicio)));
            $fechacorteFin = date('Y-m-d', strtotime(str_replace('/', '-', $fechacorteFin)));

            if(empty($condicion)) $condicion = " WHERE M.fechaCorte BETWEEN '".$fechacorteInicio."' AND '".$fechacorteFin."'";
            else $condicion .= " AND M.fechaCorte BETWEEN '".$fechapagoInicio."' AND '".$fechacorteFin."'";
        }

        $limite_txt = '';
        if(!empty($limite)) { $limite_txt = ' LIMIT ' . $limite; }

        if($estatus == 1) {
            if(empty($condicion)) $condicion = " WHERE M.fechaPago <= '".date('Y-m-d')."' AND M.fechaCorte >= '".date('Y-m-d')."'";
            else $condicion = " AND M.fechaPago <= '".date('Y-m-d')."' AND M.fechaCorte >= '".date('Y-m-d')."'";
        }
        else if($estatus == 2) {
            if(empty($condicion)) $condicion = " WHERE M.fechaCorte < '".date('Y-m-d')."'";
            else $condicion = " AND M.fechaCorte < '".date('Y-m-d')."'";
        }

        $consulta = "SELECT
                        CONCAT( O.nombre, ' ', O.apellidos ) AS nombre,
                        O.email,
                        O.cumpleahos,
                        O.genero,
                        O.telefono,
                        M.fechaPago,
                        M.fechaCorte,
                        COALESCE(M.subtotal, 0) AS subtotal,
                        COALESCE(M.descuento, 0) AS descuento,
                        COALESCE(M.total, 0) AS total 
                    FROM
                        membresia M
                        LEFT JOIN operador O ON M.idOperador = O.id 
                        $condicion
                    ORDER BY
                        M.fechaPago DESC
                        $limite_txt;";
    
        // echo $consulta;

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['info'] = $datos;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE RECUPERA LAS MEMBRESIAS DE UN OPERADOR */
    $app->get('/operador_membresias/:id', function($id) use ($app) {
        require_once("conexion.php");
        $response = array('code' => 200);

        $limit = $app->request->get('limit') ?? 5;

        $consulta = "SELECT
                        fechaPago,
                        fechaCorte,
                        COALESCE(subtotal, 0) AS subtotal,
                        COALESCE(total, 0) AS total
                    FROM
                        membresia 
                    WHERE
                        idOperador = :id 
                        ORDER BY fechaCorte DESC
                        LIMIT :limite";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);
        $consulta->bindParam(':limite', $limit, PDO::PARAM_INT);
        $consulta->execute();

        $response['info'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

        foreach($response['info'] as $key => $dato) {
            if(!isset($response['info'][$key]['activo'])) $response['info'][$key]['activo'] = false;

            if(strtotime($dato['fechaPago']) <= strtotime(date('Y-m-d')) 
            && strtotime($dato['fechaCorte']) >= strtotime(date('Y-m-d')))
                $response['info'][$key]['activo'] = true;

            $response['info'][$key]['fechaPago'] = date('d/m/Y', strtotime($dato['fechaPago']));
            $response['info'][$key]['fechaCorte'] = date('d/m/Y', strtotime($dato['fechaCorte']));
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION PARA ACTIVAR LA MEMBRESIA DE UN OPERADOR */
    $app->post('/activar_membresia', function() use ($app) {
        require_once("conexion.php");
        $response = array();

        $operador = $app->request->post('operador');
        $fechapago = $app->request->post('fechapago');
        $fechacorte = $app->request->post('fechacorte');
        $subtotal = floatval($app->request->post('subtotal'));
        $descuento = floatval($app->request->post('descuento'));
        $total = floatval($app->request->post('total'));

        $fechapago = date('Y-m-d', strtotime(str_replace('/', '-', $fechapago)));
        $fechacorte = date('Y-m-d', strtotime(str_replace('/', '-', $fechacorte)));

        $consulta = "INSERT INTO membresia (id, idOperador, fechaPago, descuento, subtotal, total, fechaCorte)
                    VALUES (DEFAULT, :idOperador, :fechaPago, :descuento, :subtotal, :total, :fechaCorte)";

        $consulta = $conectar->prepare($consulta);
        $consulta->bindValue(':idOperador'      , $operador);
        $consulta->bindValue(':fechaPago'       , $fechapago);
        $consulta->bindValue(':descuento'       , $descuento);
        $consulta->bindValue(':subtotal'        , $subtotal);
        $consulta->bindValue(':total'           , $total);
        $consulta->bindValue(':fechaCorte'      , $fechacorte);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg'] = '500';

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>