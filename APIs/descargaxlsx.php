<?php

	//Configuraciones iniciales
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	date_default_timezone_set("America/Mexico_City");
	
	$archivo = $_GET['file'];
	
	
	header('Content-Disposition: attachment; filename=' . $archivo );
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Content-Length: '.filesize($archivo));
	
	ob_end_clean();
	readfile($archivo);
	unlink($archivo);

?>