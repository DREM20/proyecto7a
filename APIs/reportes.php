<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include("../assets/Slim/Slim.php");
    require('helpers/generarexcel.php');

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();
    $app->response->headers->set('Content-Type', 'application/json');

    $app->get('/reporte_viajes', function() use ($app) {
        require("conexion.php");
        $response = array();

        $fecha_inicio = $app->request->get('fecha_inicio');
        $fecha_fin = $app->request->get('fecha_fin');
        $zona = $app->request->get('zona');

        $condicion = '';
        if(!empty($fecha_inicio) && !empty($fecha_fin)) {
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_inicio)));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_fin)));

            if(empty($condicion)) $condicion = " WHERE V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
            else $condicion .= " AND V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
        }
        
        if(!empty($zona)) {
            if(empty($condicion)) $condicion = " WHERE V.zona = '".$zona."'";
            else $condicion .= " AND V.zona = '".$zona."'";
        }

        $consulta = "SELECT 
                    V.idOperador AS 'ID OPERADOR',
                    IF(V.nombreOperador = '' OR V.nombreOperador IS NULL, CONCAT(O.nombre, ' ', O.apellidos), V.nombreOperador) AS 'NOMBRE OPERADOR',
                    V.nombrePasajero AS 'NOMBRE PASAJERO',
                    V.origen_direccion AS origen,
                    V.destino_direccion AS destino,
                    CONCAT(V.distancia, ' KM') AS distancia,
                    CONCAT(FLOOR(duracion/60),'h ',MOD(duracion,60),'m') AS duracion,
                    CONCAT('$', FORMAT(V.costo_viaje, 2)) AS 'COSTO VIAJE',
                    CONCAT('$', FORMAT(V.tarifa_final, 2)) AS 'TARIFA FINAL',
                    CONCAT('$', FORMAT(V.iva, 2)) AS 'IVA',
                    V.porcentaje,
                    V.impuesto,
                    V.forma_pago,
                    V.tipo,
                    V.descuento,
                    V.peticion_hora AS 'PETICION POR USUARIO',
                    V.aceptar_viaje AS 'VIAJE ACEPTADO POR OPERADOR',
                    V.aviso_llegada AS 'LLEGADA DE OPERADOR',
                    V.inicio_viaje AS 'VIAJE INICIADO',
                    V.fin_viaje AS 'VIAJE FINALIZADO',
                    V.fecha,
                    V.cancelado AS 'REALIZO CANCELACION',
                    V.zona,
                    V.estatus,
                    V.placas,
                    V.modelo,
                    V.marca
                    FROM viajes_historico V
                    LEFT JOIN operador O ON O.id = V.idOperador" . $condicion;

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $cabeceras = [];
        foreach($datos as $dato) {
            foreach($dato as $key => $info) { $cabeceras[] = $key; }

            break;
        }

        $excel = new CrearExcel('VIAJES', $cabeceras, $datos);
        $response['file'] = $excel->CreateDoc();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/reporte_viajes_operador', function() use ($app) {
        require("conexion.php");
        $response = array();

        $fecha_inicio = $app->request->get('fecha_inicio');
        $fecha_fin = $app->request->get('fecha_fin');
        $zona = $app->request->get('zona');

        $condicion = '';
        if(!empty($fecha_inicio) && !empty($fecha_fin)) {
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_inicio)));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_fin)));

            if(empty($condicion)) $condicion = " WHERE V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
            else $condicion .= " AND V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
        }
        
        if(!empty($zona)) {
            if(empty($condicion)) $condicion = " WHERE V.zona = '".$zona."'";
            else $condicion .= " AND V.zona = '".$zona."'";
        }

        $consulta = "SELECT
                        V.idOperador,
                        IF(V.nombreOperador = '' OR V.nombreOperador IS NULL, CONCAT( O.nombre, ' ', O.apellidos ), V.nombreOperador) AS 'NOMBRE OPERADOR',
                        COUNT(*) AS 'TOTAL VIAJES',
                        CONCAT('$', FORMAT(SUM(V.costo_viaje), 2)) AS 'COSTO VIAJE',
                        CONCAT('$', FORMAT(SUM(V.tarifa_final), 2)) AS 'TARIFA FINAL',
                        CONCAT('$', FORMAT(SUM(V.iva), 2)) AS 'IVA',
                        V.estatus 
                    FROM
                        viajes_historico V
                        LEFT JOIN operador O ON O.id = V.idOperador $condicion
                    GROUP BY
                        V.idOperador,
                        V.estatus";

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $estatus = array();
        foreach ($datos as $key => $dato) { if(!in_array($dato['estatus'], $estatus)) $estatus[] = $dato['estatus']; }

        $operadores = array();
        foreach ($datos as $key => $dato) {
            if(!isset($operadores[$dato['idOperador']])) $operadores[$dato['idOperador']] = array();
            $operadores[$dato['idOperador']]['ID'] = $dato['idOperador'];
            $operadores[$dato['idOperador']]['NOMBRE OPERADOR'] = $dato['NOMBRE OPERADOR'];

            foreach ($estatus as $key => $info) {
                if(!isset($operadores[$dato['idOperador']][$info])) $operadores[$dato['idOperador']][$info] = 0;
                if(!isset($operadores[$dato['idOperador']]['TOTAL ' . $info])) $operadores[$dato['idOperador']]['TOTAL ' . $info] = 0;
                
                if($dato['estatus'] == $info) {
                    $operadores[$dato['idOperador']][$info] = $dato['TOTAL VIAJES'];
                    $operadores[$dato['idOperador']]['TOTAL ' . $info] = $dato['TARIFA FINAL'];
                }
            }
        }

        $cabeceras = [];
        foreach($operadores as $dato) {
            foreach($dato as $key => $info) { $cabeceras[] = $key; }

            break;
        }
        
        $excel = new CrearExcel('VIAJES_POR_OPERADOR', $cabeceras, $operadores);
        $response['file'] = $excel->CreateDoc();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/reporte_viajes_pasajero', function() use ($app) {
        require("conexion.php");
        $response = array();

        $fecha_inicio = $app->request->get('fecha_inicio');
        $fecha_fin = $app->request->get('fecha_fin');
        $zona = $app->request->get('zona');

        $condicion = '';
        if(!empty($fecha_inicio) && !empty($fecha_fin)) {
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_inicio)));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_fin)));

            if(empty($condicion)) $condicion = " WHERE V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
            else $condicion .= " AND V.fecha >= '".$fecha_inicio."' AND V.fecha <= '".$fecha_fin."'";
        }
        
        if(!empty($zona)) {
            if(empty($condicion)) $condicion = " WHERE V.zona = '".$zona."'";
            else $condicion .= " AND V.zona = '".$zona."'";
        }

        $consulta = "SELECT
                        V.idPasajero,
                        IF(V.nombrePasajero = '' OR V.nombrePasajero IS NULL, CONCAT(P.Nombre, ' ', P.Apellido), V.nombrePasajero) AS 'NOMBRE PASAJERO',
                        COUNT(*) AS 'TOTAL VIAJES',
                        CONCAT('$', FORMAT(SUM(V.costo_viaje), 2)) AS 'COSTO VIAJE',
                        CONCAT('$', FORMAT(SUM(V.tarifa_final), 2)) AS 'TARIFA FINAL',
                        CONCAT('$', FORMAT(SUM(V.iva), 2)) AS 'IVA',
                        V.estatus 
                    FROM
                        viajes_historico V
                        LEFT JOIN pasajero P ON P.id = V.idPasajero $condicion
                    GROUP BY
                        V.idPasajero,
                        V.estatus";

        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $estatus = array();
        foreach ($datos as $key => $dato) { if(!in_array($dato['estatus'], $estatus)) $estatus[] = $dato['estatus']; }

        $operadores = array();
        foreach ($datos as $key => $dato) {
            if(!isset($operadores[$dato['idPasajero']])) $operadores[$dato['idPasajero']] = array();
            $operadores[$dato['idPasajero']]['ID'] = $dato['idPasajero'];
            $operadores[$dato['idPasajero']]['NOMBRE PASAJERO'] = $dato['NOMBRE PASAJERO'];

            foreach ($estatus as $key => $info) {
                if(!isset($operadores[$dato['idPasajero']][$info])) $operadores[$dato['idPasajero']][$info] = 0;
                if(!isset($operadores[$dato['idPasajero']]['TOTAL ' . $info])) $operadores[$dato['idPasajero']]['TOTAL ' . $info] = 0;
                
                if($dato['estatus'] == $info) {
                    $operadores[$dato['idPasajero']][$info] = $dato['TOTAL VIAJES'];
                    $operadores[$dato['idPasajero']]['TOTAL ' . $info] = $dato['TARIFA FINAL'];
                }
            }
        }

        $cabeceras = [];
        foreach($operadores as $dato) {
            foreach($dato as $key => $info) { $cabeceras[] = $key; }

            break;
        }
        
        $excel = new CrearExcel('VIAJES_POR_PASAJERO', $cabeceras, $operadores);
        $response['file'] = $excel->CreateDoc();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/reporte_pasajeros', function() use ($app) {
        require("conexion.php");
        $response = array();

        $fecha_inicio = $app->request->get('fecha_inicio');
        $fecha_fin = $app->request->get('fecha_fin');
        $zona = $app->request->get('zona');

        $condicion = '';
        if(!empty($fecha_inicio) && !empty($fecha_fin)) {
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_inicio)));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_fin)));

            if(empty($condicion)) $condicion = " WHERE P.fecha >= '".$fecha_inicio." 00:00:00' AND P.fecha <= '".$fecha_fin." 23:59:59'";
            else $condicion .= " AND P.fecha >= '".$fecha_inicio." 00:00:00' AND P.fecha <= '".$fecha_fin." 23:59:59'";
        }

        $consulta = "SELECT P.Nombre, P.Apellido, P.Email, P.Telefono, P.Fecha FROM pasajero P" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $cabeceras = [];
        foreach($datos as $dato) {
            foreach($dato as $key => $info) { $cabeceras[] = $key; }

            break;
        }
        
        $excel = new CrearExcel('PASAJEROS', $cabeceras, $datos);
        $response['file'] = $excel->CreateDoc();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->get('/reporte_operadores', function() use ($app) {
        require("conexion.php");
        $response = array();

        $fecha_inicio = $app->request->get('fecha_inicio');
        $fecha_fin = $app->request->get('fecha_fin');
        $zona = $app->request->get('zona');

        $condicion = '';
        if(!empty($fecha_inicio) && !empty($fecha_fin)) {
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_inicio)));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fecha_fin)));

            if(empty($condicion)) $condicion = " WHERE created_at >= '".$fecha_inicio." 00:00:00' AND created_at <= '".$fecha_fin." 23:59:59'";
            else $condicion .= " AND created_at >= '".$fecha_inicio." 00:00:00' AND created_at <= '".$fecha_fin." 23:59:59'";
        }

        $consulta = "SELECT id, cumpleahos as 'Fecha Nacimiento', CONCAT(nombre, ' ', apellidos) AS 'Nombre', direccion, email, telefono FROM operador" . $condicion;
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $cabeceras = [];
        foreach($datos as $dato) {
            foreach($dato as $key => $info) { $cabeceras[] = $key; }

            break;
        }
        
        $excel = new CrearExcel('OPERADORES', $cabeceras, $datos);
        $response['file'] = $excel->CreateDoc();

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    $app->run();

?>