<?php 

    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');

    /** FUNCION QUE LISTA TODOS LOS PERFILES EXISTENTES */
    $app->get('/tbdatos', function() use ($app) {
        require_once('conexion.php'); 
        $response = array(); 

        $consulta = "SELECT group_id, name, description FROM tb_groups";
        $consulta = $conectar->prepare($consulta);
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $response['info'] = $datos;

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE CARGA LA CONFIGURACION DEL PERFIL */
    $app->get('/cargar_configuracion', function() use ($app) {
        require_once("conexion.php");

        $id = $app->request->get('id');

        $response = array('code' => 200);

        if(empty($id)) $response = array('code' => 400, 'notif' => 'Debe indicar el identificador del perfil');
        else {
            $consulta = "SELECT 
                            M.id, 
                            M.clave, 
                            M.pagina, 
                            M.descripcion, 
                            M.padre, 
                            M.icono, 
                            M.principal 
                        FROM 
                            menus M 
                        WHERE 
                            M.principal = '1'";

            $consulta = $conectar->prepare($consulta);
            $consulta->execute(); 
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $consulta = "SELECT 
                            PM.menu, 
                            PM.activo 
                        FROM 
                            groupmenus PM 
                        WHERE 
                            PM.perfil = :id";

            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $id);
            $consulta->execute(); 
            $menus = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($datos as $key => $dato) {

                $datos[$key]['activo'] = 0;
                foreach($menus as $menu) {
                    if($menu['menu'] == $dato['clave'] && $menu['activo'] == '1') $datos[$key]['activo'] = 1;
                }

                $consulta = "SELECT 
                                M.clave, 
                                M.pagina, 
                                M.descripcion, 
                                M.padre, 
                                M.icono
                            FROM 
                                menus M
                            WHERE 
                                M.padre = :padre";

                $consulta = $conectar->prepare($consulta); 
                $consulta->bindParam(':padre', $dato['clave']);
                $consulta->execute(); 
                $datos[$key]['hijos'] = $consulta->fetchAll(PDO::FETCH_ASSOC);

                foreach($datos[$key]['hijos'] as $clave => $info) {
                    $datos[$key]['hijos'][$clave]['activo'] = 0;
                    foreach($menus as $menu) {
                        if($menu['menu'] == $info['clave'] && $menu['activo'] == '1') $datos[$key]['hijos'][$clave]['activo'] = 1;
                    }
                }
            }

            $response['info'] = $datos;

            /** OBTENEMOS LOS PERMISOS EXISTENTES */
            $response['permisos'] = array();
            $consulta = "SELECT
                            P.id,
                            P.clave,
                            P.descripcion,
                            PS.clave AS clave_segmento,
                            PS.descripcion AS segmento,
                            COALESCE(PG.activo, 0) as activo
                        FROM
                            permisos P
                            LEFT JOIN permisos_segmento PS ON P.segmento = PS.id
                            LEFT JOIN permisos_grupo PG ON P.id = PG.permiso 
                            AND PG.grupo = :id;";

            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $id);
            $consulta->execute();
            $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach($datos as $key => $dato) {
                if(!isset($response['permisos'][$dato['clave_segmento']])) 
                    $response['permisos'][$dato['clave_segmento']] = array(
                        'DESCRIPCION' => $dato['segmento'],
                        'INFO' => array()
                    );

                $response['permisos'][$dato['clave_segmento']]['INFO'][] = array(
                    'ID' => $dato['id'],
                    'CLAVE' => $dato['clave'],
                    'DESCRIPCION' => $dato['descripcion'],
                    'ACTIVO' => $dato['activo']
                );
            }
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ACTIVA / DESACTIVA EL MENU DEL PERFIL */
    $app->post('/activar_menu', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $clave = $app->request->post('clave');
        $id = $app->request->post('id');
        $estado = $app->request->post('estado');

        // Verificamos si existe la combinacion entre perfil y menu
        $consulta = "SELECT PM.id FROM groupmenus PM WHERE PM.perfil = :id AND PM.menu = :clave"; 
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);
        $consulta->bindParam(':clave', $clave);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        if(count($datos) == 0) {
            $consulta = "INSERT INTO groupmenus (id, perfil, menu, activo)
                        VALUES(DEFAULT, :id, :clave, :estado)"; 

            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $id);
            $consulta->bindParam(':clave', $clave);
            $consulta->bindParam(':estado', $estado);
            if($consulta->execute()) $response['msg'] = '200';
            else $response['msg'] = '500';
        }
        else {
            $consulta = "UPDATE groupmenus SET activo = :estado
                        WHERE perfil = :id AND menu = :clave"; 
            
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $id);
            $consulta->bindParam(':clave', $clave);
            $consulta->bindParam(':estado', $estado);
            if($consulta->execute()) $response['msg'] = '200';
            else $response['msg'] = '500';
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ACTIVA / DESACTIVA EL PERMISO DEL PERFIL */
    $app->post('/activar_permiso', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $clave = $app->request->post('clave');
        $id = $app->request->post('id');
        $estado = $app->request->post('estado');

        $consulta = "SELECT id FROM permisos_grupo WHERE grupo = :id AND permiso = :permiso";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':id', $id);
        $consulta->bindParam(':permiso', $clave);
        $consulta->execute();

        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        if(count($datos) == 0) {
            $consulta = "INSERT INTO permisos_grupo (grupo, permiso, activo)
                        VALUES (:grupo, :permiso, :activo)";
            
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':grupo', $id);
            $consulta->bindParam(':permiso', $clave);
            $consulta->bindParam(':activo', $estado);

            if($consulta->execute()) $response['msg'] = '200';
            else $response['msg'] = '500';
        }
        else {
            $consulta = "UPDATE permisos_grupo SET activo = :activo
                        WHERE grupo = :grupo AND permiso = :permiso";

            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':grupo', $id);
            $consulta->bindParam(':permiso', $clave);
            $consulta->bindParam(':activo', $estado);

            if($consulta->execute()) $response['msg'] = '200';
            else $response['msg'] = '500';
        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE OBTIENE LA ZONAS DEL PERFIL */
    $app->get('/get_zonas', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $id = $app->request->get('id');

        $consulta = "SELECT id, zona, idEstado, idMunicipio FROM zonas";
        $consulta = $conectar->prepare($consulta);

        $consulta->execute();
        $response['info'] = array();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
        foreach($datos as $dato) { $response['info'][$dato['id']] = $dato; $response['info'][$dato['id']]['existe'] = false; $response['info'][$dato['id']]['activo'] = false; }

        $consulta = "SELECT idZona, activo FROM zonas_grupos WHERE idGrupo = :idGrupo";
        $consulta = $conectar->prepare($consulta);

        $consulta->bindParam(':idGrupo', $id);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
        foreach($datos as $dato) { 
            if(isset($response['info'][$dato['idZona']])) { 
                $response['info'][$dato['idZona']]['existe'] = true; 
                $response['info'][$dato['idZona']]['activo'] = boolval($dato['activo']); 
            } 
        }
        
        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ACTIVA / DESACTIVA LAS ZONAS DEL PERFIL */
    $app->post('/zona_update', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $id = $app->request->post('id');
        $zona = $app->request->post('zona');
        $estado = $app->request->post('estado');

        $consulta = "SELECT * FROM zonas_grupos WHERE idGrupo = :idGrupo AND idZona = :idZona";
        $consulta = $conectar->prepare($consulta);

        $consulta->bindParam(':idGrupo', $id);
        $consulta->bindParam(':idZona', $zona);

        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_ASSOC);

        if(count($datos) == 0) $consulta = "INSERT INTO zonas_grupos (idGrupo, idZona, activo) VALUES (:idGrupo, :idZona, :estado)"; 
        else $consulta = "UPDATE zonas_grupos SET activo = :estado WHERE idGrupo = :idGrupo AND idZona = :idZona";

        $consulta = $conectar->prepare($consulta);
        
        $consulta->bindParam(':estado', $estado);
        $consulta->bindParam(':idGrupo', $id);
        $consulta->bindParam(':idZona', $zona);

        if($consulta->execute()) $response['msg'] = '200';
        else $response['msg'] = '500';

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE CREA UN PERFIL */
    $app->post('/crear_prefil', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $name = $app->request->post('name');
        $desc = $app->request->post('desc');

        $consulta = "INSERT INTO tb_groups (group_id, name, description) VALUES (DEFAULT, '".$name."', '".$desc."')"; 
        $consulta = $conectar->prepare($consulta); 

        if($consulta->execute()) $response['msg'] = '200'; 
        else $response['msg'] = '500'; 

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });

    /** FUNCION QUE ACTUALIZA LA INFORMACION DEL PERFIL */
    $app->put('/actualizar_perfil', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $desc = $app->request->put('desc'); 
        $name = $app->request->put('name');
        $id = $app->request->post('id'); 

        $consulta = "UPDATE tb_groups SET description = :description, name = :name WHERE group_id = :id"; 
        $consulta = $conectar->prepare($consulta); 
        $consulta->bindParam(':description', $desc);
        $consulta->bindParam(':name', $name);
        $consulta->bindParam(':id', $id);
        
        if($consulta->execute()) $response['msg'] = '200'; 
        else $response['msg'] = '500'; 

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));

    });

    /** FUNCION QUE ELIMINA UN PERFIL */
    $app->delete('/eliminar_perfil', function() use ($app) {
        require_once("conexion.php");
        $response = array(); 

        $id = $app->request->delete('id'); 

        /** ELIMINAMOS LOS PERMISOS QUE TENGA */
        $consulta = "DELETE FROM permisos_grupo WHERE grupo = :grupo";
        $consulta = $conectar->prepare($consulta);
        $consulta->bindParam(':grupo', $id);

        if($consulta->execute()) {
            // ELIMINAMOS LOS MENUS QUE TENGA ASIGNADOS EL PERFIL
            $consulta = "DELETE FROM groupmenus WHERE perfil = :id";
            $consulta = $conectar->prepare($consulta);
            $consulta->bindParam(':id', $id);

            if($consulta->execute()) {
                $consulta = "DELETE FROM tb_groups WHERE group_id = :id"; 
                $consulta = $conectar->prepare($consulta);
                $consulta->bindParam(':id', $id);

                if($consulta->execute()) $response['msg'] = '200'; 
                else $response['msg'] = '500'; 
            }
            else $response['msg'] = '500';
        }
        else $response['msg'] = '500';

        if(isset($conectar)) unset($conectar);

        $app->response->setBody(json_encode($response));
    });
    
    $app->run();
?>