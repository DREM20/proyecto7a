<?php 

    date_default_timezone_set('America/Mexico_City');

    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    include("../assets/Slim/Slim.php");

    \Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');

    $ambientes = array(
        'PRO' => 'http://74.208.244.220:5051/admin/api/v3',
        'DEV' => 'http://74.208.244.220:5071/admin/api/v3'
    );

    $app->get('/conteo', function() use ($app, $ambientes) {

        $zona = $app->request->get('zona');
        $ambiente = $app->request->get('ambiente');

        $ruta = $ambientes[$ambiente];

        $data = json_encode([]);
        if(($data = @file_get_contents($ruta . '/conteo?zona=' . urlencode($zona))) !== false) {

        }

        $app->response->setBody($data);

    });

    $app->get('/get_detalle', function() use ($app, $ambientes) {

        $estatus = $app->request->get('estatus');
        $tipo = $app->request->get('tipo');
        $zona = $app->request->get('zona');
        $ambiente = $app->request->get('ambiente');

        $ruta = $ambientes[$ambiente];

        $data = json_encode([]);
        if(($data = @file_get_contents($ruta . '/get_detalle?estatus=' . urlencode($estatus) . '&tipo=' . urlencode($tipo) . '&zona=' . urlencode($zona))) !== false) {

        }

        $app->response->setBody($data);
    });

    $app->get('/get_travel_info', function() use ($app, $ambientes) {
        require("conexion.php");

        $travel = $app->request->get('travel');
        $ambiente = $app->request->get('ambiente');

        $ruta = (!empty($ambiente)) ? $ambientes[$ambiente] : ADMIN_APIS;

        $data = json_encode([]);
        if(($data = @file_get_contents($ruta . '/info_viaje?id=' . $travel)) !== false) {

        }

        if(isset($conectar)) unset($conectar);

        $app->response->setBody($data);
    });

    $app->run();
?>