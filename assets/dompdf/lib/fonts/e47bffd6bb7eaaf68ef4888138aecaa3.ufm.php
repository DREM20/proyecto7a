<?php return array (
  'codeToName' => 
  array (
    59392 => 'note',
    59393 => 'note-beamed',
    59394 => 'music',
    59395 => 'search',
    59396 => 'flashlight',
    59397 => 'mail',
    59398 => 'heart',
    59399 => 'heart-empty',
    59400 => 'star',
    59401 => 'star-empty',
    59402 => 'user',
    59403 => 'users',
    59404 => 'user-add',
    59405 => 'video',
    59406 => 'picture',
    59407 => 'camera',
    59408 => 'layout',
    59409 => 'menu',
    59410 => 'check',
    59411 => 'cancel',
    59412 => 'cancel-circled',
    59413 => 'cancel-squared',
    59414 => 'plus',
    59415 => 'plus-circled',
    59416 => 'plus-squared',
    59417 => 'minus',
    59418 => 'minus-circled',
    59419 => 'minus-squared',
    59420 => 'help',
    59421 => 'help-circled',
    59422 => 'info',
    59423 => 'info-circled',
    59424 => 'back',
    59425 => 'home',
    59426 => 'link',
    59427 => 'attach',
    59428 => 'lock',
    59429 => 'lock-open',
    59430 => 'eye',
    59431 => 'tag',
    59432 => 'bookmark',
    59433 => 'bookmarks',
    59434 => 'flag',
    59435 => 'thumbs-up',
    59436 => 'thumbs-down',
    59437 => 'download',
    59438 => 'upload',
    59439 => 'upload-cloud',
    59440 => 'reply',
    59441 => 'reply-all',
    59442 => 'forward',
    59443 => 'quote',
    59444 => 'code',
    59445 => 'export',
    59446 => 'pencil',
    59447 => 'feather',
    59448 => 'print',
    59449 => 'retweet',
    59450 => 'keyboard',
    59451 => 'comment',
    59452 => 'chat',
    59453 => 'bell',
    59454 => 'attention',
    59455 => 'alert',
    59456 => 'vcard',
    59457 => 'address',
    59458 => 'location',
    59459 => 'map',
    59460 => 'direction',
    59461 => 'compass',
    59462 => 'cup',
    59463 => 'trash',
    59464 => 'doc',
    59465 => 'docs',
    59466 => 'doc-landscape',
    59467 => 'doc-text',
    59468 => 'doc-text-inv',
    59469 => 'newspaper',
    59470 => 'book-open',
    59471 => 'book',
    59472 => 'folder',
    59473 => 'archive',
    59474 => 'box',
    59475 => 'rss',
    59476 => 'phone',
    59477 => 'cog',
    59478 => 'tools',
    59479 => 'share',
    59480 => 'shareable',
    59481 => 'basket',
    59482 => 'bag',
    59483 => 'calendar',
    59484 => 'login',
    59485 => 'logout',
    59486 => 'mic',
    59487 => 'mute',
    59488 => 'sound',
    59489 => 'volume',
    59490 => 'clock',
    59491 => 'hourglass',
    59492 => 'lamp',
    59493 => 'light-down',
    59494 => 'light-up',
    59495 => 'adjust',
    59496 => 'block',
    59497 => 'resize-full',
    59498 => 'resize-small',
    59499 => 'popup',
    59500 => 'publish',
    59501 => 'window',
    59502 => 'arrow-combo',
    59503 => 'down-circled',
    59504 => 'left-circled',
    59505 => 'right-circled',
    59506 => 'up-circled',
    59507 => 'down-open',
    59508 => 'left-open',
    59509 => 'right-open',
    59510 => 'up-open',
    59511 => 'down-open-mini',
    59512 => 'left-open-mini',
    59513 => 'right-open-mini',
    59514 => 'up-open-mini',
    59515 => 'down-open-big',
    59516 => 'left-open-big',
    59517 => 'right-open-big',
    59518 => 'up-open-big',
    59519 => 'down',
    59520 => 'left',
    59521 => 'right',
    59522 => 'up',
    59523 => 'down-dir',
    59524 => 'left-dir',
    59525 => 'right-dir',
    59526 => 'up-dir',
    59527 => 'down-bold',
    59528 => 'left-bold',
    59529 => 'right-bold',
    59530 => 'up-bold',
    59531 => 'down-thin',
    59532 => 'left-thin',
    59533 => 'right-thin',
    59534 => 'up-thin',
    59535 => 'ccw',
    59536 => 'cw',
    59537 => 'arrows-ccw',
    59538 => 'level-down',
    59539 => 'level-up',
    59540 => 'shuffle',
    59541 => 'loop',
    59542 => 'switch',
    59543 => 'play',
    59544 => 'stop',
    59545 => 'pause',
    59546 => 'record',
    59547 => 'to-end',
    59548 => 'to-start',
    59549 => 'fast-forward',
    59550 => 'fast-backward',
    59551 => 'progress-0',
    59552 => 'progress-1',
    59553 => 'progress-2',
    59554 => 'progress-3',
    59555 => 'target',
    59556 => 'palette',
    59557 => 'list',
    59558 => 'list-add',
    59559 => 'signal',
    59560 => 'trophy',
    59561 => 'battery',
    59562 => 'back-in-time',
    59563 => 'monitor',
    59564 => 'mobile',
    59565 => 'network',
    59566 => 'cd',
    59567 => 'inbox',
    59568 => 'install',
    59569 => 'globe',
    59570 => 'cloud',
    59571 => 'cloud-thunder',
    59572 => 'flash',
    59573 => 'moon',
    59574 => 'flight',
    59575 => 'paper-plane',
    59576 => 'leaf',
    59577 => 'lifebuoy',
    59578 => 'mouse',
    59579 => 'briefcase',
    59580 => 'suitcase',
    59581 => 'dot',
    59582 => 'dot-2',
    59583 => 'dot-3',
    59584 => 'brush',
    59585 => 'magnet',
    59586 => 'infinity',
    59587 => 'erase',
    59588 => 'chart-pie',
    59589 => 'chart-line',
    59590 => 'chart-bar',
    59591 => 'chart-area',
    59592 => 'tape',
    59593 => 'graduation-cap',
    59594 => 'language',
    59595 => 'ticket',
    59596 => 'water',
    59597 => 'droplet',
    59598 => 'air',
    59599 => 'credit-card',
    59600 => 'floppy',
    59601 => 'clipboard',
    59602 => 'megaphone',
    59603 => 'database',
    59604 => 'drive',
    59605 => 'bucket',
    59606 => 'thermometer',
    59607 => 'key',
    59608 => 'flow-cascade',
    59609 => 'flow-branch',
    59610 => 'flow-tree',
    59611 => 'flow-line',
    59612 => 'flow-parallel',
    59613 => 'rocket',
    59614 => 'gauge',
    59615 => 'traffic-cone',
    59616 => 'cc',
    59617 => 'cc-by',
    59618 => 'cc-nc',
    59619 => 'cc-nc-eu',
    59620 => 'cc-nc-jp',
    59621 => 'cc-sa',
    59622 => 'cc-nd',
    59623 => 'cc-pd',
    59624 => 'cc-zero',
    59625 => 'cc-share',
    59626 => 'cc-remix',
    59627 => 'github',
    59628 => 'github-circled',
    59629 => 'flickr',
    59630 => 'flickr-circled',
    59631 => 'vimeo',
    59632 => 'vimeo-circled',
    59633 => 'twitter',
    59634 => 'twitter-circled',
    59635 => 'facebook',
    59636 => 'facebook-circled',
    59637 => 'facebook-squared',
    59638 => 'gplus',
    59639 => 'gplus-circled',
    59640 => 'pinterest',
    59641 => 'pinterest-circled',
    59642 => 'tumblr',
    59643 => 'tumblr-circled',
    59644 => 'linkedin',
    59645 => 'linkedin-circled',
    59646 => 'dribbble',
    59647 => 'dribbble-circled',
    59648 => 'stumbleupon',
    59649 => 'stumbleupon-circled',
    59650 => 'lastfm',
    59651 => 'lastfm-circled',
    59652 => 'rdio',
    59653 => 'rdio-circled',
    59654 => 'spotify',
    59655 => 'spotify-circled',
    59656 => 'qq',
    59657 => 'instagram',
    59658 => 'dropbox',
    59659 => 'evernote',
    59660 => 'flattr',
    59661 => 'skype',
    59662 => 'skype-circled',
    59663 => 'renren',
    59664 => 'sina-weibo',
    59665 => 'paypal',
    59666 => 'picasa',
    59667 => 'soundcloud',
    59668 => 'mixi',
    59669 => 'behance',
    59670 => 'google-circles',
    59671 => 'vkontakte',
    59672 => 'smashing',
    59673 => 'sweden',
    59674 => 'db-shape',
    59675 => 'logo-db',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'entypo',
  'FullName' => 'entypo',
  'Version' => 'Version 1.0',
  'PostScriptName' => 'entypo',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '0',
  'UnderlinePosition' => '10',
  'FontHeightOffset' => '90',
  'Ascender' => '850',
  'Descender' => '-150',
  'FontBBox' => 
  array (
    0 => '-36',
    1 => '-166',
    2 => '1020',
    3 => '866',
  ),
  'StartCharMetrics' => '285',
  'C' => 
  array (
    59392 => 582,
    59393 => 740,
    59394 => 700,
    59395 => 789,
    59396 => 902,
    59397 => 900,
    59398 => 860,
    59399 => 860,
    59400 => 880,
    59401 => 880,
    59402 => 940,
    59403 => 1000,
    59404 => 1000,
    59405 => 980,
    59406 => 1000,
    59407 => 1000,
    59408 => 600,
    59409 => 700,
    59410 => 667,
    59411 => 470,
    59412 => 840,
    59413 => 800,
    59414 => 580,
    59415 => 840,
    59416 => 800,
    59417 => 580,
    59418 => 840,
    59419 => 800,
    59420 => 580,
    59421 => 920,
    59422 => 460,
    59423 => 920,
    59424 => 850,
    59425 => 900,
    59426 => 800,
    59427 => 939,
    59428 => 700,
    59429 => 700,
    59430 => 1000,
    59431 => 960,
    59432 => 360,
    59433 => 550,
    59434 => 900,
    59435 => 800,
    59436 => 800,
    59437 => 1000,
    59438 => 1000,
    59439 => 1000,
    59440 => 900,
    59441 => 1000,
    59442 => 900,
    59443 => 762,
    59444 => 1000,
    59445 => 1000,
    59446 => 780,
    59447 => 698,
    59448 => 980,
    59449 => 1000,
    59450 => 1000,
    59451 => 800,
    59452 => 1000,
    59453 => 800,
    59454 => 962,
    59455 => 901,
    59456 => 1000,
    59457 => 1000,
    59458 => 500,
    59459 => 1000,
    59460 => 860,
    59461 => 960,
    59462 => 681,
    59463 => 760,
    59464 => 700,
    59465 => 1001,
    59466 => 1000,
    59467 => 700,
    59468 => 700,
    59469 => 800,
    59470 => 900,
    59471 => 700,
    59472 => 1001,
    59473 => 981,
    59474 => 900,
    59475 => 760,
    59476 => 800,
    59477 => 840,
    59478 => 1000,
    59479 => 800,
    59480 => 1000,
    59481 => 900,
    59482 => 859,
    59483 => 900,
    59484 => 900,
    59485 => 900,
    59486 => 640,
    59487 => 884,
    59488 => 910,
    59489 => 896,
    59490 => 920,
    59491 => 560,
    59492 => 700,
    59493 => 700,
    59494 => 1000,
    59495 => 1000,
    59496 => 960,
    59497 => 792,
    59498 => 900,
    59499 => 800,
    59500 => 1000,
    59501 => 1000,
    59502 => 460,
    59503 => 920,
    59504 => 920,
    59505 => 920,
    59506 => 920,
    59507 => 580,
    59508 => 341,
    59509 => 340,
    59510 => 580,
    59511 => 466,
    59512 => 265,
    59513 => 265,
    59514 => 464,
    59515 => 866,
    59516 => 465,
    59517 => 465,
    59518 => 864,
    59519 => 660,
    59520 => 730,
    59521 => 730,
    59522 => 660,
    59523 => 460,
    59524 => 400,
    59525 => 400,
    59526 => 460,
    59527 => 760,
    59528 => 730,
    59529 => 730,
    59530 => 760,
    59531 => 500,
    59532 => 980,
    59533 => 980,
    59534 => 500,
    59535 => 940,
    59536 => 940,
    59537 => 820,
    59538 => 744,
    59539 => 850,
    59540 => 1000,
    59541 => 900,
    59542 => 1000,
    59543 => 500,
    59544 => 600,
    59545 => 530,
    59546 => 700,
    59547 => 600,
    59548 => 600,
    59549 => 880,
    59550 => 880,
    59551 => 1000,
    59552 => 1000,
    59553 => 1000,
    59554 => 1000,
    59555 => 860,
    59556 => 980,
    59557 => 700,
    59558 => 1000,
    59559 => 980,
    59560 => 900,
    59561 => 1000,
    59562 => 940,
    59563 => 1000,
    59564 => 580,
    59565 => 920,
    59566 => 920,
    59567 => 999,
    59568 => 901,
    59569 => 960,
    59570 => 1000,
    59571 => 1000,
    59572 => 400,
    59573 => 820,
    59574 => 1000,
    59575 => 921,
    59576 => 940,
    59577 => 920,
    59578 => 561,
    59579 => 1000,
    59580 => 1000,
    59581 => 220,
    59582 => 570,
    59583 => 920,
    59584 => 962,
    59585 => 820,
    59586 => 1000,
    59587 => 1002,
    59588 => 840,
    59589 => 1003,
    59590 => 800,
    59591 => 980,
    59592 => 1000,
    59593 => 1000,
    59594 => 1001,
    59595 => 940,
    59596 => 940,
    59597 => 560,
    59598 => 905,
    59599 => 1000,
    59600 => 800,
    59601 => 700,
    59602 => 860,
    59603 => 700,
    59604 => 902,
    59605 => 913,
    59606 => 540,
    59607 => 780,
    59608 => 640,
    59609 => 640,
    59610 => 940,
    59611 => 240,
    59612 => 640,
    59613 => 860,
    59614 => 1000,
    59615 => 961,
    59616 => 960,
    59617 => 960,
    59618 => 960,
    59619 => 960,
    59620 => 960,
    59621 => 960,
    59622 => 960,
    59623 => 960,
    59624 => 960,
    59625 => 960,
    59626 => 960,
    59627 => 920,
    59628 => 960,
    59629 => 900,
    59630 => 960,
    59631 => 901,
    59632 => 960,
    59633 => 920,
    59634 => 960,
    59635 => 500,
    59636 => 960,
    59637 => 860,
    59638 => 896,
    59639 => 960,
    59640 => 778,
    59641 => 960,
    59642 => 560,
    59643 => 960,
    59644 => 900,
    59645 => 960,
    59646 => 920,
    59647 => 960,
    59648 => 1000,
    59649 => 960,
    59650 => 1000,
    59651 => 960,
    59652 => 921,
    59653 => 960,
    59654 => 880,
    59655 => 960,
    59656 => 922,
    59657 => 860,
    59658 => 960,
    59659 => 766,
    59660 => 800,
    59661 => 920,
    59662 => 960,
    59663 => 920,
    59664 => 996,
    59665 => 859,
    59666 => 920,
    59667 => 1000,
    59668 => 998,
    59669 => 1000,
    59670 => 920,
    59671 => 960,
    59672 => 940,
    59673 => 900,
    59674 => 900,
    59675 => 726,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0mcD4AMBwOHf3zxn3R1nh1LZe5Odnb03mZUiI5Ssskd2dsosW9mzkL2VjDKzRSEi6lvwwvN8hqcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPiVD4zRu4zV+EzRhI5qokU3cJE3aZE3eqEY3pimasrFN1dRN07RN1/TN0BeasZmauS/2pWbpy32lrzZrszV7czRnczV38zRv8zV/C7RgC7Vwi7Roi7V4S7RkX2uplm6Zlm25lm+Fvt6KrdTKrdKqrdbqfaM1WrO1Wrt1Wrf1Wr8N2rCN2rhN2rTN2rwt2rKt2rpt+mbbtl3bt0M7tlPf6tt9p537bt9rl3bt++3W7u3Rnv2gvdq7ffphP2rfftx+7d8BHdhB/aSfdnCHdGiHdXhHdGRHdXTH9LOO7biO74RO7KRO7ued0qmd1umd0Zmd1S86u1/2q87p3M7r/C7own7db7qoi7ukS7usy7uiK/ttv+uqru6aru26ru+Gbuymbu6Wbu33/aHbur07+mN3dld3d0/3dl/390AP9lAP90iP9lh/6s893l96oid7qqf7a3/rmZ7tuZ7vhV7s773Uy73Sq73W673Rm/2jt3q7f/av3und3uvfvd8H/acP+6j/9nGf9L+hYRjGGcYdxhvGHyYYJhxGDBMNI4eJh0mGSYfJhsmHUcPoYcwwxTDlMHaYaph6mGaYdphumH6Y4bNuCwAAAAAAAAAAAADwefN/IlCBNA==',
  '_version_' => 6,
);