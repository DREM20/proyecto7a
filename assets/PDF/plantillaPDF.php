<?php
include('fpdf/fpdf.php');
include('conexion.php');


$dueñoFactura = 'Yo mero';
$codigoREFC = 'fre123';
$direccion = 'calle 1';
$NombreObra = 'prode';
$enviarA = 'tu ejemplo';
$nombreAtencion = 'nombre pp';
$fechaElaboracion = '12/12/17';
$fechaEntrega ='12/12/12';
$provedor ='abc';
$codRFC ='123as';
$telefono ='1234456';
$fax ='31245678';
$representante ='pedro';
$formaDePago ='efe';
$observaciones ='nada mas  ';
$requisisionesAsociadas ='ejemplo';
$subTotal ='123';
$descuento ='1';
$flete = '145';
$iva ='14';
$totalNeto ='6543';
$compras ='3';
  $pedidoPara ='tu';
  $proveedor ='cara';
  $compras ='3';
  $almacenObra ='543';
  $obra ='afasda';
  $comprador ='jgr';


$pdf = new FPDF();

    $pdf->AddPage();
    $pdf->Image('prodemex.png' , 6 ,6, 40 , 20,'PNG');

    $pdf->SetXY(50,5);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(80, 3, 'PRODEMEX
    
Av. Universidad # 2014 2do Piso,

04360, Copilco Universidad

TEL: 5147-7272  FAX: 5147-7279', 1,'L');

$numero=123412345678;
    $pdf->SetXY(132,5);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(40, 4, utf8_decode('NINGÚN COMENTARIO O NOTA DIRIGIDA AL PROVEEDOR DEBERÁ ESCRIBIRSE EN EL ORIGINAL SI NO ES SOLICITADA. '), 1,'L');

    $pdf->SetXY(173,5);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(25, 4, utf8_decode(' Numero de Pedido: '
.$numero.''), 1,'C');

    $pdf->SetXY(10,30);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(90, 4, utf8_decode('FACTURAR A: '.$dueñoFactura.'
R.F.C.'.$codigoREFC.'
'.$direccion.'

OBRA: '.$NombreObra.'
ENVIAR A: '.$enviarA.'
ATENCION: '.$nombreAtencion.'


'), 1,'L');
    
    $pdf->SetXY(108,30);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(90, 4, utf8_decode('FFECHA DE ELABORACIÓN:'.$fechaElaboracion.'  
FECHA DE ENTREGA:'.$fechaEntrega.'           
PROVEEDOR:'.$provedor.'  
R.F.C.:'.$codRFC.'              
TEL.:'.$telefono.'                          FAX.:'.$fax.' 
REPRESENTANTE:'.$representante.'  

FORMA DE PAGO:  '.$formaDePago.''), 1,'L');
    

    $pdf->SetXY(9,85);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(10, 3, 'PDA', 1,'L');
    
    
    $pdf->SetXY(20,85);
    $pdf->MultiCell(20, 3, 'Cantidad', 1,'C');

    $pdf->SetXY(40,85);
    $pdf->MultiCell(20, 3, 'Unidad', 1,'C');

    $pdf->SetXY(60,85);
    $pdf->MultiCell(100, 3, 'Descripcion', 1,'C');
    
    $pdf->SetXY(160,85);
    $pdf->MultiCell(20, 3, 'Precio Unitario', 1,'C');
    
    $pdf->SetXY(180,85);
    $pdf->MultiCell(20, 3, 'Precio Total', 1,'C');

    //$pdf->SetXY(170,85);
    //$pdf->MultiCell(20, 3, 'CEMEX ', 1,'C');
    
    require "conexion.php";
    $query = mysqli_query($conectar,"SELECT * FROM detallecompra WHERE ID = '0100-171-4302'");
    

 $val="85";
 while($row = mysqli_fetch_row($query))
 {
 
    $val=$val+5;
    $pdf->SetFont('Arial','B',4);   

    $pdf->SetXY(10,$val);
    $pdf->MultiCell(10, 140,utf8_decode($row['ID']), 1,'L');
    
    $pdf->SetXY(20,$val);
    $pdf->MultiCell(20, 140,$row['Cantidad'], 1,'L');
    
    $pdf->SetXY(40,$val);
    $pdf->MultiCell(20, 140,$row['Unidad'], 1,'L');
    
    $pdf->SetXY(60,$val);
    $pdf->MultiCell(100, 140,$row['Descripcion'], 1,'L');
    
    $pdf->SetXY(160,$val);
    $pdf->MultiCell(20, 140,$row['Precio'], 1,'L');
    
    $pdf->SetXY(180,$val);
    $pdf->MultiCell(20, 140,$row['Importe'], 1,'C');

    //$pdf->SetXY(170,$val);
    //$pdf->MultiCell(20, 5, ' ', 1,'C');
    

}

    $pdf->SetFont('Arial','B',7); 

    $pdf->SetXY(10,230);
    $pdf->SetFont('Arial','B',7);
    
    $pdf->MultiCell(110, 3, 'OBSERVACIONES: '.$observaciones.'', 1,'L');

    $pdf->SetXY(10,245);
    $pdf->SetFont('Arial','B',7);
    
    $pdf->MultiCell(110, 3, 'REQUISICIONES ASOCIADAS: '.$requisisionesAsociadas.' ', 1,'L');

    $pdf->SetXY(123,230);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(76, 3, 'SUB TOTAL:'.$subTotal.'  
DESCUENTO:'.$descuento.' 
FLETE:'.$flete.'
IVA:'.$iva.'
TOTAL NETO:'.$totalNeto.'', 1,'L');


    

    $pdf->SetFont('Arial','B',4);
    $pdf->SetXY(10,250);
    $pdf->MultiCell(110, 2, '1) ES INDISPENSABLE QUE ESTE ORIGINAL SE ANEXE A LA FACTURA PARA SER ACEPTADA A REVISIÓN. EN CASO QUE EL PEDIDO SEA A BASE DE ENTREGAS PARCIALES, SE ANEXARÁ UNA COPIA FOTOSTÁTICA EN CADA ENTREGA, DE NO CUMPLIR CON ESTA DISPOSICIÓN SE REGRESARA LA FACTURA AL PROVEEDOR HASTA QUE CUMPLA CON ESTE REQUISITO.
    2) SU RECEPCIÓN SERÁ LOS LUNES Y MARTES EN LAS OFICINAS DE LA OBRA ARRIBA MENCIONADA
    3)EL PLAZO DE LA REVISIÓN DE LAS FACTURAS SE INICIA A PARTIR DE LA FECHA DE RECEPCIÓN DE LOS DOCUMENTOS AMPARÁNDOLOS CON UN CONTRARECIBO.
    4)EL PROVEEDOR REPRESENTADO POR LA PERSONA QUE FIRMA AL CALCE DE ESTE DOCUMENTO ACEPTA Y SE COMPROMETE A CUMPLIR CON LO CONVENIDO EN ESTE CONTRATO DE COMPRA-VENTA SEGÚN LAS CLAUSULAS CONTENIDAS AL REVERSO DE ESTA HOJA.', 1,'L');
    
    $pdf->SetXY(123,256);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(1,20,'',0,0,'');
    $pdf->MultiCell(76, 3, 'COMPRAS:'.$compras.'', 1,'L');
    $pdf->SetXY(124,265);
    $pdf->SetFont('Arial','B',7);
    $pdf->MultiCell(76, 3, 'PROVEEDOR:'.$provedor.'', 1,'L');
    
    $pdf->SetFont('Arial','B',7);
    $pdf->SetXY(10,270);
    $pdf->MultiCell(150, 3, 'pedido para:    '.$pedidoPara.'    ___proveedor/ '.$proveedor.'          ___compras/ '.$compras.'       ___almacen de obra/  '.$almacenObra.'        ___obra  '.$obra.'', 1,'L');

    $pdf->SetXY(162,270);
    $pdf->SetFont('Arial','B',6);
    $pdf->MultiCell(38, 5, 'Comprador:'.$comprador.'', 1,'L');

$pdf->Output('Nota.pdf','I');
?>