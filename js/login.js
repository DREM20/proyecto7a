$(function () {

	/**
	 * ASIGNAMOS LA VALIDACIÓN AL FORMULARIO
	 */

	const $form = $("#form_login");
	$form.validate({
		rules: {
			username: { required: true },
			password: { required: true }
		},
		messages: {
			username: { required: "Dato requerido" },
			password: { required: "Dato requerido" }
		},
		highlight: function(element) {
			$(element).closest('.input-group').addClass('validate-has-error');
		},		
		unhighlight: function(element) {
			$(element).closest('.input-group').removeClass('validate-has-error');
		},
	})

	$("#btnLogin").on('click', function (e) {
		e.preventDefault();

		if(!$("#form_login").valid()) return;

		var modalRespuesta = swal.fire({
			title: 'Procesando...',
			onBeforeOpen: () => {
				swal.showLoading();
			},
			onOpen: () => {
				$.ajax({
					type: "POST",
					url: root_path + "APIs/login.php/login",
					data: {
						email: $("#username").val(),
						pass: $("#password").val()
					},
					success: function (response) {
						if(response.code == 200) {
                            location.href = root_path + 'menu.php'; 
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning',
                                text: response.msg,
                                confirmButtonText: 'Aceptar'
                            });
                        }
					},
					error: function () {
						swal.fire({
                            title: 'Información', 
                            type: 'warning',
                            text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico',
                            confirmButtonText: 'Aceptar'
                        });
					}
				});
			}
		})
	})

})


// $(function(){
	
// 	$("#btnLogin").click(function(){
// 		VerificaLogin();
// 	});
	
// 	$('#txtClave, #txtEmail, #txtPassword').keyup(function(e){
// 		if(e.keyCode == 13) {
// 			$("#btnLogin").trigger('click');
// 		}
// 	});

// 	VerificaLogin = function(){
		
// 		var loginData = new FormData();

//         loginData.append('numero', $("#txtClave").val());
//         loginData.append('correo', $("#txtEmail").val());
//         loginData.append('contra', $("#txtPassword").val());

// 		var modalRespuesta = Swal.fire({
// 			title: 'Validando Datos...',
// 			onBeforeOpen: () => {
// 				Swal.showLoading()
// 			},
// 			onOpen:() => {
// 				$.ajax({
//                     url: 'login.php',
//                     dataType: 'json',
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     data: loginData,
//                     type: 'post',
//                     success: function(data){
// 						var respuesta = data;
// 						modalRespuesta.closeModal();
// 						if(respuesta.ex == "001"){
// 							Swal.fire({
// 							  	type: 'error',
// 							  	text: respuesta.login,
// 							  	showConfirmButton: true
// 							});
// 						}
// 						else{
// 							window.location.replace("menu.php");
// 						}
        
//                     },error: function(data){
// 						Swal.fire({
// 							type: 'error',
// 							text: "Error",
// 							showConfirmButton: true
// 					  });
// 					}
// 				});

// 			}
// 		});
// 	}
// });