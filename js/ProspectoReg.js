$(window).load(function() {
	$('#loading').fadeOut( "fast", function() {
		$("#dvForm").removeClass('noMostrar');
		$("#dvForm").addClass('fadeIn');
	});
});

$(function(){
    $("#btnRegistry").click(function(){
		window.location.replace("registry.php")
	});
});

$(function(){
	
	$("#btnLogin").click(function(){
		VerificaLogin();
	});
	
	$('#txtEmail').keyup(function(e){
		if(e.keyCode == 13)
		{
			DetonaBoton();
		}
	});
	
	$('#txtPassword').keyup(function(e){
		if(e.keyCode == 13)
		{
			DetonaBoton();
		}
	});
	
	DetonaBoton = function(){
		$("#btnLogin").trigger('click');
	}
	
	VerificaLogin = function(){
		
		var loginData = new FormData();
		
        loginData.append('correo', $("#txtEmail").val());
        loginData.append('contra', $("#txtPassword").val());

		var modalRespuesta = Swal.fire({
			title: 'Validando Datos...',
			onBeforeOpen: () => {
				Swal.showLoading()
			},
			onOpen:() => {
			
				$.ajax({
                    url: 'loginPros.php/prospses',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: loginData,
                    type: 'post',
                    success: function(data){
                        
						var respuesta = data;
					                       
						modalRespuesta.closeModal();
						if(respuesta.ex == "001"){
							Swal.fire({
							  type: 'error',
							  text: respuesta.login,
							  showConfirmButton: true
							});
						}
						else{
							window.location.replace("wizardD.php?b=" + respuesta.id + "&c=" + respuesta.code);				
							
						}
        
                    }
				});

			}
		});
	}
});