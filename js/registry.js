$(window).load(function() {
	$('#loading').fadeOut( "fast", function() {
		$("#dvForm").removeClass('noMostrar');
		$("#dvForm").addClass('fadeIn');
	});
});

$(function(){
    $("#btnCancel").click(function(){
		window.location.replace("prospectoReg.php")
	});
});

$(function(){
	
	$("#btnSave").click(function(){
		VerificaRegistry();
    });

    $('#txtClave').keyup(function(e){
		if(e.keyCode == 13)
		{
			DetonaBoton();
		}
	});
	
	$('#txtEmail').keyup(function(e){
		if(e.keyCode == 13)
		{
			DetonaBoton();
		}
	});
	
	$('#txtPassword').keyup(function(e){
		if(e.keyCode == 13)
		{
			DetonaBoton();
		}
    });
    
    DetonaBoton = function(){
		$("#btnLogin").trigger('click');
    }
    
    VerificaRegistry = function(){
		
		var loginData = new FormData();

        loginData.append('correo', $("#txtMail").val());
        loginData.append('contra', $("#txtPassword").val());
        loginData.append('rcontra', $("#txtRPassword").val());

		var modalRespuesta = Swal.fire({
			title: 'Validando Datos...',
			onBeforeOpen: () => {
				Swal.showLoading()
			},
			onOpen:() => {
			
				$.ajax({
                    url: 'validarPros.php/valpros',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: loginData,
                    type: 'post',
                    success: function(data){
                        
						var respuesta = data;
					                       
						modalRespuesta.closeModal();
						if(respuesta.ex == "001"){
							Swal.fire({
							  type: 'error',
							  text: respuesta.registry,
							  showConfirmButton: true
							});
						}
                        else
                        {
							var timerInterval
                            Swal.fire({
                                type: 'success',
                                html: respuesta.registry +
									'Esta ventana cerrara en <strong></strong>',
                                showConfirmButton: false,
                                timer: 5500,
								onBeforeOpen: () => {
									Swal.showLoading()
									timerInterval = setInterval(() => {
									  Swal.getContent().querySelector('strong')
										.textContent = Swal.getTimerLeft()
									}, 200)
								  },
								  onClose: () => {
									clearInterval(timerInterval);
									window.location.replace("prospectoReg.php");
								  }
								}).then((result) => {
								  if (
									/* Read more about handling dismissals below */
									result.dismiss === Swal.DismissReason.timer
								  ) {
									console.log('I was closed by the timer')
								  }
                            });
							
                            
                        }
        
                    }
				});

			}
		});
	}
});