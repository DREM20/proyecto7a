$(function(){

$("#cmbtipodoc").ready(function(){

     $.get("APIs/datos.php/cmbtipodoc", 
     function(data){
        $("#cmbtipodoc").html(data.item);
    });


});

$("#load").hide();
$("#btnCargarCC").hide();
$("#btnCargarCC").click(function(){
    $("#load").show();
    $("#mscarga").html('');
     var file_data = $('#zip').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('zip_file', file_data,);
                  
    $.ajax({
                url: 'APIs/ZipCC.php?anexo='+$("#anexo").val()+"&codigo="+$("#codigo").val()+"&escenario="+$("#escenario").dropdown('get value')+"&usuario="+$("#idusuario").val(), // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(dat){
                  // sleep(7000); 
                   data=JSON.parse(dat);
                       console.log(data);
                    if(data.status==200){
                    $("#btnCargarCC").hide();
                    $("#load").hide();
                    $("#mscarga").html('<h2>'+data.Mensaje+'</h2>'+
                    '<li>'+data.XML+'</li>'+
                    '<li>'+data.PDF+'</li>'+
                    '<li>'+data.ANEXO+'</li>'+
                    '<h2>Detalle de Validacion XML</h2>'+
                    '<li>Vigente SAT: '+data.Vigente+'</li>'+
                    '<li>Version XML: '+data.Version+'</li>'+
                    '<li>Codigo Postal: '+data.CodigoPostal+'</li>'+
                    '<li>Metodo Pago: '+data.MetodoPago+'</li>'+
                    '<li>Moneda: '+data.Moneda+'</li>'+
                    '<li>Emisor: '+data.RFCemisor+'</li>'+
                    '<li>Receptor: '+data.RFCreceptor+'</li>'+
                    '<li>Total: '+data.Total+'</li>'+
                    '<li>UUID: '+data.UUID+'</li>'+
                    '<li>CFDi: '+data.UsoCFDI+'</li>'
                    );
                    }else
                    {
                    $("#load").hide();                   
                    $("#mscarga").html('<h2>'+data.Mensaje+'</h2>'+'<li>'+data.XML+'</li>'+'<li>'+data.PDF+'</li>'+'<li>'+data.ANEXO+'</li>');

                    }

                },error:function(xhr, status, error){
                    $("#btnCargarCC").hide();
                    $("#load").hide();
                    $("#mscarga").html('<h2>Error en su archivo XML</h2>');
                }
     });
});

$("#load").hide();
$("#msgCodigo").hide();


$("#buscacodigo").click(function(){

 $.get("APIs/datos.php/bcodigo?codigo="+$("#codigo").val(), 
     function(data){

        console.log(data);
        if(data.autorizacion==1){
        $("#sociedad").val(data.sociedadnombre);
        $("#obra").val(data.obranombre);
        $("#anexo").val(data.anexo);
        $("#saldo").text('Presupuesto: $'+data.saldo);
        $("#btnCargarCC").show();
        if($("#anexo").text()==''){
        $("#anexo").val('Opcional');
        }
    }
    else {
        $("#msgCodigo").show();
        $('#msgCodigo').fadeOut(7000);
        $("#textomsgCodigo").html("El codigo no esta autorizado");

    }

    });

$('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade')
    ;
  })
;
});






}); 