$(function(){
    
    $("#tableusuarios tbody").on('click', 'tr', function () {
        
            var table = $('#tableusuarios').DataTable();
            var data = table.row( this ).data();
            
            $("#usuarioid").val(data[0]);
            $("#usuario").val(data[1]);
            $("#nombre").val(data[2]);
            $("#password").val(data[3]);
            $("#perfil_id").val(data[4]);
            $("#sociedad").val(data[5]);
            $("#tipo").val(data[6]);
            $("#accesototalobras").val(data[7]);
        });

        $("#tableusuarios").ready(function(){
            
                 $.get("APIs/catalogos.php/tbusuarios", 
                 function(data){
                    $("#tbusuario").html(data.tbody);
                    $("#tableusuarios").DataTable({"language":idiomaTablas});
                });
            
            
            });

            $("#btnGUsuario").click(function(){
                var table = $('#tableusuarios').DataTable();
                    $("#frmusuarios").form("submit");
                    if($("#frmusuarios").form("is valid")){
            
                        $.get("APIs/catalogos.php/usuarios_im",$('#frmusuarios :input').serialize()+"&accion=I",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Guardados");
                                table.destroy();
                                $.get("APIs/catalogos.php/tbusuarios", 
                                function(data){
                                   $("#tbusuario").html(data.tbody);
                                   $("#tableusuarios").DataTable({"language":idiomaTablas});
                               });
                                $("#usuarioid").val("");
                                $("#usuario").val("");
                                $("#nombre").val("");
                                $("#password").val("");
                                $("#perfil_id").val("");
                                $("#sociedad").val("");
                                $("#tipo").val("");
                            }
                        })
                        console.log($('#frmusuarios :input').serialize());
                    }else{
                    }
            
            });
            
            $("#btnMUsuario").click(function(){
                var table = $('#tableusuarios').DataTable();
                    $("#frmusuarios").form("submit");
                    if($("#frmusuarios").form("is valid")){
            
                        $.get("APIs/catalogos.php/usuarios_im",$('#frmusuarios :input').serialize()+"&accion=M",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Modificados");
                                table.destroy();
                                $.get("APIs/catalogos.php/tbusuarios", 
                                function(data){
                                   $("#tbusuario").html(data.tbody);
                                   $("#tableusuarios").DataTable({"language":idiomaTablas});
                               });
                                $("#usuarioid").val("");
                                $("#usuario").val("");
                                $("#nombre").val("");
                                $("#password").val("");
                                $("#perfil_id").val("");
                                $("#sociedad").val("");
                                $("#tipo").val("");
                            }
                        })
                        console.log($('#frmusuarios :input').serialize());
                    }else{
                    }
            
            });
            
            $("#btnEUsuario").click(function(){
                var table = $('#tableusuarios').DataTable();
                    $("#frmusuarios").form("submit");
                    if($("#frmusuarios").form("is valid")){
            
                        $.get("APIs/catalogos.php/usuarios_im",$('#frmusuarios :input').serialize()+"&accion=E",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Eliminados");
                                table.destroy();
                                $.get("APIs/catalogos.php/tbusuarios", 
                                function(data){
                                   $("#tbusuario").html(data.tbody);
                                   $("#tableusuarios").DataTable({"language":idiomaTablas});
                               });
                                $("#usuarioid").val("");
                                $("#usuario").val("");
                                $("#nombre").val("");
                                $("#password").val("");
                                $("#perfil_id").val("");
                                $("#sociedad").val("");
                                $("#tipo").val("");
                            }
                        })
                        console.log($('#frmusuarios :input').serialize());
                    }else{
                    }
            
            });
                
            $("#cmbperfiles").ready(function(){
                
                     $.get("APIs/datos.php/comboperfil", 
                     function(data){
                        $("#cmbperfiles").html(data.item);
                    });
                    
                });

            $("#cmbsociedad").ready(function(){
                
                    $.get("APIs/datos.php/cmbsociedad?permiso="+$("#permiso").val(), 
                    function(data){
                    $("#cmbsociedad").html(data.item);
                });
            
            });

});