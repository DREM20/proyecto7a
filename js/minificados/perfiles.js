$(function(){
    
    $("#tableperfiles tbody").on('click', 'tr', function () {
        
            var table = $('#tableperfiles').DataTable();
            var data = table.row( this ).data();
            
            $("#perfil").val(data[0]);
            $("#descripcion").val(data[1]);
            if(data[2] = 1)
            {
                $("#comprobante").prop('checked', true);
            }
            //$("#comprobante").val(data[2]);
            if(data[3] = 1)
            {
                $("#generacion").prop('checked', true);
            }
            //$("#generacion").val(data[3]);
            if(data[4] = 1)
            {
                $("#consultas").prop('checked', true);
            }
            //$("#consultas").val(data[4]);
            if(data[5] = 1)
            {
                $("#recepcionar").prop('checked', true);
            }
            //$("#recepcionar").val(data[5]);
            if(data[6] = 1)
            {
                $("#catalogos").prop('checked', true);
            }
            //$("#catalogos").val(data[6]);
            if(data[7] = 1)
            {
                $("#enviosap").prop('checked', true);
            }
            //$("#enviosap").val(data[7]);
            if(data[8] = 1)
            {
                $("#administracion").prop('checked', true);
            }
            //$("#administracion").val(data[8]);
        
        });

        $("#tableperfiles").ready(function(){
            $.get("APIs/catalogos.php/tbperfiles", 
           function(data){
              $("#tbperfil").html(data.tbody);
              $("#tableperfiles").DataTable({"language":idiomaTablas});
          });
      });    

      $("#btnGPerfil").click(function(){
        var table = $('#tableperfiles').DataTable();

                $("#frmperfiles").form("submit");
                if($("#frmperfiles").form("is valid")){
                    $.get("APIs/catalogos.php/perfil_im",$('#frmperfiles :input').serialize()+"&accion=I",
                    function(data){
                       console.log(data.estatus);
                       if(data.estatus == "Ok")
                       {
                        $("#perfil").val("");
                        $("#descripcion").val("");
                        $("#comprobante").prop('checked', false);
                        $("#generacion").prop('checked', false);
                        $("#consultas").prop('checked', false);
                        $("#recepcionar").prop('checked', false);
                        $("#catalogos").prop('checked', false);
                        $("#enviosap").prop('checked', false);
                        $("#administracion").prop('checked', false);
                           table.destroy();
                           $.get("APIs/catalogos.php/tbperfiles", 
                           function(data){
                              $("#tbperfil").html(data.tbody);
                              $("#tableperfiles").DataTable({"language":idiomaTablas});
                          });
                       }
                    })
                    console.log($('#frmperfiles :input').serialize());
        
                }else{
        
                }
        
        });
        
        
        $("#btnMPerfil").click(function(){
            var table = $('#tableperfiles').DataTable();
                $("#frmperfiles").form("submit");
                if($("#frmperfiles").form("is valid")){
        
                    $.get("APIs/catalogos.php/perfil_im",$('#frmperfiles :input').serialize()+"&accion=M",
                    function(data){
                       console.log(data.estatus);
                       if(data.estatus == "Ok")
                       {
                        $("#perfil").val("");
                        $("#descripcion").val("");
                        $("#comprobante").prop('checked', false);
                        $("#generacion").prop('checked', false);
                        $("#consultas").prop('checked', false);
                        $("#recepcionar").prop('checked', false);
                        $("#catalogos").prop('checked', false);
                        $("#enviosap").prop('checked', false);
                        $("#administracion").prop('checked', false);
                           table.destroy();
                           $.get("APIs/catalogos.php/tbperfiles", 
                           function(data){
                              $("#tbperfil").html(data.tbody);
                              $("#tableperfiles").DataTable({"language":idiomaTablas});
                          });
                       }
                    })
                    console.log($('#frmperfiles :input').serialize());
        
                }else{
        
                }
        
        });
        
        $("#btnEPerfil").click(function(){
            var table = $('#tableperfiles').DataTable();
                $("#frmperfiles").form("submit");
                if($("#frmperfiles").form("is valid")){
        
                    $.get("APIs/catalogos.php/perfil_im",$('#frmperfiles :input').serialize()+"&accion=E",
                    function(data){
                       console.log(data.estatus);
                       if(data.estatus == "Ok")
                       {
                        $("#perfil").val("");
                        $("#descripcion").val("");
                        $("#comprobante").prop('checked', false);
                        $("#generacion").prop('checked', false);
                        $("#consultas").prop('checked', false);
                        $("#recepcionar").prop('checked', false);
                        $("#catalogos").prop('checked', false);
                        $("#enviosap").prop('checked', false);
                        $("#administracion").prop('checked', false);
                           table.destroy();
                           $.get("APIs/catalogos.php/tbperfiles", 
                           function(data){
                              $("#tbperfil").html(data.tbody);
                              $("#tableperfiles").DataTable({"language":idiomaTablas});
                          });
                       }
                    })
                    console.log($('#frmperfiles :input').serialize());
        
                }else{
        
                }
        
        });
        

});