$(function(){


$("#tableliberacion").ready(function(){

     $.get("APIs/datos.php/tbliberacion", 
     function(data){
        $("#tbliberacion").html(data.tbody);
        $("#tableliberacion").DataTable({"language":idiomaTablas});
    });
});

 $("#tableliberacion tbody").on('click', 'tr', function () {

    var table = $('#tableliberacion').DataTable();
    var data = table.row( this ).data();
    
    $("#idlibera").val(data[0]);
    $("#cmbsociedad").val(data[1]);
    $("#cmbobra").val(data[2]);
    $("#nivel").val(data[3]);
    $("#monto").val(data[4]);
    $("#usuario").val(data[5]);
    $("#correo").val(data[6]);

});

$("#btnGLiberacion").click(function(){

        $("#frliberacion").form("submit");
        if($("#frmliberacion").form("is valid")){
            $.get("APIs/catalogos.php/liberacion_im?idlibera="+$("#idlibera").val()+
                                            "&cmbsociedad="+$("#sociedadid").dropdown('get value')+
                                            "&cmbobra="+$("#obra").dropdown('get value')+
                                            "&correo="+$("#correo").val()+
                                            "&nivel="+$("#nivel").dropdown('get value')+
                                            "&monto="+$("#monto").val()+
                                            "&usuario="+$("#usuario").dropdown('get value')+"&accion=I",function(data){
               console.log(data.estatus)
            })
            console.log($("APIs/catalogos.php/liberacion_im?idlibera="+$("#idlibera").val()+
                                            "&cmbsociedad="+$("#sociedadid").dropdown('get value')+
                                            "&cmbobra="+$("#obra").dropdown('get value')+
                                            "&correo="+$("#correo").val()+
                                            "&nivel"+$("#nivel").dropdown('get value')+
                                            "&monto"+$("#monto").val()+
                                            "&usuario="+$("#usuario").dropdown('get value')+"&accion=I").serialize());

        }else{

        }

});

$("#btnMLiberacion").click(function(){

        $("#frliberacion").form("submit");
        if($("#frmliberacion").form("is valid")){

          $.get("APIs/catalogos.php/liberacion_im?idlibera="+$("#idlibera").val()+
                                            "&cmbsociedad="+$("#sociedadid").dropdown('get value')+
                                            "&cmbobra="+$("#obra").dropdown('get value')+
                                            "&correo="+$("#correo").val()+
                                            "&nivel="+$("#nivel").dropdown('get value')+
                                            "&monto="+$("#monto").val()+
                                            "&usuario="+$("#usuario").dropdown('get value')+"&accion=M",function(data){
               console.log(data.estatus)
            })
            console.log($('#frmliberacion :input').serialize());

        }else{

        }

});

$("#btnELiberacion").click(function(){

        $("#frliberacion").form("submit");
        if($("#frmliberacion").form("is valid")){

            $.get("APIs/catalogos.php/liberacion_im?idlibera="+$("#idlibera").val()+
                                            "&cmbsociedad="+$("#sociedadid").dropdown('get value')+
                                            "&cmbobra="+$("#obra").dropdown('get value')+
                                            "&correo="+$("#correo").val()+
                                            "&nivel="+$("#nivel").dropdown('get value')+
                                            "&monto="+$("#monto").val()+
                                            "&usuario="+$("#usuario").dropdown('get value')+"&accion=E",function(data){
               console.log(data.estatus)
            })
            console.log($('#frmliberacion :input').serialize());

        }else{

        }

});

  $("#cmbsociedad").ready(function(){
    
     $.get("APIs/datos.php/cmbsociedad", 
     function(data){
        $("#cmbsociedad").html(data.item);
    });

});

$("#chkobra").click(function(){
     Iobra='P';
     $.get("APIs/datos.php/cmbobras?iden=P&soc_id="+$("#sociedadid").dropdown('get value'), 
     function(data){
        $("#cmbobra").html(data.item);
    });

});

$("#chkceco").click(function(){
     Iobra='C';
     $.get("APIs/datos.php/cmbobras?iden=C&soc_id="+$("#sociedadid").dropdown('get value'), 
     function(data){
        $("#cmbobra").html(data.item);
        
    });

});

});