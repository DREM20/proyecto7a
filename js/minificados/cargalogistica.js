$(function(){

$("#cmbtipodoc").ready(function(){

     $.get("APIs/datos.php/cmbtipodoc", 
     function(data){
        $("#cmbtipodoc").html(data.item);
    });


});

 $("#buscacodigo").click(function(){

         $.get("APIs/soap.php/conpedido?nopedido="+$("#codigo").val(), 
            function(data){

                console.log(data);

                $("#sociedad").val(data.sociedad);
                $("#codigosociedad").val(data.codigosociedad);
                $("#obra").val(data.obracecoDes);
                $("#codigoobra").val(data.obraceco);
                $("#proveedor").val(data.proveedor);
                $("#codigoproveedor").val(data.codigoproveedor);
                $("#moneda").val(data.MN);
                $("#rfcprove").val(data.rfcprove);
                $("#rfcsoc").val(data.rfcsoc);

                $("#btnCargarCP").show();



            });

    });

$("#load").hide();
$("#btnCargarCP").hide();
$("#btnCargarCP").click(function(){
    $("#load").show();
    $("#mscarga").html('');
     var file_data = $('#zip').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('zip_file', file_data,);
                  
    $.ajax({
                url: 'APIs/ZipCP.php?anexo='+$("#anexo").val()+
                                  "&codigo="+$("#codigo").val()+
                                  "&escenario="+$("#escenario").dropdown('get value')+
                                  "&usuario="+$("#idusuario").val()+
                                  "&moneda="+$("#moneda").val()+
                                  "&rfcprove="+$("#rfcprove").val()+
                                  "&rfcsoc="+$("#rfcsoc").val()+
                                  "&codigoproveedor="+$("#codigoproveedor").val()+
                                  "&codigoobra="+$("#codigoobra").val(),
                                   
                                  // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(dat){
                  // sleep(7000); 
                   data=JSON.parse(dat);
                       console.log(data);
                    if(data.status==200){
                    $("#btnCargarCP").hide();
                    $("#load").hide();
                    $("#mscarga").html('<h2>'+data.Mensaje+'</h2>'+
                    '<li>'+data.XML+'</li>'+
                    '<li>'+data.PDF+'</li>'+
                    '<li>'+data.ANEXO+'</li>'+
                    '<h2>Detalle de Validacion XML</h2>'+
                    '<li>Vigente SAT: '+data.Vigente+'</li>'+
                    '<li>Version XML: '+data.Version+'</li>'+
                    '<li>Codigo Postal: '+data.CodigoPostal+'</li>'+
                    '<li>Metodo Pago: '+data.MetodoPago+'</li>'+
                    '<li>Moneda: '+data.Moneda+'</li>'+
                    '<li>Emisor: '+data.RFCemisor+'</li>'+
                    '<li>Receptor: '+data.RFCreceptor+'</li>'+
                    '<li>Total: '+data.Total+'</li>'+
                    '<li>UUID: '+data.UUID+'</li>'+
                    '<li>CFDi: '+data.UsoCFDI+'</li>'
                    );
                    }else
                    {
                    $("#load").hide();                   
                    $("#mscarga").html('<h2>'+data.Mensaje+'</h2>'+'<li>'+data.XML+'</li>'+'<li>'+data.PDF+'</li>'+'<li>'+data.ANEXO+'</li>');

                    }

                },error:function(xhr, status, error){
                    $("#btnCargarCP").hide();
                    $("#load").hide();
                    $("#mscarga").html('<h2>Error en su archivo XML</h2>');
                }
     });
});

$("#load").hide();
$("#msgCodigo").hide();

}); 