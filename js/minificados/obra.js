$(function(){
    
    $("#tableobra tbody").on('click', 'tr', function () {
        
            var table = $('#tableobra').DataTable();
            var data = table.row( this ).data();
            
            $("#codigo_obra").val(data[0]);
            $("#usuario_id").val(data[1]);
            $("#obra_id").val(data[2]);
            if(data[3] = 1)
            {
                $("#Captura").prop('checked', true);
            }
            if(data[4] = 1)
            {
                $("#Posiciones").prop('checked', true);
            }
            if(data[5] = 1)
            {
                $("#Autorizar").prop('checked', true);
            }
            if(data[6] = 1)
            {
                $("#Logistica").prop('checked', true);
            }
            if(data[7] = 1)
            {
                $("#Financiera").prop('checked', true);
            }
            if(data[8] = 1)
            {
                $("#consulta").prop('checked', true);
            }
            //$("#Captura").val(data[3]);
            //$("#Posiciones").val(data[4]);
            //$("#Autorizar").val(data[5]);
            //$("#Logistica").val(data[6]);
            //$("#Financiera").val(data[7]);
            //$("#consulta").val(data[8]);
        });

        $("#tableobra").ready(function(){
            
                 $.get("APIs/catalogos.php/tbobra", 
                 function(data){
                    $("#tbobra").html(data.tbody);
                    $("#tableobra").DataTable({"language":idiomaTablas});
                });
            
            
            });

            $("#btnGObra").click(function(){
            
                    $("#frmobra").form("submit");
                    if($("#frmobra").form("is valid")){
            
                        $.get("APIs/catalogos.php/obra_im",$('#frmobra :input').serialize()+"&accion=I",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Guardados");
                                $("#codigo_obra").val("");
                                $("#usuario_id").val("");
                                $("#obra_id").val("");
                            }
                        })
                        console.log($('#frmobra :input').serialize());
                    }else{
                    }
            
            });
            
            $("#btnMObra").click(function(){
            
                    $("#frmobra").form("submit");
                    if($("#frmobra").form("is valid")){
            
                        $.get("APIs/catalogos.php/obra_im",$('#frmobra :input').serialize()+"&accion=M",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Modificados");
                                $("#codigo_obra").val("");
                                $("#usuario_id").val("");
                                $("#obra_id").val("");
                            }
                        })
                        console.log($('#frmobra :input').serialize());
                    }else{
                    }
            
            });
            
            $("#btnEObra").click(function(){
            
                    $("#frmobra").form("submit");
                    if($("#frmobra").form("is valid")){
            
                        $.get("APIs/catalogos.php/obra_im",$('#frmobra :input').serialize()+"&accion=E",function(data){
                            console.log(data.estatus)
                            if(data.estatus = 'Ok')
                            {
                                alert("Datos Eliminados");
                                $("#codigo_obra").val("");
                                $("#usuario_id").val("");
                                $("#obra_id").val("");
                            }
                        })
                        console.log($('#frmobra :input').serialize());
                    }else{
                    }
            
            });
                
            $("#cmbusuarios").ready(function(){
                
                     $.get("APIs/datos.php/cmbusuarios", 
                     function(data){
                        $("#cmbusuarios").html(data.item);
                    });
                    
                });

                $('#usuario_id').dropdown({
                    onChange: function(val) {
                        //alert(val);
                        $.get("APIs/datos.php/cmbobra?usuario="+val, 
                        function(data){
                           $("#cmbobra").html(data.item);
                       });
                    }
                   });      

});