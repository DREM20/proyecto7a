$(function(){
    
    $("#cmbsociedad").ready(function(){
        
         $.get("APIs/datos.php/cmbsociedad?permiso="+$("#permiso").val(), 
         function(data){
            $("#cmbsociedad").html(data.item);
        });
    
    });
    
    $("#chkobra").click(function(){
         Iobra='P';
         $.get("APIs/datos.php/cmbobras?iden=P&soc_id="+$("#sociedadid").dropdown('get value'), 
         function(data){
            $("#cmbobra").html(data.item);
        });
    
    });
    
    $("#chkceco").click(function(){
         Iobra='C';
         $.get("APIs/datos.php/cmbobras?iden=C&soc_id="+$("#sociedadid").dropdown('get value'), 
         function(data){
            $("#cmbobra").html(data.item);
            
        });
    
    });
    
    $("#cmbmoneda").ready(function(){
    
        $.get("APIs/datos.php/cmbmoneda", 
         function(data){
            $("#cmbmoneda").html(data.item);
            
        });
    
    });
    
    
    
    $("#cmbEstatus").ready(function(){
        
         $.get("APIs/datos.php/cmbEstatus", 
         function(data){
            $("#cmbEstatus").html(data.item);
        });
    
    });
    
    $("#cmbtipodoc").ready(function(){
    
         $.get("APIs/datos.php/cmbtipodoc", 
         function(data){
            $("#cmbtipodoc").html(data.item);
        });
    
    });
    
    $('#sociedadid').dropdown({
     onChange: function(val) {
         
      $.get("APIs/datos.php/cmbproveedor?idsociedad="+val, 
         function(data){
            $("#cmbproveedor").html(data.item);
        });
    
    
     }
    });

    $("#BTNBuscar").click(function(){
        //$("#tablecodigo").ready(function(){
            console.log("APIs/datos.php/tbconsultacodigo?sociedadid="+$("#sociedadid").dropdown('get value')+
            "&proveedor="+$("#proveedor").dropdown('get value')+
            "&obra="+$("#obra").dropdown('get value')+
            "&noprove="+$("#noprove").val()+
            "&moneda="+$("#moneda").dropdown('get value')+
            "&estatus="+$("#estatus").dropdown('get value')+
            "&fechade="+$("#fechade").val()+
            "&fechahasta="+$("#fechahasta").val());
                 
            $.get("APIs/datos.php/tbconsultacodigo?sociedadid="+$("#sociedadid").dropdown('get value')+
                        "&proveedor="+$("#proveedor").dropdown('get value')+
                        "&obra="+$("#obra").dropdown('get value')+
                        "&noprove="+$("#noprove").val()+
                        "&moneda="+$("#moneda").dropdown('get value')+
                        "&estatus="+$("#estatus").dropdown('get value')+
                        "&fechade="+$("#fechade").val()+
                        "&fechahasta="+$("#fechahasta").val(), 
                 function(data){
                    $("#tbcodigo").html(data.tbody);
                    $("#tablecodigo").DataTable({"language":idiomaTablas});
                });
            
            
            //});
        });

    });