$(window).load(function () {
	
});

function ConvierteBytes(tamano) {
    var FileSize = 0;

    if (tamano > 1048576) {
        FileSize = Math.round(tamano * 100 / 1048576) / 100 + " MB";
    }
    else if (tamano > 1024) {
        FileSize = Math.round(tamano * 100 / 1024) / 100 + " KB";
    }
    else {
        FileSize = tamano + " Bytes";
    }

    return FileSize;
}

var animacionCargaCSS = '<div class="center"><div id="TotalPeticionAProcesar"></div>' +
'<div class= "spinner-wrapper">'+
'<div class="server-wrapper">'+
'<div class="server">'+
'<div class="HDD"></div>'+
'<div class="indicator"></div>'+
'</div>'+
'<div class="server">'+
'<div class="HDD"></div>'+
'<div class="indicator"></div>'+
'</div>'+
'<div class="server">'+
'<div class="HDD"></div>'+
'<div class="indicator"></div>'+
'</div>'+
'<div class="server">'+
'<div class="HDD"></div>'+
'<div class="indicator"></div>'+
'</div>'+
'<div class="server">'+
'<div class="HDD"></div>'+
'<div class="indicator"></div>'+
'</div>'+
'</div>'+
'<div class="spinner">'+
'<div class="rect1"></div>'+
'<div class="rect2"></div>'+
'<div class="rect3"></div>'+
'<div class="rect4"></div>'+
'<div class="rect5"></div>'+
'<div class="rect6"></div>'+
'<div class="rect7"></div>'+
'<div class="rect8"></div>'+
'<div class="rect9"></div>'+
'<div class="rect10"></div>'+
'<br>'+
'<br>'+
'<div class="rect11"></div>'+
'<div class="rect12"></div>'+
'<div class="rect13"></div>'+
'<div class="rect14"></div>'+
'<div class="rect15"></div>'+
'<div class="rect16"></div>'+
'<div class="rect17"></div>'+
'<div class="rect18"></div>'+
'<div class="rect19"></div>'+
'<div class="rect20"></div>'+
'</div>'+
'<div class="probe">'+
'<div class="screen">'+
'<br>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<div class="slit"></div>'+
'<!--'+
'<div class="blue"></div>'+
'-->'+
'</div>'+
'</div>'+
'</div>'+
'</div>';

function formatNumber(nStr, inD, outD, sep) {
    nStr += "";
    var dpos = nStr.indexOf(inD);
    var nStrEnd = "";
    if (dpos != -1) {
        nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
        nStr = nStr.substring(0, dpos);
    }
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(nStr)) {
        nStr = nStr.replace(rgx, "$1" + sep + "$2");
    }
    if (nStrEnd == "") nStrEnd = ",00";
    if (nStrEnd.length < 3) nStrEnd += "0";
    return nStr + nStrEnd;
}


$(function(){

    


	//INICIALIZA COMPONENTES
    
	// INICIO DE Panels

	function continueWrappingPanelTables()
	{
		var $tables = jQuery(".panel-body.with-table + table");

		if($tables.length)
		{
			$tables.wrap('<div class="panel-body with-table"></div>');
			continueWrappingPanelTables();
		}
	}
	
	// Form Wizard
	if($.isFunction($.fn.bootstrapWizard))
	{
		$(".form-wizard").each(function(i, el)
		{
			var $this = $(el),
				$progress = $this.find(".steps-progress div"),
				_index = $this.find('> ul > li.active').index();

			// Validation
			var checkFormWizardValidaion = function(tab, navigation, index)
				{
					if($this.hasClass('validate'))
					{
						var $valid = $this.valid();

						if( ! $valid)
						{
							$this.data('validator').focusInvalid();
							return false;
						}
					}

					return true;
				};


			$this.bootstrapWizard({
				tabClass: "",
				onTabShow: function($tab, $navigation, index)
				{

					setCurrentProgressTab($this, $navigation, $tab, $progress, index);
				},

				onNext: checkFormWizardValidaion,
				onTabClick: checkFormWizardValidaion
			});

			$this.data('bootstrapWizard').show( _index );

			/*$(window).on('neon.resize', function()
			{
				$this.data('bootstrapWizard').show( _index );
			});*/
		});
	}

	// Added on v1.1.4 - Fixed collapsing effect with panel tables
	$(".panel-heading").each(function(i, el)
	{
		var $this = $(el),
			$body = $this.next('table');

		$body.wrap('<div class="panel-body with-table"></div>');

		$body = $this.next('.with-table').next('table');
		$body.wrap('<div class="panel-body with-table"></div>');

	});

	continueWrappingPanelTables();
	// End of: Added on v1.1.4


	$('body').on('click', '.panel > .panel-heading > .panel-options > a[data-rel="reload"]', function(ev)
	{
		ev.preventDefault();

		var $this = jQuery(this).closest('.panel');

		blockUI($this);
		$this.addClass('reloading');
        
        //Limpiamos todos los controles del tipo select2
        $(".select2").each(function(el){
            $(this).select2("val", "");
        });
        
        //Limpiamos todos los controles del tipo textbox form-control
        $(".form-control").each(function(el){
            $(this).val("");
        });

        //Limpiamos todos los radio butons
        $('input.icheckOpts').iCheck('uncheck');
        
        //Limpiamos todos los controles del tipo input
 //       $("input.file2[type=file]").each(function(i, el){
//            var $this = $(el),
//				label = attrDefault($this, 'label', 'Browse');
//
//			$this.bootstrapFileInput(label);
//        });

        setTimeout(function()
		{
			unblockUI($this)
			$this.removeClass('reloading');

		}, 500);

	}).on('click', '.panel > .panel-heading > .panel-options > a[data-rel="close"]', function(ev)
	{
		ev.preventDefault();

		var $this = $(this),
			$panel = $this.closest('.panel');

		var t = new TimelineLite({
			onComplete: function()
			{
				$panel.slideUp(function()
				{
					$panel.remove();
				});
			}
		});

		t.append( TweenMax.to($panel, .2, {css: {scale: 0.95}}) );
		t.append( TweenMax.to($panel, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}) );

	}).on('click', '.panel > .panel-heading > .panel-options > a[data-rel="collapse"]', function(ev)
	{
		ev.preventDefault();

		var $this = $(this),
			$panel = $this.closest('.panel'),
			$body = $panel.children('.panel-body, .table'),
			do_collapse = ! $panel.hasClass('panel-collapse');

		if($panel.is('[data-collapsed="1"]'))
		{
			$panel.attr('data-collapsed', 0);
			$body.hide();
			do_collapse = false;
		}

		if(do_collapse)
		{
			$body.slideUp('normal');
			$panel.addClass('panel-collapse');
		}
		else
		{
			$body.slideDown('normal');
			$panel.removeClass('panel-collapse');
		}
	});
		
	// FIN DE PANELS
	
	// INICIO Datepicker
	if($.isFunction($.fn.datepicker))
	{
		$(".datepicker").each(function(i, el)
		{
			var $this = $(el),
				opts = {
					format: attrDefault($this, 'format', 'dd/mm/yyyy'),
					startDate: attrDefault($this, 'startDate', ''),
					endDate: attrDefault($this, 'endDate', ''),
					daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
					startView: attrDefault($this, 'startView', 0),
					rtl: rtl()
				},
				$n = $this.next(),
				$p = $this.prev();

			$this.datepicker(opts);

			if($n.is('.input-group-addon') && $n.has('a'))
			{
				$n.on('click', function(ev)
				{
					ev.preventDefault();

					$this.datepicker('show');
				});
			}

			if($p.is('.input-group-addon') && $p.has('a'))
			{
				$p.on('click', function(ev)
				{
					ev.preventDefault();

					$this.datepicker('show');
				});
			}
		});
	}
	// INICIO Datepicker
	
	
	// Inicio Date Range Picker
	if($.isFunction($.fn.daterangepicker))
	{
		$(".daterange").each(function(i, el)
		{
			// Change the range as you desire
			var ranges = {
				'Hoy': [moment(), moment()],
				'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
				'Ultimos 7 Dias': [moment().subtract('days', 6), moment()],
				'Ultimos 30 Dias': [moment().subtract('days', 29), moment()],
				'Este Mes': [moment().startOf('month'), moment().endOf('month')],
				'Mes Pasado': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
			};

			var $this = $(el),
				opts = {
					format: attrDefault($this, 'format', 'MM/DD/YYYY'),
					timePicker: attrDefault($this, 'timePicker', false),
					timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
					separator: attrDefault($this, 'separator', ' - '),
				},
				min_date = attrDefault($this, 'minDate', ''),
				max_date = attrDefault($this, 'maxDate', ''),
				start_date = attrDefault($this, 'startDate', ''),
				end_date = attrDefault($this, 'endDate', '');

			if($this.hasClass('add-ranges'))
			{
				opts['ranges'] = ranges;
			}

			if(min_date.length)
			{
				opts['minDate'] = min_date;
			}

			if(max_date.length)
			{
				opts['maxDate'] = max_date;
			}

			if(start_date.length)
			{
				opts['startDate'] = start_date;
			}

			if(end_date.length)
			{
				opts['endDate'] = end_date;
			}

			$this.daterangepicker(opts, function(start, end)
			{
				var drp = $this.data('daterangepicker');

				if($this.is('[data-callback]'))
				{
					////////////daterange_callback(start, end);
					callback_test(start, end);
				}

				if($this.hasClass('daterange-inline'))
				{
					$this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
				}
			});
		});
	}
	// Fin Date Range Picker
    
    //Inicializacion de los combos del tipo Select2
    if($.isFunction($.fn.select2))
    {
        $(".select24444").each(function(i, el)
        {
            var $this = $(el),
                opts = {
                    placeholder: 'Select an option',
                    allowClear: attrDefault($this, 'allowClear', false)
                };

            $this.select2(opts);
            $this.addClass('visible');

            //$this.select2("open");
        });


        if($.isFunction($.fn.niceScroll))
        {
            $(".select2-results").niceScroll({
                cursorcolor: '#d4d4d4',
                cursorborder: '1px solid #ccc',
                railpadding: {right: 3}
            });
        }
    }
    //Fin de la Inicializacion de los combos del tipo Select2
	
	
	//ToolTips
	// Popovers and tooltips
	
	
	RenderizarToolTips = function(){
		
		$('[data-toggle="popover"]').each(function(i, el)
		{
			var $this = $(el),
				placement = attrDefault($this, 'placement', 'right'),
				trigger = attrDefault($this, 'trigger', 'click'),
				popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

			$this.popover({
				placement: placement,
				trigger: trigger
			});

			$this.on('shown.bs.popover', function(ev)
			{
				var $popover = $this.next();

				$popover.addClass(popover_class);
			});
		});

		$('[data-toggle="tooltip"]').each(function(i, el)
		{
			var $this = $(el),
				placement = attrDefault($this, 'placement', 'top'),
				trigger = attrDefault($this, 'trigger', 'hover'),
				popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

			$this.tooltip({
				placement: placement,
				trigger: trigger
			});

			$this.on('shown.bs.tooltip', function(ev)
			{
				var $tooltip = $this.next();

				$tooltip.addClass(popover_class);
			});
		});
		
		
	}
	
	

    
    
    
    //Validacion de formularios
    
    if($.isFunction($.fn.validate))
    {
        $("form.validate").each(function(i, el)
        {
            
            var $this = $(el),
                opts = {
                    ignore: [],
                    rules: {},
                    messages: {},
                    errorElement: 'span',
                    errorClass: 'validate-has-error',
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('validate-has-error');
                        //$(element).closest('.selectDivError').addClass('validate-has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('validate-has-error');
                        //$(element).closest('.selectDivError').removeClass('validate-has-error');
                    },
                    errorPlacement: function (error, element)
                    {
                        if(element.closest('.selectDivError').length)
                        {
                            error.insertAfter(element.closest('.selectDivError'));
                        }
                        if(element.closest('.has-switch').length)
                        {
                            error.insertAfter(element.closest('.has-switch'));
                        }
                        else
                        if(element.parent('.selectDivError').length){
                            error.insertAfter(element.parent());
                        }
                        if(element.parent('.checkbox, .radio').length || element.parent('.input-group').length)
                        {
                            error.insertAfter(element.parent());
                        }
                        else
                        {
                            error.insertAfter(element);
                        }
                    }
                },
            
            $fields = $this.find('[data-validate]');

            $fields.each(function(j, el2)
            {
                var $field = $(el2),
                    name = $field.attr('name'),
                    validate = attrDefault($field, 'validate', '').toString(),
                    _validate = validate.split(',');

                for(var k in _validate)
                {
                    var rule = _validate[k],
                        params,
                        message;

                    if(typeof opts['rules'][name] == 'undefined')
                    {
                        opts['rules'][name] = {};
                        opts['messages'][name] = {};
                    }

                    if($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1)
                    {
                        opts['rules'][name][rule] = true;

                        message = $field.data('message-' + rule);

                        if(message)
                        {
                            opts['messages'][name][rule] = message;
                        }
                    }
                    // Parameter Value (#1 parameter)
                    else
                    if(params = rule.match(/(\w+)\[(.*?)\]/i))
                    {
                        if($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1)
                        {
                            opts['rules'][name][params[1]] = params[2];


                            message = $field.data('message-' + params[1]);

                            if(message)
                            {
                                opts['messages'][name][params[1]] = message;
                            }
                        }
                    }
                }
            });

            console.log( opts );
            $this.validate(opts);
        });
    }
    //Fin de validaciones de usuarios
	
	//ACTUALIZAR PASSWORD DE USUARIO
	ActualizaPassword = function(){
		Swal.fire({
		    title: '<i class="fa fa-keyboard-o"></i>&nbsp; Ingresa su nueva contraseña.',
		    input: 'password',
		    inputAttributes: {
			    autocapitalize: 'off',
			    autofocus: true
		    },
		    showCancelButton: true,
		    confirmButtonText: 'Actualizar',
		    cancelButtonText: 'Cancelar',
		    showLoaderOnConfirm: true,
		    preConfirm: (login) => {
			    return fetch($("#atras").val()+`APIs/generales.php/cambiarpwd?correo=`+ $("#correo").val() +`&contra=${login}`)
                .then(response => {
				    if (!response.ok) {
				        throw new Error(response.statusText)
				    }
				    return response.json()
			    })
			    .catch(error => {
				    Swal.showValidationMessage(
				    `Request failed: ${error}`
				    )
			    })
		    },
		    allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
		    if (result.value.estatus == 'OK') {
			    Swal.fire({
			    title: 'Se actualizó la contraseña correctamente',
			    type: 'success',
			    confirmButtonText: 'Aceptar',
			    backdrop: `rgba(48, 54, 65, 0.4)
						    filter: blur(8px)
						    -webkit-filter: blur(8px)`,
			  
			    });
		    }
		    else {
			    Swal.fire({
				    title: 'No se pudo actualizar el Password. Intente mas tarde, si el problema <br>perciste favor de comunicarse al area de soporte.',
				    type: 'error',
				    confirmButtonText: 'Aceptar',
				    backdrop: `rgba(48, 54, 65, 0.4)
							    filter: blur(8px)
							    -webkit-filter: blur(8px)`,
				    });
		    }
		});
	}
	
    $('.table tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            $('.table tr.selected').removeClass('selected');
            $(this).addClass('selected');
            
        }
    } ); 
	
		
		
		
		
	$.validator.messages.required = 'Dato Requerido';



});

function MensajeLoad(titulo,mostrar){
     $('#modalload').modal({inverted: true}).modal(mostrar);
   $("#textload").html(titulo);
}



var idiomaTablas={
	"sProcessing":     "Procesando...",
	"sLengthMenu":     "Mostrar _MENU_ registros",
	"sZeroRecords":    "No se encontraron resultados",
	"sEmptyTable":     "Ningún dato disponible en esta tabla",
	"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	"sInfoPostFix":    "",
	"sSearch":         "Buscar:",
	"sUrl":            "",
	"sInfoThousands":  ",",
	"sLoadingRecords": "Cargando...",
	"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};