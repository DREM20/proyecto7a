$(function () {

	/** SE CONFIGURAN SELECT */
    const optSelect = {
        allowClear: true,
        theme: "classic",
        placeholder: "Seleccione una opción"
    }

    $(".select2").select2(optSelect);
    $(".select2").addClass('visible');

    CargaDatosIniciales = function(){
		$.ajax({
			type: "GET",
			url: root_path + "APIs/catalogos.php/zonas",
			success: function (response) {
				let html = '',
				activo = (response.info.length == 1) ? 'selected' : '' ;
				// html += '<option></option>';
				$.each(response.info, function (index, value) { 
					html += '<option ' + activo +' data-estado="' + value.idEstado + '" data-municipio="' + value.idMunicipio + '">' + value.zona + '</option>';
				});

				$("#search_zona").html(html);
				$("#search_zona").select2(optSelect);
				$("#search_zona").addClass('visible')

				if($("#search_zona").val()) {
					loadDashboard();
				}
			}
		});	
    }

	$("#search_zona").on('change', function () {
		loadDashboard();
	})

	loadDashboard = () => {
		$.ajax({
			type: "GET",
			url: root_path + "APIs/dashboard.php/dashboard",
			data: {
				zona: $("#search_zona").val(),
				estado: $("#search_zona").select2().find(":selected").data("estado"),
				municipio: $("#search_zona").select2().find(":selected").data("municipio")
			},
			success: function (response) {
				if(response) {
					let html = '';
					html += '<div class="row">';
					let columnas = 0;
					$.each(response, function (index, value) { 
						columnas += (value.SIZE) ? value.SIZE : 3;
						if(columnas > 12) { html += '</div><hr /><div class="row">'; columnas = (value.SIZE) ? value.SIZE : 3; };

						let size = (value.SIZE) ? value.SIZE : '3';

						switch(value.TIPO) {
							case 'CONTADOR':
								color = (value.COLOR) ? value.COLOR : 'tile-primary';

								html += '<div class="col-md-' + size + ' counter-stats" data-reporte="' + index + '">';
								html += '   <div class="tile-stats ' + color + '">';
								html += '       <div class="icon"><i class="entypo-suitcase"></i></div>';
								html += '       <div class="num"><h3>' + value.TITULO + '</h3></div>';
								html += '       <h3>' + value.DATA + '</h3>';
								html += '   </div>';
								html += '</div>';
							break;
							case 'TABLA':
								color = (value.COLOR) ? value.COLOR : 'panel-default';

								if($(value.DATA).length <= 0) return;

								html += '<div class="col-md-' + size + '">';
								html += '	<div class="panel ' + color + '" data-collapsed="1">';
								html += '		<div class="panel-heading">';
								html += '			<div class="panel-title">' + value.TITULO + '</div>';
								html += '			<div class="panel-options">';
								html += '				<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>';
								html += '			</div>';
								html += '		</div>';
								html += '		<div class="panel-body">';

								html += '			<table class="table table-bordered table-hover">';
								html += '				<thead>';
								html += '					<tr>';

								$.each(value.CABECERA, function (indexInArray, valueOfElement) { 
									html += '				<th>' + valueOfElement + '</th>';
								});

								html += '					</tr>';
								html += '				</thead>';
								html += '				<tbody>';

								$.each(value.DATA, function (indexInArray, valueOfElement) { 
									html += '				<tr>';
			
									$.each(valueOfElement, function (index_element, value_element) { 
										html += '				<td>' + value_element + '</td>';
									});
			
									html += '				</tr>';
								});
			
								html += '				</tbody>';
								html += '			</table>';

								html += '		</div>';
								html += '	</div>';
								html += '</div>';
							break;
						}
					});
					html += '</div>';

					$("#show_menu").html(html);

					$("#show_menu table").DataTable({
						language: idiomaTablas,
						dom: 'lBfrtip',
						buttons: ['excelHtml5'],
						"autoWidth": false
					});
				}
			}
		});	
	}

	$(document).on('click', '.counter-stats', function () {
        const reporte = $(this).data('reporte');

		var modalRespuesta = swal.fire({
            title: 'Cargando Información...', 
            onBeforeOpen: () => {
                swal.showLoading()
            },
            onOpen: () => {
				$.ajax({
					type: "GET",
					url: root_path + "APIs/dashboard.php/reporte",
					data: {
						reporte,
						zona: $("#search_zona").val(),
						estado: $("#search_zona").select2().find(":selected").data("estado"),
						municipio: $("#search_zona").select2().find(":selected").data("municipio")
					},
					success: function (response) {
						if(response.file) {
                            Swal.fire({
								type: 'success',
								title: 'Informacion',
								text: 'Se generará el reporte en Excel, espere un momento',
							});
                            location.href = "APIs/descargaxlsx.php?file=reportes/" + response.file;
                            modalRespuesta.closeModal();
                        }
                        else {
                            swal.fire({
                                title: 'Información', 
                                type: 'warning', 
                                text: 'Ocurrio un error, intentelo más tarde, si el error persiste comuniquese con soporte técnico'
                            })
                        }
					},
					error: function () {
						swal.fire({
                            title: 'Información',
                            text: 'No se pudo cargar la información',
                            type: 'error'
                        })
					}
				});
			}
		});

	});
});