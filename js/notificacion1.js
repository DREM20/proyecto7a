$(function(){



$("#guardarMotivo").click(function(){
    var form_data = new FormData();
    $('#cargandoPopUp11').modal({inverted: false}).modal('show');
    $.ajax({
                url: 'APIs/Cancelaciones.php/procesaRespuestaParcial?uuid='+$("#uuid").val()
                                                    +"&res="+$("#res").val()
                                                    +"&nombre="+$("#nombre").val()
                                                    +"&correo="+$("#correo").val()
                                                    +"&motivo="+$("#motivo").val()
													+"&rfc="+$("#rfc").val(),
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'get',
                success: function(dat){

				   try{
						data=JSON.parse(dat);

					   if(data.estatus=="OK"){
				           $("#formulario").addClass('noMostrar');
                           $("#carta").removeClass('noMostrar');
                           $("#textoOK").removeClass('noMostrar');
                           
                           setTimeout(function(){
                               window.close();
                           }, 10000);
				       }
                       else if(data.estatus=="Ya se guardo"){
                           alert('El motivo ya ha sido guardado previamente.');
                           window.close();
                       }
                       else {
                           alert('No se pudo procesar la petición. Favor de intentar mas tarde o comuniquese con su area de soporte.');
                           window.close();
                       }

                       $('#cargandoPopUp11').modal('hide');
				   }
				   catch(error){
					   alert('No se pudo procesar la petición. Favor de intentar mas tarde o comuniquese con su area de soporte.');
					   $('#cargandoPopUp11').modal('hide');
                       window.close();
				   }
  
                },
				error: function(e){
					alert('No se pudo procesar la petición. Favor de intentar mas tarde o comuniquese con su area de soporte.');
					$('#cargandoPopUp11').modal('hide');
                    window.close();
				}
     });
	 
	 
	 
	 
});


});
