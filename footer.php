   
			</div>
		</div>
	</div>

	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/flat/_all.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/polaris/polaris.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/line/_all.css">

	<!-- Imported styles on this page -->
    <link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/select2/select2.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/selectboxit/jquery.selectBoxIt.css">
	
	<!-- Bottom scripts (common) -->
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/gsap/main-gsap.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/bootstrap.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/joinable.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/resizeable.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/neon-api.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
    <script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery.validate.min.js"></script>
    <script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/select2/select2.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/raphael-min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/morris.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery.peity.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/toastr.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery.bootstrap.wizard.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/icheck.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/typeahead.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/daterangepicker/moment.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery.multi-select.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/chosen/chosen.jquery.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/bootstrap-switch.min.js"></script>

	<!-- DATEPICKER -->
	<script src="<?php print(ROOT_PATH) ?>js/yearpicker.js"></script>
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>css/yearpicker.css">

	<script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/jquery.mask.js"></script>
	
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/rickshaw/rickshaw.min.css">
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/jquery.sparkline.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/neon-chat.js"></script>


    <script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/icheck/icheck.min.js"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/neon-login.js"></script>
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/neon-custom.js"></script>

	<!-- Demo Settings -->
	<script src="<?php print(ROOT_PATH) ?>neonUI/html/neon/assets/js/neon-demo.js"></script>

	<!-- DATATABLES -->
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/buttons.flash.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/buttons.print.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.checkboxes.min.js"></script>
    <link rel="stylesheet" href="<?php print(ROOT_PATH) ?>css/datatables.min.css">
    <link rel="stylesheet" href="<?php print(ROOT_PATH) ?>css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php print(ROOT_PATH) ?>css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php print(ROOT_PATH) ?>css/dataTables.checkboxes.css">
    <link rel="stylesheet" type="text/css" href="<?php print(ROOT_PATH) ?>css/fixedHeader.bootstrap.min.css">
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.select.min.js"></script>

    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php print(ROOT_PATH) ?>js/dataTables.bootstrap.min.js"></script>

	<script>$("body").tooltip({ selector: '[data-toggle=tooltip]' });</script>

	
</body>
</html>